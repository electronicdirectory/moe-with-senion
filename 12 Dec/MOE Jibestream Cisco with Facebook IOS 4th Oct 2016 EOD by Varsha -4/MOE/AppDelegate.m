//
//  AppDelegate.m
//  MOE
//
//  Created by Nivrutti on 6/11/15.
//  Copyright (c) 2015 Neosoft. All rights reserved.
//

#import "AppDelegate.h"
#import "SearchViewController.h"
#import "HomeViewController.h"
#import "DEMOLeftMenuViewController.h"
#import <FBSDKCoreKit/FBSDKCoreKit.h>

@interface AppDelegate ()
{
    NSUserDefaults *pref ;
}
@end
static MBProgressHUD *hudObj;
@implementation AppDelegate
@synthesize strSiteID;
#define TOKEN @"jMz3IyozeZ8arjUC9oP0hRaaDYca"
#define BUNDEL_ID [[NSBundle mainBundle] bundleIdentifier]

#pragma  mark didFinishLaunchingWithOptions
- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    
    //FB
    [[FBSDKApplicationDelegate sharedInstance] application:application
                             didFinishLaunchingWithOptions:launchOptions];
   
    
    pref=[NSUserDefaults standardUserDefaults];
    [pref setObject:OUTSIDE_MALL forKey:mall_Checker];
    [pref synchronize];
    
    [self DatebaseInit];
    [DBOpreation createSqlTables];
    
    [UIApplication sharedApplication].statusBarStyle=UIStatusBarStyleLightContent;
    
    //set up default as outside mall
    self.showLocationAds=NO;
    [self setupCisco];
    
    [self getCurrentLoation];
    [self configPushNotification];
    [GoogleAnalyticsHandler setupGoggleAnalytics];
    // Override point for customization after application launch.
    
    // new code for testing notification
    NSDictionary *payload = [launchOptions objectForKey:UIApplicationLaunchOptionsRemoteNotificationKey];
    if(payload) {
        BOOL sdkHandlesNotification = [[EMSP sharedInstance] handleReceivedRemoteNotification:payload withAppViewController:self.window.rootViewController];
        if(!sdkHandlesNotification){
            //handle notification in the app since it cannot be handled by SDK.
            NSLog(@"error in payload");
        }
    }

    return YES;
}
-(NSString *)getBundelID{
    return  BUNDEL_ID;
}
-(void)setupRootView
{
    [self proceedsWithLocation];
    
    UIStoryboard *storyBoard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    [pref synchronize];
    NSString *strID;
    if([[pref objectForKey:mall_Checker]isEqual:INSIDE_MALL])
    {
        strID=@"HomeViewControllerInSide";
    }
    else
    {
        strID=@"HomeViewControllerOutSide";
    }
    [HomeDataModel   sharedInstance].mallMenus=[[NSMutableArray alloc]init];
    [[HomeDataModel   sharedInstance].mallMenus addObjectsFromArray:[CommanMethods getSideMenuFields]];
    if([[[HomeDataModel   sharedInstance].dictCampaign allKeys]count]==0)
    {
        [[HomeDataModel   sharedInstance].mallMenus removeObject:@"SPIN & WIN"];
    }
    
    UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:[storyBoard instantiateViewControllerWithIdentifier:strID]];
    [self setNavigationBarValues:navigationController];
    
    DEMOLeftMenuViewController *leftMenuViewController = [[DEMOLeftMenuViewController alloc] init];
    // Create side menu controller
    RESideMenu *sideMenuViewController = [[RESideMenu alloc] initWithContentViewController:navigationController  leftMenuViewController:leftMenuViewController rightMenuViewController:nil];
    sideMenuViewController.backgroundImage =[UIImage imageNamed:@"background6"];
    // Make it a root controller
    [AppDelegate appDelegateInstance].window.rootViewController = sideMenuViewController;
    [AppDelegate appDelegateInstance].window.rootViewController.navigationController.interactivePopGestureRecognizer.enabled = NO;
   
    [[AppDelegate appDelegateInstance].window makeKeyAndVisible];
    
}
-(void)setNavigationBarValues:(UINavigationController *)navigationController
{
    [UIApplication sharedApplication].statusBarStyle=UIStatusBarStyleLightContent;
    [[UIBarButtonItem appearance] setBackButtonTitlePositionAdjustment:UIOffsetMake(-1000.f, 0) forBarMetrics:UIBarMetricsDefault];
    [[UIBarButtonItem appearance] setTitleTextAttributes:
     [NSDictionary dictionaryWithObjectsAndKeys:[CommanMethods getHeaderFont], NSFontAttributeName,nil]forState:UIControlStateNormal];
    UIImage *navimg=[CommanMethods getNavigationBarImage];
    [navigationController.navigationBar setBackgroundImage:navimg forBarMetrics:UIBarMetricsDefault];
    [navigationController.navigationBar setShadowImage:[UIImage new]];
    navigationController.navigationBar.tintColor = [CommanMethods getTextColor];
    navigationController.navigationBar.barTintColor = [CommanMethods getTextColor];
    navigationController.navigationBar.translucent=NO;
    navigationController.interactivePopGestureRecognizer.enabled=NO;

}

-(void)openSearchScreen:(UINavigationController *)navObj
{
    UIStoryboard *storyBoard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    SearchViewController *searchView=[storyBoard instantiateViewControllerWithIdentifier:@"SearchViewController"];
    [navObj pushViewController:searchView animated:YES];
}
-(void)callViaTelephone:(NSString *)strTel
{
    NSURL *phoneUrl;
    strTel=[strTel stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    strTel=[strTel stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    NSRange range = [strTel  rangeOfString:@"tel" options: NSCaseInsensitiveSearch];
    if (range.location == NSNotFound)
    {
        NSString *strPhone=[NSString  stringWithFormat:@"tel://%@",strTel];
        phoneUrl=[NSURL URLWithString:strPhone];
    }
    else
    {
        phoneUrl=[NSURL URLWithString:strTel];
    }
    
    if ([[UIApplication sharedApplication] canOpenURL:phoneUrl])
    {
        [[UIApplication sharedApplication] openURL:phoneUrl];
    }
    else
    {
        [APP_DELEGATE PlayProgressHudAsPerType:call_facility_not_Available View:nil];
    }
}
#pragma  mark Push Notification
-(void)configPushNotification
{
    /*
    [[UIApplication sharedApplication] registerUserNotificationSettings:[UIUserNotificationSettings settingsForTypes:(UIUserNotificationTypeSound | UIUserNotificationTypeAlert | UIUserNotificationTypeBadge) categories:nil]];
    [[UIApplication sharedApplication] registerForRemoteNotifications];
     */
    
    // new code for testing notification
    if([[UIApplication sharedApplication] respondsToSelector:@selector(registerUserNotificationSettings:)]) {
        UIUserNotificationSettings *settings = [UIUserNotificationSettings settingsForTypes:(UIUserNotificationTypeBadge
                                                                                             |UIUserNotificationTypeSound
                                                                                             |UIUserNotificationTypeAlert) categories:nil];
        [[UIApplication sharedApplication] registerUserNotificationSettings:settings];
    } else {
        UIRemoteNotificationType myTypes = UIRemoteNotificationTypeBadge | UIRemoteNotificationTypeAlert | UIRemoteNotificationTypeSound;
        [[UIApplication sharedApplication] registerForRemoteNotificationTypes:myTypes];
    }

}

// new code for testing notification
- (void)application:(UIApplication *)application didRegisterUserNotificationSettings:(UIUserNotificationSettings *)notificationSettings
{
    [application registerForRemoteNotifications];
}

#pragma  mark  UIApplication delegate method
-(void)application:(UIApplication *)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken
{
    
    //register EMSP for push
    [[EMSP sharedInstance] registerDeviceTokenForPushNotification:deviceToken];
    
    NSCharacterSet *angleBrackets = [NSCharacterSet characterSetWithCharactersInString:@"<>"];
    NSString *strdeviceToken = [[deviceToken description] stringByTrimmingCharactersInSet:angleBrackets];
    strdeviceToken = [strdeviceToken stringByReplacingOccurrencesOfString:@" " withString:@""];
    [pref synchronize];
    if(strdeviceToken!=nil)
    {
        [pref  setObject:strdeviceToken forKey:device_token];
        [pref synchronize];
    }
}
- (void)application:(UIApplication *)application didFailToRegisterForRemoteNotificationsWithError:(NSError *)error
{
}

- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo
{
    // new code for testing notification
    
    BOOL sdkHandlesNotification = [[EMSP sharedInstance] handleReceivedRemoteNotification:userInfo
                                                                    withAppViewController:self.window.rootViewController];
    if(!sdkHandlesNotification){
        //handle notification in the app since it cannot be handled by SDK.
    }
    
}

#pragma mark Internet Connection
-(BOOL)CheckNetWorkConnection
{
    BOOL check=NO;
    Reachability *Reach = [Reachability reachabilityForInternetConnection];
    [Reach startNotifier];
    NetworkStatus netStatus = [Reach currentReachabilityStatus];
    if(netStatus == NotReachable)
    {
        check=NO;
    }
    else
    {
        check=YES;
    }
    return check;
}

+ (BOOL)allowsAnyHTTPSCertificateForHost:(NSString *)host {
    return YES;
}
#pragma mark PlayProgressHudAsPerType
-(void)PlayProgressHudAsPerType:(NSString *)text View:(UIView*)inputView
{
    if(!inputView)
    {
        inputView=self.window;
    }
    if(!hudObj)
    {
        hudObj = [[MBProgressHUD alloc] initWithView:inputView];
        hudObj.userInteractionEnabled=NO;
    }
    [inputView addSubview:hudObj];
    [inputView bringSubviewToFront:hudObj];
    // Configure for text only and offset down
    hudObj.mode = MBProgressHUDModeText;
    hudObj.detailsLabelText = text;
    hudObj.margin = 10.f;
    hudObj.yOffset = 100.f;
    hudObj.removeFromSuperViewOnHide = YES;
    [hudObj show:YES];
    [hudObj hide:YES afterDelay:2.0];
}
-(void)PlayProgressHudForStoreLocator:(NSString *)text View:(UIView*)inputView
{
    if(!inputView)
    {
        inputView=self.window;
    }
    if(!hudObj)
    {
        hudObj = [[MBProgressHUD alloc] initWithView:inputView];
    }
    [inputView addSubview:hudObj];
    [inputView bringSubviewToFront:hudObj];
    hudObj.userInteractionEnabled=YES;
    
    // Configure for text only and offset down
    hudObj.mode = MBProgressHUDModeText;
    hudObj.detailsLabelText = text;
    hudObj.margin = 10.f;
    hudObj.yOffset = 100.f;
    hudObj.removeFromSuperViewOnHide = YES;
    [hudObj show:YES];
    [hudObj hide:YES afterDelay:2.0];
}
-(void)bringHudToFront
{
    if(hudObj)
        [self.window bringSubviewToFront:hudObj];
}
#pragma  mark  showHud  method
-(void)showHud:(UIView*)inputView Title:(NSString *)strTitle
{
    if(hud)
    {
        [hud removeFromSuperview];
    }
    if(!inputView)
    {
        inputView=self.window.rootViewController.view;
    }
    if(!hud)
    {
        hud = [[MBProgressHUD alloc] initWithView:inputView];
    }
    [inputView addSubview:hud];
    [inputView bringSubviewToFront:hud];
    hud.labelText = strTitle;
    [hud show:YES];
}
#pragma  mark  closeHud  method
-(void)closeHud
{
    [hud closeHud];
}
#pragma mark
#pragma mark  Database initialization
+(AppDelegate *)appDelegateInstance
{
    return (AppDelegate *) [[UIApplication sharedApplication] delegate];
}
#pragma  mark  dbPathAddress  method
-(NSString *)dbPathAddress
{
    return [self dbPath:[NSString stringWithFormat:@"%@.sqlite",DB_NAME]];
}
#pragma  mark  dbPath  method
-(NSString *)dbPath:(NSString *)fileName
{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDir = [paths objectAtIndex:0];
    NSString *dbDocumentsPath =@"";
    dbDocumentsPath=[documentsDir stringByAppendingPathComponent:fileName];
    return dbDocumentsPath;
}
#pragma  mark  DatebaseInit  method
-(void)DatebaseInit
{
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSString *strPath=[self dbPathAddress];
    NSError *error;
    BOOL success = [fileManager fileExistsAtPath:strPath];
    if(!success)
    {
        NSString *dbPath = [[NSBundle mainBundle] pathForResource:DB_NAME ofType:@"sqlite"];
        success = [fileManager copyItemAtPath:dbPath toPath:strPath error:&error];
        if (!success)
            NSAssert1(0, @"Failed to create writable database file with message '%@'.", [error localizedDescription]);
    }
}
#pragma  mark  PathForFile  method
+(NSString *)PathForFile:(NSString *)fileName
{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory , NSUserDomainMask, YES);
    NSString *documentsDir = [paths objectAtIndex:0];
    NSString *dbPath = [documentsDir stringByAppendingPathComponent:fileName];
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSError *error;
    
    BOOL success = [fileManager fileExistsAtPath:dbPath];
    if(!success)
    {
        NSString  *plistPath = [[NSBundle mainBundle] pathForResource:fileName ofType:nil];
        success = [fileManager copyItemAtPath:plistPath toPath:dbPath error:&error];
        
        if (!success)
            NSAssert1(0, @"Failed to create writable database file with message '%@'.", [error localizedDescription]);
    }
    return dbPath;
}
#pragma  mark  getCurrentLoation  method
-(void)getCurrentLoation
{
    BOOL isInternetAvaliable=[self CheckNetWorkConnection];
    if(isInternetAvaliable)
    {
        [[CurrentCLController sharedInstance] refreshCurrentLocationWithView:self];
    }
    else
    {
        [self proceedsWithLocation];
    }
}
#pragma  mark  proceedsWithLocation  method
-(void)proceedsWithLocation
{
    [pref synchronize];
    
    double latitudeVal=0;
    latitudeVal=[pref doubleForKey:latitude];
    double longitudeVal=0;
    longitudeVal=[pref doubleForKey:longitude];
    
    CLLocationDistance distance=0;
    if(latitudeVal!=0)
    {
        CLLocation *location=[[CLLocation alloc]initWithLatitude:latitudeVal longitude:longitudeVal];
        CLLocation *mallLocation=[[CLLocation alloc]initWithLatitude:MALL_LATITUDE longitude:MALL_LONGITUDE];
        distance=[location distanceFromLocation:mallLocation];
        if(distance >=0 && distance<DISTANCE_METER)
        {
            [pref setObject:INSIDE_MALL forKey:mall_Checker];
        }
        else
        {
            [pref setObject:OUTSIDE_MALL forKey:mall_Checker];
        }
    }
    [pref synchronize];
}
#pragma mark Location Delegate methods
- (void)locationUpdate:(CLLocation *)location
{
    [pref setDouble:location.coordinate.latitude    forKey:latitude];
    [pref setDouble:location.coordinate.longitude    forKey:longitude];
    [pref synchronize];
    
    [self findWIFILocationFromLatLong:location];
    
}
- (void)locationError:(NSError *)error
{
}
- (void)locationServiceDisable
{
}
#pragma  mark  UIApplication Delegate  method

- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    //FB
    [FBSDKAppEvents activateApp];
    
    [self registerWithCurrentLocation];
    
}

- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}


//--------------------------------------------------------------------------------
#pragma  mark
#pragma  mark Cisco Methods
#pragma  mark setupCisco Methods

-(void)setupCisco{
    //NSLog(@"  AppDelegte --------- setupCisco");
    
    //EMSP setup
    [[EMSP sharedInstance]setLogLevel:EMSP_LOG_LEVEL_TRACE];
    [EMSP sharedInstance].delegate = self;
    [EMSP sharedInstance].appId =BUNDEL_ID;
    //register application with Cisco
    [[EMSP sharedInstance] registerWithAuthToken:TOKEN];
    
}
#pragma  mark registerWithCurrentLocation Methods
-(void)registerWithCurrentLocation{
    //NSLog(@"  AppDelegte --------- registerWithCurrentLocation called");
    [EMSP sharedInstance].location.useLocation = YES;
    [EMSP sharedInstance].location.delegate = self;
    [[EMSP sharedInstance].location registerDeviceAtCurrentLocation];
}
#pragma  mark findWIFILocationFromLatLong Methods
-(void)findWIFILocationFromLatLong:(CLLocation *)location
{
    if(self.showLocationAds)
    {
        [[EMSP sharedInstance].location findWifiLocationsWithLatitude:location.coordinate.latitude longitude:location.coordinate.longitude];
    }
}
//--------------------------------------------------------------------------------
#pragma mark
#pragma mark EMSP Registration Delegate methods
- (void)emspDidFinishRegistration:(EMSP *)emsp{
   // NSLog(@"  AppDelegte ---------:EMSP SDK registration succeeded...");
    [self registerWithCurrentLocation];
}
- (void)emspDidFailToRegisterWithError:(NSError *)error{
   // NSLog(@"  AppDelegte ---------EMSP SDK failed to register./nError: %@", error);
}
//---------------------------------------------------------------------------------
#pragma mark EMSP Location Registeration Delegate Methods
- (void)emspLocationDidRegisterDevice:(EMSPLocation *)empLocation{
   // NSLog(@"  AppDelegte ---------:EMSP SDK Device registration succeeded... ");
    self.showLocationAds=YES;
    [self getCurrentLoation];
}
- (void)emspLocation:(EMSPLocation *)emspLocation didFailToRegisterDevice:(NSError *)error{
   // NSLog(@"  AppDelegte ---------didFailToRegisterDevice error %@", error);
    self.showLocationAds=NO;
}
//---------------------------------------------------------------------------------
#pragma mark EMSPLocationDelegate didReceiveWifiLocations methods
- (void)emspLocation:(EMSPLocation *)emspLocation didReceiveWifiLocations:(NSArray *)locations{
    NSString *locationId = nil;
    for(EMSPWifiLocationInfo *venueInfo in locations){
        locationId = venueInfo.locid;
        if(locationId.length)
            break;
    }
    if (locationId.length)
    {
        self.currentLocationId = locationId;
    }
    NSLog(@"  AppDelegte ---------: locationId %@",  self.currentLocationId);
}
- (void)emspLocation:(EMSPLocation *)emspLocation didFailToReceiveWifiLocations:(NSError *)error{
  //  NSLog(@"  AppDelegte ---------:didFailToReceiveWifiLocations error %@", error);
}
//-----------------------------------------------------------------
-(void)showAlert:(NSString *)strTitle Message:(NSString *)strMessage
{
    UIAlertView *alert=[[UIAlertView alloc]initWithTitle:strTitle message:strMessage delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
    [alert show];
}

#pragma mark
#pragma mark  Cisco -full screen ad integration
-(void)showFullScreenCiscoAd:(UIViewController *)viewController ScreenName:(NSString *)strScreenName
{
    [self removeCustomApex];
    BOOL isInternetAvaliable=[self CheckNetWorkConnection];
    if(isInternetAvaliable)
    {
        [self showCiscoAd:viewController ScreenName:strScreenName Location:NO];
        if(self.showLocationAds && self.currentLocationId.length >0)
        {
            [self showCiscoAd:viewController ScreenName:strScreenName Location:YES];
        }
    }
}
-(void)showCiscoAd:(UIViewController *)viewController ScreenName:(NSString *)strScreenName Location:(BOOL)location
{
    NSString *strOverLay;
    if(location)
    {
        strOverLay=[NSString stringWithFormat:@"%@_%@_%@",self.currentLocationId,strScreenName,tag_OverlayLocation];

    }
    else
    {
        
        strOverLay=[NSString stringWithFormat:@"%@_%@_%@",cisco_mall_code,strScreenName,tag_Overlay];
    }
    
    NSLog(@"  ---------: tag name is %@", strOverLay);

    AppXOverlay *overlay = [[AppXOverlay alloc] initWithFrame:CGRectMake(10,0, viewController.view.frame.size.width - 20.0, viewController.view.frame.size.height -300) tag:strOverLay controller:viewController showAlways:YES];
    overlay.center=viewController.view.center;
    overlay.backgroundColor = [UIColor whiteColor];
    overlay.delegate=self;
    if(location )
    {
        locationOverlay=overlay;
        [locationOverlay show];
    }
    else
    {
        normalOverlay=overlay;
        [normalOverlay show];
    }
    
    
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    [params setValue:BUNDEL_ID forKey:@"myKey"];
    //    if(!location)
    //    {
    //        customAppX = [[CustomAppXView alloc]initWithFrame:self.window.frame tag:strFullScreen];
    //        customAppX.apexDelegate=self;
    //        [self.window addSubview:customAppX];
    //    }
    //    else
    //    {
    //        locationAppX = [[CustomAppXView alloc]initWithFrame:self.window.frame tag:strFullScreen];
    //        locationAppX.apexDelegate=self;
    //        [self.window addSubview:locationAppX];
    //    }
}

-(void)removeCustomApex
{
    if(normalOverlay)
    {
        [normalOverlay setDelegate:nil];
        normalOverlay=nil;
    }
    if(locationOverlay)
    {
        [locationOverlay setDelegate:nil];
        locationOverlay=nil;
    }
    if(customAppX)
    {
        [customAppX close:nil];
    }
    if(locationAppX)
    {
        [locationAppX close:nil];
    }
}

#pragma  mark CustomApexDelegate
-(void)removeCustomApexView:(CustomAppXView *)view
{
    [view setDelegate:nil];
    view=nil;
}
#pragma mark -
#pragma mark AppXViewOverlay delegate
-(void)didShowAppXOverlay
{
}
- (void)didCloseAppXOverlay
{
}

- (BOOL)application:(UIApplication *)application
            openURL:(NSURL *)url
  sourceApplication:(NSString *)sourceApplication
         annotation:(id)annotation {
    //FB
    return [[FBSDKApplicationDelegate sharedInstance] application:application
                                                          openURL:url
                                                sourceApplication:sourceApplication
                                                       annotation:annotation];
}


@end
