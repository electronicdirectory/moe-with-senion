//
//  HomeDataModel.h
//  MOE
//
//  Created by Nivrutti on 6/18/15.
//  Copyright (c) 2015 Neosoft. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface HomeDataModel : NSObject
@property(nonatomic,strong)NSMutableArray *arrSpotLightBanner,*arrEvent,*arrEntList,*arrOffer,*arrMovie,*mallMenus;
+ (instancetype)sharedInstance;
@property(nonatomic,assign)NSUInteger eventCount;
@property(nonatomic,strong) NSDictionary *dictCampaign;

@end
