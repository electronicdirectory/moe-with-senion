//
//  CategoryModel.h
//  MOE
//
//  Created by Nivrutti on 6/24/15.
//  Copyright (c) 2015 Neosoft. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CategoryModel : NSObject
@property(nonatomic,strong)NSString *categoryName;
@property   int catID;
@end
