

//
//  HomeDataModel.m
//  MOE
//
//  Created by Nivrutti on 6/18/15.
//  Copyright (c) 2015 Neosoft. All rights reserved.
//

#import "HomeDataModel.h"
@implementation HomeDataModel
+ (instancetype)sharedInstance
{
    //create single ton instance of webService class
    static id instance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        instance = [[self alloc] init];
    });
    return instance;
}
@end
