//
//  ServicesModel.h
//  MOE
//
//  Created by Nivrutti on 6/26/15.
//  Copyright (c) 2015 Neosoft. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ServicesModel : NSObject
@property(nonatomic,strong)NSString *strPageDetail,*strTitle,*strImage_URL,*strType;
@property(nonatomic,strong)NSNumber *tblId;
@end
