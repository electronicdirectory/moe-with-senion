//
//  ShopBaseModel.h
//  MOE
//
//  Created by Nivrutti on 6/24/15.
//  Copyright (c) 2015 Neosoft. All rights reserved.
//
#import <Foundation/Foundation.h>

 @interface ShopBaseModel : NSObject
@property(nonatomic,strong)NSString *strDescription,*strTitle,*strTelephone,*strFBURL,*strFloor,*strNearest_Parking,*strSummary,*strImage,*strTwitterUrL,*strType,*strDestination_ID;
@property(nonatomic,strong)NSNumber *numID;
@property(nonatomic,strong)NSString *categoryName;
@property   int catID;
@property(nonatomic,strong)NSArray *arrayFeatureProduct,*arrOffer,*arrImageList;
@property(nonatomic,strong)NSString *strShop_Code,*strLogo,*strStore_Timings,*strFloor_ID,*strLandmark;
@end
