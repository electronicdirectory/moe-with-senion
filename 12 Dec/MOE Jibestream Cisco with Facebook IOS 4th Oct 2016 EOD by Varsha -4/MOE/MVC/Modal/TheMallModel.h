//
//  TheMallModel.h
//  MOE
//
//  Created by Neosoft on 6/26/15.
//  Copyright (c) 2015 Neosoft. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface TheMallModel : NSObject
@property(nonatomic,strong)NSNumber *theMall_ID;
@property(nonatomic,strong)NSString *theMall_Titile;
@property(nonatomic,strong)NSString *theMall_PageDetail;
@property(nonatomic,strong)NSString *theMall_Image;
@end
