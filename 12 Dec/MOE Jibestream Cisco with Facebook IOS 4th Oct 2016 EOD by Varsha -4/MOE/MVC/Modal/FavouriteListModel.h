//
//  FavouriteListModel.h
//  MOE
//
//  Created by Nivrutti on 6/24/15.
//  Copyright (c) 2015 Neosoft. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface FavouriteListModel : NSObject
@property(nonatomic,strong)NSNumber *favID;
@property(nonatomic,strong)NSString *favType,*strTitle;
@end
