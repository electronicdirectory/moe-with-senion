//
//  BaseModel.h
//  MOE
//
//  Created by webwerks on 6/24/15.
//  Copyright (c) 2015 Neosoft. All rights reserved.
//

#import <Foundation/Foundation.h>
@interface BaseModel : NSObject

@property(nonatomic,strong)NSNumber *tblId;
@property(nonatomic,strong)NSString *strTitle,*strDesc,*strStartDate,*strEndDate,*strImageURL,*strType,*strTelephone,*strURL,*strCategory_Name,*strDetailsDesc;
@property(nonatomic,strong)NSNumber *tblStore_ID,*tblCategory_ID;
@end