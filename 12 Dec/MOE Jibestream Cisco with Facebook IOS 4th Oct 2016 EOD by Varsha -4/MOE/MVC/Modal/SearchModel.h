//
//  SearchModel.h
//  MOE
//
//  Created by Nivrutti on 6/17/15.
//  Copyright (c) 2015 Neosoft. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SearchModel : NSObject
@property(nonatomic,strong)NSString *search_Title;
@property(nonatomic,strong)NSString *search_ID;
@property(nonatomic,strong)NSString *search_Type;
@end
