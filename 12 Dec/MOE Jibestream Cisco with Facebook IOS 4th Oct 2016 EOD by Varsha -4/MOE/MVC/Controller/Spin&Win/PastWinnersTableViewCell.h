//
//  PastWinnersTableViewCell.h
//  MOE
//
//  Created by Neosoft on 6/23/15.
//  Copyright (c) 2015 Neosoft. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PastWinnersTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIView *baseView;

@property (weak, nonatomic) IBOutlet UILabel *lblWinnerTitle;
@property (weak, nonatomic) IBOutlet UILabel *lblContestTitle;
@property (weak, nonatomic) IBOutlet UILabel *lblDateTitle;


@property (weak, nonatomic) IBOutlet UILabel *lblWinner;
@property (weak, nonatomic) IBOutlet UILabel *lblContest;
@property (weak, nonatomic) IBOutlet UILabel *lblDate;

@property (weak, nonatomic) IBOutlet UIView *seperatorView;

@end
