//
//  SpinAndWinWrongAnswerViewController.m
//  MOE
//
//  Created by Neosoft on 6/24/15.
//  Copyright (c) 2015 Neosoft. All rights reserved.
//

#import "SpinAndWinWrongAnswerViewController.h"
#import "SpinAndWinLandingPageViewController.h"
#import "SpinAndWinPlayNowViewController.h"

@interface SpinAndWinWrongAnswerViewController ()

@end

@implementation SpinAndWinWrongAnswerViewController {
    int attemptLeft;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [CommanMethods getBgImage:self.view];
    self.navigationItem.titleView=[CommanMethods getLabel:SPIN_AND_WIN_TITLE];
    self.navigationItem.hidesBackButton = YES;

    self.lblCorrectAnswer.text = self.strCorrectAnswer;

    self.lblSorry.font = [CommanMethods getFont:18.0];
    self.lblCorrectAnswerTitle.font = [CommanMethods getDetailsFont];
    self.lblAvailableChances.font = [CommanMethods getHeaderFont];
    self.lblCorrectAnswer.font = [CommanMethods getFont:16.0];
    self.btnTryAgain.titleLabel.font = [CommanMethods getHeaderFont];
    
    self.lblAvailableChances.adjustsFontSizeToFitWidth = YES;
    attemptLeft = [self.strAttemptsLeft intValue];

    UIImage *imgStarAttempDone = [UIImage imageNamed:@"spin-attempt-solid"];

    switch (attemptLeft) {
        case 3: {
        }
            break;
        case 2: {
            self.imgStar1.image = imgStarAttempDone;
        }
            break;
        case 1: {
            self.imgStar1.image = imgStarAttempDone;
            self.imgStar2.image = imgStarAttempDone;
        }
            break;
        case 0: {
            self.imgStar1.image = imgStarAttempDone;
            self.imgStar2.image = imgStarAttempDone;
            self.imgStar3.image = imgStarAttempDone;
        }
            break;
            
        default:
            break;
    }

    if(attemptLeft<=0) {
        [CommanMethods displayAlert:alert_spin_and_win_title :@"Sorry you have reached the maximum tries for the day, please try again tomorrow"];
        self.lblAvailableChances.text = [NSString stringWithFormat:@"Sorry you have reached the maximum tries for the day, please try again tomorrow"];
    }
    else {
        self.lblAvailableChances.text = [NSString stringWithFormat:@"You have %d more attempts left", attemptLeft];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (IBAction)btnTryAgain:(id)sender {
    
    if(attemptLeft<=0) {
        for(UIViewController *vc in [self.navigationController viewControllers]) {
            if([vc isKindOfClass:[SpinAndWinLandingPageViewController class]])
                [self.navigationController popToViewController:vc animated:YES];
        }
    }
    else {
        for(UIViewController *vc in [self.navigationController viewControllers]) {
            if([vc isKindOfClass:[SpinAndWinPlayNowViewController class]])
                [self.navigationController popToViewController:vc animated:YES];
        }
    }
}
@end
