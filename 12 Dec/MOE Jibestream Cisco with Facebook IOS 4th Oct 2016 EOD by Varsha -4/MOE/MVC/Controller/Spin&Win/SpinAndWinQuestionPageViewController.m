//
//  SpinAndWinQuestionPageViewController.m
//  MOE
//
//  Created by Neosoft on 6/22/15.
//  Copyright (c) 2015 Neosoft. All rights reserved.
//

#import "SpinAndWinQuestionPageViewController.h"
#import "SpinAndWinQuestionHeaderTableViewCell.h"
#import "SpinAndWinQuestionOptionTableViewCell.h"
#import "SpinAndWinCorrectAnswerViewController.h"
#import "SpinAndWinWrongAnswerViewController.h"

#define SEL_CHECK [UIImage imageNamed:@"spin-reg-field-sel-chk"]
#define SEL_UNCHECK [UIImage imageNamed:@"spin-reg-field-sel-unchk"]

@interface SpinAndWinQuestionPageViewController ()
{
    float answerHeight;
    float questionHeight;

}
@end

@implementation SpinAndWinQuestionPageViewController {
    
    NSArray *arrOption;
    NSArray *arrOptionId;
    NSMutableArray *arrAnswerId;
    int answerIdIndex;
    int userAnswerIndex; //4=no answer; 0-3=index.row of user selected answer
    int correctAnswerIndex;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
  [CommanMethods getBgImage:self.view];
    
    if(IS_IPHONE_4_OR_LESS)
    {
        answerHeight = 50;
        questionHeight = 120;
    }
    else if (IS_IPHONE_5)
    {
        answerHeight = 60;
        questionHeight = 175;
    }
    else if (IS_IPHONE_6)
    {
         answerHeight = 65;
        questionHeight = 200;

    }
    else if (IS_IPHONE_6P)
    {
        answerHeight = 65;
        questionHeight = 200;


    }

    self.navigationItem.titleView=[CommanMethods getLabel:SPIN_AND_WIN_TITLE];
    self.navigationItem.hidesBackButton = YES;

    arrAnswerId = [[NSMutableArray alloc] init];
    
    [self.tableView registerClass:[SpinAndWinQuestionOptionTableViewCell class] forCellReuseIdentifier:@"OptionCell"];
    self.tableView.scrollEnabled = NO;
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    
    userAnswerIndex = 4;
    
    
    arrOption = [NSArray arrayWithObjects:@"Answer1", @"Answer2", @"Answer3", @"Answer4",  nil];
    arrOptionId = [NSArray arrayWithObjects:@"AnswerId1", @"AnswerId2", @"AnswerId3", @"AnswerId4",  nil];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 2;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if(section == 1)
        return 4;
    else
        return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(indexPath.section == 1)
        return answerHeight;
    else
        return questionHeight;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    if(section == 1)
        return 100;
    else
        return 0;
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{

    if(section == 1)
    {
        UIView *footerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, 100)];
        footerView.backgroundColor = [UIColor clearColor];
        UIButton *btnSubmit = [[UIButton alloc] initWithFrame:CGRectMake(0, 20, SCREEN_WIDTH*0.5, 40)];
        [btnSubmit setTitle:@"SUBMIT" forState:UIControlStateNormal];
        [btnSubmit setTitleColor:[CommanMethods getTextColor] forState:UIControlStateNormal];
        [btnSubmit setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"spin-ques-btn-submit-bg"]]];
        btnSubmit.titleLabel.font = [CommanMethods getHeaderFont];
        btnSubmit.center = CGPointMake(SCREEN_WIDTH/2, footerView.center.y);
        [btnSubmit addTarget:self action:@selector(btnSubmitMethod:) forControlEvents:UIControlEventTouchUpInside];
        [footerView addSubview:btnSubmit];
        return footerView;
    }
    else
        return [[UIView alloc] init];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    [tableView registerNib:[UINib nibWithNibName:@"SpinAndWinQuestionOptionTableViewCell" bundle:[NSBundle mainBundle]] forCellReuseIdentifier:@"OptionCell"];

    if(indexPath.section == 1) {
        
        SpinAndWinQuestionOptionTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"OptionCell"];
        cell.backgroundColor = [UIColor clearColor];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.baseView.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"spin-ques-answer-bg"]];
        cell.seperatorView.backgroundColor = [UIColor clearColor];

        cell.lblOption.text = [[self.dict objectForKey:@"Answer"] objectForKey:[arrOption objectAtIndex:indexPath.row]];
        cell.lblOption.font = [CommanMethods getHeaderFont];
        [arrAnswerId addObject:[[self.dict objectForKey:@"Answer"] objectForKey:[arrOptionId objectAtIndex:indexPath.row]]];
        return cell;
    }

    SpinAndWinQuestionHeaderTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"HeaderCell"];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.lblHeader.font = [CommanMethods getNavigationTitleFont];
    cell.lblHeader.adjustsFontSizeToFitWidth = YES;
    cell.txtQuestion.text = [self.dict objectForKey:@"Question"];
    cell.txtQuestion.textColor = [CommanMethods getTextColor];
    cell.txtQuestion.textAlignment = NSTextAlignmentCenter;
    cell.txtQuestion.font = [CommanMethods getDetailsFont];
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if(indexPath.section == 1) {
        userAnswerIndex = (int)indexPath.row;
        for(int i=0; i<4; i++) {
            NSIndexPath *index = [NSIndexPath indexPathForRow:i inSection:1];
            SpinAndWinQuestionOptionTableViewCell *cell = (SpinAndWinQuestionOptionTableViewCell*)[tableView cellForRowAtIndexPath:index];
            cell.imgOption.image = SEL_UNCHECK;
        }
        SpinAndWinQuestionOptionTableViewCell *cell = (SpinAndWinQuestionOptionTableViewCell*)[tableView cellForRowAtIndexPath:indexPath];
        cell.imgOption.image = SEL_CHECK;
    }
}

- (void)btnSubmitMethod:(id)sender {
    
    
    if(userAnswerIndex == 4)
    {
        [CommanMethods displayAlert:alert_spin_and_win_title :error_spin_no_answer_option_selected];
    }
    else {
        
        [APP_DELEGATE showHud:self.view Title:nil];
        [defaults synchronize];
        int userId = (int)[defaults integerForKey:@"UserId"];
        int questionId = [[self.dict objectForKey:@"QuestionId"] intValue];
        int AnswerId = [[arrAnswerId objectAtIndex:userAnswerIndex] intValue];
        int AttemptId = [[self.dict objectForKey:@"AttemptId"] intValue];
        
        [[WebServiceCommunicator sharedInstance] spinNwin_RegisterUserRightAnswer:Service_SpinAndWin_RegisterUserRightAnswer ResultRespose:Service_SpinAndWin_RegisterUserRightAnswer_Response_key ResultValKey:Service_SpinAndWin_RegisterUserRightAnswer_Result_key userId:userId CampaignId:self.intCampaignId questionId:questionId AnswerId:AnswerId AttemptId:AttemptId Block:^(id response, NSError *error)
        {
            [APP_DELEGATE closeHud];
            
             if([response isKindOfClass:[NSDictionary class]])
             {
                 if([[response objectForKey:@"Error"] isEqualToString:@""])
                 {
                     if([[response objectForKey:@"Data"] isEqualToString:@"Success"])
                     {
                         SpinAndWinCorrectAnswerViewController *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"SpinAndWinCorrectAnswerViewController"];
                         vc.strDrawTitle = self.strDrawTitle;
                         [self.navigationController pushViewController:vc animated:YES];
                         return ;
                     }
                 }
                 else
                 {
                     if([[response objectForKey:@"Error"] isEqualToString:@"Wrong answer"])
                     {
                         correctAnswerIndex = (int)[arrAnswerId indexOfObject:[self.dict objectForKey:@"CorrectAnswerId"]];
                         SpinAndWinWrongAnswerViewController *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"SpinAndWinWrongAnswerViewController"];
                         vc.strCorrectAnswer = [[self.dict objectForKey:@"Answer"] objectForKey:[arrOption objectAtIndex:correctAnswerIndex]];
                         vc.strAttemptsLeft = [NSString stringWithFormat:@"%d", [[response objectForKey:@"AttemptsLeft"] intValue]];
                         [self.navigationController pushViewController:vc animated:YES];
                         return;
                     }
                     else
                     {
                         [CommanMethods displayAlert:alert_spin_and_win_title :[response objectForKey:@"Error"]];
                         return;
                     }
                 }
             }
           
                [CommanMethods displayAlert:@"Error" :error_in_connection];
         }];
    }
}



@end


