//
//  SpinAndWinForgotPasswordViewController.h
//  MOE
//
//  Created by Neosoft on 6/22/15.
//  Copyright (c) 2015 Neosoft. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SpinAndWinForgotPasswordViewController : UIViewController

@property (weak, nonatomic) IBOutlet TPKeyboardAvoidingScrollView *scrollView;
@property (weak, nonatomic) IBOutlet UILabel *lblBannerTitle;
@property (weak, nonatomic) IBOutlet UILabel *lblBannerDesc;
@property (weak, nonatomic) IBOutlet UITextField *txtEmail;
@property (weak, nonatomic) IBOutlet UILabel *lblSecurityCode;

- (IBAction)btnSubmitEmailMethod:(id)sender;
- (IBAction)btnResetMethod:(id)sender;

@end
