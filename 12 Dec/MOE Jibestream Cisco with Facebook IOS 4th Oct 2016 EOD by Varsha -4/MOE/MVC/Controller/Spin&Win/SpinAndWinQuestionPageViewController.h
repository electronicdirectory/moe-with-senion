//
//  SpinAndWinQuestionPageViewController.h
//  MOE
//
//  Created by Neosoft on 6/22/15.
//  Copyright (c) 2015 Neosoft. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SpinAndWinQuestionPageViewController : UIViewController <UITableViewDataSource, UITableViewDelegate>

@property (nonatomic) NSDictionary *dict;
@property (nonatomic) int intCampaignId;
@property (nonatomic) NSString *strDrawTitle;

@property (weak, nonatomic) IBOutlet UITableView *tableView;


@end
