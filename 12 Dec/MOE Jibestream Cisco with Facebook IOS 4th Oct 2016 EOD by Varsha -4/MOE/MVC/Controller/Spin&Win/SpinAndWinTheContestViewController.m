//
//  SpinAndWinTheContestViewController.m
//  MOE
//
//  Created by Neosoft on 6/22/15.
//  Copyright (c) 2015 Neosoft. All rights reserved.
//

#import "SpinAndWinTheContestViewController.h"

@interface SpinAndWinTheContestViewController ()

@end

@implementation SpinAndWinTheContestViewController

#pragma mark - view lifecycle
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [CommanMethods getBgImage:self.view];
    self.navigationItem.titleView=[CommanMethods getLabel:SPIN_AND_WIN_TITLE];

    
    self.lblTitle.font = [CommanMethods getFont:18.0];
    self.txtDetail.font = [CommanMethods getHeaderFont];
    
    self.txtDetail.text = self.strDetail;
}

#pragma mark - memory warning
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
