//
//  SpinAndWinPlayNowViewController.m
//  MOE
//
//  Created by Neosoft on 6/22/15.
//  Copyright (c) 2015 Neosoft. All rights reserved.
//

#import "SpinAndWinPlayNowViewController.h"
#import "SpinAndWinQuestionPageViewController.h"

@interface SpinAndWinPlayNowViewController ()

@end

@implementation SpinAndWinPlayNowViewController {
    
    UITapGestureRecognizer *tapToRotate;
    BOOL isRotate;
}

#pragma mark - view life cycle
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [CommanMethods getBgImage:self.view];

   self.navigationItem.titleView=[CommanMethods getLabel:SPIN_AND_WIN_TITLE];
    [GoogleAnalyticsHandler sendScreenName:screenName_SpinWinPlay];

    
    self.lblTitle.font = [CommanMethods getHeaderFont];
    self.lblTitle.text = self.strTitle;
    self.viewLbl.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"spin-spin-contest-title-bg"]];
    self.lblFooter.font = [CommanMethods getDetailsFont];
    //tap gesture
    tapToRotate = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapToRotateMethod:)];
    [self.viewWheel addGestureRecognizer:tapToRotate];
    //set flag
    isRotate = NO;

    CGFloat heightConstraintT = 0.0;
    CGFloat heightConstraintB = 0.0;
    CGFloat heightConstraintAboveWheel = 0.0;
    CGFloat heightConstraintbelowWheel = 0.0;
    
    if(IS_IPHONE_5) {
        heightConstraintT = 5.0;
        heightConstraintB = 30.0;
        heightConstraintAboveWheel = 20.0;
        heightConstraintbelowWheel = 20.0;
    }
    if(IS_IPHONE_6) {
        heightConstraintT = 10.0;
        heightConstraintB = 50.0;
        heightConstraintAboveWheel = 30.0;
        heightConstraintbelowWheel = 30.0;
    }
    if(IS_IPHONE_6P) {
        heightConstraintT = 20.0;
        heightConstraintB = 60.0;
        heightConstraintAboveWheel = 40.0;
        heightConstraintbelowWheel = 40.0;
    }
    
    [self adjustConstraints:heightConstraintT :heightConstraintB :heightConstraintAboveWheel :heightConstraintbelowWheel];
}

- (void)adjustConstraints: (CGFloat)heightConstraintT :(CGFloat)heightConstraintB :(CGFloat)heightConstraintAboveWheel :(CGFloat)heightConstraintbelowWheel{
    self.topViewHeightConstraint.constant = self.topViewHeightConstraint.constant + heightConstraintT;
    self.bottomViewHeightConstraint.constant = self.bottomViewHeightConstraint.constant + heightConstraintB;
    self.aboveWheelViewHeightConstraint.constant = self.aboveWheelViewHeightConstraint.constant + heightConstraintAboveWheel;
    self.belowWheelViewHeightConstraint.constant = self.belowWheelViewHeightConstraint.constant + heightConstraintbelowWheel;

}

#pragma mark - tap gesture method
- (void)tapToRotateMethod:(UITapGestureRecognizer *)gesture {
    
    if(isRotate) {
        isRotate = NO;
        [APP_DELEGATE showHud:self.view Title:nil];

        [[WebServiceCommunicator sharedInstance] spinNwin_getQuestionsByCampaignId:Service_SpinAndWin_getQuestionsByCampaignId ResultRespose:Service_SpinAndWin_getQuestionsByCampaignId_Response_key ResultValKey:Service_SpinAndWin_getQuestionsByCampaignId_Result_key CampaignId:self.intCampaignId Block:^(id response, NSError *error) {
            [APP_DELEGATE closeHud];

            if([response isKindOfClass:[NSDictionary class]])
            {
                if([[response objectForKey:@"Error"]isEqual:@"Attempts Exceeded"])
                {
                    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:alert_spin_and_win_title message:@"Sorry you have reached the maximum tries for the day, please try again tomorrow." delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil];
                    alert.tag = 101;
                    [alert show];
                }
                else  if([[response objectForKey:@"Error"]isEqual:@"Already Answered"])
                {
                    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:alert_spin_and_win_title message:@"Sorry, your entry is already submitted with this campaign. Try your Luck with other campaigns." delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil];
                    alert.tag = 101;
                    [alert show];
                }
                else  if([[response objectForKey:@"Error"]isEqual:@""])
                {
                    SpinAndWinQuestionPageViewController *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"SpinAndWinQuestionPageViewController"];
                    vc.dict = response;
                    vc.intCampaignId = self.intCampaignId;
                    vc.strDrawTitle = self.strTitle;
                    [self.navigationController pushViewController:vc animated:YES];
                }
                else
                {
                    [CommanMethods displayAlert:@"Error" :error_in_connection];
                }
            }
            else
            {
                [CommanMethods displayAlert:@"Error" :error_in_connection];
            }
        }];
    }
    else {
        isRotate = YES;
        [self rotateImageView];
    }
}

#pragma mark - animation
- (void)rotateImageView
{
    if(isRotate) {
        [UIView animateWithDuration:0.1 delay:0 options:UIViewAnimationOptionCurveLinear animations:^{
            [self.imgWheel setTransform:CGAffineTransformRotate(self.imgWheel.transform, DEGREES_TO_RADIANS(15))];
        }completion:^(BOOL finished){
            if (finished) {
                [self rotateImageView];
            }
        }];
    }
    else
        return;
}

#pragma mark - memory warning
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - alert view delegate
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    if([alertView tag] == 101) {
        [self.navigationController popViewControllerAnimated:YES];
    }
}


@end
