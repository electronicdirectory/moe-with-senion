//
//  SpinAndWinCorrectAnswerViewController.h
//  MOE
//
//  Created by Neosoft on 6/24/15.
//  Copyright (c) 2015 Neosoft. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SpinAndWinCorrectAnswerViewController : UIViewController

@property (nonatomic) NSString *strDrawTitle;


@property (weak, nonatomic) IBOutlet UIImageView *imgHeader;
@property (weak, nonatomic) IBOutlet UILabel *lblCongratsHeader;
@property (weak, nonatomic) IBOutlet UILabel *lblCongratsSubHeader;
@property (weak, nonatomic) IBOutlet UILabel *lblYouAreSelectedFor;
@property (weak, nonatomic) IBOutlet UILabel *lblDrawTitle;
@property (weak, nonatomic) IBOutlet UIImageView *imgDraw;
@property (weak, nonatomic) IBOutlet UILabel *lblBestOfLuck;
@property (weak, nonatomic) IBOutlet UIButton *btnDone;

- (IBAction)btnDone:(id)sender;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *viewAboveSumbitButtonHeightConstraint;

@end
