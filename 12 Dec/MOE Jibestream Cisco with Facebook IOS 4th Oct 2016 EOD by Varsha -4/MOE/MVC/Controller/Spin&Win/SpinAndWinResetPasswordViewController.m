//
//  SpinAndWinResetPasswordViewController.m
//  MOE
//
//  Created by Neosoft on 6/22/15.
//  Copyright (c) 2015 Neosoft. All rights reserved.
//

#import "SpinAndWinResetPasswordViewController.h"

#define ICON_EMAIL [UIImage imageNamed:@"spin-reset-ico-email"]
#define ICON_SECURITY_CODE [UIImage imageNamed:@"spin-reset-ico-code"]
#define ICON_PASSWORD [UIImage imageNamed:@"spin-reset-ico-pass"]

@interface SpinAndWinResetPasswordViewController ()

@end

@implementation SpinAndWinResetPasswordViewController {
    
    NSMutableDictionary *dict;
}

#pragma mark - view lifecycle
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [GoogleAnalyticsHandler sendScreenName:screenName_SpinWinResetPassword];

  [CommanMethods getBgImage:self.view];

    self.navigationItem.titleView=[CommanMethods getLabel:SPIN_AND_WIN_RESET_PASSWORD_TITLE];


    dict = [[NSMutableDictionary alloc] init];
    [self designTextField:self.txtEmail :@"Email ID" :ICON_EMAIL];
    [self designTextField:self.txtSecurityCode :@"Security Code" :ICON_SECURITY_CODE];
    [self designTextField:self.txtPassword :@"New Password" :ICON_PASSWORD];
    [self designTextField:self.txtConfirmPassword :@"Confirm New Password" :ICON_PASSWORD];
    [self.scrollView contentSizeToFit];
}

#pragma mark - memory warning
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - button method
- (IBAction)btnResetMethod:(id)sender {
    
    if([self validateFields]) {
        [dict setObject:self.txtEmail.text forKey:spinNwin_reset_Email];
        [dict setObject:self.txtSecurityCode.text forKey:spinNwin_reset_SecurityCode];
        [dict setObject:self.txtPassword.text forKey:spinNwin_reset_newpassword];
        [self callWebService];
    }
}
#pragma mark - design fields
- (void)designTextField :(UITextField *)txt :(NSString *)strPlaceholder :(UIImage *)imgIcon {
    
    UIColor *color = [CommanMethods getTextPlaceholderColor];

    UIView *paddingViewEmail = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 35, 20)];
    UIImageView *imgIcoEmail = [[UIImageView alloc] initWithImage:imgIcon];
    [paddingViewEmail addSubview:imgIcoEmail];
    imgIcoEmail.center = CGPointMake(20, paddingViewEmail.center.y);
    txt.leftView = paddingViewEmail;
    txt.leftViewMode = UITextFieldViewModeAlways;
    txt.textColor = [CommanMethods getTextColor];
    txt.attributedPlaceholder = [[NSAttributedString alloc] initWithString:strPlaceholder attributes:@{NSForegroundColorAttributeName: color}];
    
        txt.autocorrectionType = UITextAutocorrectionTypeNo;
}

#pragma mark - validate fields
- (BOOL)validateFields {
    
    BOOL isValidatedTrue = true;
    
    if([self.txtEmail.text isEqualToString:@""]) {
        isValidatedTrue = false;
        [CommanMethods displayAlert:alert_spin_and_win_title :error_invalid_email];
    }
    else if (![CommanMethods validateEmail:self.txtEmail.text]) {
        isValidatedTrue = false;
        [CommanMethods displayAlert:alert_spin_and_win_title :error_invalid_email];
    }
    else if([self.txtSecurityCode.text isEqualToString:@""]) {
        isValidatedTrue = false;
        [CommanMethods displayAlert:alert_spin_and_win_title :error_invalid_security_code];
    }
    else if([self.txtPassword.text isEqualToString:@""]) {
        isValidatedTrue = false;
        [CommanMethods displayAlert:alert_spin_and_win_title :error_invalid_password];
    }
    else if([self.txtConfirmPassword.text isEqualToString:@""]) {
        isValidatedTrue = false;
        [CommanMethods displayAlert:alert_spin_and_win_title :error_invalid_confirm_password];
    }
    else if(![self.txtPassword.text isEqualToString:self.txtConfirmPassword.text]) {
        isValidatedTrue = false;
        [CommanMethods displayAlert:alert_spin_and_win_title :error_invalid_confirm_password_mismatach];
    }
    return isValidatedTrue;
}

#pragma mark - alertView delegate
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    if(alertView.tag == 101) {
        [self.navigationController popToRootViewControllerAnimated:YES];
    }
}

#pragma mark - web service
- (void)callWebService
{
    [APP_DELEGATE showHud:self.view Title:nil];
    [[WebServiceCommunicator sharedInstance] spinNwin_ResetPassword:Service_SpinAndWin_ResetPassword ResultRespose:Service_SpinAndWin_ResetPassword_Response_key ResultValKey:Service_SpinAndWin_ResetPassword_Result_key dataDict:dict Block:^(id response, NSError *error)
     {
         [APP_DELEGATE closeHud];
         if([response isKindOfClass:[NSDictionary class]])
         {
             if(response == nil)
             {
                 [CommanMethods displayAlert:@"Error" :error_in_connection];
                 return;
             }
             else if([[response valueForKey:@"Data"] isKindOfClass:[NSNull class]])
             {
                 [CommanMethods displayAlert:@"Error" :[response valueForKey:@"Error"]];
                 return;
             }
             else  if([[response valueForKey:@"Error"] isEqualToString:@""])
             {
                 UIAlertView *alert = [[UIAlertView alloc] initWithTitle:alert_spin_and_win_title message:[response valueForKey:@"Data"] delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil];
                 alert.tag = 101;
                 [alert show];
             }
             else
             {
                 [CommanMethods displayAlert:@"Error" :[response valueForKey:@"Data"]];
             }
         }
         else {
             [CommanMethods displayAlert:@"Error" :error_in_connection];
         }
     }];
}
@end
