//
//  SpinAndWinPlayNowViewController.h
//  MOE
//
//  Created by Neosoft on 6/22/15.
//  Copyright (c) 2015 Neosoft. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SpinAndWinPlayNowViewController : UIViewController <UIAlertViewDelegate>

@property (nonatomic) NSString *strTitle;
@property (nonatomic) int intCampaignId;

@property (weak, nonatomic) IBOutlet UILabel *lblTitle;
@property (weak, nonatomic) IBOutlet UILabel *lblFooter;
@property (weak, nonatomic) IBOutlet UIView *viewLbl;
@property (weak, nonatomic) IBOutlet UIView *viewWheel;
@property (weak, nonatomic) IBOutlet UIImageView *imgWheel;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *bottomViewHeightConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *topViewHeightConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *aboveWheelViewHeightConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *belowWheelViewHeightConstraint;

@end
