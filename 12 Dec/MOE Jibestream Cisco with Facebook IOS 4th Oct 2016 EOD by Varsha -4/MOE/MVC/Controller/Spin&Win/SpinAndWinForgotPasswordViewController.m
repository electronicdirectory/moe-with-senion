//
//  SpinAndWinForgotPasswordViewController.m
//  MOE
//
//  Created by Neosoft on 6/22/15.
//  Copyright (c) 2015 Neosoft. All rights reserved.
//

#import "SpinAndWinForgotPasswordViewController.h"
#import "SpinAndWinResetPasswordViewController.h"

@interface SpinAndWinForgotPasswordViewController ()

@end

@implementation SpinAndWinForgotPasswordViewController

#pragma mark - view lifecycle
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [GoogleAnalyticsHandler sendScreenName:screenName_SpinWinForgotPassword];

    [CommanMethods getBgImage:self.view];
    self.navigationItem.titleView=[CommanMethods getLabel:SPIN_AND_WIN_FORGOT_PASSWORD_TITLE];

    
    [self designLabels];
    [self designTextField];
    
    [self.scrollView contentSizeToFit];
}

#pragma mark - memory warning
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - button methods

- (IBAction)btnSubmitEmailMethod:(id)sender {
    
    if([self validateFields]) {
        
        [self callWebService];
    }
}

- (IBAction)btnResetMethod:(id)sender {

    SpinAndWinResetPasswordViewController *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"SpinAndWinResetPasswordViewController"];
    [self.navigationController pushViewController:vc animated:YES];
}

#pragma mark - design fields
- (void)designLabels {
    
    self.lblBannerTitle.font = [CommanMethods getHeaderFont];
//    self.lblBannerDesc.font = [CommanMethods getDetailsFont];
    self.lblSecurityCode.font = [CommanMethods getDetailsFont];
    
    self.lblBannerTitle.adjustsFontSizeToFitWidth = YES;
    self.lblBannerDesc.adjustsFontSizeToFitWidth = YES;
}

- (void)designTextField {
    
    UIColor *color = [CommanMethods getTextPlaceholderColor];

    UIView *paddingViewEmail = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 35, 20)];
    UIImageView *imgIcoEmail = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"spin-forgot-ico-email"]];
    [paddingViewEmail addSubview:imgIcoEmail];
    imgIcoEmail.center = CGPointMake(20, paddingViewEmail.center.y);
    self.txtEmail.leftView = paddingViewEmail;
    self.txtEmail.leftViewMode = UITextFieldViewModeAlways;
    self.txtEmail.textColor = [CommanMethods getTextColor];
    self.txtEmail.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Email ID" attributes:@{NSForegroundColorAttributeName: color}];
    
  self.txtEmail.autocorrectionType = UITextAutocorrectionTypeNo;
}

#pragma mark - validate fields
- (BOOL)validateFields {
    
    BOOL isValidatedTrue = true;
    
    if([self.txtEmail.text isEqualToString:@""]) {
        
        isValidatedTrue = false;
        [CommanMethods displayAlert:alert_spin_and_win_title :error_invalid_email];
    }
    else if (![CommanMethods validateEmail:self.txtEmail.text]) {
        
        isValidatedTrue = false;
        [CommanMethods displayAlert:alert_spin_and_win_title :error_invalid_email];
    }
    return isValidatedTrue;
}

#pragma mark - Webservice
- (void)callWebService {
    
    [APP_DELEGATE showHud:self.view Title:nil];

    NSString *EMail = self.txtEmail.text;
    [[WebServiceCommunicator sharedInstance] spinNwin_ForgotPassword:Service_SpinAndWin_ForgotPassword ResultRespose:Service_SpinAndWin_ForgotPassword_Response_key ResultValKey:Service_SpinAndWin_ForgotPassword_Result_key Email:EMail Block:^(id response, NSError *error) {
        
        [APP_DELEGATE closeHud];

        if([response isKindOfClass:[NSDictionary class]])
        {
            
            if(response == nil)
            {
                [CommanMethods displayAlert:@"Error" :error_in_connection];
                return;
            }
            else  if([[response valueForKey:@"Data"] isKindOfClass:[NSNull class]])
            {
                [CommanMethods displayAlert:@"Error" :[response valueForKey:@"Error"]];
                return;
            }
            if([[response valueForKey:@"Error"] isEqualToString:@""])
            {
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"MOE" message:[response valueForKey:@"Data"] delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil];
                alert.tag = 101;
                [alert show];
            }
            else
            {
                [CommanMethods displayAlert:@"Error" :[response valueForKey:@"Data"]];
            }
        }
        else
        {
            [CommanMethods displayAlert:@"Error" :error_in_connection];
        }
    }];
}
@end





