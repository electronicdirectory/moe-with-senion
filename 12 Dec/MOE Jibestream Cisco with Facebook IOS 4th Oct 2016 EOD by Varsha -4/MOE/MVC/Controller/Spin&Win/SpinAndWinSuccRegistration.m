//
//  SpinAndWinSuccRegistration.m
//  MOE
//
//  Created by webwerks on 7/15/15.
//  Copyright (c) 2015 Neosoft. All rights reserved.
//
#import "Spin&WinViewController.h"
#import "SpinAndWinSuccRegistration.h"
#define TITLELABLE @"Get lucky with Spin & Win"
#define SUCCCESSFULLREGISTRATION @"Your registration completed successfully. Thank you for registering. "
@interface SpinAndWinSuccRegistration ()

@end

@implementation SpinAndWinSuccRegistration

- (void)viewDidLoad {
    
    
    [super viewDidLoad];
    
    
    self.lblTitle.font=[CommanMethods getHeaderFont];
    self.lblSuccMsg.font=[CommanMethods getDetailsFont];
    self.lblTitle.text=TITLELABLE;
    self.lblSuccMsg.text=SUCCCESSFULLREGISTRATION;
    self.lblTitle.textColor=[CommanMethods getTextColor];
    self.lblSuccMsg.textColor=[CommanMethods getTextColor];
    self.btnSpinWin.titleLabel.font=[CommanMethods getHeaderFont];
    self.lblTitle.hidden=NO;
    self.lblSuccMsg.hidden=NO;
    self.btnSpinWin.hidden=NO;
    self.navigationItem.titleView = [CommanMethods getLabel:@"SPIN & WIN"];
    [self.navigationItem setHidesBackButton:YES animated:YES];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)btnJumpToLandingScreen:(id)sender
{
    [self.navigationController popToRootViewControllerAnimated:YES];
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
