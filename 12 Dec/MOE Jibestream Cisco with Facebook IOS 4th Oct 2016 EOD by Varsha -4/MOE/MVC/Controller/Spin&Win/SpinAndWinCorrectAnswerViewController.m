//
//  SpinAndWinCorrectAnswerViewController.m
//  MOE
//
//  Created by Neosoft on 6/24/15.
//  Copyright (c) 2015 Neosoft. All rights reserved.
//

#import "SpinAndWinCorrectAnswerViewController.h"
#import "SpinAndWinLandingPageViewController.h"

@interface SpinAndWinCorrectAnswerViewController ()

@end

@implementation SpinAndWinCorrectAnswerViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [CommanMethods getBgImage:self.view];
    self.navigationItem.titleView=[CommanMethods getLabel:SPIN_AND_WIN_TITLE];
    self.navigationItem.hidesBackButton = YES;

    self.lblDrawTitle.text = self.strDrawTitle;

    self.lblCongratsHeader.font = [CommanMethods getFont:20.0];
    self.lblCongratsSubHeader.font = [CommanMethods getFont:16.0];
    self.lblYouAreSelectedFor.font = [CommanMethods getHeaderFont];
    self.lblDrawTitle.font = [CommanMethods getHeaderFont];
    self.lblBestOfLuck.font = [CommanMethods getHeaderFont];
    self.btnDone.titleLabel.font = [CommanMethods getHeaderFont];

    CGFloat heightConstraint = 0.0;
    if(IS_IPHONE_5) {
        heightConstraint = 25.0;
    }
    if(IS_IPHONE_6) {
        heightConstraint = 45.0;
    }
    if(IS_IPHONE_6P) {
        heightConstraint = 60.0;
    }
    [self adjustConstraints:heightConstraint];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)adjustConstraints :(CGFloat)height
{
    self.viewAboveSumbitButtonHeightConstraint.constant = self.viewAboveSumbitButtonHeightConstraint.constant + height;
}

- (IBAction)btnDone:(id)sender {
    
    for(UIViewController *vc in [self.navigationController viewControllers]) {
        
        if([vc isKindOfClass:[SpinAndWinLandingPageViewController class]])
            [self.navigationController popToViewController:vc animated:YES];
    }
}
@end
