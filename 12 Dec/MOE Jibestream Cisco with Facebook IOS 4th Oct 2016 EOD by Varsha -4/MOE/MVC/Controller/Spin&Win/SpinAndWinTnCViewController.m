//
//  SpinAndWinTnCViewController.m
//  MOE
//
//  Created by Neosoft on 6/24/15.
//  Copyright (c) 2015 Neosoft. All rights reserved.
//

#import "SpinAndWinTnCViewController.h"

@interface SpinAndWinTnCViewController ()

@end

@implementation SpinAndWinTnCViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [CommanMethods getBgImage:self.view];
    self.navigationItem.titleView=[CommanMethods getLabel:SPIN_AND_WIN_TITLE];
    self.lblHeader.font = [CommanMethods getFont:18.0];
    self.txtTnC.font = [CommanMethods getHeaderFont];
    self.txtTnC.text = self.strTnC;
    self.lblHeader.adjustsFontSizeToFitWidth = YES;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



@end
