//
//  SpinAndWinWrongAnswerViewController.h
//  MOE
//
//  Created by Neosoft on 6/24/15.
//  Copyright (c) 2015 Neosoft. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SpinAndWinWrongAnswerViewController : UIViewController

@property (nonatomic) NSString *strCorrectAnswer;
@property (nonatomic) NSString *strAttemptsLeft;

@property (weak, nonatomic) IBOutlet UILabel *lblSorry;
@property (weak, nonatomic) IBOutlet UILabel *lblCorrectAnswerTitle;
@property (weak, nonatomic) IBOutlet UILabel *lblCorrectAnswer;
@property (weak, nonatomic) IBOutlet UILabel *lblAvailableChances;
@property (weak, nonatomic) IBOutlet UIButton *btnTryAgain;

@property (weak, nonatomic) IBOutlet UIImageView *imgStar1;
@property (weak, nonatomic) IBOutlet UIImageView *imgStar2;
@property (weak, nonatomic) IBOutlet UIImageView *imgStar3;
- (IBAction)btnTryAgain:(id)sender;

@end
