//
//  SpinAndWinLandingPageViewController.h
//  MOE
//
//  Created by Neosoft on 6/22/15.
//  Copyright (c) 2015 Neosoft. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SpinAndWinLandingPageViewController : UIViewController <UIPickerViewDataSource, UIPickerViewDelegate, UIActionSheetDelegate>

@property (nonatomic) NSDictionary *dictCampaign;

@property (weak, nonatomic) IBOutlet UILabel *lblHeader;
@property (weak, nonatomic) IBOutlet UIButton *btnContest;
@property (weak, nonatomic) IBOutlet UIImageView *imgContest;
@property (weak, nonatomic) IBOutlet UIButton *btnTnC;
@property (weak, nonatomic) IBOutlet UILabel *lblTitle;
@property (weak, nonatomic) IBOutlet UILabel *lblTnC;
@property (weak, nonatomic) IBOutlet UIButton *btnPlayNow;
@property (weak, nonatomic) IBOutlet UIButton *btnHowToPlay;
@property (weak, nonatomic) IBOutlet UIButton *btnDatesOfDraw;
@property (weak, nonatomic) IBOutlet UIButton *btnPastWinners;

- (IBAction)btnContestMethod:(id)sender;
- (IBAction)btnAcceptTnCMethod:(id)sender;
- (IBAction)btnPlayNowMethod:(id)sender;
- (IBAction)btnContestDetailMethod:(id)sender;
- (IBAction)btnDrawDateMethod:(id)sender;
- (IBAction)btnPastWinnerMethod:(id)sender;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *btnContestHeightConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *belowTnCViewHeightConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *aboveTnCViewHeightConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *btnViewHeightConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *bottomViewHeightConstraint;


@end
