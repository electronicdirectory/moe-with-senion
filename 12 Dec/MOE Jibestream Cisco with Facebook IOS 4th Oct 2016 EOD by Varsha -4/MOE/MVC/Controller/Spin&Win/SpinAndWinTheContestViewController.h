//
//  SpinAndWinTheContestViewController.h
//  MOE
//
//  Created by Neosoft on 6/22/15.
//  Copyright (c) 2015 Neosoft. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SpinAndWinTheContestViewController : UIViewController

@property (nonatomic) NSString *strDetail;

@property (weak, nonatomic) IBOutlet UILabel *lblTitle;
@property (weak, nonatomic) IBOutlet UITextView *txtDetail;

@end
