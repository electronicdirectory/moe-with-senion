//
//  SpinAndWinSuccRegistration.h
//  MOE
//
//  Created by webwerks on 7/15/15.
//  Copyright (c) 2015 Neosoft. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SpinAndWinSuccRegistration : UIViewController
{

}
@property (weak, nonatomic) IBOutlet UILabel *lblTitle;
@property (weak, nonatomic) IBOutlet UILabel *lblSuccMsg;
@property (weak, nonatomic) IBOutlet UIButton *btnSpinWin;
- (IBAction)btnJumpToLandingScreen:(id)sender;
@end
