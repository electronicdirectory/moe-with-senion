//
//  SpinAndWin_RegisterViewController.m
//  MOE
//
//  Created by Neosoft on 6/18/15.
//  Copyright (c) 2015 Neosoft. All rights reserved.
//

#import "SpinAndWin_RegisterViewController.h"
#import "SpinAndWinSuccRegistration.h"

#define ICON_NAME [UIImage imageNamed:@"spin-reg-ico-name"]
#define ICON_NATIONALITY [UIImage imageNamed:@"spin-reg-ico-nationality"]
#define ICON_MOBILE [UIImage imageNamed:@"spin-reg-ico-mobile"]
#define ICON_EMAIL [UIImage imageNamed:@"spin-reg-ico-email"]
#define ICON_PASSWORD [UIImage imageNamed:@"spin-reg-ico-pass"]
#define ICON_DROP_DOWN [UIImage imageNamed:@"spin-reg-ico-dropDown"]
#define SEL_CHECK [UIImage imageNamed:@"spin-reg-field-sel-chk"]
#define SEL_UNCHECK [UIImage imageNamed:@"spin-reg-field-sel-unchk"]

@interface SpinAndWin_RegisterViewController ()

@end

@implementation SpinAndWin_RegisterViewController {
    
    NSString *strTitle;
    int gender; //0=none selected; 1=male; 2=female
    UIImage *img_checked;
    UIImage *img_unchecked;
    UIView *actionSheetView;
    NSArray *arrNationality;
    NSArray *arrResident;
    UIPickerView *pickerObj;
    int typeOfDropDown;///0=none  1=nationality; 2=resident
}

#pragma mark - view lifecycle
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [GoogleAnalyticsHandler sendScreenName:screenName_SpinWinRegistration];

    [CommanMethods getBgImage:self.view];
    self.navigationItem.titleView=[CommanMethods getLabel:SPIN_AND_WIN_REGISTER_TITLE];
    
    gender = 0;
    typeOfDropDown=0;
    arrNationality = [CommanMethods getNationalities];
    arrResident=[CommanMethods getResident];
    [self designTextField:self.txtFName Placeholder:@"First Name" LeftImage:ICON_NAME RightImage:nil];
    [self designTextField:self.txtLName Placeholder:@"Last Name" LeftImage:ICON_NAME RightImage:nil];
    [self designTextField:self.txtNationality Placeholder:@"Nationality" LeftImage:ICON_NATIONALITY RightImage:ICON_DROP_DOWN];
    [self designTextField:self.txtResidentType Placeholder:@"Resident Type" LeftImage:ICON_NATIONALITY RightImage:ICON_DROP_DOWN];
    [self designTextField:self.txtMobile Placeholder:@"+971 55 0000000" LeftImage:ICON_MOBILE RightImage:nil];
    [self designTextField:self.txtEmail Placeholder:@"Email Id" LeftImage:ICON_EMAIL RightImage:nil];
    [self designTextField:self.txtPassword Placeholder:@"Password" LeftImage:ICON_PASSWORD RightImage:nil];
    [self designTextField:self.txtConfirmPassword Placeholder:@"Confirm Password" LeftImage:ICON_PASSWORD RightImage:nil];
    self.lblAllFieldsAreMandatory.font = [CommanMethods getDetailsFont];
    self.lblAllFieldsAreMandatory.adjustsFontSizeToFitWidth = YES;
    img_checked = SEL_CHECK;
    img_unchecked = SEL_UNCHECK;
    
    [self.btnMr setBackgroundImage:img_unchecked forState:UIControlStateNormal];
    [self.btnMs setBackgroundImage:img_unchecked forState:UIControlStateNormal];
    
    UITapGestureRecognizer *tapOnViewNationality = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapOnViewNationalityMethod:)];
    [self.viewNationality addGestureRecognizer:tapOnViewNationality];
    
    UITapGestureRecognizer *tapOnViewResidentType = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapOnViewResidentTypeMethod:)];
    [self.viewResidentType addGestureRecognizer:tapOnViewResidentType];
    
    [self.scrollView contentSizeToFit];
}

#pragma mark - memory warning
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - button actions

- (IBAction)btnMr:(id)sender {
    gender = 1;
    [self.btnMr setBackgroundImage:img_checked forState:UIControlStateNormal];
    [self.btnMs setBackgroundImage:img_unchecked forState:UIControlStateNormal];
}

- (IBAction)btnMs:(id)sender {
    gender = 2;
    [self.btnMr setBackgroundImage:img_unchecked forState:UIControlStateNormal];
    [self.btnMs setBackgroundImage:img_checked forState:UIControlStateNormal];
}

- (IBAction)btnRegisterMethod:(id)sender {
    [self callWebservice];
}

#pragma mark tap gesture

- (void)tapOnViewNationalityMethod:(UITapGestureRecognizer *)gesture
{
    [self.view endEditing:YES];

    if(!actionSheetView)
    {
        typeOfDropDown=1;
        [self showPickerWithNationalities];
    }
    else
    {
        typeOfDropDown=1;
        [pickerObj reloadAllComponents];

    }
}

- (void)tapOnViewResidentTypeMethod:(UITapGestureRecognizer *)gesture {
    [self.view endEditing:YES];

    if(!actionSheetView)
    {
        typeOfDropDown=2;
        [self showPickerWithNationalities];
    }
    else
    {
        typeOfDropDown=2;
        [pickerObj reloadAllComponents];
    }
}

#pragma mark - design fields
- (void)designTextField :(UITextField *)txt Placeholder:(NSString *)strPlaceholder LeftImage:(UIImage *)imgLeft RightImage:(UIImage *)imgRight {
    
    UIColor *color = [CommanMethods getTextPlaceholderColor];
    
    txt.textColor = [CommanMethods getTextColor];
    txt.attributedPlaceholder = [[NSAttributedString alloc] initWithString:strPlaceholder attributes:@{NSForegroundColorAttributeName: color}];
    
    UIView *paddingViewLeft = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 35, 20)];
    UIImageView *imgIconLeft = [[UIImageView alloc] initWithImage:imgLeft];
    [paddingViewLeft addSubview:imgIconLeft];
    imgIconLeft.center = CGPointMake(20, paddingViewLeft.center.y);
    txt.leftView = paddingViewLeft;
    txt.leftViewMode = UITextFieldViewModeAlways;
    
    if(imgRight) {
        UIView *paddingViewRight = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 35, 20)];
        UIImageView *imgIconRight = [[UIImageView alloc] initWithImage:imgRight];
        [paddingViewRight addSubview:imgIconRight];
        imgIconRight.center = CGPointMake(20, paddingViewRight.center.y);
        txt.rightView = paddingViewRight;
        txt.rightViewMode = UITextFieldViewModeAlways;
    }
    
        txt.autocorrectionType = UITextAutocorrectionTypeNo;
}

#pragma mark - validate fields
- (BOOL)validateFields {
    
    BOOL isValidatedTrue = true;
    
    if(gender == 0) {
        isValidatedTrue = false;
        [CommanMethods displayAlert:alert_spin_and_win_title :error_invalid_title_MRorMS];
    }
    else if([self.txtFName.text isEqualToString:@""]) {
        isValidatedTrue = false;
        [CommanMethods displayAlert:alert_spin_and_win_title :error_invalid_first_name];
    }
    else if([self.txtLName.text isEqualToString:@""]) {
        isValidatedTrue = false;
        [CommanMethods displayAlert:alert_spin_and_win_title :error_invalid_last_name];
    }
    else if([self.txtNationality.text isEqualToString:@""]) {
        isValidatedTrue = false;
        [CommanMethods displayAlert:alert_spin_and_win_title :error_invalid_nationality];
    }
    else if([self.txtResidentType.text isEqualToString:@""]) {
        isValidatedTrue = false;
        [CommanMethods displayAlert:alert_spin_and_win_title :error_invalid_resident_type];
    }
    else if([self.txtMobile.text isEqualToString:@""]) {
        isValidatedTrue = false;
        [CommanMethods displayAlert:alert_spin_and_win_title :error_invalid_mobile];
    }
    else if([self.txtEmail.text isEqualToString:@""]) {
        isValidatedTrue = false;
        [CommanMethods displayAlert:alert_spin_and_win_title :error_invalid_email];
    }
    else if(![CommanMethods validateEmail:self.txtEmail.text]) {
        isValidatedTrue = false;
        [CommanMethods displayAlert:alert_spin_and_win_title :error_invalid_email];
    }
    else if([self.txtPassword.text isEqualToString:@""]) {
        isValidatedTrue = false;
        [CommanMethods displayAlert:alert_spin_and_win_title :error_invalid_password];
    }
    else if([self.txtConfirmPassword.text isEqualToString:@""]) {
        isValidatedTrue = false;
        [CommanMethods displayAlert:alert_spin_and_win_title :error_invalid_confirm_password];
    }
    else if(![self.txtPassword.text isEqualToString:self.txtConfirmPassword.text]) {
        isValidatedTrue = false;
        [CommanMethods displayAlert:alert_spin_and_win_title :error_invalid_confirm_password_mismatach];
    }
    return isValidatedTrue;
}

#pragma mark - picker methods/delegates
- (void)showPickerWithNationalities {
    
    actionSheetView = [[UIView alloc] initWithFrame:CGRectMake(0, SCREEN_HEIGHT-260, SCREEN_WIDTH, 260)];
    actionSheetView.backgroundColor = [CommanMethods getTextColor];
    [self.view addSubview:actionSheetView];
    
    pickerObj  = [[UIPickerView alloc] initWithFrame:CGRectMake(0,44, SCREEN_WIDTH, 216)];
    pickerObj.delegate = self;
    pickerObj.dataSource = self;
    pickerObj.showsSelectionIndicator = YES;
    [actionSheetView addSubview:pickerObj];
    
    UIToolbar *tools=[[UIToolbar alloc]initWithFrame:CGRectMake(0, 0,SCREEN_WIDTH,44)];
    tools.barStyle = UIBarStyleBlack;
    [actionSheetView addSubview:tools];
    
    UIBarButtonItem *flexSpace= [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
    UIBarButtonItem *cancelButton=[[UIBarButtonItem alloc]initWithTitle:@"Cancel" style:UIBarButtonItemStylePlain target:self action:@selector(btnActinCancelClicked)];
    cancelButton.tintColor=[CommanMethods getTextColor];
    
    UIBarButtonItem *doneButton=[[UIBarButtonItem alloc]initWithTitle:@"Done" style:UIBarButtonItemStyleDone target:self action:@selector(btnActinDoneClicked)];
    doneButton.tintColor=[CommanMethods getTextColor];
    NSArray *array = [[NSArray alloc]initWithObjects: cancelButton, flexSpace, doneButton,nil];
    [tools setItems:array];
    
    //picker title
    UILabel *lblPickerTitle=[[UILabel alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT*0.3)];
    lblPickerTitle.text=@"";
    lblPickerTitle.backgroundColor=[UIColor clearColor];
    lblPickerTitle.textColor=[CommanMethods getTextColor];
    lblPickerTitle.textAlignment=NSTextAlignmentCenter;
    lblPickerTitle.font=[CommanMethods getHeaderFont];
    [tools addSubview:lblPickerTitle];
    [pickerObj reloadAllComponents];
    
    [UIView animateWithDuration:0.3 animations:^{
        actionSheetView.frame =CGRectMake(0, SCREEN_HEIGHT-260, SCREEN_WIDTH, 260);
        NSUInteger row=[pickerObj selectedRowInComponent:0];
         if (typeOfDropDown==1 && [arrNationality count]>0)
        {
            self.txtNationality.text= [arrNationality objectAtIndex:row];
        }
        else if([arrResident count]>0)
        {
            self.txtResidentType.text=[arrResident objectAtIndex:row];
        }
    }];
}

- (void)deallocActionSheetView {
    [actionSheetView removeFromSuperview];
    actionSheetView = nil;
    pickerObj=nil;
    typeOfDropDown=0;
}

#pragma mark picker buttons

- (void)btnActinCancelClicked {
    
    [UIView animateWithDuration:0.3 animations:^{
        actionSheetView.frame = CGRectMake(0, SCREEN_HEIGHT, SCREEN_WIDTH, 260);
    }completion:^(BOOL finished) {
        if(finished) {
            [self deallocActionSheetView];
        }
    }];
}

- (void)btnActinDoneClicked {
    
    [UIView animateWithDuration:0.3 animations:^{
        actionSheetView.frame = CGRectMake(0, SCREEN_HEIGHT, SCREEN_WIDTH, 260);
    }completion:^(BOOL finished) {
        if(finished) {
            NSUInteger row=[pickerObj selectedRowInComponent:0];
            if (typeOfDropDown==1)
            {
                self.txtNationality.text= [arrNationality objectAtIndex:row];
            }
            else
            {
                self.txtResidentType.text=[arrResident objectAtIndex:row];
            }
            [self deallocActionSheetView];
        }
    }];
}
#pragma mark picker delegates
- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    return 1;
}
- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    if (typeOfDropDown==1)
    {
        return [arrNationality count];
    }
    return [arrResident count];
}

-(NSString*) pickerView:(UIPickerView*)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    if (typeOfDropDown==1)
    {
        return [arrNationality objectAtIndex:row];
    }
    return [arrResident objectAtIndex:row];
}
- (UIView *)pickerView:(UIPickerView *)pickerView viewForRow:(NSInteger)row forComponent:(NSInteger)component reusingView:(UIView *)view
{
    UILabel *lblTitle;
    if(!view)
    {
        view=[[UIView alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 44)];
        lblTitle=[[UILabel alloc]initWithFrame:view.frame];
        lblTitle.backgroundColor=[UIColor clearColor];
        lblTitle.tag=111;
        [view addSubview:lblTitle];
    }
    
    lblTitle=(UILabel *)[view viewWithTag:111];
    lblTitle.textAlignment=NSTextAlignmentCenter;
    lblTitle.font=[CommanMethods getHeaderFont];
    NSString *strName=nil;
    
    if (typeOfDropDown==1)
    {
        strName= [arrNationality objectAtIndex:row];
    }
    else
    {
        strName=[arrResident objectAtIndex:row];
    }
    view.backgroundColor=[UIColor clearColor];
    lblTitle.text=strName;
    return  view;
}
- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
    if (typeOfDropDown==1)
    {
        self.txtNationality.text= [arrNationality objectAtIndex:row];
    }
    else
    {
        self.txtResidentType.text=[arrResident objectAtIndex:row];
    }
}


#pragma mark - alert view delegate
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if(alertView.tag == 101)
    {
        [self.navigationController popViewControllerAnimated:YES];
    }
}

#pragma mark - web service
- (void)callWebservice {
    
    if([self validateFields]) {
        
        NSMutableDictionary *dict = [[NSMutableDictionary alloc] init];
        
        if(gender == 1)
            strTitle = @"Mr.";
        else if (gender == 2)
            strTitle = @"Ms.";
        NSString *strName = [NSString stringWithFormat:@"%@ %@", self.txtFName.text, self.txtLName.text];
        NSString *strOptionId = SPINANDWIN_OPTION_ID;
        NSString *strEmirates_passportId = SPINANDWIN_EMIRATES_PASSPORT_ID;
        
        [dict setObject:strTitle forKey:spinNwin_register_title];
        [dict setObject:strName forKey:spinNwin_register_name];
        [dict setObject:self.txtEmail.text forKey:spinNwin_register_Email];
        [dict setObject:self.txtMobile.text forKey:spinNwin_register_Mobile];
        [dict setObject:self.txtResidentType.text forKey:spinNwin_register_Country_of_Residence];
        [dict setObject:self.txtPassword.text forKey:spinNwin_register_Password];
        [dict setObject:strOptionId forKey:spinNwin_register_OptionId];
        [dict setObject:strEmirates_passportId forKey:spinNwin_register_emirates_passportId];
        [dict setObject:self.txtNationality.text forKey:spinNwin_register_nationality];
        [APP_DELEGATE showHud:self.view Title:nil];
        
        [[WebServiceCommunicator sharedInstance] spinNwin_RegisterUser:Service_SpinAndWin_RegisterUser ResultRespose:Service_SpinAndWin_RegisterUser_Response_key ResultValKey:Service_SpinAndWin_RegisterUser_Result_key DataDict:dict Block:^(id response, NSError *error) {
            [APP_DELEGATE closeHud];
            if([response isKindOfClass:[NSDictionary class]])
            {
                if(response == nil)
                {
                    [CommanMethods displayAlert:@"Error" :error_in_connection];
                    return;
                }
                else if([[response valueForKey:@"Data"] isKindOfClass:[NSNull class]]) {
                    [CommanMethods displayAlert:@"Error" :[response valueForKey:@"Error"]];
                    return;
                }
                else if([[response valueForKey:@"Data"] isEqualToString:@"Success"])
                {
                    SpinAndWinSuccRegistration *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"SpinAndWinSuccRegistration"];
                    [self.navigationController pushViewController:vc animated:YES];
                }
                else
                {
                    [CommanMethods displayAlert:@"Error" :@"Error msg"];
                }
            }
            else
            {
                [CommanMethods displayAlert:@"Error" :error_in_connection];
            }
        }];
    }
}
@end
