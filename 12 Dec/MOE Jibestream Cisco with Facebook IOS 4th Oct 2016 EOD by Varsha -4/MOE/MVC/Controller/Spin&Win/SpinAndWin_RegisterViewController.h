//
//  SpinAndWin_RegisterViewController.h
//  MOE
//
//  Created by Neosoft on 6/18/15.
//  Copyright (c) 2015 Neosoft. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SpinAndWin_RegisterViewController : UIViewController<UIAlertViewDelegate, UIPickerViewDataSource, UIPickerViewDelegate>

@property (weak, nonatomic) IBOutlet TPKeyboardAvoidingScrollView *scrollView;
- (IBAction)btnMr:(id)sender;
- (IBAction)btnMs:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *btnMr;
@property (weak, nonatomic) IBOutlet UIButton *btnMs;
@property (weak, nonatomic) IBOutlet UITextField *txtFName;
@property (weak, nonatomic) IBOutlet UITextField *txtLName;
@property (weak, nonatomic) IBOutlet UIView *viewNationality;
@property (weak, nonatomic) IBOutlet UIView *viewResidentType;
@property (weak, nonatomic) IBOutlet UITextField *txtNationality;
@property (weak, nonatomic) IBOutlet UITextField *txtResidentType;
@property (weak, nonatomic) IBOutlet UITextField *txtMobile;
@property (weak, nonatomic) IBOutlet UITextField *txtEmail;
@property (weak, nonatomic) IBOutlet UITextField *txtPassword;
@property (weak, nonatomic) IBOutlet UITextField *txtConfirmPassword;
@property (weak, nonatomic) IBOutlet UILabel *lblAllFieldsAreMandatory;
- (IBAction)btnRegisterMethod:(id)sender;

@end
