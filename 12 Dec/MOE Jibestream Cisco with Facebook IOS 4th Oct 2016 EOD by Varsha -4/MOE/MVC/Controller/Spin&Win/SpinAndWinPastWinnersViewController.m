//
//  SpinAndWinPastWinnersViewController.m
//  MOE
//
//  Created by Neosoft on 6/22/15.
//  Copyright (c) 2015 Neosoft. All rights reserved.
//

#import "SpinAndWinPastWinnersViewController.h"

@interface SpinAndWinPastWinnersViewController ()

@end

@implementation SpinAndWinPastWinnersViewController {
    NSMutableArray *arrPrevWinners;
}

#pragma mark - synthesize
@synthesize dict;

#pragma mark - view lifecycle

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [CommanMethods getBgImage:self.view];
    self.navigationItem.titleView=[CommanMethods getLabel:SPIN_AND_WIN_TITLE];

    self.lblHeader.font = [CommanMethods getHeaderFont];

    self.tableView.dataSource = self;
    self.tableView.delegate = self;
    
    [self.tableView registerClass:[PastWinnersTableViewCell class] forCellReuseIdentifier:@"PastWinnersTableViewCell"];
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;

    NSArray *arrPastWinnersDictKey = [[NSArray alloc] initWithObjects:@"user_name", @"campaignTitle", @"drawdate", nil];
    arrPrevWinners = [[NSMutableArray alloc] init];

    NSArray *arr = [dict objectForKey:@"Data"];
    for(NSDictionary *dic in arr) {
        for(int i=0; i<self.numberOfPastWinners; i++) {
            NSDictionary *dictData=[NSDictionary dictionaryWithObjectsAndKeys:[dic objectForKey:[arrPastWinnersDictKey objectAtIndex:0]],@"winner", [dic objectForKey:[arrPastWinnersDictKey objectAtIndex:1]],@"contest", [dic objectForKey:[arrPastWinnersDictKey objectAtIndex:2]],@"date", nil];
            [arrPrevWinners addObject:dictData];
        }
    }
    [self.tableView reloadData];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - table view delegate/data source
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.numberOfPastWinners;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 150;
}


- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section {
    return [[UIView alloc] init];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    [tableView registerNib:[UINib nibWithNibName:@"PastWinnersTableViewCell" bundle:[NSBundle mainBundle]] forCellReuseIdentifier:@"PastWinnersTableViewCell"];
    
    NSDictionary *dictData=[arrPrevWinners objectAtIndex:indexPath.row];
    
    PastWinnersTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"PastWinnersTableViewCell"];
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.seperatorView.backgroundColor = [UIColor clearColor];
    cell.backgroundColor = [UIColor clearColor];
    
    if([[dictData objectForKey:@"DrawDesc"] isEqualToString:@""]) {
        cell.lblContest.text = @"";
        cell.lblDate.text = @"";
        cell.lblContestTitle.text = @"";
        cell.lblDateTitle.text = @"";
        cell.baseView.backgroundColor = [UIColor clearColor];
        return cell;
    }
    
    cell.baseView.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"draw-list-bg"]];
    cell.lblWinner.text = [dictData objectForKey:@"winner"];
    cell.lblContest.text = [dictData objectForKey:@"contest"];
    cell.lblDate.text = [CommanMethods changeDateFormatWithTimeZone:[dictData objectForKey:@"date"]];
    
    cell.lblWinnerTitle.font = [CommanMethods getDetailsFont];
    cell.lblContestTitle.font = [CommanMethods getDetailsFont];
    cell.lblDateTitle.font = [CommanMethods getDetailsFont];
    cell.lblWinner.font = [CommanMethods getHeaderFont];
    cell.lblContest.font = [CommanMethods getHeaderFont];
    cell.lblDate.font = [CommanMethods getHeaderFont];

    cell.lblContest.adjustsFontSizeToFitWidth = YES;
    return cell;
}


@end
