//
//  Spin&WinViewController.m
//  MOE
//
//  Created by Neosoft on 6/18/15.
//  Copyright (c) 2015 Neosoft. All rights reserved.
//

#import "Spin&WinViewController.h"
#import "SpinAndWin_RegisterViewController.h"
#import "SpinAndWinForgotPasswordViewController.h"
#import "SpinAndWinLandingPageViewController.h"
 @interface Spin_WinViewController ()

@end

@implementation Spin_WinViewController {
    
    NSMutableDictionary *dict;
}

#pragma mark - view lifecycle
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [GoogleAnalyticsHandler sendScreenName:screenName_SpinWinLogin];

    [CommanMethods getBgImage:self.view];
    self.navigationItem.rightBarButtonItem=[CommanMethods getRightButton:self Action:@selector(search)];
    self.navigationItem.leftBarButtonItem =[CommanMethods getLeftButton:self Action:@selector(menuMethod)];
     self.navigationItem.titleView = [CommanMethods getLabel:[@"SPIN & WIN"uppercaseString]];

    
    dict = [[NSMutableDictionary alloc] init];
    [self designTextField];
    [self designButton];
    [self designLabel];
    [self.scrollView contentSizeToFit];

    CGFloat heightConstraint = 0.0;
    CGFloat bottomViewSubviewsHeightConstraint = 0.0;
    
    if(IS_IPHONE_5) {
        heightConstraint = 15.0;
        bottomViewSubviewsHeightConstraint = 5.0;
    }
    if(IS_IPHONE_6) {
        heightConstraint = 45.0;
        bottomViewSubviewsHeightConstraint = 10.0;
    }
    if(IS_IPHONE_6P) {
        heightConstraint = 60.0;
        bottomViewSubviewsHeightConstraint = 15.0;
    }
    
    [self adjustConstraints:heightConstraint];
    [self fieldConstraints:bottomViewSubviewsHeightConstraint];
}

#pragma mark - memory warning
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)adjustConstraints :(CGFloat)height
{
    self.topViewHeightConstraint.constant = self.topViewHeightConstraint.constant + height;
    self.spinViewHeightConstraint.constant = self.spinViewHeightConstraint.constant + height;
    self.bottomViewHeightConstraint.constant = self.bottomViewHeightConstraint.constant + (height/4);
}

- (void)fieldConstraints :(CGFloat)height {

    self.fieldEmailHeightConstraint.constant = self.fieldEmailHeightConstraint.constant + height;
    self.fieldPasswordHeightConstraint.constant = self.fieldPasswordHeightConstraint.constant + height;
    self.btnLoginHeightConstraint.constant = self.btnLoginHeightConstraint.constant + height;
    self.viewBtnHeightConstraint.constant = self.viewBtnHeightConstraint.constant + height;

    
}

#pragma mark - btn methods

- (IBAction)btnLoginMethod:(id)sender {
    
    if([self validateFields]) {
        [dict setObject:self.txtEmail.text forKey:spinNwin_login_Email];
        [dict setObject:self.txtPassword.text forKey:spinNwin_login_Password];
        [APP_DELEGATE showHud:self.view Title:nil];
        [self callLoginWebService];
    }
}

- (IBAction)btnForgotDetailsMethod:(id)sender {
    SpinAndWinForgotPasswordViewController *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"SpinAndWinForgotPasswordViewController"];
    [self.navigationController pushViewController:vc animated:YES];
}

- (IBAction)btnNewUserMethod:(id)sender {
    SpinAndWin_RegisterViewController *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"SpinAndWin_RegisterViewController"];
    [self.navigationController pushViewController:vc animated:YES];
}

#pragma mark navigation bar button methods
- (void)menuMethod {
    [self.sideMenuViewController presentLeftMenuViewController];
}
- (void)search
{
    [[AppDelegate appDelegateInstance]openSearchScreen:self.navigationController];
}

#pragma mark - design fields
- (void)designTextField {
    
    UIView *paddingViewEmail = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 35, 15)];
    UIImageView *imgIcoEmail = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"spin-login-ico-email"]];
    [imgIcoEmail setContentMode:UIViewContentModeScaleAspectFit];
    imgIcoEmail.frame=CGRectMake(0, 0, 18, 18);
    imgIcoEmail.center = CGPointMake(22, paddingViewEmail.center.y);
    [paddingViewEmail addSubview:imgIcoEmail];
    self.txtEmail.leftView = paddingViewEmail;
    self.txtEmail.leftViewMode = UITextFieldViewModeAlways;
    
    UIView *paddingViewPass = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 35, 15)];
    UIImageView *imgIcoPass = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"spin-login-ico-pass"]];
    [imgIcoPass setContentMode:UIViewContentModeScaleAspectFit];
    imgIcoPass.frame=CGRectMake(0, 0, 18, 18);
    imgIcoPass.center = CGPointMake(22, paddingViewEmail.center.y);
    [paddingViewPass addSubview:imgIcoPass];
    self.txtPassword.leftView = paddingViewPass;
    self.txtPassword.leftViewMode = UITextFieldViewModeAlways;
    UIColor *color = [CommanMethods getTextPlaceholderColor];

    self.txtEmail.textColor = [CommanMethods getTextColor];
    self.txtEmail.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Email Id" attributes:@{NSForegroundColorAttributeName: color}];
    
    self.txtPassword.textColor = [CommanMethods getTextColor];
    self.txtPassword.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Password" attributes:@{NSForegroundColorAttributeName: color}];
    
    self.txtEmail.font = [CommanMethods getHeaderFont];
    self.txtPassword.font = [CommanMethods getHeaderFont];
    
    self.txtEmail.autocorrectionType = UITextAutocorrectionTypeNo;
    self.txtPassword.autocorrectionType = UITextAutocorrectionTypeNo;

}
- (void)designLabel {
    self.lblHello.font = [CommanMethods getFont:22.0];
}
- (void)designButton {
    self.btnLogin.titleLabel.font = [CommanMethods getHeaderFont];
    self.btnForgotPass.titleLabel.font = [CommanMethods getServiceTitleFont];
    self.btnForgotPass.titleLabel.adjustsFontSizeToFitWidth = YES;
    self.btnNewUser.titleLabel.font = self.btnForgotPass.titleLabel.font;
}

#pragma mark - validate fields
- (BOOL)validateFields {
    
    BOOL isValidatedTrue = true;
    
    if([self.txtEmail.text isEqualToString:@""]) {
        isValidatedTrue = false;
        [CommanMethods displayAlert:alert_spin_and_win_title :error_invalid_email];
    }
    else if (![CommanMethods validateEmail:self.txtEmail.text]) {
        isValidatedTrue = false;
        [CommanMethods displayAlert:alert_spin_and_win_title :error_invalid_email];
    }
    else if([self.txtPassword.text isEqualToString:@""]) {
        isValidatedTrue = false;
        [CommanMethods displayAlert:alert_spin_and_win_title :error_invalid_password];
    }
    return isValidatedTrue;
}

#pragma mark - web service method
- (void)callLoginWebService {
    
    [[WebServiceCommunicator sharedInstance] spinNwin_getUseronLogin:Service_SpinAndWin_getUseronLogin ResultRespose:Service_SpinAndWin_getUseronLogin_Response_key ResultValKey:Service_SpinAndWin_getUseronLogin_Result_key dataDict:dict Block:^(id response, NSError *error) {
        [APP_DELEGATE closeHud];
        if([response isKindOfClass:[NSDictionary class]]) {
            if(response == nil)
            {
                [CommanMethods displayAlert:@"Error" :error_in_connection];
                return;
            }
            if([[response valueForKey:@"Data"] isKindOfClass:[NSNull class]]) {
                [CommanMethods displayAlert:alert_spin_and_win_title :[response valueForKey:@"Error"]];
                return;
            }
            if([[response valueForKey:@"Error"] isEqualToString:@""])
            {
                [defaults setInteger:[[[response objectForKey:@"Data"] objectForKey:@"UserId"] integerValue] forKey:@"UserId"];
                [defaults synchronize];
                SpinAndWinLandingPageViewController *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"SpinAndWinLandingPageViewController"];
                vc.dictCampaign = self.dictCampaign;
                [self.navigationController pushViewController:vc animated:YES];
            }
            else
            {
                [CommanMethods displayAlert:alert_spin_and_win_title :[[response objectForKey:@"Data"] valueForKey:@"UserId"]];
            }
        }
        else {
            [CommanMethods displayAlert:@"Error" :error_in_connection];
        }
    }];
}
@end



