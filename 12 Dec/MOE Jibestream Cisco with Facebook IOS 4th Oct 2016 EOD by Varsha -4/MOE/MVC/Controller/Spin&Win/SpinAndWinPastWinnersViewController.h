//
//  SpinAndWinPastWinnersViewController.h
//  MOE
//
//  Created by Neosoft on 6/22/15.
//  Copyright (c) 2015 Neosoft. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "PastWinnersTableViewCell.h"

@interface SpinAndWinPastWinnersViewController : UIViewController <UITableViewDataSource, UITableViewDelegate>

@property (weak, nonatomic) IBOutlet UILabel *lblHeader;
@property (weak, nonatomic) IBOutlet UITableView *tableView;

@property (nonatomic) NSMutableDictionary *dict;
@property (nonatomic) NSInteger numberOfPastWinners;
@property (nonatomic) int campaignId;

@end
