//
//  SpinAndWinLandingPageViewController.m
//  MOE
//
//  Created by Neosoft on 6/22/15.
//  Copyright (c) 2015 Neosoft. All rights reserved.
//

#import "SpinAndWinLandingPageViewController.h"
#import "SpinAndWinTnCViewController.h"
#import "SpinAndWinPlayNowViewController.h"
#import "SpinAndWinTheContestViewController.h"
#import "SpinAndWinDatesOfDrawViewController.h"
#import "SpinAndWinPastWinnersViewController.h"

#define actionSheetView_height SCREEN_HEIGHT*0.4

#define SEL_CHECK [UIImage imageNamed:@"spin_checked"]
#define SEL_UNCHECK [UIImage imageNamed:@"spin_uncheck"]
#define IMAGE_BANNER_PLACEHOLDER [UIImage imageNamed:@"spin-selectContest-img-banner-placeholder"]

@interface SpinAndWinLandingPageViewController ()
@end

@implementation SpinAndWinLandingPageViewController {
    
    NSInteger NumberOfCampaign;
    NSMutableArray *arrCampaignTitle;
    NSString *strCampaignTitle;
    NSString *strCampaignDesc;
    NSString *strCampaignSummary;
    UIView *actionSheetView;
    NSDictionary *dict;
    int intCampaignId;
    NSString *strTnC;
    BOOL isTnCAccepted;
    UIImage *img_checked;
    UIImage *img_unchecked;
}

@synthesize dictCampaign;

#pragma mark - view lifecycle
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
  [CommanMethods getBgImage:self.view];
    [GoogleAnalyticsHandler sendScreenName:screenName_SpinWin];

 
     self.navigationItem.titleView = [CommanMethods getLabel:[SPIN_AND_WIN_TITLE uppercaseString]];

    [self designFields];

    self.imgContest.image = IMAGE_BANNER_PLACEHOLDER;
    self.imgContest.contentMode = UIViewContentModeScaleToFill;
    
    UITapGestureRecognizer *tapToViewTnC = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapToViewTnCMethod:)];
    [self.lblTnC addGestureRecognizer:tapToViewTnC];
    
    self.lblHeader.adjustsFontSizeToFitWidth = YES;
    self.lblTnC.adjustsFontSizeToFitWidth = YES;
    [self setupArrayCampaign];
    strCampaignTitle = [arrCampaignTitle objectAtIndex:0];
    strCampaignSummary = [[[dict objectForKey:@"Data"] objectAtIndex:[arrCampaignTitle indexOfObject:strCampaignTitle]] objectForKey:@"summary"];
    strTnC = [[[dict objectForKey:@"Data"] objectAtIndex:[arrCampaignTitle indexOfObject:strCampaignTitle]] objectForKey:@"terms_conditions"];
    intCampaignId = [[[[dict objectForKey:@"Data"] objectAtIndex:[arrCampaignTitle indexOfObject:strCampaignTitle]] objectForKey:@"CampaignId"] intValue];
    NSURL *url = [[[dict objectForKey:@"Data"] objectAtIndex:[arrCampaignTitle indexOfObject:strCampaignTitle]] objectForKey:@"CampaignThmb"];
    [self.btnContest setTitle:strCampaignTitle forState:UIControlStateNormal];
    self.lblTitle.text = strCampaignSummary;
    self.lblTitle.adjustsFontSizeToFitWidth = YES;
    [self setBannerImage:url];
    
    self.btnContest.imageEdgeInsets = UIEdgeInsetsMake(0, self.btnContest.frame.size.width, 0, 0);

    CGFloat bottomHeightConstraint = 0.0;
    CGFloat belowTnCViewSubviewsHeightConstraint = 0.0;
    CGFloat aboveTnCViewSubviewsHeightConstraint = 0.0;
    CGFloat btnContestViewSubviewsHeightConstraint = 0.0;
    CGFloat btnViewHeightConstraint = 0.0;
    
    if(IS_IPHONE_5) {
        bottomHeightConstraint = 35.0;
        belowTnCViewSubviewsHeightConstraint = 15.0;
        aboveTnCViewSubviewsHeightConstraint = 5.0;
        btnContestViewSubviewsHeightConstraint = 10.0;
    }
    if(IS_IPHONE_6) {
        bottomHeightConstraint = 60.0;
        belowTnCViewSubviewsHeightConstraint = 25.0;
        aboveTnCViewSubviewsHeightConstraint = 10.0;
        btnContestViewSubviewsHeightConstraint = 20.0;
    }
    if(IS_IPHONE_6P) {
        bottomHeightConstraint = 60.0;
        belowTnCViewSubviewsHeightConstraint = 40.0;
        aboveTnCViewSubviewsHeightConstraint = 5.0;
        btnContestViewSubviewsHeightConstraint = 30.0;
        btnViewHeightConstraint = 20.0;
    }
    
    [self adjustConstraints:bottomHeightConstraint :aboveTnCViewSubviewsHeightConstraint :belowTnCViewSubviewsHeightConstraint :btnContestViewSubviewsHeightConstraint :btnViewHeightConstraint];

    [CommanMethods displayAlert:alert_spin_and_win_title :alert_spin_per_day_3_chances];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:YES];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)adjustConstraints :(CGFloat)bottomH :(CGFloat)aboveT :(CGFloat)belowT :(CGFloat)btnContest :(CGFloat)btnGroup
{
    self.bottomViewHeightConstraint.constant = self.bottomViewHeightConstraint.constant + bottomH;
    self.aboveTnCViewHeightConstraint.constant = self.aboveTnCViewHeightConstraint.constant + aboveT;
    self.belowTnCViewHeightConstraint.constant = self.belowTnCViewHeightConstraint.constant + belowT;
    self.btnContestHeightConstraint.constant = self.btnContestHeightConstraint.constant + btnContest;
    self.btnViewHeightConstraint.constant = self.btnViewHeightConstraint.constant + btnGroup;

}

#pragma mark - button method

- (IBAction)btnContestMethod:(id)sender {
    
    if(dictCampaign == nil)
        return;
    if (!actionSheetView)
    {
        [self.view endEditing:YES];
        [self setupArrayCampaign];
        [self showPickerWithCampaignTitle];
    }
}

- (IBAction)btnAcceptTnCMethod:(id)sender {
    
    if(isTnCAccepted) {
        [self.btnTnC setBackgroundImage:img_unchecked forState:UIControlStateNormal];
        isTnCAccepted = NO;
    }
    else {
        [self.btnTnC setBackgroundImage:img_checked forState:UIControlStateNormal];
        isTnCAccepted = YES;
    }
}

- (IBAction)btnPlayNowMethod:(id)sender {
    
    if(intCampaignId == 0) {
        [CommanMethods displayAlert:alert_spin_and_win_title :error_spin_no_contest_selected];
    }
    else if(!isTnCAccepted) {
        [CommanMethods displayAlert:alert_spin_and_win_title :error_spin_termsAndConditions_not_accepted];
    }
    else {
        SpinAndWinPlayNowViewController *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"SpinAndWinPlayNowViewController"];
        vc.strTitle = strCampaignTitle;
        vc.intCampaignId = intCampaignId;
        [self.navigationController pushViewController:vc animated:YES];
    }
}

- (IBAction)btnContestDetailMethod:(id)sender {
    
    if(intCampaignId == 0) {
        [CommanMethods displayAlert:alert_spin_and_win_title :error_spin_no_contest_selected];
    }
    else {
        SpinAndWinTheContestViewController *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"SpinAndWinTheContestViewController"];
        vc.strDetail = [[[dict objectForKey:@"Data"] objectAtIndex:[arrCampaignTitle indexOfObject:strCampaignTitle]] objectForKey:@"campaignDetailDesc"];
        [self.navigationController pushViewController:vc animated:YES];
    }
}

- (IBAction)btnDrawDateMethod:(id)sender {
    
    if(intCampaignId == 0) {
        [CommanMethods displayAlert:alert_spin_and_win_title :error_spin_no_contest_selected];
    }
    else {
        [APP_DELEGATE showHud:self.view Title:nil];
        
        [[WebServiceCommunicator sharedInstance] spinNwin_getDrawDates:Service_SpinAndWin_getDrawDates ResultRespose:Service_SpinAndWin_getDrawDates_Response_key ResultValKey:Service_SpinAndWin_getDrawDates_Result_key CampaignId:intCampaignId Block:^(id response, NSError *error)
         {
             [APP_DELEGATE closeHud];
             NSMutableDictionary *dictDrawDate = [[NSMutableDictionary alloc] initWithDictionary:response];
             if([[dictDrawDate objectForKey:@"Data"] isKindOfClass:[NSArray class]])
             {
                 NSArray *arr = [dictDrawDate objectForKey:@"Data"];
                 if([arr count] == 0)
                 {
                     [CommanMethods displayAlert:alert_spin_and_win_title :error_spin_no_draw_date];
                 }
                 else
                 {
                     SpinAndWinDatesOfDrawViewController *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"SpinAndWinDatesOfDrawViewController"];
                     vc.numberOfDraws = 5;
                     vc.campaignId = intCampaignId;
                     vc.dict = dictDrawDate;
                     [self.navigationController pushViewController:vc animated:YES];
                 }
             }
             else
             {
                 [CommanMethods displayAlert:@"Error" :error_in_connection];
             }
         }];
    }
}

- (IBAction)btnPastWinnerMethod:(id)sender {

    if(intCampaignId == 0) {
        [CommanMethods displayAlert:alert_spin_and_win_title :error_spin_no_contest_selected];
    }
    else {
        [APP_DELEGATE showHud:self.view Title:nil];
        
        [[WebServiceCommunicator sharedInstance] spinNwin_getPastWinners:Service_SpinAndWin_getPastWinners ResultRespose:Service_SpinAndWin_getPastWinners_Response_key ResultValKey:Service_SpinAndWin_getPastWinners_Result_key CampaignId:intCampaignId Block:^(id response, NSError *error)
         {
             [APP_DELEGATE closeHud];
             NSMutableDictionary *dictPastWinners = [[NSMutableDictionary alloc] initWithDictionary:response];
             if([[dictPastWinners objectForKey:@"Data"] isKindOfClass:[NSArray class]])
             {
                 NSArray *arr = [dictPastWinners objectForKey:@"Data"];
                 if([arr count] == 0)
                 {
                     [CommanMethods displayAlert:alert_spin_and_win_title :error_spin_no_past_winners];
                 }
                 else
                 {
                     SpinAndWinPastWinnersViewController *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"SpinAndWinPastWinnersViewController"];
                     vc.numberOfPastWinners = [arr count];
                     vc.campaignId = intCampaignId;
                     vc.dict = dictPastWinners;
                     [self.navigationController pushViewController:vc animated:YES];
                 }
             }
             else
             {
                 [CommanMethods displayAlert:@"Error" :error_in_connection];
             }
         }];
    }
}

#pragma mark tap gesture method
- (void)tapToViewTnCMethod:(UITapGestureRecognizer *)gesture {
    if(intCampaignId == 0) {
        [CommanMethods displayAlert:alert_spin_and_win_title :error_spin_no_contest_selected];
    }
    else {
        SpinAndWinTnCViewController *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"SpinAndWinTnCViewController"];
        vc.strTnC = strTnC;
        [self.navigationController pushViewController:vc animated:YES];
    }
}

#pragma mark picker action
- (void)btnActinCancelClicked {
    
    [UIView animateWithDuration:0.3 animations:^{
        actionSheetView.frame = CGRectMake(0, SCREEN_HEIGHT, SCREEN_WIDTH, actionSheetView_height);
    }completion:^(BOOL finished) {
        if(finished) {
            [self deallocActionSheetView];
        }
    }];
}

- (void)btnActinDoneClicked {
    
    strCampaignSummary = [[[dict objectForKey:@"Data"] objectAtIndex:[arrCampaignTitle indexOfObject:strCampaignTitle]] objectForKey:@"summary"];
    strTnC = [[[dict objectForKey:@"Data"] objectAtIndex:[arrCampaignTitle indexOfObject:strCampaignTitle]] objectForKey:@"terms_conditions"];
    intCampaignId = [[[[dict objectForKey:@"Data"] objectAtIndex:[arrCampaignTitle indexOfObject:strCampaignTitle]] objectForKey:@"CampaignId"] intValue];
    NSURL *url = [[[dict objectForKey:@"Data"] objectAtIndex:[arrCampaignTitle indexOfObject:strCampaignTitle]] objectForKey:@"CampaignThmb"];
    
    [UIView animateWithDuration:0.3 animations:^{
        actionSheetView.frame = CGRectMake(0, SCREEN_HEIGHT, SCREEN_WIDTH, actionSheetView_height);
    }completion:^(BOOL finished) {
        if(finished) {
            [self.btnContest setTitle:strCampaignTitle forState:UIControlStateNormal];
            self.lblTitle.text = strCampaignSummary;
            self.lblTitle.adjustsFontSizeToFitWidth = YES;
            [self setBannerImage:url];
            [self.btnTnC setBackgroundImage:img_unchecked forState:UIControlStateNormal];
            isTnCAccepted = NO;
            [self deallocActionSheetView];
        }
    }];
}

#pragma mark - design fields
- (void)designFields {
    
    //Labels
    self.lblHeader.font = [CommanMethods getDateFont];
    self.lblTitle.font = [CommanMethods getDetailsFont];
    self.lblTnC.font = [CommanMethods getDateFont];
    //Buttons
    self.btnContest.titleLabel.font = [CommanMethods getHeaderFont];
    self.btnPlayNow.titleLabel.font = [CommanMethods getHeaderFont];
    self.btnHowToPlay.titleLabel.font = [CommanMethods getHeaderFont];
    self.btnDatesOfDraw.titleLabel.font = [CommanMethods getHeaderFont];
    self.btnPastWinners.titleLabel.font = [CommanMethods getHeaderFont];
    //Text
    self.lblTitle.text = @"";
    NSMutableAttributedString *attributeString = [[NSMutableAttributedString alloc] initWithString:self.lblTnC.text];
    [attributeString addAttribute:NSUnderlineStyleAttributeName
                            value:[NSNumber numberWithInt:1]
                            range:(NSRange){0,[attributeString length]}];
    self.lblTnC.attributedText = attributeString;
    intCampaignId = 0;
    img_checked = SEL_CHECK;
    img_unchecked = SEL_UNCHECK;
    [self.btnTnC setBackgroundImage:img_unchecked forState:UIControlStateNormal];
    isTnCAccepted = NO;
    
    self.lblTnC.userInteractionEnabled = YES;

}

#pragma mark - setup array
- (void)setupArrayCampaign {
    
    dict = [[NSDictionary alloc] initWithDictionary:dictCampaign];
    [defaults setInteger:[[dictCampaign objectForKey:@"Data"] count]  forKey:@"NumberOfCampaign"];
    [defaults synchronize];
    NumberOfCampaign = [defaults integerForKey:@"NumberOfCampaign"];
    arrCampaignTitle = nil;
    arrCampaignTitle = [[NSMutableArray alloc] init];
    for(int i = 0; i<NumberOfCampaign; i++) {
        [arrCampaignTitle addObject:[[[dictCampaign objectForKey:@"Data"] objectAtIndex:i] objectForKey:@"CampaignTitle"]];
    }
}

#pragma mark - set image
- (void)setBannerImage:(NSURL *)url
{
    if(url)
    {
        [self.imgContest sd_setImageWithURL:url placeholderImage:IMAGE_BANNER_PLACEHOLDER];
    }
    else
    {
        [self.imgContest setImage:IMAGE_BANNER_PLACEHOLDER];
    }
}

#pragma mark - picker methods/delegates

- (void)showPickerWithCampaignTitle {
    
    actionSheetView = [[UIView alloc] initWithFrame:CGRectMake(0, SCREEN_HEIGHT, SCREEN_WIDTH, actionSheetView_height)];
    actionSheetView.backgroundColor = [CommanMethods getTextColor];
    [self.view addSubview:actionSheetView];
    
    UIPickerView *picker = [[UIPickerView alloc] initWithFrame:CGRectMake(0, actionSheetView.frame.size.height*0.3, SCREEN_WIDTH, SCREEN_HEIGHT*0.2)];
    picker.delegate = self;
    picker.dataSource = self;
    picker.showsSelectionIndicator = YES;
    [actionSheetView addSubview:picker];
    
    UIToolbar *tools=[[UIToolbar alloc]initWithFrame:CGRectMake(0, 0,SCREEN_WIDTH,40)];
    tools.barStyle = UIBarStyleBlack;
    [actionSheetView addSubview:tools];
    
    UIBarButtonItem *flexSpace= [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
    UIBarButtonItem *cancelButton=[[UIBarButtonItem alloc]initWithTitle:@"Cancel" style:UIBarButtonItemStylePlain target:self action:@selector(btnActinCancelClicked)];
    cancelButton.tintColor=[CommanMethods getTextColor];

    UIBarButtonItem *doneButton=[[UIBarButtonItem alloc]initWithTitle:@"Done" style:UIBarButtonItemStyleDone target:self action:@selector(btnActinDoneClicked)];
    doneButton.tintColor=[CommanMethods getTextColor];

    NSArray *array = [[NSArray alloc]initWithObjects: cancelButton, flexSpace, doneButton,nil];
    [tools setItems:array];
    
    //picker title
    UILabel *lblPickerTitle=[[UILabel alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT*0.3)];
    lblPickerTitle.text=@"Select Contest";
    lblPickerTitle.backgroundColor=[UIColor clearColor];
    lblPickerTitle.textColor=[CommanMethods getTextColor];
    lblPickerTitle.textAlignment=NSTextAlignmentCenter;
    lblPickerTitle.font=[CommanMethods getHeaderFont];
    [tools addSubview:lblPickerTitle];
    
    [UIView animateWithDuration:0.3 animations:^{
        actionSheetView.frame = CGRectMake(0, SCREEN_HEIGHT*0.6, SCREEN_WIDTH, actionSheetView_height);
    }];
}


- (void)deallocActionSheetView {
    
    [actionSheetView removeFromSuperview];
    actionSheetView = nil;
}
//Columns in picker views

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView {
    return 1;
}
//Rows in each Column

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component {
    return [arrCampaignTitle count];
}

//UIPickerViewDelegate
// these methods return either a plain NSString, a NSAttributedString, or a view (e.g UILabel) to display the row for the component.
-(NSString*) pickerView:(UIPickerView*)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    strCampaignTitle = [arrCampaignTitle objectAtIndex:0];
    return [arrCampaignTitle objectAtIndex:row];
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
    //Write the required logic here that should happen after you select a row in Picker View.
    strCampaignTitle = [arrCampaignTitle objectAtIndex:row];
}
- (UIView *)pickerView:(UIPickerView *)pickerView viewForRow:(NSInteger)row forComponent:(NSInteger)component reusingView:(UIView *)view
{
    UILabel *lblTitle;
    if(!view)
    {
        view=[[UIView alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 44)];
        lblTitle=[[UILabel alloc]initWithFrame:view.frame];
        lblTitle.backgroundColor=[UIColor clearColor];
        lblTitle.tag=111;
        [view addSubview:lblTitle];
    }
    
    lblTitle=(UILabel *)[view viewWithTag:111];
    lblTitle.textAlignment=NSTextAlignmentCenter;
    lblTitle.font=[CommanMethods getHeaderFont];
    view.backgroundColor=[UIColor clearColor];
    NSString *title;
    title = [arrCampaignTitle objectAtIndex:row];
    lblTitle.text=title;
    return  view;
}
@end










