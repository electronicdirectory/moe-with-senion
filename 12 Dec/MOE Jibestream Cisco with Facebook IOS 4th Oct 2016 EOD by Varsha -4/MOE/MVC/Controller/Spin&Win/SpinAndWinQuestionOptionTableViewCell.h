//
//  SpinAndWinQuestionTableViewCell.h
//  MOE
//
//  Created by Neosoft on 6/24/15.
//  Copyright (c) 2015 Neosoft. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SpinAndWinQuestionOptionTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIView *baseView;
@property (weak, nonatomic) IBOutlet UIImageView *imgOption;
@property (weak, nonatomic) IBOutlet UILabel *lblOption;
@property (weak, nonatomic) IBOutlet UIView *seperatorView;

@property (nonatomic) int optionId;
@end
