//
//  SpinAndWinDatesOfDrawViewController.m
//  MOE
//
//  Created by Neosoft on 6/22/15.
//  Copyright (c) 2015 Neosoft. All rights reserved.
//

#import "SpinAndWinDatesOfDrawViewController.h"
#import "SpinAndWinDrawDateTableViewCell.h"

@interface SpinAndWinDatesOfDrawViewController ()

@end

@implementation SpinAndWinDatesOfDrawViewController {
    
    NSMutableArray *arrDrawDate;
}

#pragma mark - synthesize
@synthesize dict;

#pragma mark - view lifecycle
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
  [CommanMethods getBgImage:self.view];

    self.navigationItem.titleView=[CommanMethods getLabel:SPIN_AND_WIN_TITLE];


    self.tableView.dataSource = self;
    self.tableView.delegate = self;

    self.lblHeader.font = [CommanMethods getHeaderFont];
    
    NSArray *arrDrawDate_key = [[NSArray alloc] initWithObjects:@"drawDate1", @"drawDate2", @"drawDate3", @"drawDate4", @"drawDate5", nil];
    NSArray *arrDrawDesc_key = [[NSArray alloc] initWithObjects:@"drawDesc1", @"drawDesc2", @"drawDesc3", @"drawDesc4", @"drawDesc5", nil];
    
    arrDrawDate = [[NSMutableArray alloc] init];

    [self.tableView registerClass:[SpinAndWinDrawDateTableViewCell class] forCellReuseIdentifier:@"SpinAndWinDrawDateTableViewCell"];
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    
    NSArray *arr = [dict objectForKey:@"Data"];
    for(NSDictionary *dic in arr) {
        for(int i=0; i<5; i++) {
            NSDictionary *dictData=[NSDictionary dictionaryWithObjectsAndKeys:[dic objectForKey:[arrDrawDate_key objectAtIndex:i]],@"DrawDate",[dic objectForKey:[arrDrawDesc_key objectAtIndex:i]],@"DrawDesc", nil];
            [arrDrawDate addObject:dictData];
        }
    }
    [self.tableView reloadData];

}

#pragma mark - memory warning
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - table view delegate/data source
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [arrDrawDate count];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 110;
}


- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section {
    return [[UIView alloc] init];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    [tableView registerNib:[UINib nibWithNibName:@"SpinAndWinDrawDateTableViewCell" bundle:[NSBundle mainBundle]] forCellReuseIdentifier:@"SpinAndWinDrawDateTableViewCell"];
    
    NSDictionary *dictData=[arrDrawDate objectAtIndex:indexPath.row];

    SpinAndWinDrawDateTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"SpinAndWinDrawDateTableViewCell"];
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.seperatorView.backgroundColor = [UIColor clearColor];
    cell.backgroundColor = [UIColor clearColor];

    if([[dictData objectForKey:@"DrawDesc"] isEqualToString:@""]) {
        cell.lblContest.text = @"";
        cell.lblDate.text = @"";
        cell.lblContestTitle.text = @"";
        cell.lblDateTitle.text = @"";
        cell.baseView.backgroundColor = [UIColor clearColor];
        return cell;
    }

    cell.baseView.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"draw-list-bg"]];
    cell.lblContest.text = [dictData objectForKey:@"DrawDesc"];
    cell.lblDate.text = [CommanMethods changeDateFormatWithTimeZone:[dictData objectForKey:@"DrawDate"]];
    cell.lblContestTitle.font = [CommanMethods getDetailsFont];
    cell.lblDateTitle.font = [CommanMethods getDetailsFont];
    cell.lblContest.font = [CommanMethods getHeaderFont];
    cell.lblDate.font = [CommanMethods getHeaderFont];
    cell.lblContest.adjustsFontSizeToFitWidth = YES;
    return cell;
}


@end
