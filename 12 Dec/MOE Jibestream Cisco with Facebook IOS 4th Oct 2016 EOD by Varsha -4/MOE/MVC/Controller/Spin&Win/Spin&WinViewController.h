//
//  Spin&WinViewController.h
//  MOE
//
//  Created by Neosoft on 6/18/15.
//  Copyright (c) 2015 Neosoft. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface Spin_WinViewController : UIViewController

@property (nonatomic) NSDictionary *dictCampaign;

@property (weak, nonatomic) IBOutlet TPKeyboardAvoidingScrollView *scrollView;
@property (weak, nonatomic) IBOutlet UIImageView *imgBanner;
@property (weak, nonatomic) IBOutlet UILabel *lblHello;
@property (weak, nonatomic) IBOutlet UITextField *txtEmail;
@property (weak, nonatomic) IBOutlet UITextField *txtPassword;
@property (weak, nonatomic) IBOutlet UIButton *btnLogin;
@property (weak, nonatomic) IBOutlet UIButton *btnNewUser;
@property (weak, nonatomic) IBOutlet UIButton *btnForgotPass;


- (IBAction)btnLoginMethod:(id)sender;
- (IBAction)btnForgotDetailsMethod:(id)sender;
- (IBAction)btnNewUserMethod:(id)sender;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *topViewHeightConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *spinViewHeightConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *bottomViewHeightConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *fieldEmailHeightConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *fieldPasswordHeightConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *btnLoginHeightConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *viewBtnHeightConstraint;

@end
