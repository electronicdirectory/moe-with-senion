//
//  SplashViewController.h
//  MOE
//
//  Created by Nivrutti on 6/15/15.
//  Copyright (c) 2015 Neosoft. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SplashViewController : UIViewController
@property (strong, nonatomic) IBOutlet UILabel *lblShowDownloding;
@property (strong, nonatomic) IBOutlet UIImageView *imgViewForBG;
@property (strong, nonatomic) IBOutlet UIProgressView *progressView;
@end
