//
//  CustomInfoView.m
//  MOE
//
//  Created by Nivrutti on 8/17/15.
//  Copyright (c) 2015 Neosoft. All rights reserved.
//

#import "CustomInfoView.h"

@interface CustomInfoView ()
@property(nonatomic,strong)UIImageView *imgView;
@end
@implementation CustomInfoView

- (id)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self)
    {
        self.backgroundColor = [UIColor clearColor];
        self.alpha=1.0;
    }
    [self addImageViewInBackgound];
    return self;
}
-(void)addImageViewInBackgound
{
    CGRect rect=self.frame;
    self.imgView=[[UIImageView alloc]initWithFrame:rect];
    self.imgView.contentMode=UIViewContentModeScaleToFill;
    [self addSubview:self.imgView];
    
    UIButton *btnSkip=[[UIButton alloc]initWithFrame:CGRectMake(self.imgView.frame.size.width-100, 0, 100, 100)];
    [btnSkip setTitle:@"" forState:UIControlStateNormal];
    [btnSkip setTitle:@"" forState:UIControlStateHighlighted];
    [btnSkip setTitle:@"" forState:UIControlStateSelected];
    [btnSkip addTarget:self action:@selector(close) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:btnSkip];
}
-(void)setStrImageName:(NSString *)strImageName
{
    self.imgView.image=[UIImage imageNamed:strImageName];
}
-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
   // [self removeFromSuperview];
}
-(void)close
{
    [self removeFromSuperview];
}
@end
