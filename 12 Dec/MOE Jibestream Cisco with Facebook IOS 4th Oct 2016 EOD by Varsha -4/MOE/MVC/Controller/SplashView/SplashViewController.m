//
//  SplashViewController.m
//  MOE
//
//  Created by Nivrutti on 6/15/15.
//  Copyright (c) 2015 Neosoft. All rights reserved.
//

#import "SplashViewController.h"
#import "SearchViewController.h"
#import "DEMOLeftMenuViewController.h"
#import "ManageNotificationsViewController.h"
@interface SplashViewController ()
{
    NSUInteger _totalNumberOfOperations;
    NSUInteger _numberOfFinishedOperations;
    
}
@end
@implementation SplashViewController
-(void)viewDidLoad
{
    [super viewDidLoad];
    [UIApplication sharedApplication].statusBarStyle=UIStatusBarStyleLightContent;
    
    [GoogleAnalyticsHandler sendScreenName:screenName_SplashView];
    self.progressView.progress = 0.01;
    self.progressView.transform = CGAffineTransformMakeScale(1.0f,3.50f);
    self.progressView.hidden=YES;
    
    self.navigationController.navigationBar.hidden=YES;
    self.navigationController.navigationController.interactivePopGestureRecognizer.enabled = NO;
    //get SiteId from server
    BOOL internet=[[AppDelegate appDelegateInstance]CheckNetWorkConnection];
    if(internet==NO)
    {
        if(![AppDelegate appDelegateInstance].strSiteID)
        {
            [AppDelegate appDelegateInstance].strSiteID=DEFAULT_SITE_ID;
        }
        [self hideLabels];
        [[AppDelegate appDelegateInstance]PlayProgressHudAsPerType:error_message View:nil];
    }
    else
    {
        [DBOpreation deleteDataFromlocal];
        [[WebServiceCommunicator sharedInstance]getSiteID:^(id response, NSError *error)
         {
             NSUserDefaults *pref = [NSUserDefaults standardUserDefaults];
             
             if([response isKindOfClass:[NSDictionary class]])
             {
                 if([[response objectForKey:@"MallID"]isKindOfClass:[NSString class]])
                 {
                     [AppDelegate appDelegateInstance].strSiteID=[DBOpreation validateText:[NSString stringWithFormat:@"%@",[response objectForKey:@"MallID"]]];
                 }
                 if([[response objectForKey:@"FeedbackURL"]isKindOfClass:[NSString class]])
                 {
                     NSString *strURL=[DBOpreation validateText:[response objectForKey:@"FeedbackURL"]];
                     strURL=[strURL stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding ];
                     [AppDelegate appDelegateInstance].strFeedback=strURL;
                 }
                 if([[response objectForKey:@"WhatsApp"]isKindOfClass:[NSString class]])
                 {
                     [AppDelegate appDelegateInstance].strWhatsApp=[DBOpreation validateText:[response objectForKey:@"WhatsApp"]];
                 }
             }
             
             if(![AppDelegate appDelegateInstance].strFeedback)
             {
                 
                 [AppDelegate appDelegateInstance].strFeedback=[CommanMethods getFeedBackURL];
             }
             if(![AppDelegate appDelegateInstance].strSiteID)
             {
                 [AppDelegate appDelegateInstance].strSiteID=DEFAULT_SITE_ID;
             }
             //call Listing services
             [[WebServiceCommunicator sharedInstance]getListingDataFromServerUsingQueue:^(NSUInteger numberOfFinishedOperations, NSUInteger totalNumberOfOperations)
              {
                  _totalNumberOfOperations=(NSUInteger)(totalNumberOfOperations+3);
                  _numberOfFinishedOperations=(NSUInteger)(numberOfFinishedOperations);
                  
                  [self DisplayProgress];
              } Completion:^{
                  [pref synchronize];
                  if([pref objectForKey:device_token]!=nil)
                  {
                  }
                  else
                  {
                      [pref setObject:USER_NOTIFICATION_TOKEN forKey:device_token];
                  }
                  [pref synchronize];
                  //register Device service Queue
                  [[WebServiceCommunicator sharedInstance]RegisterDeviceFromServerUsingQueue:^(NSUInteger numberOfFinishedOperations, NSUInteger totalNumberOfOperations)
                   {
                       _numberOfFinishedOperations+=(NSUInteger)(numberOfFinishedOperations);
                       [self DisplayProgress];
                   } Completion:^{
                       [DBOpreation   copyAndCreateSearchData];
                       [self completionProgress];
                   }];
              }];
         }];
    }
    // Do any additional setup after loading the view.
}
-(void)completionProgress
{
    [self hideLabels];
}
-(void)DisplayProgress
{
    float percentage=((float) _numberOfFinishedOperations / _totalNumberOfOperations);
    self.progressView.progress=percentage;
    NSString *strVal=[NSString stringWithFormat:@"%0.0f %%",percentage*100];
    self.lblShowDownloding.text=strVal;
    if(percentage>0.01)
    {
        self.progressView.hidden=NO;
    }
}
-(void)hideLabels
{
    [UIApplication sharedApplication].statusBarStyle=UIStatusBarStyleLightContent;
    
    self.progressView.hidden=YES;
    self.lblShowDownloding.hidden=YES;
    [defaults synchronize];
    if ([defaults boolForKey:HasLaunchedOnce])
    {
        // app already launched
        [[AppDelegate appDelegateInstance]setupRootView];
    }
    else
    {
        [defaults setBool:YES forKey:NOTIFICATION_FASHION_LADIES];
        [defaults setBool:YES forKey:NOTIFICATION_FASHION_MEN];
        [defaults setBool:YES forKey:NOTIFICATION_DINNING];
        [defaults setBool:YES forKey:NOTIFICATION_OFFERS];
        [defaults setBool:YES forKey:NOTIFICATION_EVENTS];
        [defaults setBool:YES forKey:NOTIFICATION_WEEKLY_1_6ALERTS];
        [defaults setBool:YES forKey:NOTIFICATION_WEEKLY_6_12ALERTS];
        [defaults setObject:[NSNumber numberWithInt:-1] forKey:NOTIFICATION_IS_MR];
        [defaults setObject:@"null" forKey:NOTIFICATION_USER_NAME];
        [defaults setObject:@"null" forKey:NOTIFICATION_USER_EMAIL];
        [defaults setObject:@"null" forKey:NOTIFICATION_USER_MOBILE];
        [defaults synchronize];
        ManageNotificationsViewController *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"ManageNotificationsViewController"];
        [self.navigationController pushViewController:vc animated:NO];
    }
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
@end
