//
//  MyFavTableViewController.m
//  MOE
//
//  Created by webwerks on 6/27/15.
//  Copyright (c) 2015 Neosoft. All rights reserved.
//

#import "MyFavViewController.h"
#import "SearchViewCell.h"
#import "DBOpreation.h"
#import "offerDetailViewController.h"
#import "ShoppingDetailsTableViewController.h"
#import "FavouriteListModel.h"
@interface MyFavViewController ()
@property(nonatomic,strong)UIImage *imgEvent,*imgServices,*imgOffer,*imgHotels,*imgDine,*imgEnt,*imgshop;

@end

@implementation MyFavViewController
@synthesize  imgEvent,imgServices,imgOffer,imgHotels,imgDine,imgEnt,imgshop;

- (void)viewDidLoad
{
    [super viewDidLoad];
    [GoogleAnalyticsHandler sendScreenName:screenName_Favorites];
    
    arrResult=[[NSMutableArray alloc]init];
    UIBarButtonItem *backBtn=[UIBarButtonItem new];
    self.navigationItem.backBarButtonItem=backBtn;
    self.navigationItem.titleView = [CommanMethods getLabel:[screenHeader_Favorites uppercaseString]];
    self.lblPlaceHolder.font=[CommanMethods getNavigationTitleFont];
    
    imgEvent=[UIImage imageNamed:@"search-icon-event6p"];
    imgServices=[UIImage imageNamed:@"search-icon-services6p"];
    imgOffer=[UIImage imageNamed:@"search-icon-offer6p"];
    imgHotels=[UIImage imageNamed:@"search-icon-hotlel6p"];
    imgDine=[UIImage imageNamed:@"search-icon-dine6p"];
    imgEnt=[UIImage imageNamed:@"search-icon-entertaint6p"];
    imgshop=[UIImage imageNamed:@"search-icon-shopping6p"];
    [self.tblView registerNib:[UINib nibWithNibName:@"SearchViewCell" bundle:nil] forCellReuseIdentifier:@"SearchViewCell"];
}
-(void)viewWillAppear:(BOOL)animated
{
    [arrResult removeAllObjects];
    [self.tblView reloadData];
    [arrResult  addObjectsFromArray:[DBOpreation FavouriteList]];
    [self.tblView reloadData];
}
- (void)menuMethod {
    [self.sideMenuViewController presentLeftMenuViewController];
}
- (void)search
{
    [[AppDelegate appDelegateInstance]openSearchScreen:self.navigationController];
}
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
#pragma mark - TableView Delegate Method
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if([arrResult count]==0)
    {
        self.lblPlaceHolder.hidden=NO;
        self.lblPlaceHolder.text=APP_NODATA;
        self.lblPlaceHolder.font=[CommanMethods getHeaderFont];
        self.lblPlaceHolder.textColor=[CommanMethods getTextColor];
        [self.view bringSubviewToFront:self.lblPlaceHolder];
        
    }
    else
    {
        self.lblPlaceHolder.hidden=YES;
    }
    return [arrResult count];
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *identifier = @"SearchViewCell";
    SearchViewCell *cell = (SearchViewCell *)[self.tblView dequeueReusableCellWithIdentifier:identifier];
    FavouriteListModel *favModel=[arrResult objectAtIndex:indexPath.row];
    
    cell.lbl.text =[CommanMethods rePlaceTagsWithPlainText:favModel.strTitle];
    cell.lbl.font =[CommanMethods getDetailsFont];
    
    UIImage *img;
    if ([[favModel favType]isEqualToString:APP_DINE])
    {
        img=imgDine;
    }
    else  if ([[favModel favType]isEqualToString:APP_ENTERTAINMENT])
    {
        img=imgEnt;
    }
    else if ([[favModel favType]isEqualToString:APP_SHOP])
    {
        img=imgshop;
    }
    else if ([[favModel favType]isEqualToString:APP_EVENTS])
    {
        img=imgEvent;
    }
    else if ([[favModel favType]isEqualToString:APP_OFFER])
    {
        img=imgOffer;
    }
    else if ([[favModel favType]isEqualToString:APP_SERVICES])
    {
        img=imgServices;
    }
    else if ([[favModel favType]isEqualToString:APP_HOTEL])
    {
        img=imgHotels;
    }
    cell.imageView.image=img;
    cell.imageView.contentMode=UIViewContentModeScaleAspectFit;
    [cell.deleteBtn addTarget:self action:@selector(deleteBtnClicked:) forControlEvents:UIControlEventTouchUpInside];
    cell.selectionStyle=UITableViewCellEditingStyleNone;
    return cell;
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 49;
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    FavouriteListModel *favModel=[arrResult objectAtIndex:indexPath.row];
    if ([[favModel favType]isEqualToString:APP_DINE])
    {
        [self moveToShopDetails:[DBOpreation getDineById:favModel.favID]];
    }
    else  if ([[favModel favType]isEqualToString:APP_ENTERTAINMENT])
    {
        [self moveToShopDetails:[DBOpreation getEntListById:favModel.favID]];
    }
    else if ([[favModel favType]isEqualToString:APP_SHOP])
    {
        [self moveToShopDetails:[DBOpreation getShopById:favModel.favID]];
    }
    else if ([[favModel favType]isEqualToString:APP_EVENTS])
    {
        [self moveToOffer:[DBOpreation getEventById:favModel.favID]];
    }
    else if ([[favModel favType]isEqualToString:APP_OFFER])
    {
        [self moveToOffer:[DBOpreation getOfferData:[favModel.favID stringValue]]];
    }
    else if ([[favModel favType]isEqualToString:APP_HOTEL])
    {
        [self moveToOffer:[DBOpreation getHotelsById:favModel.favID]];
    }
    else if ([[favModel favType]isEqualToString:APP_HOTEL])
    {
    }
}
-(void)moveToShopDetails:(ShopBaseModel *)model
{
    ShoppingDetailsTableViewController   *shop=[self.storyboard instantiateViewControllerWithIdentifier:@"ShoppingDetailsTableViewController"];
    shop.shopBaseModelObj=model;
    [self.navigationController pushViewController:shop animated:YES];
}
-(void)moveToOffer:(BaseModel *)model
{
    offerDetailViewController *offerdetailview = [self.storyboard instantiateViewControllerWithIdentifier:@"offerDetailViewController"];
    offerdetailview.offerInfo=model;
    [self.navigationController pushViewController:offerdetailview animated:YES];
}
-(void)deleteBtnClicked:(id)sender
{
    UIButton *button = (UIButton*)sender;
    CGPoint buttonOrigin = [sender frame].origin;
    CGPoint originInTableView = [self.tblView convertPoint:buttonOrigin fromView:[button superview]];
    NSIndexPath *indexPath = [self.tblView  indexPathForRowAtPoint:originInTableView];
    FavouriteListModel *favModel=[arrResult objectAtIndex:indexPath.row];
    BOOL delete= [DBOpreation deleteFav:favModel.favType ID:favModel.favID];
    if ( delete)
    {
        [arrResult removeObjectAtIndex:indexPath.row];
        [self.tblView beginUpdates];
        [self.tblView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
        [self.tblView endUpdates];
        
    }
}
/******************************************
 Method name - willDisplayCell
 Parameter   - UITableView, NSIndexPath
 Return      - nil
 Desc        - Method Called before row getting displayed in table view (Used for animation)
 ****************************************/
- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    CATransition *transition = [CATransition animation];
    [transition setType:kCATransitionPush];
    [transition setSubtype:kCATransitionFromLeft];
    cell.layer.shadowColor = [[CommanMethods getblackColor]CGColor];
    cell.layer.shadowOffset = CGSizeMake(10, 10);
    cell.alpha = 0;
    CATransform3D rotation;
    rotation = CATransform3DMakeRotation( 0, 0.0, 0, 0);
    rotation.m34 = 1.0/ -600;
    cell.layer.transform = rotation;
    [cell.layer addAnimation:transition forKey:@"rotation1"];
    [UIView beginAnimations:@"rotation" context:NULL];
    [UIView setAnimationDuration:0.3];
    cell.layer.transform = CATransform3DIdentity;
    cell.alpha = 1;
    cell.layer.shadowOffset = CGSizeMake(0, 0);
    [UIView commitAnimations];
}
-(void)dealloc
{
    arrResult=nil;
    self.tblView .delegate=nil;
    self.tblView =nil;
    imgEvent=nil;
    imgServices=nil;
    imgOffer=nil;
    imgHotels=nil;
    imgDine=nil;
    imgEnt=nil;
    imgshop=nil;
    
}
@end
