//
//  MyFavTableViewController.h
//  MOE
//
//  Created by webwerks on 6/27/15.
//  Copyright (c) 2015 Neosoft. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MyFavViewController : UIViewController<UISearchBarDelegate>
{
    UISearchBar *searchBarObj;
    NSMutableArray  *arrResult;
    
}

@property (weak, nonatomic) IBOutlet UITableView *tblView;
@property(nonatomic,weak)IBOutlet UILabel *lblPlaceHolder;

@end
