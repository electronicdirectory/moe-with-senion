
//
//  BaseTableViewController.m

//
//  Created by Nivrutti on 6/24/15.
//  Copyright (c) 2015 Neosoft. All rights reserved.
//
#import "UILabel+Html.h"
#import "BaseTableViewController.h"
#import "OfferTableViewCell.h"
#import "offerDetailViewController.h"
@interface BaseTableViewController ()
{
    UIImage *placeHolderImage;

}
@end
@implementation BaseTableViewController
- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.navigationItem.rightBarButtonItem=[CommanMethods getRightButton:self Action:@selector(search)];
    self.navigationItem.leftBarButtonItem =[CommanMethods getLeftButton:self Action:@selector(menuMethod)];
    
    [self.tableView registerNib:[UINib nibWithNibName:@"OfferTableViewCell" bundle:nil] forCellReuseIdentifier:@"OfferTableViewCell"];
    [self.tableView registerNib:[UINib nibWithNibName:@"HotelTableViewCell" bundle:nil] forCellReuseIdentifier:@"HotelTableViewCell"];
    
    
    
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    if([self.strType  isEqualToString:@"events"])
    {
        placeHolderImage=[UIImage imageNamed:@"place-holder-event1-6p"];
        self.navigationItem.titleView = [CommanMethods getLabel:screenHeader_Events];
        [GoogleAnalyticsHandler sendScreenName:screenName_Events];
        [[AppDelegate appDelegateInstance] showFullScreenCiscoAd:self ScreenName:tag_EventScreen];
    }
    else if([self.strType  isEqualToString:@"offers"])
    {
        self.navigationItem.titleView = [CommanMethods getLabel:screenHeader_Offers];
        placeHolderImage=[UIImage imageNamed:@"place-holder-offer1-6p"];
        [GoogleAnalyticsHandler sendScreenName:screenName_Offer];
        [[AppDelegate appDelegateInstance] showFullScreenCiscoAd:self ScreenName:tag_OfferScreen];
    }
    else
    {
        self.navigationItem.titleView = [CommanMethods getLabel:screenName_Hotel];
        [GoogleAnalyticsHandler sendScreenName:screenName_Hotel];
        placeHolderImage=[UIImage imageNamed:@"place-holder-offer1-6p"];
    }
    
    dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
    dispatch_async(queue, ^{
        if([self.strType  isEqualToString:@"events"])
        {
            self.arrData=[DBOpreation getEvent];
        }
        else if([self.strType  isEqualToString:@"offers"])
        {
            self.arrData=[DBOpreation   getOffer];
        }
        else
        {
            self.arrData=[DBOpreation   getHotels];
        }
        dispatch_async(dispatch_get_main_queue(),^{
            [self.tableView reloadData];});
    });

}
- (void)menuMethod
{
    [self.sideMenuViewController presentLeftMenuViewController];
}
- (void)search
{
    [[AppDelegate appDelegateInstance]openSearchScreen:self.navigationController];
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma mark - Table view data source
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return  117;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    return [self.arrData count];
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *strIdentifier=@"OfferTableViewCell";
    if([self.strType  isEqualToString:@"hotels"])
    {
        strIdentifier=@"HotelTableViewCell";
    }
    OfferTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:strIdentifier forIndexPath:indexPath];
    cell.selectionStyle=UITableViewCellSelectionStyleNone;
    BaseModel *model=[self.arrData objectAtIndex:indexPath.row];
    NSString *strTitle=[CommanMethods rePlaceTagsWithPlainText: model.strTitle];
    cell.lblTitle.text=[strTitle uppercaseString];
    cell.lblTitle.font=[CommanMethods getHeaderFont];
    if([self.strType  isEqualToString:@"hotels"])
    {
        cell.lblDesc.font=[CommanMethods getHomeTextFont];
    }
    else
    {
        cell.lblDesc.font=[CommanMethods getDateFont];
        cell.lblDate.font=[CommanMethods getDateFont];
    }
    NSString *strValue=[CommanMethods rePlaceTagsWithPlainText: model.strDesc];
    cell.lblDesc.text=strValue;
    
//    cell.lblDate.text=[NSString stringWithFormat:@"%@ - %@",model.strStartDate,model.strEndDate];
    
    cell.lblDate.text=[NSString stringWithFormat:@"%@ - %@", [CommanMethods changeDateFormat:model.strStartDate upperCase:NO],[CommanMethods changeDateFormat:model.strEndDate upperCase:NO]];
    
    
    NSURL *url;
    url=[CommanMethods getImageURL:model.strImageURL];
    if(url)
    {
        [cell.imgView sd_setImageWithURL:url placeholderImage:placeHolderImage];
    }
    else
    {
        [cell.imgView setImage:placeHolderImage];
    }
    if ([model.strType isEqual:APP_HOTEL])
    {
        cell.lblDesc.numberOfLines=4;
    }
    else
    {
        cell.lblDesc.numberOfLines=2;
    }
    
    cell.lblTitle.textColor=[CommanMethods getTextColor];
    cell.lblDesc.textColor=[CommanMethods getTextColor];
    cell.lblDate.textColor=[CommanMethods getTextColor];

    return cell;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    offerDetailViewController *offerdetailview = [self.storyboard instantiateViewControllerWithIdentifier:@"offerDetailViewController"];
    offerdetailview.offerInfo=[self.arrData objectAtIndex:indexPath.row];
    [self.navigationController pushViewController:offerdetailview animated:YES];
}

/******************************************
 Method name - willDisplayCell
 Parameter   - UITableView, NSIndexPath
 Return      - nil
 Desc        - Method Called before row getting displayed in table view (Used for animation)
 ****************************************/
- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    __block CGRect frmCell=cell.frame;
     frmCell.origin.x = self.view.frame.size.width;
    cell.frame = frmCell;
    [UIView animateWithDuration:0.2 delay:0.025*indexPath.row options:UIViewAnimationOptionCurveEaseInOut  animations:^{
        frmCell.origin.x = 0;
        cell.frame = frmCell;
        cell.alpha = 1.0f;
    } completion:nil];
}
-(void)dealloc
{
    self.arrData=nil;
}
@end
