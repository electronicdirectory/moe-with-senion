//
//  OfferTableViewCell.h

//
//  Created by Nivrutti on 6/24/15.
//  Copyright (c) 2015 Neosoft. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface OfferTableViewCell : UITableViewCell
@property(nonatomic,weak)IBOutlet UIImageView *imgView;
@property(nonatomic,weak)IBOutlet UILabel *lblTitle;
@property(nonatomic,weak)IBOutlet UILabel *lblDate;
@property(nonatomic,weak)IBOutlet UILabel *lblDesc;
@end
