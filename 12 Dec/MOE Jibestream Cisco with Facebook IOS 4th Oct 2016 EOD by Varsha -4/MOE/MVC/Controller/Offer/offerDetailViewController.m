//
//  offerDetailViewController.m

//
//  Created by webwerks on 6/24/15.
//  Copyright (c) 2015 Neosoft. All rights reserved.
//

#import "offerDetailViewController.h"
#import "UILabel+Html.h"
#import "WebViewController.h"
#import "FavouriteListModel.h"
#import "shopDetailsTableViewCell.h"
#import "offerDetailDescCell.h"
@interface offerDetailViewController ()
{
    BOOL isScrollAdded;

    UIImage *placeHolderImage;
    float imageHeight;
    float webViewHeight;
    BOOL isAnimated,istranfarm;

}
@property(nonatomic,strong)FavouriteListModel *favModel;
@property(nonatomic,strong)NSString *strHtml;
@end
#define INDEX_IMAGE 0
#define INDEX_MIDDLECELL 1
#define INDEX_DESC 2

#define IMAGECELL @"shopDetailsTableViewCell"
#define DESCCELL @"offerDetailDescCell"
#define SEPARATORIMAGE [UIImage imageNamed:@"shop-det-div"]

@implementation offerDetailViewController

@synthesize favModel;

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    if(IS_IPHONE_4_OR_LESS)
        imageHeight = IPHONE4_DETAILS_IMAGE_HEIGHT;
    else if (IS_IPHONE_5)
        imageHeight = IPHONE5_DETAILS_IMAGE_HEIGHT;
    else if (IS_IPHONE_6)
        imageHeight = IPHONE6_DETAILS_IMAGE_HEIGHT;
    else if (IS_IPHONE_6P)
        imageHeight = IPHONE6PLUS_DETAILS_IMAGE_HEIGHT;
    
    webViewHeight=250;
    NSString *strTitle=[CommanMethods rePlaceTags:[self.offerInfo.strTitle uppercaseString]];
    self.navigationItem.titleView = [CommanMethods getLabel:[self.offerInfo.strType uppercaseString]];
    [self.tblView registerNib:[UINib nibWithNibName:IMAGECELL bundle:nil] forCellReuseIdentifier:IMAGECELL];
    [self.tblView registerNib:[UINib nibWithNibName:DESCCELL bundle:nil] forCellReuseIdentifier:DESCCELL];
    istranfarm=YES;
    favModel=[DBOpreation getFav:self.offerInfo.tblId Type:self.offerInfo.strType];
    if([self.offerInfo.strType isEqualToString:APP_OFFER])
    {
        [[WebServiceCommunicator sharedInstance]updateOfferCategoryHits:[[AppDelegate appDelegateInstance].strSiteID intValue] CategoryID:[self.offerInfo.tblCategory_ID intValue]];
    }
    
    NSString *strDate=@" ";
    if (![self.offerInfo.strType isEqualToString:APP_HOTEL])
    {
        strDate=[NSString stringWithFormat:@" %@ - %@ </br>", [CommanMethods changeDateFormat:self.offerInfo.strStartDate upperCase:YES],[CommanMethods changeDateFormat:self.offerInfo.strEndDate upperCase:YES]];
//        strDate = [NSString stringWithFormat:@" %@ - %@ </br>", self.offerInfo.strStartDate, self.offerInfo.strEndDate];
    }
   
     NSString *strDesc;
    NSString *strScreenValue;
    if ([self.offerInfo.strType isEqualToString:APP_HOTEL])
    {
        placeHolderImage=[UIImage imageNamed:@"place-holder-offer1-6p"];
        strScreenValue=[NSString stringWithFormat:@"%@ - %@",screenName_Hotel,self.offerInfo.strTitle];
        strDesc = [CommanMethods rePlaceTags:self.offerInfo.strDesc];
    }
    else if ([self.offerInfo.strType isEqualToString:APP_EVENTS ])
    {
        self.navigationItem.rightBarButtonItem=[CommanMethods getRightShareButton:self Action:@selector(share)];
        placeHolderImage=[UIImage imageNamed:@"place-holder-event1-6p"];
        strScreenValue=[NSString stringWithFormat:@"%@ - %@",screenName_Events,self.offerInfo.strTitle];
        strDesc = [CommanMethods rePlaceTags:self.offerInfo.strDetailsDesc];
     }
    else if ([self.offerInfo.strType isEqualToString:APP_OFFER])
    {
        strDesc = [CommanMethods rePlaceTags:self.offerInfo.strDesc];
        placeHolderImage=[UIImage imageNamed:@"place-holder-offer1-6p"];
        strScreenValue=[NSString stringWithFormat:@"%@ - %@",screenName_Offer,self.offerInfo.strCategory_Name];
    }
  
    self.strHtml=[CommanMethods getHtml:strTitle Date:strDate Desc:strDesc];
     [GoogleAnalyticsHandler sendScreenName:strScreenValue];
}
-(void)viewWillAppear:(BOOL)animated
{
    [self.tblView reloadData];
}
-(void)viewWillDisappear:(BOOL)animated
{
    self.webView.delegate=nil;
    [self.webView removeFromSuperview];
    self.webView=nil;
}
- (void)search
{
    [[AppDelegate appDelegateInstance]openSearchScreen:self.navigationController];
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
-(NSInteger)tableView:(UITableView *)tablView numberOfRowsInSection:(NSInteger)section
{
    return 2;
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return [self heightForCellAtIndexPath:indexPath ];
}
- (CGFloat)heightForCellAtIndexPath:(NSIndexPath * )indexPath
{
    if(indexPath.row==0)
    {
        float height=imageHeight+50;
        if([self.offerInfo.strType isEqualToString:APP_EVENTS])
        {
            height -=50;
        }
        return height;
    }
 
    return webViewHeight;
 }
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(indexPath.row==0)
    {
        shopDetailsTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"shopDetailsTableViewCell" forIndexPath:indexPath];
        [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
        
        if([self.offerInfo.strType isEqualToString:APP_EVENTS])
        {
            cell.bottomView.hidden=YES;
        }
        else   if([self.offerInfo.strType isEqualToString:APP_OFFER])
        {
            cell.bottomView.hidden=YES;
            cell.bottomView1.hidden=NO;
        }
        else
        {
            cell.bottomView1.hidden=YES;
            cell.bottomView.hidden=NO;
        }
        
        if(!isScrollAdded)
        {
            isScrollAdded=YES;
            UIScrollView *scrollView=cell.scrollView;
            float height= cell.scrollView.frame.size.height;
            height=imageHeight;
            cell.scrollViewHeight.constant=height;
            
            float width =self.view.frame.size.width;
            CustomImageView *imageView=[[CustomImageView alloc]initWithFrame:CGRectMake(0,0,self.view.frame.size.width,height)];
            [cell.scrollView addSubview:imageView];
            imageView.backgroundColor=[UIColor clearColor];
            
            NSURL *url = [CommanMethods getImageURL:self.offerInfo.strImageURL];
            if(url)
            {
                [imageView sd_setImageWithURL:url placeholderImage:placeHolderImage];
            }
            else
            {
                [imageView setImage:placeHolderImage];
            }
            imageView.contentMode=UIViewContentModeScaleToFill;
            scrollView.contentSize=CGSizeMake(width,height);
            cell.pageControl.numberOfPages=0;
            [cell.btnCall addTarget:self action:@selector(call) forControlEvents:UIControlEventTouchUpInside];
            [cell.btnFav addTarget:self action:@selector(fav:) forControlEvents:UIControlEventTouchUpInside];
            [cell.btnMap addTarget:self action:@selector(map) forControlEvents:UIControlEventTouchUpInside];
            [cell.btnShare addTarget:self action:@selector(share) forControlEvents:UIControlEventTouchUpInside];
            [cell.btnMap1 addTarget:self action:@selector(map) forControlEvents:UIControlEventTouchUpInside];
            [cell.btnShare1 addTarget:self action:@selector(share) forControlEvents:UIControlEventTouchUpInside];
        }
        if(favModel)
        {
            [cell.imgFav setImage:[UIImage imageNamed:@"ico-fav-active6p"]];
        }
        else
        {
            [cell.imgFav setImage:[UIImage imageNamed:@"shop-whiteFav"]];
        }
        [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
        
        return  cell;
    }
    static offerDetailDescCell *cell=nil;
    cell = (offerDetailDescCell *)[self.tblView dequeueReusableCellWithIdentifier:DESCCELL];
    if(!self.webView)
    {
    
        self.webView=[[UIWebView alloc]initWithFrame:cell.frame];
        [cell.contentView addSubview:self.webView];
        self.webView.dataDetectorTypes=UIDataDetectorTypePhoneNumber|UIDataDetectorTypeLink ;
         self.webView.opaque = NO;
        self.webView.backgroundColor = [UIColor clearColor];
        [self.webView setScalesPageToFit:YES];
        self.webView.scrollView.scrollEnabled=NO;
        [self.webView loadHTMLString:self.strHtml baseURL:nil];
    }
    self.webView.delegate=self;
    return  cell;
}
-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath{
    if(!isAnimated)
    {
        isAnimated=YES;
        istranfarm=YES;
        if (indexPath.row==0)
        {
            shopDetailsTableViewCell *imgcell = (shopDetailsTableViewCell *)cell;
            imgcell.scrollView.transform = CGAffineTransformMakeScale(0.5, 0.5);
            [UIView animateWithDuration:0.5
                             animations:^{
                                 imgcell.scrollView.transform  =CGAffineTransformIdentity ;
                             }completion:^(BOOL finished)
             {
                 istranfarm=NO;
                 [self.tblView reloadData];
             }];
            
        }
    }
}

#pragma mark - UIWebViewDelegate
- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType
{
    if (navigationType == UIWebViewNavigationTypeLinkClicked)
    {
        NSURL *url = request.URL;
        if ([url.scheme hasPrefix:APP_HTPP_KEYWORD])
        {
            WebViewController *webObj=[[WebViewController alloc]initWithNibName:@"WebViewController" bundle:nil];
            webObj.url=url;
            webObj.strType=self.offerInfo.strType;
            [self.navigationController pushViewController:webObj animated:YES];
            return  NO;
        }
    }
     return YES;
}
- (void)webViewDidStartLoad:(UIWebView *)webView
{

}
- (void)webViewDidFinishLoad:(UIWebView *)webView
{
    NSString *result = [webView stringByEvaluatingJavaScriptFromString:@"document.body.offsetHeight;"];
    float height = [result integerValue];
    webViewHeight=height+50;
    self.webView.frame=CGRectMake(0, 10,self.view.frame.size.width, webViewHeight);
    self.webView.scrollView.scrollEnabled=NO;
    if(!istranfarm)
        [self.tblView reloadData];
}
- (void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error
{
}
-(void)share
{
    NSURL *url=[CommanMethods getImageURL:self.offerInfo.strImageURL];
    SDWebImageManager *manager = [SDWebImageManager sharedManager];
    [manager downloadImageWithURL:url
                          options:0
                         progress:^(NSInteger receivedSize, NSInteger expectedSize) {
                             // progression tracking code
                         }
                        completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, BOOL finished, NSURL *imageURL)
     {
         if (!image)
         {
             image=placeHolderImage;
         }
         
         dispatch_async(dispatch_get_main_queue(), ^{
             NSString *strTitle=self.offerInfo.strTitle;
             NSString *texttoshare = strTitle;
             UIImage *imagetoshare = image;
             NSArray *activityItems = @[texttoshare, imagetoshare];
             UIActivityViewController *activityVC = [[UIActivityViewController alloc] initWithActivityItems:activityItems applicationActivities:nil];
             activityVC.excludedActivityTypes = @[UIActivityTypeAssignToContact, UIActivityTypePrint];
             [self presentViewController:activityVC animated:TRUE completion:nil];
         });
     }];
    
}
-(void)call
{
    [[AppDelegate appDelegateInstance]callViaTelephone:self.offerInfo.strTelephone];
}
-(void)map
{
    NSURL *url=[NSURL URLWithString:[CommanMethods getMapURL:nil]];
    WebViewController *webObj=[[WebViewController alloc]initWithNibName:@"WebViewController" bundle:nil];
    webObj.url=url;
    webObj.strType=screenHeader_StoreLocator;
    NSString *strScreenValue;
    if([self.offerInfo.strType isEqualToString:APP_EVENTS])
    {
        strScreenValue=[NSString stringWithFormat:@"%@ - %@ - %@",screenName_Events,self.offerInfo.strTitle,screenName_StoreLocator];
    }
    else if([self.offerInfo.strType isEqualToString:APP_OFFER])
    {
        strScreenValue=[NSString stringWithFormat:@"%@ - %@ - %@",screenName_Offer,self.offerInfo.strCategory_Name,screenName_StoreLocator];
    }
    else
    {
        strScreenValue=[NSString stringWithFormat:@"%@ - %@ - %@",screenName_Hotel,self.offerInfo.strTitle,screenName_StoreLocator];
    }
    [GoogleAnalyticsHandler sendScreenName:strScreenValue];
    [self.navigationController pushViewController:webObj animated:YES];
}
-(void)fav:(id)sender
{
    UIButton *button = (UIButton*)sender;
    CGPoint buttonOrigin = [sender frame].origin;
    CGPoint originInTableView = [self.tblView convertPoint:buttonOrigin fromView:[button superview]];
    NSIndexPath *indexPath = [self.tblView  indexPathForRowAtPoint:originInTableView];
    shopDetailsTableViewCell *cell =(shopDetailsTableViewCell *) [self.tblView  cellForRowAtIndexPath:indexPath];
    
    if(favModel)
    {
        BOOL delete=[DBOpreation deleteFav:self.offerInfo.strType ID:self.offerInfo.tblId];
        if(delete)
        {
            self.favModel=nil;
        }
    }
    else
    {
        BOOL insert=[DBOpreation insertFavData:self.offerInfo.strType ID:self.offerInfo.tblId title:self.offerInfo.strTitle];
        if(insert)
        {
            if([self.offerInfo.strType isEqualToString:APP_OFFER])
            {
                [[WebServiceCommunicator sharedInstance]updateOfferCategoryHits:[[AppDelegate appDelegateInstance].strSiteID intValue] CategoryID:[self.offerInfo.tblCategory_ID intValue]];
            }
            
            FavouriteListModel *model=[[FavouriteListModel alloc]init];
            model.favID=self.offerInfo.tblId;
            model.favType=self.offerInfo.strType;
            model.strTitle=self.offerInfo.strTitle;
            self.favModel=model;
        }
    }
    if(favModel)
    {
        [cell.imgFav setImage:[UIImage imageNamed:@"ico-fav-active6p"]];
    }
    else
    {
        [cell.imgFav setImage:[UIImage imageNamed:@"shop-whiteFav"]];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)dealloc
{
    self.tblView.delegate=nil;
}
@end
