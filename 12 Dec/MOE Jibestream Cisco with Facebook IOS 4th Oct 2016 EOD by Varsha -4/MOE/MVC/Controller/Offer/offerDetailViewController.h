//
//  offerDetailViewController.h

//
//  Created by webwerks on 6/24/15.
//  Copyright (c) 2015 Neosoft. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface offerDetailViewController : UIViewController<UITableViewDataSource,UITableViewDelegate,UIWebViewDelegate>
@property(nonatomic,strong) IBOutlet UITableView *tblView;
@property (nonatomic,strong)BaseModel *offerInfo;
@property (nonatomic,strong)UIWebView *webView;

@end
