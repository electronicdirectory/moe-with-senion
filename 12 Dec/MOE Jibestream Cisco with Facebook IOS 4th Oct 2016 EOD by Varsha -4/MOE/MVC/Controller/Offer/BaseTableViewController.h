//
//  BaseTableViewController.h

//
//  Created by Nivrutti on 6/24/15.
//  Copyright (c) 2015 Neosoft. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BaseTableViewController : UIViewController
@property(nonatomic,strong)NSMutableArray *arrData;
@property(nonatomic,strong)NSString *strType;
@property(nonatomic,weak)IBOutlet UITableView *tableView;
@end