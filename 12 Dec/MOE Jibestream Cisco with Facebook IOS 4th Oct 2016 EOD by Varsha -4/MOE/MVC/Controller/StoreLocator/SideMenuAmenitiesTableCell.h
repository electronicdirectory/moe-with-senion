//
//  SideMenuAmenitiesTableCell.h
//  MOE
//
//  Created by varsha on 6/7/16.
//  Copyright © 2016 Neosoft. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SideMenuAmenitiesTableCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIWebView *iconImage;
@property (weak, nonatomic) IBOutlet UILabel *lblAmName;

@property (weak, nonatomic) IBOutlet UILabel *amenitiesNameLabel;

@end
