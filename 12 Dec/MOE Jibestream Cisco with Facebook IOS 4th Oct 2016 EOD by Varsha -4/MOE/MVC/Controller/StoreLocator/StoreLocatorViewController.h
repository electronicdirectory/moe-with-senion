//
//  StoreLocatorViewController.h
//  MOE
//
//  Created by webwerks on 5/17/16.
//  Copyright © 2016 Neosoft. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "TextDirectionView.h"
#import "BuildingInfoLoader.h" 

#import "SLMapView.h"
#import <SLIndoorLocation/SLIndoorLocation.h> //SenionSdk
#import <GameKit/GameKit.h>


@interface StoreLocatorViewController : UIViewController<UIGestureRecognizerDelegate,GKPeerPickerControllerDelegate,SLMapViewDelegate, SLGeofencingDelegate, SLGeoMessengerDelegate>
{
    NSArray *locStateArray;
     double timeInterval;
    //to enable Bluetooth programatically
    GKPeerPickerController *connectionPicker;
}

@property(nonatomic,strong)NSString *strStoreId;

@property (nonatomic, strong, readonly) SLCoordinate3D *location;
@property(nonatomic,strong)SLPixelPoint3D *finalCoordinates;
@property(nonatomic,strong)NSDictionary *infoDict;
-(void)assignCoOrdinate:(SLPixelPoint3D *)slCoOrdinate;
@end
