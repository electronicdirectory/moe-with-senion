//
//  StoreListViewController.m
//  MOE
//
//  Created by webwerks on 5/19/16.
//  Copyright © 2016 Neosoft. All rights reserved.
//
#import "StoreListViewController.h"
#import "StoreTableViewCell.h"

@interface StoreListViewController ()
{
    NSMutableArray *dataArray;
}
@property (weak, nonatomic) IBOutlet UITableView *tblView;
@property (weak, nonatomic) IBOutlet UITextField *txtSearch;
@property (weak, nonatomic) IBOutlet UILabel *lblHeader;

@end

@implementation StoreListViewController
- (void)viewDidLoad {
    [super viewDidLoad];
    
    UIView *leftView=[[UIView alloc]initWithFrame:CGRectMake(0, 0, 5, 20)];
    leftView.backgroundColor=[UIColor clearColor];
    self.txtSearch.leftView = leftView;
    self.txtSearch.leftViewMode = UITextFieldViewModeAlways;
    dataArray=[[NSMutableArray alloc]init];
    
    UIColor *color = [UIColor whiteColor];
    UIFont *font = [CommanMethods getDetailsFont];
    [self.txtSearch setBackgroundColor:[UIColor grayColor]];
    
    self.txtSearch.layer.cornerRadius = 8;
    self.txtSearch.clipsToBounds = YES;
    self.txtSearch.layer.borderColor = [UIColor clearColor].CGColor;
    
 
    self.lblHeader.font=[CommanMethods getNavigationTitleFont];
    self.lblHeader.textColor=[CommanMethods getblackColor];

    [self.txtSearch setReturnKeyType:UIReturnKeyDone];
    self.txtSearch.autocorrectionType = UITextAutocorrectionTypeNo;
    
    self.txtSearch.font=font;
    self.txtSearch.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Search " attributes:@{NSForegroundColorAttributeName: color, NSFontAttributeName: font}];
    
 
    [dataArray addObjectsFromArray:self.arrVenueData];
    
    [self.tblView registerNib:[UINib nibWithNibName:@"StoreTableViewCell" bundle:nil] forCellReuseIdentifier:@"StoreTableViewCell"];
    [self.tblView reloadData];
    
    if([self.strType isEqualToString:@"To"])
    {
        self.lblHeader.text=@"Select Destination";
    }
    else
    {
        self.lblHeader.text=@"Select Current Location";

    }
    // Do any additional setup after loading the view from its nib.
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)backButtonClicked:(id)sender {
    [self dismissViewControllerAnimated:YES completion:NULL];
}
//--------------------------------------------------------------------------------
#pragma UITableView Data Source Methods
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return  [dataArray count];
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *identifier = @"StoreTableViewCell";
    StoreTableViewCell *cell = (StoreTableViewCell *)[self.tblView dequeueReusableCellWithIdentifier:identifier];
     cell.contentMode=UIViewContentModeScaleAspectFit;
    NSDictionary *dict=[dataArray objectAtIndex:indexPath.row];
   cell.lblTitle.text=[dict[st_Title] stringByReplacingOccurrencesOfString:@"&amp;" withString:@"and"];
    cell.selectionStyle=UITableViewCellSelectionStyleNone;
    cell.lblTitle.font=[CommanMethods getHeaderFont];
     cell.lblTitle.textColor=[CommanMethods getblackColor];
    return  cell;
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 44;
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [self.view endEditing:YES];
    if([self.storeDelegate   respondsToSelector:@selector(storeSelectedValue:Type:)])
    {
        [self.storeDelegate storeSelectedValue:[dataArray objectAtIndex:indexPath.row] Type:self.strType];
        [self dismissViewControllerAnimated:YES completion:NULL];

    }
}
//--------------------------------------------------------------------------------
#pragma UITextField Delegate Methods
- (BOOL)textFieldShouldClear:(UITextField *)textField{
    if( [dataArray count]==0)
    {
        [dataArray addObjectsFromArray:self.arrVenueData];
         [self.tblView reloadData];
    }
    return  YES;
}
- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    [self.txtSearch resignFirstResponder];
    return  YES;
}
-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    NSString *searchText = [textField.text stringByReplacingCharactersInRange:range withString:string];
    searchText=[searchText stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];

    //for testing
    [dataArray removeAllObjects];
    if (searchText.length > 0)
    {
        [self searchData:searchText];
    }
    else if( [dataArray count]==0)
    {
        [dataArray addObjectsFromArray:self.arrVenueData];
    }
     [self.tblView reloadData];
    return YES;
}
-(void)searchData:(NSString *)strSearch
{
    NSPredicate *predicatetext;
    predicatetext=[NSPredicate predicateWithFormat:@"Title beginswith[cd] %@",strSearch];
    [dataArray addObjectsFromArray:[self.arrVenueData filteredArrayUsingPredicate:predicatetext]];
}
@end
