//
//  DemoInUnitLabel.m
//  MOE
//
//  Created by varsha on 6/2/16.
//  Copyright © 2016 Neosoft. All rights reserved.
//

#import "DemoInUnitLabel.h"

@interface DemoInUnitLabel ()

@end

@implementation DemoInUnitLabel

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)viewWillAppear:(BOOL)animated
{
    // Taller than wider?
    if(self.textLabel.bounds.size.width < self.textLabel.bounds.size.height)
    {
        self.textLabel.bounds = CGRectMake(self.textLabel.bounds.origin.x, self.textLabel.bounds.origin.y, self.textLabel.bounds.size.height, self.textLabel.bounds.size.width);
        self.textLabel.transform = CGAffineTransformMakeRotation(-M_PI_2);
    }
    
    if(self.webView.bounds.size.width < self.webView.bounds.size.height)
    {
        self.webView.bounds = CGRectMake(self.webView.bounds.origin.x, self.webView.bounds.origin.y, self.webView.bounds.size.height, self.webView.bounds.size.width);
        self.webView.transform = CGAffineTransformMakeRotation(-M_PI_2);
    }
    [super viewWillAppear:animated];
}

-(void)contentScaleFactorChanged:(NSNumber *)newScale  // new method given by jibestream
{
    float scaledSize = hideAfterSize / [newScale floatValue];
     UIFont *theFont = [UIFont fontWithName:@"CenturyGothic" size:scaledSize];

    CGSize size = [self.textLabel.text sizeWithAttributes:@{NSFontAttributeName:theFont}];
    CGSize adjustedSize = CGSizeMake(ceilf(size.width), ceilf(size.height));
    
    BOOL decisionToHide = NO;
    if ((self.textLabel.frame.size.width < adjustedSize.width) ||
        (self.textLabel.frame.size.height < adjustedSize.height))
    {
        // Hide
        decisionToHide = YES;
    }
    else
    {
        // Show
        decisionToHide = NO;
        // On main thread
        [self.textLabel setFont:theFont];
        //  self.textLabel.contentScaleFactor = [newScale floatValue];

        [self.textLabel.layer setContentsScale:newScale.floatValue*[[UIScreen mainScreen] scale]];
       // [self.textLabel.layer setNeedsDisplay];
      
    }
    [self.textLabel setHidden:decisionToHide];
}

-(void)contentScaleFactorChanged:(NSNumber *)newScale atMax:(BOOL)atMax{
    
    float scaledSize = hideAfterSize / [newScale floatValue];
    UIFont *theFont = [UIFont fontWithName:@"CenturyGothic" size:scaledSize];
    CGSize size = [self.textLabel.text sizeWithAttributes:@{NSFontAttributeName:theFont}];
    CGSize adjustedSize = CGSizeMake(ceilf(size.width), ceilf(size.height));
    
    
    BOOL decisionToHide = NO;
    if ((self.textLabel.frame.size.width < adjustedSize.width) ||
        (self.textLabel.frame.size.height < adjustedSize.height))
    {
        // Hide
        decisionToHide = YES;
        if (atMax) {
            decisionToHide = NO;
            [self.textLabel setFont:theFont];

            //self.textLabel.contentScaleFactor = [newScale floatValue];
            [self.textLabel.layer setContentsScale:newScale.floatValue*[[UIScreen mainScreen] scale]];
          //  [self.textLabel.layer setNeedsDisplay];
        }
    }
    else
    {
        // Show
        decisionToHide = NO;
        [self.textLabel setFont:theFont];

        //self.textLabel.contentScaleFactor = [newScale floatValue];
        [self.textLabel.layer setContentsScale:newScale.floatValue*[[UIScreen mainScreen] scale]];
      //  [self.textLabel.layer setNeedsDisplay];
        // On main thread
    }
    [self.textLabel setHidden:decisionToHide];

}
@end
