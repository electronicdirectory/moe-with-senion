//
//  StoreTableViewCell.h
//  MOE
//
//  Created by webwerks on 5/19/16.
//  Copyright © 2016 Neosoft. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface StoreTableViewCell : UITableViewCell
@property(nonatomic,weak)IBOutlet UILabel *lblTitle;
@property(nonatomic,weak)IBOutlet UILabel *lblSubTitle;

@end
