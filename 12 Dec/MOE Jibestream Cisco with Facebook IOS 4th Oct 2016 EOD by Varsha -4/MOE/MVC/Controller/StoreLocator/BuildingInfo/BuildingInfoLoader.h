//
//  BuildingInfoLoader.h
//  SLDemo
//
//  Copyright (c) 2010-2016 Senion. All rights reserved.
//

#import <Foundation/Foundation.h>

#import <SLIndoorLocation/BuildingInfo.h>
#import <SLIndoorLocation/FloorInfo.h>

@class BuildingInfo;

@interface BuildingInfoLoader : NSObject

- (BuildingInfo *)loadBuildingInfo;

- (UIImage *)getImageForFloor:(FloorNr)floorNr;

@end
