//
//  BuildingInfoLoader.m
//  SLDemo
//
//  Copyright (c) 2010-2016 Senion. All rights reserved.
//

#import "BuildingInfoLoader.h"

@interface BuildingInfoLoader ()

@property (nonatomic, strong) BuildingInfo *buildingInfo;

@end

@implementation BuildingInfoLoader

- (BuildingInfo *)loadBuildingInfo
{
    NSString *filePath = [[NSBundle mainBundle] pathForResource:@"ID_8a2e3784-1ad4-4c5c-bf86-4374398ae348_bi" ofType:@"json"];
    NSData *data = [NSData dataWithContentsOfFile:filePath];
    
    self.buildingInfo = [[BuildingInfo alloc] initWithBitmapJsonData:data];
    
    return self.buildingInfo;
}

- (UIImage *)getImageForFloor:(FloorNr)floorNr
{
    NSString *imagePath = [NSString stringWithFormat:@"Map/%@", [[self.buildingInfo getFloorInfo:floorNr] bitmapFilename]];
    return [UIImage imageNamed:imagePath];
}

@end
