//
//  DemoWayFindVC.h
//  MOE
//
//  Created by varsha on 5/31/16.
//  Copyright © 2016 Neosoft. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <JMap/JMap.h>
#define wayFindAnimationLasts   5.0

@interface DemoWayFindVC : UIViewController
@property JMapPathPerFloor *localPathPerFloor;

-(void)dataLoad:(JMapPathPerFloor *)data withDistance:(NSNumber *)distance;

@end

