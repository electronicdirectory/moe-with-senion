//
//  SingleButtonView.m
//  SLDemo
//
//  Copyright (c) 2010-2016 Senion. All rights reserved.
//

#import "CircularMapButtonView.h"

#import "UIColor+SLColor.h"
#import "UIImage+Extension.h"

@interface CircularMapButtonView ()

@property (nonatomic, strong) UIView *buttonHolderView;

@end

@implementation CircularMapButtonView

- (instancetype)init
{
    self = [super init];
    if (self) {
        [self baseInit];
    }
    return self;
}

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self baseInit];
    }
    return self;
}

- (instancetype)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self) {
        [self baseInit];
    }
    return self;
}

- (instancetype)initWithFrame:(CGRect)frame andImage:(UIImage *)image
{
    self = [super initWithFrame:frame];
    if (self) {
        [self baseInit];
        [self.button setImage:image forState:UIControlStateNormal];
        [self.button setImage:image forState:UIControlStateHighlighted];
        [self.button setImage:image forState:UIControlStateSelected];
        [self.button setImage:image forState:UIControlStateDisabled];
    }
    return self;
}

- (void)baseInit
{
    self.layer.masksToBounds = NO;
    self.layer.cornerRadius = 30.0;
    self.layer.shadowColor = [[UIColor blackColor] CGColor];
    self.layer.shadowOffset = CGSizeMake(0, 0);
    self.layer.shadowOpacity = 0.3f;
    self.layer.shadowRadius = 1.0;
    self.layer.shadowPath = [UIBezierPath bezierPathWithRoundedRect:self.bounds cornerRadius:30.0].CGPath;
    
    self.buttonHolderView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.frame.size.width, self.frame.size.height)];
    self.buttonHolderView.layer.masksToBounds = YES;
    self.buttonHolderView.layer.cornerRadius = 30.0;
    
    [self addSubview:self.buttonHolderView];
    
    self.button = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, self.frame.size.width, self.frame.size.height)];
    
    self.button.tintColor = [UIColor whiteColor];
    self.button.layer.cornerRadius = 30.0;
    
    [self.button setBackgroundImage:[UIImage imageWithColor:[UIColor senionBlue]] forState:UIControlStateNormal];
    [self.button setBackgroundImage:[UIImage imageWithColor:[UIColor colorWithRed:137.0/255.0 green:190.0/255.0 blue:230.0/255.0 alpha:1.0]] forState:UIControlStateHighlighted];
    self.button.clipsToBounds = YES;
    
    [self.buttonHolderView addSubview:self.button];
}

- (void)layoutSubviews
{
    [super layoutSubviews];
    
    self.buttonHolderView.frame = CGRectMake(0, 0, self.frame.size.width, self.frame.size.height);
    self.button.frame = CGRectMake(0, 0, self.frame.size.width, self.frame.size.height);
}

@end
