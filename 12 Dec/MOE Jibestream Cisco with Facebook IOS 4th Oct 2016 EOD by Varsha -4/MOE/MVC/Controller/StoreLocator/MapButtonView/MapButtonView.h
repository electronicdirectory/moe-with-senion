//
//  MapButtonView.h
//  SLDemo
//
//  Copyright (c) 2010-2016 Senion. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MapButtonView : UIView

@property (nonatomic, strong) UIView *buttonHolderView;

- (void)baseInit;

- (UIButton *)createMapButtonWithFrame:(CGRect)frame;

@end
