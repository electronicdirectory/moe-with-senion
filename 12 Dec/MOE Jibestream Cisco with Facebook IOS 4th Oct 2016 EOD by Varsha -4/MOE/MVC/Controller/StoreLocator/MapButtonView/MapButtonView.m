//
//  MapButtonView.m
//  SLDemo
//
//  Copyright (c) 2010-2016 Senion. All rights reserved.
//

#import "MapButtonView.h"

#import "UIImage+Extension.h"

@implementation MapButtonView

- (void)baseInit
{
    self.layer.masksToBounds = NO;
    self.layer.cornerRadius = 10.0;
    self.layer.shadowColor = [[UIColor blackColor] CGColor];
    self.layer.shadowOffset = CGSizeMake(0, 0);
    self.layer.shadowOpacity = 0.3f;
    self.layer.shadowRadius = 1.0;
    self.layer.shadowPath = [UIBezierPath bezierPathWithRoundedRect:self.bounds cornerRadius:10.0].CGPath;
    
    self.buttonHolderView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.frame.size.width, self.frame.size.height)];
    self.buttonHolderView.layer.masksToBounds = YES;
    self.buttonHolderView.layer.cornerRadius = 10.0;
    
    [self addSubview:self.buttonHolderView];
}

- (UIButton *)createMapButtonWithFrame:(CGRect)frame
{
    UIButton *button = [[UIButton alloc] initWithFrame:frame];
    
    [button setBackgroundImage:[UIImage imageWithColor:[UIColor colorWithWhite:0.98 alpha:1]] forState:UIControlStateNormal];
    [button setBackgroundImage:[UIImage imageWithColor:[UIColor colorWithWhite:0.90 alpha:1]] forState:UIControlStateHighlighted];
    [button setBackgroundImage:[UIImage imageWithColor:[UIColor colorWithWhite:0.90 alpha:1]] forState:UIControlStateSelected];
    
    return button;
}

- (void)layoutSubviews
{
    [super layoutSubviews];
    
    self.buttonHolderView.frame = CGRectMake(0, 0, self.frame.size.width, self.frame.size.height);
}

@end
