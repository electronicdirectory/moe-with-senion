//
//  CenterMapButtonView.m
//  SLDemo
//
//  Copyright (c) 2010-2016 Senion. All rights reserved.
//

#import "CenterMapButtonView.h"

#import "UIColor+SLColor.h"
#import "UIImage+Extension.h"

@interface CenterMapButtonView ()

@property (nonatomic) CenterMode centerMode;

@property (nonatomic, strong) UIActivityIndicatorView *spinner;

@end

@implementation CenterMapButtonView

- (instancetype)init
{
    self = [super init];
    if (self) {
        [self updateCenterMode:CenterModeRotate];
    }
    return self;
}

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self updateCenterMode:CenterModeRotate];
    }
    return self;
}

- (instancetype)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self) {
        [self updateCenterMode:CenterModeRotate];
    }
    return self;
}

- (instancetype)initWithFrame:(CGRect)frame andImage:(UIImage *)image
{
    self = [super initWithFrame:frame andImage:image];
    if (self) {
        [self updateCenterMode:CenterModeRotate];
    }
    return self;
}

- (void)toggleCenterMode
{
    switch (self.centerMode) {
        case CenterModeNone:
            [self updateCenterMode:CenterModeCenter];
            break;
        case CenterModeCenter:
            [self updateCenterMode:CenterModeRotate];
            break;
        case CenterModeRotate:
            [self updateCenterMode:CenterModeNone];
            break;
    }
}

- (void)updateCenterMode:(CenterMode)centerMode
{
    _centerMode = centerMode;
    [self updateCenterButton];
}

- (void)updateCenterButton
{
    if ([self isSpinning]) {
        return;
    }
    [self.button setBackgroundImage:[UIImage imageWithColor:[UIColor colorWithWhite:0.98 alpha:1]] forState:UIControlStateNormal];
    [self.button setBackgroundImage:[UIImage imageWithColor:[UIColor colorWithWhite:0.90 alpha:1]] forState:UIControlStateHighlighted];
    [self.button setBackgroundImage:[UIImage imageWithColor:[UIColor colorWithWhite:0.90 alpha:1]] forState:UIControlStateSelected];
    [self.button setBackgroundImage:[UIImage imageWithColor:[UIColor colorWithWhite:0.80 alpha:1]] forState:UIControlStateDisabled];
    switch (self.centerMode) {
        case CenterModeNone:
            self.button.tintColor = [UIColor colorWithWhite:0.9 alpha:1.0];
            [self.button setImage:[UIImage imageNamed:@"CenterButtonInactive"] forState:UIControlStateNormal];
            [self.button setImage:[UIImage imageNamed:@"CenterButtonInactive"] forState:UIControlStateHighlighted];
            [self.button setImage:[UIImage imageNamed:@"CenterButtonInactive"] forState:UIControlStateSelected];
            [self.button setImage:[UIImage imageNamed:@"CenterButtonInactive"] forState:UIControlStateDisabled];
            break;
        case CenterModeCenter:
            self.button.tintColor = [UIColor senionBlue];
            [self.button setImage:[UIImage imageNamed:@"CenterButtonSelected"] forState:UIControlStateNormal];
            [self.button setImage:[UIImage imageNamed:@"CenterButtonSelected"] forState:UIControlStateHighlighted];
            [self.button setImage:[UIImage imageNamed:@"CenterButtonSelected"] forState:UIControlStateSelected];
            [self.button setImage:[UIImage imageNamed:@"CenterButtonSelected"] forState:UIControlStateDisabled];
            break;
        case CenterModeRotate:
            self.button.tintColor = [UIColor senionBlue];
            [self.button setImage:[UIImage imageNamed:@"CenterButtonRotation"] forState:UIControlStateNormal];
            [self.button setImage:[UIImage imageNamed:@"CenterButtonRotation"] forState:UIControlStateHighlighted];
            [self.button setImage:[UIImage imageNamed:@"CenterButtonRotation"] forState:UIControlStateSelected];
            [self.button setImage:[UIImage imageNamed:@"CenterButtonRotation"] forState:UIControlStateDisabled];
            break;
    }
}

- (void)showLoadingSpinner
{
    if ([self isSpinning]) {
        [self removeLoadingSpinner];
    }
    [self.button setImage:nil forState:UIControlStateNormal];
    [self.button setImage:nil forState:UIControlStateHighlighted];
    [self.button setImage:nil forState:UIControlStateSelected];
    [self.button setImage:nil forState:UIControlStateDisabled];
    
    self.spinner = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhite];
    [self.spinner startAnimating];
    self.spinner.center = self.button.center;
    [self.button addSubview:self.spinner];
    self.button.enabled = NO;
}

- (void)removeLoadingSpinner
{
    if (self.spinner) {
        [self.spinner stopAnimating];
        [self.spinner removeFromSuperview];
        self.spinner = nil;
    }
    
    self.button.enabled = YES;
    [self updateCenterButton];
}

- (BOOL)isSpinning
{
    return (self.spinner && [self.spinner isAnimating]);
}



@end
