//
//  SingleButtonView.h
//  SLDemo
//
//  Copyright (c) 2010-2016 Senion. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MapButtonView.h"

@interface CircularMapButtonView : UIView

@property (nonatomic, strong) UIButton *button;

- (instancetype)initWithFrame:(CGRect)frame andImage:(UIImage *)image;

@end
