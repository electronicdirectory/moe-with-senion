//
//  CenterMapButtonView.h
//  SLDemo
//
//  Copyright (c) 2010-2016 Senion. All rights reserved.
//

#import "CircularMapButtonView.h"

typedef NS_ENUM(NSInteger, CenterMode) {
    CenterModeNone,
    CenterModeCenter,
    CenterModeRotate
};

@interface CenterMapButtonView : CircularMapButtonView

@property (nonatomic, readonly) CenterMode centerMode;

- (void)toggleCenterMode;
- (void)updateCenterMode:(CenterMode)centerMode;

- (void)showLoadingSpinner;
- (void)removeLoadingSpinner;
- (BOOL)isSpinning;

@end
