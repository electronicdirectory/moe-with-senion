//
//  FloorButtonView.m
//  SLDemo
//
//  Copyright (c) 2010-2016 Senion. All rights reserved.
//

#import "FloorMapButtonView.h"

#import "UIColor+SLColor.h"

@interface FloorMapButtonView ()

@property (nonatomic, strong) UIView *buttonSeparatorView;

@end

@implementation FloorMapButtonView

- (instancetype)init
{
    self = [super init];
    if (self) {
        [self baseInit];
    }
    return self;
}

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self baseInit];
    }
    return self;
}

- (instancetype)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self) {
        [self baseInit];
    }
    return self;
}

- (void)baseInit
{
    [super baseInit];
    
    double floorSize = floor(self.frame.size.height/2);
    double ceilSize = ceil(self.frame.size.height/2);
    if (floorSize == ceilSize) {
        ceilSize++;
    }
    
    self.buttonSeparatorView = [[UIView alloc] initWithFrame:CGRectMake(0, floorSize, self.frame.size.width, 1)];
    self.buttonSeparatorView.backgroundColor = [UIColor colorWithWhite:0.90 alpha:1.0];
    
    UIImage *floorUpImage = [UIImage imageNamed:@"FloorUpButton"];
    UIImage *floorDownImage = [UIImage imageNamed:@"FloorDownButton"];
    
    self.floorUpButton = [self createMapButtonWithFrame:CGRectMake(0, 0, self.frame.size.width, floorSize)];
    [self.floorUpButton setImage:floorUpImage forState:UIControlStateNormal];
    [self.floorUpButton setImage:floorUpImage forState:UIControlStateHighlighted];
    [self.floorUpButton setImage:floorUpImage forState:UIControlStateSelected];
    [self.floorUpButton setImage:floorUpImage forState:UIControlStateDisabled];
    self.floorDownButton = [self createMapButtonWithFrame:CGRectMake(0, ceilSize, self.frame.size.width, floorSize)];
    [self.floorDownButton setImage:floorDownImage forState:UIControlStateNormal];
    [self.floorDownButton setImage:floorDownImage forState:UIControlStateHighlighted];
    [self.floorDownButton setImage:floorDownImage forState:UIControlStateSelected];
    [self.floorDownButton setImage:floorDownImage forState:UIControlStateDisabled];
    
    [self.buttonHolderView addSubview:self.buttonSeparatorView];
    [self.buttonHolderView addSubview:self.floorUpButton];
    [self.buttonHolderView addSubview:self.floorDownButton];
}

- (void)layoutSubviews
{
    double floorSize = floor(self.frame.size.height/2);
    double ceilSize = ceil(self.frame.size.height/2);
    if (floorSize == ceilSize) {
        ceilSize++;
    }
    
    self.buttonSeparatorView.frame = CGRectMake(0, floorSize, self.frame.size.width, 1);
    self.floorUpButton.frame = CGRectMake(0, 0, self.frame.size.width, floorSize);
    self.floorDownButton.frame = CGRectMake(0, ceilSize, self.frame.size.width, floorSize);
}

@end
