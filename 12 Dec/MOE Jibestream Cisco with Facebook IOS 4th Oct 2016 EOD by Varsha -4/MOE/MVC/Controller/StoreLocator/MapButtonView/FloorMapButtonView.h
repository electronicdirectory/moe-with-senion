//
//  FloorButtonView.h
//  SLDemo
//
//  Copyright (c) 2010-2016 Senion. All rights reserved.
//

#import "MapButtonView.h"

@interface FloorMapButtonView : MapButtonView

@property (nonatomic, strong) UIButton *floorUpButton;
@property (nonatomic, strong) UIButton *floorDownButton;

@end
