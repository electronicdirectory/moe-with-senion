
//
//  StoreLocatorViewController.m
//  MOE
//
//  Created by webwerks on 5/17/16.
//  Copyright © 2016 Neosoft. All rights reserved.
//

#import "StoreLocatorViewController.h"
#import <JMap/JMap.h>
#import "JibeStreamConstant.h"
#import "StoreListViewController.h"
#import "DemoWayFindVC.h"
#import "DemoInUnitLabel.h"
#import "SideMenuAmenitiesTableCell.h"
#import "DemoPopUpVC.h"
#import "TextDirectionView.h"
#import "CustomJMap.h"
#import "CustomSideMenuTable.h"

#import "BuildingInfoLoader.h"
#import "SLMapView.h"
#import "SLMapOverlayView/SLGeofencingArea.h"
#import "SLMapOverlayView.h"


static BOOL ActivateGeofencing = YES;
static BOOL ActivateGeoMessenger = YES;


static NSString *MapKey = @"06ff676d-6fc7-434b-83e8-aa2bd54d19f6"; //<----------- INSERT YOUR MAP KEY HERE (OBTAINED FROM SENIONLAB);
static NSString *CustomerId = @"7c50c540-0dd9-4d5e-9264-2a10c88464ea"; //<----------- INSERT YOUR CUSTOMER ID HERE (OBTAINED FROM SENIONLAB);


@interface StoreLocatorViewController ()<CustomJMapDelegate,StoreListDelegate,CustomSideMenuTableDelegate>
{
    JMapWaypoint *toWaypoint;
    JMapWaypoint *fromWaypoint,*lastFromWayPoint;
    JMapFloor *mapCurrentFloor;
    JMAPPoint senionPosition;
    
    NSMutableArray *arrayOfAnimationViews,*floorsArray;
    NSArray *arrayOfTextDirectionArray;
    
    id lastSelectedToLocation,lastSelectedFromLocation;
    UIView *popViewForMarkerTo ,*popViewForMarkerFrom;
    UIImage *sourceMarker ,*destinationMarker;
    NSMutableArray *amenitiesSeltedArray;
    NSMutableArray *sectionIndexPathZeroSelectedRow;
    NSMutableArray *arrLevelsButtons;
    NSOperationQueue *directionQueue;
 }

@property(nonatomic,strong)NSMutableArray *arrayOfWayFindViews;
@property(nonatomic,strong) NSArray *arrayOfWayFindFloors;

@property(nonatomic,strong)NSDictionary *toDict,*fromDict;
@property (nonatomic, assign) BOOL containerIsOpen;
@property (weak, nonatomic) IBOutlet UIImageView *imgElevator;
@property (weak, nonatomic) IBOutlet UIImageView *imgElevatorDirection;

@property (weak, nonatomic) IBOutlet UILabel *labelSource;
@property (weak, nonatomic) IBOutlet UILabel *labelDestination;
@property (weak, nonatomic) IBOutlet UILabel *lblInfo;
@property (weak, nonatomic) IBOutlet UILabel *lblProductName;
@property (weak, nonatomic) IBOutlet UILabel *lblStoreName;
@property (weak, nonatomic) IBOutlet UILabel *lblGetDirection;
@property (weak, nonatomic) IBOutlet UILabel *lblAccebiltyLine;
@property (weak, nonatomic) IBOutlet UILabel *lblTo;
@property (weak, nonatomic) IBOutlet UILabel *lblFrom;

@property (weak, nonatomic) IBOutlet UIImageView *imgDirection;
@property (weak, nonatomic) IBOutlet UIImageView *imgAccesibilty;
@property (weak, nonatomic) IBOutlet UIImageView *imgReplay;

@property (weak, nonatomic) IBOutlet UITextField *txtFrom;
@property (weak, nonatomic) IBOutlet UITextField *txtTo;

@property (weak, nonatomic) IBOutlet UIView *contentView;
@property (weak, nonatomic) IBOutlet UIView *accessbilityView;
@property (weak, nonatomic) IBOutlet UIView *bottomView;
@property (weak, nonatomic) IBOutlet UIView *destChooserView;

@property (weak, nonatomic) IBOutlet UIButton *replaybutton;
@property (weak, nonatomic) IBOutlet UIButton *btnSideMenu;
@property (weak, nonatomic) IBOutlet UIButton *txtDirectionButton;
@property (weak, nonatomic) IBOutlet UIButton *btnAccessility;
@property (strong, nonatomic) IBOutlet UIButton   *btnSwap;

@property (weak, nonatomic) IBOutlet CustomSideMenuTable *sideMenuAmenitiesTblView;

@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *indicator;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *acsbiltyViewBottomCostraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *heightContraint;

@property(strong,nonatomic)TextDirectionView *textDirView;

@property JMapPathPerFloor *localPathPerFloor;
@property (nonatomic, strong) SLMapOverlayView *mapOverlayView;


- (IBAction)swapButtonClicked:(id)sender;
- (IBAction)btnGetDirectionClicked:(id)sender;
- (IBAction)btnCloseClicked:(id)sender;
- (IBAction)accesibilityBtnClick:(UIButton *)sender;
- (IBAction)txtDirectionBtnClick:(UIButton *)sender;
- (IBAction)replayBtnClick:(UIButton *)sender;
- (IBAction)closeAcssbltyVewBtnClick:(UIButton *)sender;
- (IBAction)btnClickfloorNavigation:(UIButton *)sender;
- (IBAction)amenitiesSideMenu:(UIButton *)sender;

@property (nonatomic, strong)SLMapView *slMapView;//SLMapView property

@property (nonatomic, strong) NSMutableArray *pointsOfInterest;
@property (nonatomic, strong) NSMutableArray *geofencingAreas;

@property (nonatomic, strong) UIAlertView *geofencingAlertView;
@property (nonatomic, strong) id<SLGeometry> currentGeometry;


@end
@implementation StoreLocatorViewController
#pragma  mark viewDidLoad

#define  kTo @"To"
#define  kFrom @"From"

- (void)viewDidLoad {
    [super viewDidLoad];
    
    //SLIndoorLocation Setup
//    [self bluetoothSetting];
//     [self initGui];
    
    //Jibestream Setup
    self.navigationController.interactivePopGestureRecognizer.enabled=NO;
    
    self.imgElevator.hidden=YES;
    directionQueue=[[NSOperationQueue alloc]init];
  
    UITapGestureRecognizer *singleTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleTap:)];
    singleTap.cancelsTouchesInView = NO;
    singleTap.delegate =self;
    [self.contentView addGestureRecognizer:singleTap];
    
    [self.indicator startAnimating];
    self.navigationController.navigationBar.translucent = NO;
    self.navigationItem.titleView=[CommanMethods getLabel:screenHeader_StoreLocator];
    if ([self.navigationController.viewControllers count]==1)
    {
        self.sideMenuViewController.panGestureEnabled=NO;
        self.navigationItem.leftBarButtonItem =[CommanMethods getLeftButton:self Action:@selector(menuMethod)];
    }
    [self setRightBarButton];
    
       // Load view for showing Text Directions of To and From
    _textDirView = [[[NSBundle mainBundle]loadNibNamed:@"TextDirectionView" owner:self options:nil] objectAtIndex:0];
    
    //Load images for markers
    sourceMarker = [UIImage imageNamed:@"st-pin"];
    destinationMarker = [UIImage imageNamed:@"st-pointer"];
    self.imgDirection.image=[UIImage imageNamed:@"st-get-direction"];
    
    [self addLeftView:self.txtFrom withImage:@"st_StartDot"];
    [self addLeftView:self.txtTo withImage:@"st-EndPin"];
    
    self.destChooserView.hidden=YES;
    self.heightContraint.constant=0;
    self.containerIsOpen = NO;
    
    self.imgReplay.image=[UIImage imageNamed:@"replay"];
    self.imgAccesibilty.image=[UIImage imageNamed:@"accessibility"];
    
    self.imgReplay.contentMode=UIViewContentModeScaleAspectFit;
    self.imgAccesibilty.contentMode=UIViewContentModeScaleAspectFit;
    self.imgElevator.contentMode=UIViewContentModeScaleAspectFit;
    self.labelSource.font=[CommanMethods getHeaderFont];
    self.labelDestination.font=[CommanMethods getHeaderFont];
    
    self.lblTo.font=[CommanMethods getDetailsFont];
    self.lblFrom.font=[CommanMethods getDetailsFont];
    
    
    //array allocation
    _arrayOfWayFindViews = [[NSMutableArray alloc]init];
    arrayOfAnimationViews = [[NSMutableArray alloc]init];
    amenitiesSeltedArray = [[NSMutableArray alloc]init];
    arrayOfTextDirectionArray= [[NSArray alloc]init];

    
    // Initialize Jmap
    [self initialiseMap];
    [self initGui];
    [self updateUserLocation];
    
}

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gesture shouldReceiveTouch:(UITouch *)touch {
    if (_contentView.frame.origin.x != 0) {
        return YES;
    }
    
    return NO;
}

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer
shouldRecognizeSimultaneouslyWithGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer
{
    return YES;
}

-(void)handleTap:(UIGestureRecognizer*)gesture{
    [self hideMenu];
    
}

 -(void)willMoveToParentViewController:(UIViewController *)parent {
    [super willMoveToParentViewController:parent];
    if (!parent)
    {
        [NSObject cancelPreviousPerformRequestsWithTarget:self];
        
        
        [self btnCloseClicked:nil];
        [CustomJMap sharedInstance].jMDelegate=nil;
        [[CustomJMap sharedInstance].mapView removeFromSuperview];
        [[CustomJMap sharedInstance].mapView resetRotation];
        
        _arrayOfWayFindViews=nil;
        arrayOfAnimationViews=nil;
        amenitiesSeltedArray=nil;
        arrayOfTextDirectionArray=nil;
        _arrayOfWayFindFloors=nil;
        _toDict=nil;
        _fromDict=nil;
        self.textDirView=nil;
        arrLevelsButtons=nil;
          [directionQueue cancelAllOperations];
        directionQueue=nil;
    }
}

//#pragma mark - View Method(Senion)
//- (void)viewDidAppear:(BOOL)animated
//{
//    [super viewDidAppear:animated];
//    if (self.isLocationManagerLoaded) {
//        [self startUpdatingLocation];
//    }
//}
//
//- (void)viewDidDisappear:(BOOL)animated
//{
//    [super viewDidDisappear:animated];
//    [self stopUpdatingLocation];
//}

#pragma  mark addLeftView
-(void)addLeftView:(UITextField *)txtField withImage:(NSString*)imagename
{
    UIView *leftView=[[UIView alloc]initWithFrame:CGRectMake(0, 0, 24, 22)];
    leftView.backgroundColor = [UIColor clearColor];
    UIImageView *fromimageView = [[UIImageView alloc] initWithFrame:CGRectMake(5, 4, 14, 14)];
    fromimageView.image = [UIImage imageNamed:imagename];
    fromimageView.backgroundColor=[UIColor clearColor];
    [leftView addSubview:fromimageView];
    txtField.leftView = leftView;
    txtField.leftViewMode = UITextFieldViewModeAlways;
    
    [txtField setBackgroundColor:[UIColor grayColor]];
    txtField.layer.cornerRadius = 8;
    txtField.clipsToBounds = YES;
    txtField.layer.borderColor = [UIColor clearColor].CGColor;
    txtField.font=[CommanMethods getDetailsFont];
    
}
#pragma  add Level on the Map View
-(void)addButtonsOnTheView
{
    arrLevelsButtons=[[NSMutableArray alloc]init];
    UIView *bgView=[[UIView alloc]initWithFrame:CGRectMake(0, 75, [floorsArray count]*40, 40)];
    [self.contentView addSubview:bgView];
    [self.contentView bringSubviewToFront:bgView];
    [bgView setBackgroundColor:[UIColor clearColor]];
    bgView.center=CGPointMake(APP_DELEGATE.window.center.x, bgView.center.y);
    int x=3;
    for (int i=0;i<[floorsArray count];i++)
    {
        JMapFloor *floor=[floorsArray objectAtIndex:i];
        NSString *strName=[self.sideMenuAmenitiesTblView getLevel:floor.floorSequence];
        UIButton *btnLevel=[UIButton buttonWithType:UIButtonTypeCustom];
        btnLevel.frame=CGRectMake(x, 5, 30, 30);
        [btnLevel setTitle:strName forState:UIControlStateNormal];
        [btnLevel setTitle:strName forState:UIControlStateSelected];
        [btnLevel setTitle:strName forState:UIControlStateHighlighted];
        [btnLevel addTarget:self action:@selector(btnClickfloorNavigation:) forControlEvents:UIControlEventTouchUpInside];
        btnLevel.titleLabel.font=[UIFont boldSystemFontOfSize:13];
        btnLevel.tag=i;
        [bgView addSubview:btnLevel];
        [arrLevelsButtons addObject:btnLevel];
        x=x+35+6;
    }
    [self setupDefaultBGColor];
}
#pragma  mark setRightBarButton
-(void)setRightBarButton
{
    self.navigationItem.rightBarButtonItem=[CommanMethods getRightButton:self Action:@selector(search)];
}
#pragma  mark menuMethod
- (void)menuMethod {
    [self.sideMenuViewController presentLeftMenuViewController];
}
#pragma  mark UITextField Delegate Methods
- (void)textFieldDidBeginEditing:(UITextField *)textField{
    if([textField isEqual:self.txtFrom])
    {
        [self LoadList:kFrom SelectedData:self.fromDict];
    }
    else
    {
        [self LoadList:kTo SelectedData:self.toDict];
    }
    [self.view endEditing:YES];
}
#pragma  mark LoadList
-(void)LoadList:(NSString *)strType SelectedData:(id)data
{
    StoreListViewController *listObj=[[StoreListViewController alloc]initWithNibName:@"StoreListViewController" bundle:nil];
    listObj.storeDelegate=self;
    listObj.strType=strType;
    listObj.selectedObject=data;
    listObj.arrVenueData=[[CustomJMap sharedInstance].arrVenueList copy];
    [self presentViewController:listObj animated:YES completion:NULL];
}
#pragma  mark search
-(void)search
{
    [self LoadList:kTo SelectedData:nil];
}

#pragma  mark didReceiveMemoryWarning
- (void)didReceiveMemoryWarning {
    NSLog(@"didReceiveMemoryWarning");
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
#pragma  mark SwapButtonClicked
- (IBAction)swapButtonClicked:(id)sender{
    [NSObject cancelPreviousPerformRequestsWithTarget:self selector:@selector(changeFloor:) object:nil];

    NSDictionary *dict=self.fromDict;
    self.fromDict=self.toDict;
    self.toDict=dict;
    self.txtTo.text=  self.toDict[st_Title];
    self.txtFrom.text = self.fromDict[st_Title];
    self.lblProductName.text=  self.toDict[st_Title];
    self.lblStoreName.text=  self.toDict[st_Title];
    self.lblInfo.text = self.toDict[st_CatName];

    JMapWaypoint *swapVal=toWaypoint;
    toWaypoint=fromWaypoint;
    fromWaypoint=swapVal;
    
    
    // Unhilighr all units
    //[mapView unhighlightAllUnits];
    // Remove path drawn between source and destination
    // Remove array of floors
    [[CustomJMap sharedInstance].mapView removeAllWayFindViews];
    //remove annotation/marker for selected location
    [[CustomJMap sharedInstance].mapView removePopupCardToMap:popViewForMarkerFrom];
    [[CustomJMap sharedInstance].mapView removePopupCardToMap:popViewForMarkerTo];
    
    // Draw path for swapped source and destination
    NSNumber *accessibility = [NSNumber numberWithInt:100];
    // Call for path generation
    [self displayWayFindFrom:fromWaypoint to:toWaypoint withAccessibility:accessibility];
    // Add pin markers for swapped source and destination
    [[CustomJMap sharedInstance].mapView setLevelById:toWaypoint.mapId];
    // Set map level to the source location
    
    [self addPopupCardToMap:sourceMarker wayPoint:toWaypoint isDestination:YES];
    [[CustomJMap sharedInstance].mapView setLevelById:fromWaypoint.mapId];
    [self addPopupCardToMap:destinationMarker wayPoint:fromWaypoint isDestination:NO];
}
//------------------------------------------------------------------------------
#pragma  mark btnGetDirectionClicked
- (IBAction)btnGetDirectionClicked:(id)sender{
    self.imgDirection.image=[UIImage imageNamed:@"st-get-direction"];
    self.lblGetDirection.text=@"Get Direction";
    self.navigationItem.rightBarButtonItem=nil;
    self.destChooserView.hidden=NO;
    [self LoadList:kTo SelectedData:self.toDict];
}

#pragma  mark btnCloseClicked
- (IBAction)btnCloseClicked:(id)sender{
    
    [NSObject cancelPreviousPerformRequestsWithTarget:self selector:@selector(changeFloor:) object:nil];
    self.lblGetDirection.text=@"Get Direction";
    self.imgDirection.image=[UIImage imageNamed:@"st-get-direction"];
    self.lblProductName.text=@"";
    self.heightContraint.constant=0;
    // Unhilighr all units
    [[CustomJMap sharedInstance].mapView hideAllIcons];
    [self removeUnwantedObjects];
    
    [self showMapIcons];
    [[CustomJMap sharedInstance].mapView    resetMapPosition];
    
    [CustomJMap sharedInstance].mapView.contentSize=[CustomJMap sharedInstance].mapContentSize;
    [CustomJMap sharedInstance].mapView.contentOffset=[CustomJMap sharedInstance].mapOffset;
    dispatch_async(dispatch_get_main_queue(), ^(void) {
        [[CustomJMap sharedInstance].mapView zoomToRect:[CustomJMap sharedInstance].zoomRect animated:NO];
        [[CustomJMap sharedInstance].mapView setZoomScale:0.2];
    });
}
#pragma  mark removeUnwantedObjects
-(void)removeUnwantedObjects{
    self.lblStoreName.text=@"";
    self.lblInfo.text=@"";
    self.strStoreId=@"";
    self.lblInfo.text=@"";
    self.txtTo.text=@"";
    self.txtFrom.text=@"";
    self.destChooserView.hidden=YES;
    [self setRightBarButton];
    
    lastSelectedToLocation = nil;
    lastSelectedFromLocation = nil;
    //Stop and remove animation for path between from and to
    for(UIImageView *nextAnim in arrayOfAnimationViews) {
        [nextAnim.layer removeAllAnimations];
        [nextAnim removeFromSuperview];
    }
    if(arrayOfAnimationViews.count>0)
        [arrayOfAnimationViews removeAllObjects];
    
    
    // Unhilighr all units
    [[CustomJMap sharedInstance].mapView unhighlightAllUnits];
    // Remove path drawn between source and destination
    // Remove array of floors
    self.arrayOfWayFindFloors = nil;
    [[CustomJMap sharedInstance].mapView removeAllWayFindViews];
    //remove annotation/marker for selected location
    [[CustomJMap sharedInstance].mapView removePopupCardToMap:popViewForMarkerFrom];
    [[CustomJMap sharedInstance].mapView removePopupCardToMap:popViewForMarkerTo];
   
    
}



#pragma  mark StoreList Delegate Method
-(void)storeSelectedValue:(id)value Type:(NSString *)strType
{
    
  //  [self dismissViewControllerAnimated:YES completion:NULL];
    NSLog(@"ENTERED IN storeSelectedValue");
    
    [self.contentView layoutIfNeeded];
    _textDirView.frame = CGRectMake(0, self.view.frame.size.height, self.view.frame.size.width, _textDirView.frame.size.height);
    [_textDirView removeFromSuperview];
    if([strType isEqualToString:kTo])
    {
        self.toDict=value;
        self.txtTo.text=[value objectForKey:st_Title];
        self.lblProductName.text=[value objectForKey:st_Title];
        self.lblStoreName.text=[value objectForKey:st_Title];
        self.lblInfo.text=[[value objectForKey:st_CatName] stringByReplacingOccurrencesOfString:@"&amp;" withString:@"and"];
        
        self.lblInfo.text = [self.lblInfo.text uppercaseString];
        
        self.heightContraint.constant=90;
        // To Waypoint
        if(lastSelectedToLocation)
        {
            [[CustomJMap sharedInstance].mapView setDestinationUnHighlight:[[CustomJMap sharedInstance].mapView getDestinationByDestinationId:[lastSelectedToLocation objectForKey:st_Id]]];
            [[CustomJMap sharedInstance].mapView removePopupCardToMap:popViewForMarkerTo];
        }
        
        toWaypoint = [[CustomJMap sharedInstance].mapView getWayPointByDestinationId:[value objectForKey:st_Id]];
        
        [[CustomJMap sharedInstance].mapView setDestinationHighlight:[[CustomJMap sharedInstance].mapView getDestinationByDestinationId:[value objectForKey:st_Id]] withColor:[UIColor lightGrayColor]];
        lastSelectedToLocation = value;
       
        [[CustomJMap sharedInstance].mapView setLevelById:toWaypoint.mapId];
        [self setUpCurrentLevel];
        // Add Pin marker to source location
        
        [self addPopupCardToMap:sourceMarker wayPoint:toWaypoint isDestination:YES];
    }
    else{
        
        self.fromDict=value;
        self.txtFrom.text=[value objectForKey:st_Title];
        // From Waypoint
        if(lastSelectedFromLocation)
        {
            [[CustomJMap sharedInstance].mapView setDestinationUnHighlight:[[CustomJMap sharedInstance].mapView getDestinationByDestinationId:[lastSelectedFromLocation objectForKey:st_Id]]];
            [[CustomJMap sharedInstance].mapView removePopupCardToMap:popViewForMarkerFrom];
        }
        

        lastSelectedFromLocation = value;
        [[CustomJMap sharedInstance].mapView
         setLevelById:fromWaypoint.mapId];
        
        [self setUpCurrentLevel];
    
        // Add Pin marker to destination location
//        [self addPopupCardToMap:destinationMarker wayPoint:fromWaypoint isDestination:NO];
    }
    
    // Generate path between source and destination.
    if([self.txtTo.text length]>0){
        // Accessibility 50 avoids stairs and escalators (handicapped), 100 takes escalators and stairs...
        NSNumber *accessibility = [NSNumber numberWithInt:100];
        // Call for path generation
        [[CustomJMap sharedInstance].mapView setLevelById:fromWaypoint.mapId];
        [self setUpCurrentLevel];
        
        if(lastFromWayPoint != fromWaypoint){
        NSLog(@"displayWayFindFrom called");
        [self displayWayFindFrom:fromWaypoint to:toWaypoint withAccessibility:accessibility];
        lastFromWayPoint = fromWaypoint;
        }

    }
    
  }
//------------------------------------------------------------------------------
#pragma mark Side Menu button action method
- (IBAction)amenitiesSideMenu:(UIButton *)sender {
    
    self.btnSideMenu.selected = !self.btnSideMenu.selected;
    
    if(_contentView.frame.origin.x == 0) //only show the menu if it is not already shown
    {
        
        [self showMenu];
    }
    else
    {
        [self hideMenu];
    }
}


-(void)showMenu{
    
    [self.sideMenuAmenitiesTblView reloadData];
    
    //slide the content view to the right to reveal the menu
    [UIView animateWithDuration:.25
                     animations:^{
                         [_contentView setFrame:CGRectMake(_sideMenuAmenitiesTblView.frame.size.width, _contentView.frame.origin.y, _contentView.frame.size.width, _contentView.frame.size.height)];
                     }completion:^(BOOL finished) {
                     }
     ];
}
-(void)hideMenu{
    
    //slide the content view to the left to hide the menu
    [UIView animateWithDuration:.25
                     animations:^{
                         [_contentView setFrame:CGRectMake(0, _contentView.frame.origin.y, _contentView.frame.size.width, _contentView.frame.size.height)];
                     }completion:^(BOOL finished) {
                     }
     ];
}
//------------------------------------------------------------------------------
#pragma mark Button Click methods for map interaction
- (IBAction)accesibilityBtnClick:(UIButton *)sender {
    sender.selected = !sender.selected;
    // Set map level to the source location
    [[CustomJMap sharedInstance].mapView setLevelById:fromWaypoint.mapId];
    if(sender.selected){
        
        arrayOfTextDirectionArray = nil;
        NSNumber *accessibility = [NSNumber numberWithInt:50];
        // Call for path generation
        [self displayWayFindFrom:fromWaypoint to:toWaypoint withAccessibility:accessibility];
        self.imgAccesibilty.image=[UIImage imageNamed:@"accessibility-Red"];
        self.imgReplay.image=[UIImage imageNamed:@"replay-red"];
        
        
    }
    else{
        
        arrayOfTextDirectionArray = nil;
        NSNumber *accessibility = [NSNumber numberWithInt:100];
        // Call for path generation
        [self displayWayFindFrom:fromWaypoint to:toWaypoint withAccessibility:accessibility];
        self.imgAccesibilty.image=[UIImage imageNamed:@"accessibility"];
        self.imgReplay.image=[UIImage imageNamed:@"replay"];
        
    }
}
- (IBAction)txtDirectionBtnClick:(UIButton *)sender {
    sender.selected = !sender.selected;
    if(sender.selected){
        [self.contentView addSubview:_textDirView];
        CGFloat y =self.accessbilityView.frame.origin.y;
        _textDirView.frame = CGRectMake(0, self.view.frame.size.height, self.view.frame.size.width, _textDirView.frame.size.height);
        [UIView animateWithDuration:0.2 animations:^{
            [self.contentView layoutIfNeeded];
            _textDirView.frame = CGRectMake(0, y-_textDirView.frame.size.height, self.view.frame.size.width, _textDirView.frame.size.height);
        }];
    }
    else
    {
        [UIView animateWithDuration:0.2 animations:^{
            [self.contentView layoutIfNeeded];
            _textDirView.frame = CGRectMake(0, self.view.frame.size.height, self.view.frame.size.width, _textDirView.frame.size.height);
        } completion:^(BOOL finished) {
            [_textDirView removeFromSuperview];
        }];
    }
}
//------------------------------------------------------------------------------
- (IBAction)replayBtnClick:(UIButton *)sender {
    if(_btnAccessility.selected){
        _btnAccessility.selected = !_btnAccessility.selected;
        self.imgAccesibilty.image=[UIImage imageNamed:@"accessibility"];
        self.imgReplay.image=[UIImage imageNamed:@"replay"];
    }
    // Remove all local views
    for(UIImageView *nextAnim in arrayOfAnimationViews)
    {
        [nextAnim.layer removeAllAnimations];
        [nextAnim removeFromSuperview];
    }
    [arrayOfAnimationViews removeAllObjects];
    
    for(DemoWayFindVC *nextFloorWayFind in _arrayOfWayFindViews)
    {
        [nextFloorWayFind.view removeFromSuperview];
    }
    
    [NSObject cancelPreviousPerformRequestsWithTarget:self selector:@selector(changeFloor:) object:nil];

    // Remove all local views
    [_arrayOfWayFindViews removeAllObjects];
    // Remove privious path
    [[CustomJMap sharedInstance].mapView removeAllWayFindViews];
    // Call for path animation
    
    
    // Set map level to the source location
    [[CustomJMap sharedInstance].mapView setLevelById:fromWaypoint.mapId];
    [self setUpCurrentLevel];
    
    if([_arrayOfWayFindFloors count]>0)
    {
        //get waypoints first and last Object
        JMapPathPerFloor *ffloor=[_arrayOfWayFindFloors firstObject];
        JMapPathPerFloor *lfloor=[_arrayOfWayFindFloors lastObject];
        
        float pathDistance =  [self animatePathWithImageview];
        if(pathDistance==0)
        {
            pathDistance=2.5;
        }
        if(![lfloor.seq isEqual:ffloor.seq])
            [self performSelector:@selector(changeFloor:) withObject:nil afterDelay:pathDistance+pathDistance];
   
        [self getTextDirectionArray];
    }
 
}

- (IBAction)closeAcssbltyVewBtnClick:(UIButton *)sender {
    [NSObject cancelPreviousPerformRequestsWithTarget:self selector:@selector(changeFloor:) object:nil];

    [self.imgElevatorDirection.layer removeAllAnimations];
    self.imgElevator.hidden=YES;
    self.heightContraint.constant=90;
    // Remove unwanted objects
    [self removeUnwantedObjects];
    self.acsbiltyViewBottomCostraint.constant = -90;
    self.lblProductName.text=@"";
    //Remove Text Direction view
    [self.textDirView removeFromSuperview];
    _btnAccessility.selected = NO;
    self.imgAccesibilty.image=[UIImage imageNamed:@"accessibility"];
    self.imgReplay.image=[UIImage imageNamed:@"replay"];
    [[CustomJMap sharedInstance].mapView hideAllIcons];
    [self showMapIcons];
    
    [[CustomJMap sharedInstance].mapView    resetMapPosition];
    
    [CustomJMap sharedInstance].mapView.contentSize=[CustomJMap sharedInstance].mapContentSize;
    [CustomJMap sharedInstance].mapView.contentOffset=[CustomJMap sharedInstance].mapOffset;
    dispatch_async(dispatch_get_main_queue(), ^(void) {
        [[CustomJMap sharedInstance].mapView zoomToRect:[CustomJMap sharedInstance].zoomRect animated:NO];
        [[CustomJMap sharedInstance].mapView setZoomScale:0.2];
        
    });
 }
-(void)setUpCurrentLevel
{
    mapCurrentFloor=[self   getMapViewCurrentFloor];
    [self setupDefaultBGColor];
    [self setupSelectedBGColor:[arrLevelsButtons objectAtIndex:mapCurrentFloor.floorSequence.intValue-1]];
}
-(JMapFloor *)getMapViewCurrentFloor
{
    return  [[CustomJMap sharedInstance].mapView getCurrentFloor];
}
-(void)setupDefaultBGColor
{
    UIColor *color=[UIColor colorWithRed:128.0f/255.0f green:168.0f/255.0f blue:193.0f/255.0f alpha:1.0f];
    for (UIButton *btn in arrLevelsButtons) {
        [btn setBackgroundColor:color];
    }
}
-(void)setupSelectedBGColor:(UIButton *)btn
{
    UIColor *color=[UIColor colorWithRed:194.0f/255.0f green:29.0f/255.0f blue:48.0f/255.0f alpha:1.0f];
    [btn setBackgroundColor:color];
}
- (IBAction)btnClickfloorNavigation:(UIButton *)sender {
    [self setupDefaultBGColor];
    [self setupSelectedBGColor:sender];
    JMapFloor *currentLevel = [floorsArray objectAtIndex:sender.tag];
    [[CustomJMap sharedInstance].mapView resetRotation];
    [[CustomJMap sharedInstance].mapView setLevelById:currentLevel.mapId];
    mapCurrentFloor=currentLevel;
    [self.sideMenuAmenitiesTblView reloadData];
}
//---------------------------------------------------------------------------
#pragma  mark initialiseMap
-(void)initialiseMap{
    // initalize map and  download data
    [CustomJMap sharedInstance].jMDelegate=self;
    [[CustomJMap sharedInstance]initMapView];
    [CustomJMap sharedInstance].mapView.frame=CGRectMake(0, 64, self.view.frame.size.width, self.view.frame.size.height-64);
    [self.contentView addSubview:[CustomJMap sharedInstance].mapView];
    [self.contentView sendSubviewToBack:[CustomJMap sharedInstance].mapView];
    //load data
    [[CustomJMap sharedInstance]loadJMapData];
}

#pragma  mark
#pragma  mark CustomJMap Delegate Methods
- (void)customJMapViewDataReady
{
    if([[CustomJMap sharedInstance].arrVenueList count]>0)
    {
        floorsArray=[[NSMutableArray alloc]initWithArray:[CustomJMap sharedInstance].venueObject.maps];
        if([floorsArray count]>0)
        {
            
            [[CustomJMap sharedInstance].mapView setZoomScale:1.0];
            
            if(IS_IPHONE_6P)
            {
                [CustomJMap sharedInstance].mapView.maximumZoomScale = 15;
            }
            else{
                [CustomJMap sharedInstance].mapView.maximumZoomScale = 10;
            }
            JMapFloor *currentLevel = [CustomJMap sharedInstance].mapView.currentFloor;
            [[CustomJMap sharedInstance].mapView setLevelById:currentLevel.mapId];
            mapCurrentFloor=[self   getMapViewCurrentFloor];
            [[CustomJMap sharedInstance].mapView resetRotation];
            
            
            if(CGSizeEqualToSize([CustomJMap sharedInstance].mapContentSize, CGSizeZero))
            {
                
                CGRect theUnitsRect = [[CustomJMap sharedInstance].mapView rectForStyleString:@"Mall-Boundary" onFloor:mapCurrentFloor];
                // Reset map rotation
                // Zoom to rect
                [[CustomJMap sharedInstance].mapView zoomToRect:theUnitsRect animated:NO];
                [[CustomJMap sharedInstance].mapView setZoomScale:0.2];
                
                [CustomJMap sharedInstance].mapContentSize=[CustomJMap sharedInstance].mapView.contentSize;
                [CustomJMap sharedInstance].mapOffset=[CustomJMap sharedInstance].mapView.contentOffset;
                [CustomJMap sharedInstance].zoomRect=theUnitsRect;
            }
            else
            {
                
                [CustomJMap sharedInstance].mapView.contentSize=[CustomJMap sharedInstance].mapContentSize;
                [CustomJMap sharedInstance].mapView.contentOffset=[CustomJMap sharedInstance].mapOffset;
                dispatch_async(dispatch_get_main_queue(), ^(void) {
                    [[CustomJMap sharedInstance].mapView zoomToRect:[CustomJMap sharedInstance].zoomRect animated:NO];
                    [[CustomJMap sharedInstance].mapView setZoomScale:0.2];
                    
                });
                
            }
            
            self.sideMenuAmenitiesTblView.amenitiesArray= [NSArray arrayWithArray:[CustomJMap sharedInstance].venueObject.amenities];
            self.sideMenuAmenitiesTblView.floorsArray=[[NSMutableArray alloc]initWithArray:floorsArray];
            self.sideMenuAmenitiesTblView.sideMenuDelegate=self;
            [self.sideMenuAmenitiesTblView setupTableView];
            [self.sideMenuAmenitiesTblView reloadData];
            [self setUpCurrentLevel];
            [self addButtonsOnTheView];
            
            
            if(_strStoreId!=nil)
            {
                for(JMapDestination *destination in [CustomJMap sharedInstance].venueObject.destinations){
                    NSString *idString =[NSString stringWithFormat:@"%@", destination.id];
                    // Match Store id passesd from Stores screen
                    if([idString isEqualToString:_strStoreId]){
                        [self storeSelectedValue:[[CustomJMap sharedInstance]getDictFromJMapDestination:destination] Type:kTo];
                        
                        JMapWaypoint  *waypoint = [[CustomJMap sharedInstance].mapView getWayPointByDestinationId:destination.id];
                        
                        [self zoomToWayPoint:waypoint];
                        
                        break;
                    }
                }
            }
        }
    }
    [self.indicator stopAnimating];
    self.indicator.hidden=YES;
    [self showMapIcons];
}


-(void)customJMapViewDidTapOnDestination:(NSArray *)thisDestinationArray destinationCenterPoint:(JMapPointOnFloor *)destinationCenterPoint
{
    if(lastFromWayPoint)
        lastFromWayPoint = nil;
    
    if(thisDestinationArray.count>0 && _destChooserView.hidden==YES){
        
        JMapDestination *destination= [thisDestinationArray objectAtIndex:0];
        [self storeSelectedValue:[[CustomJMap sharedInstance]getDictFromJMapDestination:destination] Type:kTo];
    }
}

-(void)AmenitiesSelected:(NSArray *)array
{
    [amenitiesSeltedArray removeAllObjects];
    [amenitiesSeltedArray addObjectsFromArray:array];
    
    [self showMapIcons];
    [self setUpCurrentLevel];
    [self amenitiesSideMenu:nil];
}
-(void)LevelSelected:(JMapFloor *)floor
{
    [[CustomJMap sharedInstance].mapView setLevelById:floor.mapId];
    [self setUpCurrentLevel];
}
-(void)showMapIcons
{
    NSMutableArray *combinedArr = [[NSMutableArray alloc] initWithArray:[CustomJMap sharedInstance].peopleMoverArray];
    [combinedArr addObjectsFromArray:amenitiesSeltedArray];
    [[CustomJMap sharedInstance].mapView showOnlyIconsTypeArray:combinedArr];
}
#pragma mark Add Markers Delegate Methods for showing image on To and From
// Add a popup card to map
-(void)addPopupCardToMap:(UIImage*)imageName wayPoint:(JMapWaypoint*)wayPoint isDestination:(BOOL)isDestination{
    
    // XY from Destination
    CGPoint centerPoint = CGPointMake(wayPoint.x.floatValue, wayPoint.y.floatValue);
    DemoPopUpVC *demoPopup = [[DemoPopUpVC alloc] initWithNibName:@"DemoPopUpVC" bundle:nil];
    UIImageView *imaview = [[UIImageView alloc]initWithImage:imageName];
    imaview.frame = CGRectMake(0, 0, 15, 15);
    [demoPopup.view addSubview:imaview];
    //    demoPopup.imgView.image = imageName;
    demoPopup.view.backgroundColor = [UIColor clearColor];
    //    demoPopup.view.frame = CGRectMake(0, 0, 20 , 20);
    demoPopup.view.center = centerPoint;
    [demoPopup.view setHidden:NO];
    
    UIView *demoView = demoPopup.view;
    [[CustomJMap sharedInstance].mapView addPopupCardToMap:demoView atXY:centerPoint frozenFrame:YES toFloor:[self   getMapViewCurrentFloor]];
    
    // Keep reference for later interaction
    if(isDestination)
        popViewForMarkerTo = demoPopup.view;
    else
        popViewForMarkerFrom = demoPopup.view;
}

#pragma  mark path generation delegate Method
-(void)displayWayFindFrom:(JMapWaypoint*)from to:(JMapWaypoint*)to withAccessibility:(NSNumber*)withAccessibility
{
       [NSObject cancelPreviousPerformRequestsWithTarget:self selector:@selector(changeFloor:) object:nil];

     [[[NSOperationQueue alloc]init] addOperationWithBlock:^{
        [[NSOperationQueue mainQueue] addOperationWithBlock:^{

            self.labelSource.text = @"";
            self.labelDestination.text = @"";
            // Remove all local views
            for(UIImageView *nextAnim in arrayOfAnimationViews)
            {
                [nextAnim.layer removeAllAnimations];
                [nextAnim removeFromSuperview];
            }
            [arrayOfAnimationViews removeAllObjects];
            
            for(DemoWayFindVC *nextFloorWayFind in _arrayOfWayFindViews)
            {
                [nextFloorWayFind.view removeFromSuperview];
            }
            // Remove all local views
            [_arrayOfWayFindViews removeAllObjects];
        }];
        //get list of waypoints
        _arrayOfWayFindFloors=[[CustomJMap sharedInstance].mapView findPathForWaypoint:from toWaypoint:to accessibility:withAccessibility];

        [self showMapIcons];
        
        [[NSOperationQueue mainQueue] addOperationWithBlock:^{
            [self.imgElevatorDirection.layer removeAllAnimations];
            self.imgElevatorDirection.hidden=NO;
            CGPoint pointStart = self.imgElevatorDirection.layer.position;
            
            CGPoint pointEnd = { self.lblAccebiltyLine.frame.origin.x + self.lblAccebiltyLine.frame.size.width, pointStart.y };
            
            CABasicAnimation *animationMove = [CABasicAnimation animationWithKeyPath:@"position.x"];
            animationMove.fromValue    = @(self.lblAccebiltyLine.frame.origin.x);
            animationMove.repeatCount = INFINITY;
            animationMove.toValue  = @(pointEnd.x);
            animationMove.duration   = 2.0;
            animationMove.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseIn];
            self.imgElevatorDirection.layer.position = pointEnd;
            [self.imgElevatorDirection.layer  addAnimation:animationMove forKey:@"position.x"];
            
            //----------------------------------------
            
            //get waypoints first and last Object
            JMapPathPerFloor *ffloor=[_arrayOfWayFindFloors firstObject];
            JMapPathPerFloor *lfloor=[_arrayOfWayFindFloors lastObject];
            
            //get level
            NSString *strfName=[self.sideMenuAmenitiesTblView getLevel:ffloor.seq];
            NSString *strLName=[self.sideMenuAmenitiesTblView getLevel:lfloor.seq];
            
            self.imgElevator.hidden=YES;
            if(![ffloor.seq isEqual:lfloor.seq])
            {
                self.imgElevator.hidden=NO;
                if( _btnAccessility.selected)
                {
                    self.imgElevator.image=[UIImage imageNamed:@"Elevators"];
                }
                else{
                    self.imgElevator.image=[UIImage imageNamed:@"Escalators"];
                }
            }
            
            //display it on labels
            self.labelSource.text = strfName;
            self.labelDestination.text = strLName;
            
            //get current floor
            [self setUpCurrentLevel];
            //----------------------------------------
            
            if([_arrayOfWayFindFloors count]>0)
            {
                float pathDistance =  [self animatePathWithImageview];
                if(pathDistance==0)
                {
                    pathDistance=2.5;
                }
                if(![lfloor.seq isEqual:ffloor.seq])
                    [self performSelector:@selector(changeFloor:) withObject:nil afterDelay:pathDistance+pathDistance];
                [self getTextDirectionArray];
            }
            self.heightContraint.constant=0;
            self.acsbiltyViewBottomCostraint.constant = 0;
        }];
    }];
}

-(CGFloat)animatePathWithImageview{
    
    float pathDistance = 0;
    //The frame we will zoom to later
    // Go through array of floors
    for(JMapPathPerFloor *nextPathPerFloor in _arrayOfWayFindFloors)
    {
        if([nextPathPerFloor.seq isEqual: [CustomJMap sharedInstance].mapView.currentFloor.floorSequence])
        {
            // Gather info
            // Floor sequence
            UIView *floorView = [[CustomJMap sharedInstance].mapView floorViewFromSequence:nextPathPerFloor.seq];
            // Number of points
            // animationTime
            
            //Initialize the UIView that will be added
            CGSize worldSize = [CustomJMap sharedInstance].mapView.worldSize;
            CGPoint centerXY = CGPointMake(worldSize.width / 2, worldSize.height / 2);
            
            
            CGPoint p1 = CGPointMake([((JMapASNode *)[nextPathPerFloor.points objectAtIndex:0]).x floatValue],
                                     [((JMapASNode *)[nextPathPerFloor.points objectAtIndex:0]).y floatValue]);
            
            for(int i = 1; i < [nextPathPerFloor.points count]; i++)
            {
                CGPoint p2 = CGPointMake([((JMapASNode *)[nextPathPerFloor.points objectAtIndex:i]).x floatValue],
                                         [((JMapASNode *)[nextPathPerFloor.points objectAtIndex:i]).y floatValue]);
                
                //Find distance between last point and p2
                CGFloat xDistance = (p1.x - p2.x);
                CGFloat yDistance = (p1.y - p2.y);
                CGFloat totalDistance = sqrt((xDistance * xDistance) + (yDistance * yDistance));
                pathDistance += totalDistance;
                p1 = p2;
            }
            
            // Prepare Demo View to insert on top of map
            DemoWayFindVC *demoNextFloor = [[DemoWayFindVC alloc] initWithNibName:@"DemoWayFindVC" bundle:nil];
            demoNextFloor.view.frame = CGRectMake(0, 0, worldSize.width, worldSize.height);
            [demoNextFloor dataLoad:[nextPathPerFloor copy] withDistance:[NSNumber numberWithFloat:pathDistance]];
            [demoNextFloor.view setHidden:NO];
            
            // Where to load
            [[CustomJMap sharedInstance].mapView addWayFindView:demoNextFloor.view atXY:centerXY forFloorId:floorView];
            // Add to self (not necessery but will be useful in case of need for animation...
            [_arrayOfWayFindViews addObject:demoNextFloor];
        }
    }
    if(fromWaypoint)
    {
        [self zoomToWayPoint:fromWaypoint];
    }
    if(pathDistance==0)
    {
        return pathDistance;
    }
    return pathDistance/100;
}
-(void)zoomToWayPoint:(JMapWaypoint *)point   {
    //point obtained from tap, convert NSValue -> CGPoint
    //Create rect with X and Y, add desired padding
    CGRect zoomRect = CGRectMake([point.x floatValue] - 225, [point.y floatValue] - 225, 450, 450);
    [[CustomJMap sharedInstance].mapView resetRotation];
    [[CustomJMap sharedInstance].mapView zoomToRect:zoomRect animated:NO];
}

//Add method changeFloor:
-(void)changeFloor:(id)sender
{
    if([self.arrayOfWayFindFloors count]>1)
    {
        [[NSOperationQueue mainQueue] addOperationWithBlock:^{

            JMapPathPerFloor *nextPathPerFloor=[self.arrayOfWayFindFloors lastObject];
            [[CustomJMap sharedInstance].mapView setLevelById:nextPathPerFloor.mapId];
            float pathDistance =  [self animatePathWithImageview];
            [self setUpCurrentLevel];
        }];
    }
    if(toWaypoint)
    {
        [self zoomToWayPoint:toWaypoint];
    }
}
#pragma  mark Get Text Direction  Method
-(void)getTextDirectionArray{
    [directionQueue cancelAllOperations ];
    [directionQueue addOperationWithBlock:^{
        // Dsiplay Text Direction for to and from
        // Use custom thresh holds
        UIKitHelperCustomThresholds *newThresholds = [[UIKitHelperCustomThresholds alloc] init];
        newThresholds.forwardFrom = [NSNumber numberWithFloat:-25.1];
        newThresholds.forwardTo = [NSNumber numberWithFloat:25.1];
        arrayOfTextDirectionArray = [[CustomJMap sharedInstance].mapView makeTextDirections:self.arrayOfWayFindFloors filterOn:YES addTDifEmptyMeters:30.0 UTurnInMeters:30.0 customThreshHolds:newThresholds];
        // code the updates the main thread (UI) here, for example...
        NSMutableArray *newArrayForTextDir = [[NSMutableArray alloc]init];
        if(arrayOfTextDirectionArray.count>0)
        {
            for(int i=0; i<arrayOfTextDirectionArray.count; i++){
                NSArray *new = arrayOfTextDirectionArray[i];
                for(JMapTextDirectionInstruction *txt in new){
                    [newArrayForTextDir addObject:txt.output];
                }
            }
        }
         [[NSOperationQueue mainQueue] addOperationWithBlock:^{
            _textDirView.arrData=nil;
            _textDirView.arrData=[[NSMutableArray alloc]initWithArray:newArrayForTextDir];
            [_textDirView loadData];
     }];
 }];
}


#pragma mark - Setup of Senion SDK

-(void)bluetoothSetting
{
    UIAlertController * alert=   [UIAlertController
                                  alertControllerWithTitle:@""
                                  message:@"Turn on Bluetooth"
                                  preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction* yesButton = [UIAlertAction
                                actionWithTitle:@"Ok"
                                style:UIAlertActionStyleDefault
                                handler:^(UIAlertAction * action)
                                {
                             [alert dismissViewControllerAnimated:YES completion:nil];
                                    connectionPicker = [[GKPeerPickerController alloc] init];
                                    connectionPicker.delegate = self;
                                    [connectionPicker show];
//                                    [connectionPicker dismiss];
                              }];
    [alert addAction:yesButton];
    [self presentViewController:alert animated:YES completion:nil];
}

#pragma mark - GUI

- (void)initGui
{
    NSLog(@"Init GUI");
    self.slMapView = [SLMapView sharedInstance];
    
    self.slMapView.delegate = self;
    [self.slMapView loadLocationManagerWithMapKey:MapKey andCustomerId:CustomerId];

    [self.slMapView startUpdatingLocation];
    
}


#pragma mark - updateUserLocation Using JMap
-(void)updateUserLocation
{
    NSLog(@"ENTERED IN updateUserLocation and location floor is %d",self.location.floorNr);
    
    NSLog(@"SLMap Coordinates are %@",[SLMapView sharedInstance].coordinates);
    //user live location
    JMapFloor *jflor = [JMapFloor new];
    if(self.location.floorNr ==0){
        jflor = [floorsArray objectAtIndex:0];
    }
    else if(self.location.floorNr ==1){
        jflor = [floorsArray objectAtIndex:1];
    }
    else if(self.location.floorNr ==2){
        jflor = [floorsArray objectAtIndex:2];
    }
    else if(self.location.floorNr ==3){
        jflor = [floorsArray objectAtIndex:3];
    }
    //newXY is the (x,y) coordinates on the map
    //z value should be the user location's mapId int value, use current floor for now
    NSLog(@"jfloor is %@",jflor);
//    
    [[[CustomJMap sharedInstance]mapView] setUserLocationScale:1];
    [[[CustomJMap sharedInstance]mapView] setConfidenceAlpha:0.5];
    [[[CustomJMap sharedInstance]mapView] setInnerPulseColor:[UIColor blueColor]];
    [[[CustomJMap sharedInstance]mapView] setStartAlpha:0];
    [[[CustomJMap sharedInstance]mapView] setEndAlpha:1];
    [[[CustomJMap sharedInstance]mapView] setOuterPulseColor:[UIColor colorWithRed:0.000 green:0.478 blue:1.000 alpha:1]];
    [[[CustomJMap sharedInstance]mapView] setPulseAnimationDuration:3];
    [[[CustomJMap sharedInstance]mapView] setAnnotationColor:[UIColor clearColor]];
    
//    senionPosition = JMAPPointMake([SLMapView sharedInstance].coordinates.x, [SLMapView sharedInstance].coordinates.y, jflor.mapId.doubleValue);
     senionPosition = JMAPPointMake(2665.209875, 2644.796614, 6000);
   // x: 2665.209875, y: 2644.796614 ,6000
    
    [[[CustomJMap sharedInstance]mapView] updateCurrentLocationWithFloor:jflor.mapId.stringValue currentPosition:senionPosition orientation:90 confidence:0.5 speed:0.5];
    
    fromWaypoint = [[CustomJMap sharedInstance].mapView getNearestWayPointForLocationOnMap:senionPosition];
    
    
    
    NSLog(@"NEAREST WAYPOINT FROM SENION LOCATION X=%@,Y=%@",fromWaypoint.x,fromWaypoint.y);
    [[[CustomJMap sharedInstance]mapView] setLevelById:fromWaypoint.mapId];
    
    JMAPPoint userPosition = JMAPPointMake([(fromWaypoint.x)doubleValue], [(fromWaypoint.y)doubleValue], fromWaypoint.mapId.doubleValue);
    NSLog(@"NEAREST USER POSITION X = %f,Y=%f",userPosition.x,userPosition.y);

    
    [self setupDefaultBGColor];
    [self setupSelectedBGColor:[arrLevelsButtons objectAtIndex:self.location.floorNr]];
//    [self customYAHStyling:senionPosition];
    
    //storing user location
    NSMutableDictionary *idDict = [[NSMutableDictionary alloc]init];
    NSString *str =[NSString stringWithFormat:@"%@",fromWaypoint.mapId];
    [idDict setValue:str forKey:@"Id"];
    self.fromDict = [idDict mutableCopy];
    
    [self storeSelectedValue:self.fromDict Type:kFrom];
    
    
    NSLog(@"fromDict %@",self.fromDict);
    NSLog(@"fromwaypoint %@",fromWaypoint.mapId);
    
}

#pragma mark - Anotation Pin for users current location

-(void)customYAHStyling:(JMAPPoint)yahPosition {
//    NSLog(@"blue dot drawn");
//    NSLog(@"jmappoint yahPosition x-%f,y-%f,z-%f",yahPosition.x,yahPosition.y,yahPosition.z);
//    CGPoint centerPoint = CGPointMake(yahPosition.x, yahPosition.y);
//    DemoPopUpVC *demoPopup = [[DemoPopUpVC alloc] initWithNibName:@"DemoPopUpVC" bundle:nil];
//    UIImageView *blueCircle = [[UIImageView alloc]initWithImage:sourceMarker];
//    blueCircle.frame = CGRectMake(0, 0, 15, 15);
//    [demoPopup.view addSubview:blueCircle];
//    demoPopup.view.backgroundColor = [UIColor clearColor];
//    demoPopup.view.center = centerPoint;
//    [demoPopup.view setHidden:NO];
//    
//    [[[CustomJMap sharedInstance]mapView] addSubview:blueCircle];
//    self.heightContraint.constant=90;
//    
//    self.mapOverlayView = [SLMapOverlayView new];
//    self.mapOverlayView.backgroundColor = [UIColor redColor];
//    self.mapOverlayView.userInteractionEnabled = YES;
//    self.mapOverlayView.layer.anchorPoint = CGPointMake(0.5, 0.5);
//    self.mapOverlayView.frame = CGRectMake(0,0, 320, 30);
//    
//    [self.contentView addSubview:self.mapOverlayView];

}

#pragma mark - SLMapViewDelegate

- (void)mapViewDidFinishLoadingManager:(SLMapView *)mapView
{
    NSLog(@" ENTERED IN mapViewDidFinishLoadingManager ");
    NSLog(@"lat %fand long %f",_slMapView.location.latitude,_slMapView.location.longitude);
}

- (void)mapView:(SLMapView *)mapView didFailLoadingManagerWithError:(NSString *)errorMessage
{
    NSLog(@" ENTERED IN didFailLoadingManagerWithError ");
    [[[UIAlertView alloc] initWithTitle:@"Error" message:errorMessage delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil] show];
}

- (void)mapView:(SLMapView *)mapView didSwitchToFloor:(FloorNr)floorNr
{
//    self.navigationItem.title = [NSString stringWithFormat:@"%@ - %@", self.slMapView.buildingInfo.name, [[self.slMapView.buildingInfo getFloorInfo:floorNr] floorName]];
}

-(void)didFindUserLocation
{
    NSLog(@" ENTERED IN didFindUserLocation");
    [self updateUserLocation];
}

@end
