//
//  TextDirectionView.h
//  MOE
//
//  Created by varsha on 6/9/16.
//  Copyright © 2016 Neosoft. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TextDirectionView : UIView
@property (weak, nonatomic) IBOutlet UITableView *tblView;
@property (strong, nonatomic) NSMutableArray *arrData;
-(void)loadData;
@end
