//
//  CustomSideMenuTable.m
//  MOE
//
//  Created by webwerks on 7/8/16.
//  Copyright © 2016 Neosoft. All rights reserved.
//

#import "CustomSideMenuTable.h"
#import "SideMenuAmenitiesTableCell.h"
#import <JMap/JMap.h>
#import "CustomJMap.h"
#import "JibeStreamConstant.h"
@interface CustomSideMenuTable()<UITableViewDelegate,UITableViewDataSource>
{
    NSMutableArray *amenitiesSeltedArray;
    NSMutableArray *sectionIndexPathZeroSelectedRow;
}
@end
@implementation CustomSideMenuTable
-(void)setupTableView
{
    amenitiesSeltedArray=[[NSMutableArray alloc]init];
    sectionIndexPathZeroSelectedRow=[[NSMutableArray alloc]init];

    // Load table cell for Side menu of amenities
    [self registerNib:[UINib nibWithNibName:@"SideMenuAmenitiesTableCell" bundle:nil] forCellReuseIdentifier:@"SideMenuAmenitiesTableCell"];
    self.delegate   =self;
    self.dataSource   =self;
}
#pragma mark UITableView Data Source Methods for Side Menu tableview
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 2;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if(section==0)
        return  self.amenitiesArray.count;
    if(section==1)
        return self.floorsArray.count;
    return 0;
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *identifier = @"SideMenuAmenitiesTableCell";
    SideMenuAmenitiesTableCell *cell= [self dequeueReusableCellWithIdentifier:identifier];
    
    cell.selectionStyle=UITableViewCellSelectionStyleNone;
    switch (indexPath.section) {
        case 0:
        {
            if ([sectionIndexPathZeroSelectedRow containsObject:indexPath]) {
                [cell setBackgroundColor:[UIColor grayColor]];
            }
            else
            {
                [cell setBackgroundColor:[UIColor clearColor]];
            }
            cell.iconImage.opaque = NO;
            
            cell.lblAmName.hidden=YES;
            cell.iconImage.hidden=NO;
            JMapAmenity *amenity = [self.amenitiesArray objectAtIndex:indexPath.row];
            cell.amenitiesNameLabel.text=amenity.description;
            NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@",strJibeServerURL, amenity.filePath]];
            [cell.iconImage setBackgroundColor:[UIColor clearColor]];
            NSURLRequest *request = [NSURLRequest requestWithURL:url];
            if(request)
                [cell.iconImage loadRequest:request];
        }
            break;
        case 1:
        {
            JMapFloor *floor = [self.floorsArray objectAtIndex:indexPath.row];
            [cell setBackgroundColor:[UIColor clearColor]];
            if ([[[CustomJMap sharedInstance].mapView getCurrentFloor]  isEqual:floor] )
            {
                [cell setBackgroundColor:[UIColor grayColor]];
            }
            
            if([floor.name isEqualToString:@"Third Floor"]) {
                cell.amenitiesNameLabel.text = @"Level Three";
            }
            else {
                cell.amenitiesNameLabel.text=floor.name;
            }
            
            cell.lblAmName.hidden=NO;
            cell.iconImage.hidden=YES;
            NSString *strfName=[self getLevel:floor.floorSequence];
            cell.lblAmName.text=strfName;
            [cell.lblAmName setBackgroundColor:[UIColor redColor]];
            
            cell.lblAmName.layer.cornerRadius = 4;
            cell.lblAmName.clipsToBounds = YES;
            cell.lblAmName.layer.borderColor = [UIColor clearColor].CGColor;
            
            
        }
        default:
            break;
    }
    return cell;
}
-(NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section{
    
    if(section==0)
        return @"Amenities";
    else
        return @"Levels";
}
- (void)tableView:(UITableView *)tableView willDisplayHeaderView:(UIView *)view forSection:(NSInteger)section
{
    UITableViewHeaderFooterView *header = (UITableViewHeaderFooterView* )view;
    header.textLabel.font = [UIFont boldSystemFontOfSize:17];
    header.textLabel.textAlignment=NSTextAlignmentLeft;
    header.contentView.backgroundColor=[UIColor colorWithRed:200.0f/255.0f green:200.0f/255.0f blue:200.0f/255.0f alpha:1.0f];
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    switch (indexPath.section) {
        case 0:
        {
            // manage selected amenities from list and plot them on map
            JMapAmenity *amenity = [self.amenitiesArray objectAtIndex:indexPath.row];
            if ([sectionIndexPathZeroSelectedRow containsObject:indexPath])
            {
                [sectionIndexPathZeroSelectedRow removeAllObjects];
                [amenitiesSeltedArray removeAllObjects];
            }
            else if([sectionIndexPathZeroSelectedRow count]==0)
            {
                [sectionIndexPathZeroSelectedRow addObject:indexPath];
                [amenitiesSeltedArray addObject:amenity.description.lowercaseString];
            }
            else
            {
                [sectionIndexPathZeroSelectedRow removeAllObjects];
                [amenitiesSeltedArray removeAllObjects];

                [sectionIndexPathZeroSelectedRow addObject:indexPath];
                [amenitiesSeltedArray addObject:amenity.description.lowercaseString];
            }
         
            if([amenitiesSeltedArray count]>0)
            {
                 //Check to see if current floor has amenity
                NSMutableArray *hasOnFloor = [[NSMutableArray alloc] init];
                NSMutableArray *floorName = [[NSMutableArray alloc] init];

                BOOL onFloor = false;
                NSArray *amenitiesWaypoints = amenity.waypoints;
                for (JMapWaypoint *waypoint in amenitiesWaypoints) {
                    if (waypoint.mapId.intValue == [[CustomJMap sharedInstance].mapView getCurrentFloor].mapId.intValue) {
                        onFloor = true;
                    }
                    else {
                        //Map id stored as number
                        //Check if already added
                        if (![hasOnFloor containsObject:waypoint.mapId])
                        {
                            [hasOnFloor addObject:waypoint.mapId];
                            
                            for (JMapFloor *floorObj  in self.floorsArray)
                            {
                                if ([waypoint.mapId isEqual:floorObj.mapId])
                                {
                                    NSString *strfName=[self getLevel:floorObj.floorSequence ];
                                    [floorName addObject:strfName];
                                }
                            }
                            
                        }
                    }
                }
                
                if(!onFloor && [floorName count]>0)
                {
                    NSString *strMessage=[NSString stringWithFormat:@"%@ amenities available on %@ floors",amenity.description,[floorName componentsJoinedByString:@","]];
                    [APP_DELEGATE PlayProgressHudForStoreLocator:strMessage  View:nil];
                 }
            }
             if ([self.sideMenuDelegate respondsToSelector:@selector(AmenitiesSelected:)] ) {
                [self.sideMenuDelegate AmenitiesSelected:amenitiesSeltedArray];
            }
        }
            break;
        case 1:
        {
            // Set map to selected floor from table list
            JMapFloor *currentLevel = [self.floorsArray objectAtIndex:indexPath.row];
            if ([self.sideMenuDelegate respondsToSelector:@selector(LevelSelected:)] ) {
                [self.sideMenuDelegate LevelSelected:currentLevel];
            }
        }
        default:
            break;
    }
    [self reloadData];
}
-(NSString *)getLevel:(NSNumber *)seq
{
    if([seq intValue]==1)
    {
        return  @"GF";
    }
    else  if([seq intValue]==2)
    {
        return  @"L1";
    }
    else  if([seq intValue]==3)
    {
        return  @"L2";
    }
    else  if([seq intValue]==4)
    {
        return  @"L3";
    }
    else  if([seq intValue]==5)
    {
        return  @"L4";
    }
    else  if([seq intValue]==6)
    {
        return  @"L5";
    }
    return nil;
}
@end
