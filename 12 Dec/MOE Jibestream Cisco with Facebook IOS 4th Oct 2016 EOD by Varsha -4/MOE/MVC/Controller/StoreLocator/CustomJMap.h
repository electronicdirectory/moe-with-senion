//
//  CustomJMap.h
//  MOE
//
//  Created by webwerks on 7/5/16.
//  Copyright © 2016 Neosoft. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <JMap/JMap.h>

@protocol CustomJMapDelegate <NSObject>
-(void)customJMapViewDataReady;
-(void)customJMapViewDidTapOnDestination:(NSArray *)thisDestinationArray destinationCenterPoint:(JMapPointOnFloor *)destinationCenterPoint;
@end
@interface CustomJMap : NSObject<JMapDelegate,JMapDataSource>
+ (instancetype)sharedInstance;
@property (weak)id <CustomJMapDelegate> jMDelegate;
@property(nonatomic,strong)JMapContainerView *mapView;
@property(nonatomic,strong)NSMutableArray *arrVenueList;
@property(nonatomic,strong)NSArray *peopleMoverArray;



@property (nonatomic, strong) JMapVenue *venueObject;
@property (assign) CGSize mapContentSize;
@property (assign) CGPoint mapOffset;
@property (assign) CGRect zoomRect;

-(void)initMapView;
-(void)loadJMapData;
-(NSMutableDictionary *)getDictFromJMapDestination:(JMapDestination *)destination;
@end
