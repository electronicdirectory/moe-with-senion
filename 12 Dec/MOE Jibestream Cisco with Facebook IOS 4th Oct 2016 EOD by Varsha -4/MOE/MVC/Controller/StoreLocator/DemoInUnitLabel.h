//
//  DemoInUnitLabel.h
//  MOE
//
//  Created by varsha on 6/2/16.
//  Copyright © 2016 Neosoft. All rights reserved.
//

#import <UIKit/UIKit.h>

#define hideAfterSize            17.0


@interface DemoInUnitLabel : UIViewController<UIWebViewDelegate>

@property (weak, nonatomic) IBOutlet UILabel *textLabel;
@property (strong, nonatomic) IBOutlet UIWebView *webView;

-(void)contentScaleFactorChanged:(NSNumber *)newScale;
-(void)contentScaleFactorChanged:(NSNumber *)newScale atMax:(BOOL)atMax;

@end
