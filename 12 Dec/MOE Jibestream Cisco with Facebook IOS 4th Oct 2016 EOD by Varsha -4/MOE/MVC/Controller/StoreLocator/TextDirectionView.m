//
//  TextDirectionView.m
//  MOE
//
//  Created by varsha on 6/9/16.
//  Copyright © 2016 Neosoft. All rights reserved.
//

#import "TextDirectionView.h"
#import "DirectionTableViewCell.h"
@implementation TextDirectionView

#define  cellID @"DirectionTableViewCell"
-(void)loadData
{
    [self.tblView registerNib:[UINib nibWithNibName:cellID bundle:nil] forCellReuseIdentifier:cellID];
    [self.tblView reloadData];
}

#pragma mark - Table view data source
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.arrData count];
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    DirectionTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellID forIndexPath:indexPath];
    [self setUpCell:cell atIndexPath:indexPath];
    cell.imgView.layer.cornerRadius = cell.imgView.frame.size.height/2;
    cell.imgView.clipsToBounds = YES;
    cell.imgView.layer.borderColor = [UIColor clearColor].CGColor;
    cell.selectionStyle=UITableViewCellSelectionStyleNone;
    return cell;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static DirectionTableViewCell *cell = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        cell = [self.tblView dequeueReusableCellWithIdentifier:cellID];
    });
    [self setUpCell:cell atIndexPath:indexPath];
    return [self calculateHeightForConfiguredSizingCell:cell];
}
- (CGFloat)calculateHeightForConfiguredSizingCell:(UITableViewCell *)sizingCell {
    [sizingCell layoutIfNeeded];
    
    CGSize size = [sizingCell.contentView systemLayoutSizeFittingSize:UILayoutFittingCompressedSize];
    return size.height;
}
- (void)setUpCell:(DirectionTableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath {
    
     float width=[UIScreen mainScreen].bounds.size.width-(40);
    cell.lblTitle.preferredMaxLayoutWidth = width; // This is necessary to allow the label to use
    cell.lblTitle.text=[self.arrData objectAtIndex:indexPath.row];
    
}
@end
