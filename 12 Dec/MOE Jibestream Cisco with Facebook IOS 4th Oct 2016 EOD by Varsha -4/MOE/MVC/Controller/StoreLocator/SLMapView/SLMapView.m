//
//  SLMapView.m
//  SLDemo
//
//  Copyright (c) 2010-2016 Senion. All rights reserved.
//

#import "SLMapView.h"
#import "CenterMapButtonView.h"
#import "BuildingInfoLoader.h"
#import "StoreLocatorViewController.h"
static double MaxWidthInMeters = 100;

@interface SLMapView ()

@property (nonatomic) CGRect oldFrame;
@property (nonatomic, strong) SLMapOverlayView *mapOverlayView;
@property (nonatomic, strong) BuildingInfoLoader *buildingInfoLoader;

@property (nonatomic) BOOL startLocationUpdates;

@property (nonatomic, strong) UIButton *compassButton;
@property (nonatomic, strong) CenterMapButtonView *centerButton;
//@property (nonatomic, strong) CircularMapButtonView *navigateButton;
@property (nonatomic, strong) UIImageView *mapLogoImageView;


//@property (nonatomic, strong) SLPointOfInterest *selectedPointOfInterest;
@property (nonatomic, strong) SLPath *path;

@property (nonatomic, strong) UIActionSheet *navigateActionSheet;


@property (nonatomic) BOOL dragging;
@property (nonatomic) BOOL rotating;
@property (nonatomic) BOOL scaling;

@property (nonatomic) CGPoint originalCenter;

@property (nonatomic) CGFloat zoomScale;
@property (nonatomic) CGFloat previousZoomScale;
@property (nonatomic) CGFloat maximumZoomScale;
@property (nonatomic) CGFloat minimumZoomScale;

@property (nonatomic) UIEdgeInsets contentInset;

@end

@implementation SLMapView

+(instancetype)sharedInstance
{
    
    static SLMapView *slObj= nil;
    
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        slObj =[[SLMapView alloc]init];
    });
    return slObj;
}

- (instancetype)init
{
    self = [super init];
    if (self) {
        [self baseInit];
    }
    return self;
}

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self baseInit];
    }
    return self;
}

- (instancetype)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self) {
        [self baseInit];
    }
    return self;
}

- (void)baseInit
{
    _isLocationManagerLoaded = NO;
    self.startLocationUpdates = NO;
    
    self.buildingInfoLoader = [BuildingInfoLoader new];
    _buildingInfo = [self.buildingInfoLoader loadBuildingInfo];
    
    self.mapOverlayView = [SLMapOverlayView new];
    self.mapOverlayView.backgroundColor = [UIColor yellowColor];
    self.mapOverlayView.userInteractionEnabled = YES;
    self.mapOverlayView.layer.anchorPoint = CGPointMake(0.5, 0.5);
    self.mapOverlayView.heading = YES;

    
    [self setHeading:YES];
    
    [self addSubview:self.mapOverlayView];
   
    self.currentFloorNrPlotted = [self.buildingInfo getDefaultFloorNr];
    
    [self.mapOverlayView setCurrentFloorNr:self.currentFloorNrPlotted andPixelsPerMeter:[[self.buildingInfo getFloorInfo:self.currentFloorNrPlotted] pixelsPerMeter]];

}


- (void)changeFloorImageToFloorNr:(FloorNr)floorNr
{
    
    NSLog(@"floor no %d",floorNr);
    if (floorNr != self.currentFloorNrPlotted) {
        
        self.currentFloorNrPlotted = floorNr;
        
        SLCoordinate3D *coordinate = [self.buildingInfo SLPixelPoint3DToSLCoordinate3D:[[SLPixelPoint3D alloc] initWithX:self.coordinates.x andY:self.coordinates.y andFloorNr:self.currentFloorNrPlotted]];
        
        coordinate.floorNr = floorNr;
        
    }
    else
        
    {
        SLCoordinate3D *coordinate = [self.buildingInfo SLPixelPoint3DToSLCoordinate3D:[[SLPixelPoint3D alloc] initWithX:self.coordinates.x andY:self.coordinates.y andFloorNr:self.currentFloorNrPlotted]];
        
        coordinate.floorNr = floorNr;
        
    }
//    if (self.delegate && [self.delegate respondsToSelector:@selector(mapView:didSwitchToFloor:)]) {
//        [self.delegate mapView:self didSwitchToFloor:floorNr];
//    }

 }

- (void)setCurrentFloorNrPlotted:(FloorNr)currentFloorNrPlotted
{
    _currentFloorNrPlotted = currentFloorNrPlotted;
}

#pragma mark - Update positioning

- (void)setLocation:(SLCoordinate3D *)location withUncertaintyradius:(double)uncertaintyradius
{
     NSLog(@"Set location %f,%f,%d",location.latitude,location.longitude,location.floorNr);
    _location = location;
    _uncertaintyRadius = uncertaintyradius;
    
       if (self.currentFloorNrPlotted != self.location.floorNr) {
        NSLog(@"location floor %d",self.location.floorNr);
        [self changeFloorImageToFloorNr:self.location.floorNr];
    }
    
    [self SLCoordinate3DToSLPixelPoint3D:location];
}

- (void)setHeading:(double)heading;
{
    _heading = heading;
    //initWithX:2665.209875 andY:2644.796614 andFloorNr:0];

    _location =[[SLCoordinate3D alloc]initWithLatitude:2665.209875 andLongitude:2644.796614 andFloorNr:0];
    
    double pixHeading;
    if (_location) {
        pixHeading = 50;
    } else {
        pixHeading = [self.buildingInfo heading2PixelHeading:self.heading andFloorNr:self.currentFloorNrPlotted];
    }
    
    [self.mapOverlayView setHeading:pixHeading];
}


#pragma mark - SLMapOverlayView

- (BOOL)animatePositionUpdate
{
    return self.mapOverlayView.animatePositionUpdate;
}

- (void)animatePositionUpdate:(BOOL)animate
{
    self.mapOverlayView.animatePositionUpdate = animate;
}



#pragma mark - SLIndoorLocationManager

- (void)loadLocationManagerWithMapKey:(NSString *)mapKey andCustomerId:(NSString *)customerId
{
    if (self.locationManager) {
        self.locationManager.delegate = nil;
        _locationManager = nil;
    }
    
    _isLocationManagerLoaded = NO;
    _locationManager = [[SLIndoorLocationManager alloc] initWithMapKey:mapKey andCustomerId:customerId];
    self.locationManager.delegate = self;
}

- (void)startUpdatingLocation
{
    if (!self.locationManager) {
        [[[UIAlertView alloc] initWithTitle:@"Error" message:@"SLIndoorLocationManager must be loaded before you can start updateing location!" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil] show];
    }
    
    if (self.isLocationManagerLoaded) {
        self.mapOverlayView.showLocation = YES;
        [self.locationManager startUpdatingLocation];
    } else {
        self.startLocationUpdates = YES;
    }
}

- (void)stopUpdatingLocation
{
    if (self.locationManager) {
        [self.locationManager stopUpdatingLocation];
        [self.mapOverlayView clearLocation];
       
    }
}


#pragma mark - SLIndoorLocationManagerDelegate

- (void)didUpdateLocation:(SLCoordinate3D*)location withUncertainty:(double)radius
{
    NSLog(@"didupdate location %f,%f,%d",location.latitude,location.longitude,location.floorNr);
    
    [self setLocation:location withUncertaintyradius:radius];
}

- (void)didUpdateLocationAvailability:(SLLocationAvailability)locationAvailability
{
    if (locationAvailability == SLLocationAvailabilityAvailable) {
        self.mapOverlayView.showLocation = YES;
        NSLog(@"location available" );
        
    } else {
        self.mapOverlayView.showLocation = NO;
        NSLog(@"location not available");
        
    }
}

- (void)didUpdateHeading:(double)heading withStatus:(BOOL)status
{
    [self setHeading:heading];
}

- (void)didUpdateMotionType:(SLMotionState)motionState {};


- (void)didFinishLoadingManager
{
    NSLog(@"enter in did finish loading");
    _isLocationManagerLoaded = YES;
    if (self.startLocationUpdates) {
        self.mapOverlayView.showLocation = YES;
        [self.locationManager startUpdatingLocation];
        NSLog(@"start updates");
    }
    
//    if (self.delegate && [self.delegate respondsToSelector:@selector(mapViewDidFinishLoadingManager:)]) {
//        [self.delegate mapViewDidFinishLoadingManager:self];
//    }

}

- (void)didFailInternetConnectionWithError:(NSError *)error
{
    if (self.delegate && [self.delegate respondsToSelector:@selector(mapView:didFailLoadingManagerWithError:)]) {
        [self.delegate mapView:self didFailLoadingManagerWithError:@"Please enable the internet connection!"];
    }
}

- (void)didFailInvalidIds:(NSError *)error
{
    if (self.delegate && [self.delegate respondsToSelector:@selector(mapView:didFailLoadingManagerWithError:)]) {
        [self.delegate mapView:self didFailLoadingManagerWithError:@"Invalid customerId/mapKey!"];
    }
}

- (void) didFailScanningBT
{
    if (self.delegate && [self.delegate respondsToSelector:@selector(mapView:didFailLoadingManagerWithError:)]) {
        [self.delegate mapView:self didFailLoadingManagerWithError:@"Please enable Bluetooth!"];
    }
}

- (void) didFailLocationAuthorizationStatus
{
    if (self.delegate && [self.delegate respondsToSelector:@selector(mapView:didFailLoadingManagerWithError:)]) {
        [self.delegate mapView:self didFailLoadingManagerWithError:@"Location services is disabled. Please go to Settings -> Privacy -> Location Services. Then, select this app and choose Always."];
    }
}


#pragma mark - Converasion of Cordinate to Pixel

- (SLPixelPoint3D *)SLCoordinate3DToSLPixelPoint3D:(SLCoordinate3D *)location
{
    NSLog(@"Entered in SLCoordinate3DToSLPixelPoint3D");
    NSLog(@"location to convert %@",location);
    
    FloorInfo *f = [self.buildingInfo getFloorInfo:location.floorNr];
    
    // This function makes a local approximation and should not be used over long distances
    const double a = 6378137; // Equatorial earth radius
    const double b = 6356752.314245; // Polar earth radius
    const double r = 0.5*(a+b); // Use average earth radius in this function
    
    double deltaN =  (location.latitude  - f.bitmapLocation.latitude) * M_PI / 180*r;
    double deltaW = -(location.longitude - f.bitmapLocation.longitude)* M_PI / 180*r*cos(f.bitmapLocation.latitude*M_PI/180.0);
    double deltaX = cos(f.bitmapOrientation*M_PI/180.0)*deltaN - sin(f.bitmapOrientation*M_PI/180.0)*deltaW;
    double deltaY = sin(f.bitmapOrientation*M_PI/180.0)*deltaN + cos(f.bitmapOrientation*M_PI/180.0)*deltaW;
    
    double x = -deltaY*f.pixelsPerMeter + f.bitmapOffset.x;
    double y = -deltaX*f.pixelsPerMeter + f.bitmapOffset.y;
    NSLog(@"x and y = %f,%f and floor No. %d",x,y,location.floorNr);
    
    [self.locationManager stopUpdatingMockupLocation];
    self.coordinates =[[SLPixelPoint3D alloc] initWithX:x andY:y andFloorNr:location.floorNr];
    
    NSLog(@"SLCOORDINATES from SLCoordinate3DToSLPixelPoint3D coordinates = %@", self.coordinates);
    
    if ([_delegate respondsToSelector:@selector(didFindUserLocation)]) {
        [_delegate didFindUserLocation];
    }
    
    return [[SLPixelPoint3D alloc] initWithX:x andY:y andFloorNr:location.floorNr];
}





    // x, y are now the pixel coordinates of the lat/long in the Jibestream images, should be sent to Jibestream SDK for plotting
    
    
//    
//    CGPoint point = new CGPoint(x,y);
//    
//    return point;

@end
