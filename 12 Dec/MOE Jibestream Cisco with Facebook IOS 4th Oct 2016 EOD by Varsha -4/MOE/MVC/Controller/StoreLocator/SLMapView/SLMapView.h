//
//  SLMapView.h
//  SLDemo
//
//  Copyright (c) 2010-2016 Senion. All rights reserved.
//

#import <UIKit/UIKit.h>

#import <SLIndoorLocation/SLIndoorLocation.h>
#import "SLMapOverlayView.h"

@class SLMapView;
@class BuildingInfo;
@class BuildingInfoLoader;

@protocol SLMapViewDelegate <NSObject>

- (void)mapViewDidFinishLoadingManager:(SLMapView *)mapView;
- (void)mapView:(SLMapView *)mapView didFailLoadingManagerWithError:(NSString *)errorMessage;

- (void)mapView:(SLMapView *)mapView didSwitchToFloor:(FloorNr)floorNr;

-(void)didFindUserLocation;
@end

@interface SLMapView : UIView <UIActionSheetDelegate, SLIndoorLocationManagerDelegate, UIGestureRecognizerDelegate>

@property (weak) id<SLMapViewDelegate> delegate;

@property (nonatomic, strong, readonly) BuildingInfo *buildingInfo;
@property (nonatomic, strong, readonly) SLIndoorLocationManager *locationManager;
@property (nonatomic, readonly) BOOL isLocationManagerLoaded;

@property (nonatomic, readonly) FloorNr currentFloorNrPlotted;
@property (nonatomic, strong, readonly) SLCoordinate3D *location;
@property (nonatomic, readonly) double uncertaintyRadius;
@property (nonatomic, readonly) double heading;

@property (nonatomic) BOOL animatePositionUpdate;
@property (nonatomic, strong) NSArray *pointsOfInterest;

@property(nonatomic,strong)SLPixelPoint3D *coordinates;

- (void)loadLocationManagerWithMapKey:(NSString *)mapKey andCustomerId:(NSString *)customerId;
- (void)startUpdatingLocation;
- (void)stopUpdatingLocation;
+(instancetype)sharedInstance;
- (void)startMockupLocationWithLocationStateArray:(NSArray *)locationStateArray andTimeInterval:(double)timeInterval;
@end
