//
//  DirectionTableViewCell.h
//  MOE
//
//  Created by webwerks on 6/23/16.
//  Copyright © 2016 Neosoft. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DirectionTableViewCell : UITableViewCell
@property(nonatomic,weak)IBOutlet UILabel *lblTitle;
@property(nonatomic,weak)IBOutlet UIImageView *imgView;

@end
