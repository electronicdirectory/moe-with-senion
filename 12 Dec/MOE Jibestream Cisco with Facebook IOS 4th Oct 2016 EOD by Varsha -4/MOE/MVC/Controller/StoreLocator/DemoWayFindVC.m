//
//  DemoWayFindVC.m
//  MOE
//
//  Created by varsha on 5/31/16.
//  Copyright © 2016 Neosoft. All rights reserved.
//

#import "DemoWayFindVC.h"
#import "DemoWayFindView.h"

@interface DemoWayFindVC ()

@end

@implementation DemoWayFindVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)dataLoad:(JMapPathPerFloor *)data withDistance:(NSNumber *)distance

{
    _localPathPerFloor = data;
    ((DemoWayFindView *)self.view).distance = distance;
    ((DemoWayFindView *)self.view).localPathPerFloor = data;
}


@end
