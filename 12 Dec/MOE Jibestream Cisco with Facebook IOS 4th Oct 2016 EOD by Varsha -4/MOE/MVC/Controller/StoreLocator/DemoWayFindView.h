//
//  DemoWayFindView.h
//  MOE
//
//  Created by varsha on 5/31/16.
//  Copyright © 2016 Neosoft. All rights reserved.
//

#import <UIKit/UIKit.h>

#import <JMap/JMap.h>


@interface DemoWayFindView : UIView

@property JMapPathPerFloor *localPathPerFloor;
@property NSNumber *distance;

@end
