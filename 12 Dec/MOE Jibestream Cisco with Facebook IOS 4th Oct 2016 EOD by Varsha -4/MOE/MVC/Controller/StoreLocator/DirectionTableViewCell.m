//
//  DirectionTableViewCell.m
//  MOE
//
//  Created by webwerks on 6/23/16.
//  Copyright © 2016 Neosoft. All rights reserved.
//

#import "DirectionTableViewCell.h"

@implementation DirectionTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
