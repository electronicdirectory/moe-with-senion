//
//  CustomJMap.m
//  MOE
//
//  Created by webwerks on 7/5/16.
//  Copyright © 2016 Neosoft. All rights reserved.
//

#import "CustomJMap.h"
#import "JibeStreamConstant.h"
#import "DemoInUnitLabel.h"
@interface CustomJMap()
{
    NSMutableArray *arrayOfDemoInUnitLabels;
    NSDate * lastScaleFactorChange;
    
}
@end
@implementation CustomJMap
+ (instancetype)sharedInstance
{
    //create single ton instance of webService class
    static id instance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        instance = [[self alloc] init];
    });
    return instance;
}
-(void)initMapView
{
    if(!self.mapView)
    {
        self.peopleMoverArray  =[[NSArray alloc] initWithObjects:@"moving walkway", @"elevator", @"stair case", @"escalator", nil];
        lastScaleFactorChange =[NSDate date];
        self.mapContentSize=CGSizeZero;
        self.mapOffset=CGPointZero;
        self.zoomRect=CGRectZero;
        self.mapView=[[JMapContainerView alloc]initDataWithLanguage:@"en" andDelegate:self andDataSource:self andMapTilingEnabled:YES];
    }
}
-(void)loadJMapData{
    
    if(self.venueObject!=nil)
    {
        if([self.jMDelegate  respondsToSelector:@selector(customJMapViewDataReady)])
        {
            [self.jMDelegate customJMapViewDataReady];
        }
    }
    else{
        // [self.mapView getVenueLocations];
        JMapSelectedLocation *menuItem;
        menuItem = [[JMapSelectedLocation alloc] init];
        menuItem.projectId = [NSNumber numberWithInt:5989];
        [self.mapView reloadMapDataForSelectedLocation:menuItem];
    }
}
#pragma  mark JMap Data Source Method
#pragma  mark JMapAPIRequestURL
-(NSString *)JMapAPIRequestURL:(JMapContainerView *)sender{
    return strJibeServerURL;
}
#pragma  mark JMapAuthenticationTokens
-(NSDictionary *)JMapAuthenticationTokens{
    NSString *kJMapAuthenticationTokensUser = @"x-jsapi_user";
    NSString *kJMapAuthenticationTokensPassword = @"x-jsapi_passcode";
    NSString *kJMapAuthenticationTokensAPIKey = @"x-jsapi_key";
    
    return @{ kJMapAuthenticationTokensUser : @"",
              kJMapAuthenticationTokensPassword : @"",
              kJMapAuthenticationTokensAPIKey : @""
              };
}
#pragma  mark
#pragma  mark JMapDelegate for get data from URL

- (void)jMapView:(JMapContainerView *)sender didSendAllLocationsAvailable:(NSArray *)data didFailLoadWithError:(NSError *)error {
    
}
- (void)jMapViewDataReady:(JMapContainerView *)sender withVenuData:(JMapVenue *)data didFailLoadWithError:(NSError *)error
{
    if(error){
        NSLog(@"error is %@",error);
    }
    else if(data) {
        self.venueObject = data;
        self.arrVenueList=[[NSMutableArray alloc]init];
        arrayOfDemoInUnitLabels=[[NSMutableArray alloc]init];
        
        for(JMapDestination *destination in self.venueObject.destinations){
            [self.mapView locateLabelByDestinationId:self.venueObject.destinations];
            @autoreleasepool {
                
                [self.arrVenueList addObject:[self getDictFromJMapDestination:destination]];
                
            }
        }
    }
    if([self.jMDelegate  respondsToSelector:@selector(customJMapViewDataReady)])
    {
        [self.jMDelegate customJMapViewDataReady];
    }
}
-(void)jMapViewDidTapOnDestination:(NSArray *)thisDestinationArray destinationCenterPoint:(JMapPointOnFloor *)destinationCenterPoint{
    if([self.jMDelegate respondsToSelector:@selector(customJMapViewDidTapOnDestination:destinationCenterPoint:)])
    {
        [self.jMDelegate customJMapViewDidTapOnDestination:thisDestinationArray destinationCenterPoint:destinationCenterPoint];
    }
}
-(NSMutableDictionary *)getDictFromJMapDestination:(JMapDestination *)destination
{
    NSString *categoryStr = [[NSString alloc]init];
    NSString *dName=@" ";
    dName=[destination.name stringByReplacingOccurrencesOfString:@"&amp;" withString:@"and"];
    NSMutableDictionary *dict =[[NSMutableDictionary alloc]init];
    [dict setValue:destination.id forKey:@"Id"];
    [dict setValue:dName forKey:@"Title"];
    categoryStr = [destination.category componentsJoinedByString: @"-"];
    [dict setValue:categoryStr forKey:@"CatName"];
    return dict;
}
#pragma  mark display unit labesl delegate  Method
// Process Unit Labels at all or ignore them?
- (BOOL)jMapViewShouldProcessUnitLabels:(JMapFloor *)onFloor
{
    return YES;
}
-(BOOL)jMapViewShouldRotateUnitLabels {
    // Label will flip 180 degrees when map rotation exceeds threshold
    return NO;
}
//Return YES to display Lboxes on the units
// If protocol not set, it defaults to NO
-(BOOL)jMapViewShouldShowLBoxes {
    return NO;
}
// Load this particular unit/destination ?
- (BOOL)jMapViewShouldDisplayUnitLabelForDestinations:(NSArray *)destinations onFloor:(JMapFloor *)onFloor
{
    return YES;
}
/*!
 * Should we load custom css
 */
- (BOOL)jMapViewShouldLoadCustomCSSDictionaryHandler
{
    return YES;
}

/*!
 * Provide custom css dictionary or return null
 */
- (void)jMapViewWillLoadCustomCSSDictionaryHandler:(JMapCanvas *)jMapView withCompletion:(void (^)(JMapCustomStyleSheet *))completion
{
    /**
     * JSON input config in plain text
     */
    NSString *jsonConfig = [[NSBundle bundleForClass:[self class]] pathForResource:@"config" ofType:@"json"];
    NSString *jsonString = [[NSString alloc] initWithContentsOfFile:jsonConfig
                                                           encoding:NSUTF8StringEncoding
                                                              error:NULL];
    if (jsonString) {
        JMapCustomStyleSheet *sheet = [JMapCustomStyleSheet new];
        sheet.jSONSourceConfig = jsonString;
        if (completion) {
            completion(sheet);
        }
    }
}
- (UIView *)jMapViewShouldDisplayAsTooltipDestinations:(NSArray *)destinations onFloor:(JMapFloor *)onFloor insideUnit:(BOOL *)insideUnit
{
    
    // If destinations contain more than 1 object, use JMapDestination's sponsoredRating property to determine which should be set as main destination to be display
    // Look at example in section below (1)
    // Otherwise, get first Destination
    JMapDestination *destination = [destinations firstObject];
    NSInteger highestRating = 0;
    for(JMapDestination *nextDestination in destinations)
    {
        if(highestRating < nextDestination.sponsoredRating.integerValue)
        {
            // Higher
            destination = nextDestination;
            
            // Update counter
            highestRating = nextDestination.sponsoredRating.integerValue;
            
            // Print it if over 0
            if(0 < highestRating)
            {
                NSLog(@"sponsorShip Rating higher than 0. Name: %@, Rating: %@", destination.name, destination.sponsoredRating);
            }
        }
    }
    NSString *dName=@" ";
    dName=[destination.name stringByReplacingOccurrencesOfString:@"&amp;" withString:@"and"];
    
    *insideUnit = YES;
    
    // This demonstrates using actual UIViewController and storyboard to design the look of Unit Label. Same is possible with ToolTip labels
    DemoInUnitLabel *inUnitLabel = [[DemoInUnitLabel alloc] initWithNibName:@"DemoInUnitLabel" bundle:nil];
    inUnitLabel.view.frame = CGRectMake(0, 0, 400, 200);
    inUnitLabel.view.backgroundColor = [UIColor clearColor];
    inUnitLabel.textLabel.backgroundColor = [UIColor clearColor];
    
    inUnitLabel.textLabel.text = dName;
    // Add to local array
    [arrayOfDemoInUnitLabels addObject:inUnitLabel];
    // Return Label
    return inUnitLabel.view;
}
#pragma  mark handle scale factorfor unit labesl delegate  Method
-(void)jMapViewContentScaleFactorChange:(NSNumber *)nextScale onFloor:(JMapFloor *)onFloor
{
    // Want to work well with the JMap SDK, so as a user, I have to balance how often I refresh my Unit Labels
    // As my user interacts with the screen,
    NSDate *nowTime = [NSDate date];
    NSTimeInterval secs = [nowTime timeIntervalSinceDate:lastScaleFactorChange];
    // More than 1s?
    if(1.0 <= secs)
    {
        [self jMapViewContentScaleFactor:nextScale onFloor:onFloor];
        lastScaleFactorChange = [NSDate date];
    }
}
-(void)jMapViewContentScaleFactor:(NSNumber *)newScale onFloor:(JMapFloor *)onFloor
{
    bool firstContentScaleFactor=YES;
    float newScaleFloat = [newScale floatValue];
    
    CGRect rectIntersect = [self.mapView visibleRect];
    CGSize worldSize = [self.mapView worldSize];
    
    // First scale factor, redraw all
    if(firstContentScaleFactor)
    {
        rectIntersect.origin.x = 0;
        rectIntersect.origin.y = 0;
        rectIntersect.size = worldSize;
        firstContentScaleFactor = NO;
    }
    
    // Go through views and change them
    for(id nextVC in arrayOfDemoInUnitLabels)
    {
        // VC or View?
        if([nextVC isKindOfClass:[DemoInUnitLabel class]])
        {
            // This is In-Line label
            // Get the view and update scale
            //nextVC.view.contentScaleFactor = newScaleFloat;
            
            BOOL atMax = NO;
            if (newScale.floatValue == [self.mapView maximumZoomScale]) {
                atMax = YES;
            }
            
            [((DemoInUnitLabel *)nextVC) contentScaleFactorChanged:newScale atMax:atMax];
        }
        
        else if([nextVC isKindOfClass:[UIView class]]) // Regular UIView
        {
            // This is a Tooltip
            // Get the view and update scale
            //nextVC.view.contentScaleFactor = newScaleFloat;
            [nextVC setContentScaleFactor:newScaleFloat];
        }
        
        
    }
}
#pragma mark Amenities Delegate Methods for showing them on map
// Process Amenities and Movers?
-(BOOL)jMapViewShouldProcessAMIcons
{
    return YES;
}

// Ask user to provide style
-(JMapAMStyles *)jMapViewThisAMIconStyle:(JMapAmenity *)thisIcon
{
    // Foreground
    JMapSVGStyle *f = [[JMapSVGStyle alloc] init];
    JMapSVGStyle *m = [[JMapSVGStyle alloc] init];
    JMapSVGStyle *b = [[JMapSVGStyle alloc] init];

    NSMutableArray *array=[[NSMutableArray alloc]initWithArray:self.peopleMoverArray];
    [array addObject:@"exit"];
  
    if([array containsObject:
         thisIcon.description.lowercaseString])
    {
        // Set Fill Red
        [f setCGContextSetRGBFillColor:255.0 g:255.0 b:255.0 a:0.5];
        // MiddleGround
        // Set Fill Green
        [m setCGContextSetRGBFillColor:0.0 g:0.0 b:0.0 a:0.5];
        // Background
        // Set Fill Blue
        [b setCGContextSetRGBFillColor:0.0 g:0.0 b:0.0 a:0.5];
    }
    else{
        // Set Fill Red
        [f setCGContextSetRGBFillColor:255.0 g:255.0 b:255.0 a:1.0];
        
        // MiddleGround
        // Set Fill Green
        [m setCGContextSetRGBFillColor:255.0 g:255.0 b:255.0 a:1.0];
        
        // Background
        // Set Fill Blue
        [b setCGContextSetRGBFillColor:255.0 g:0.0 b:0.0 a:1.0];
    }
    
    
    // Make Amenity/Mover Styles
    JMapAMStyles *AMStyles = [[JMapAMStyles alloc] initWithStyleF:f m:m b:b];
    
    // Set Size
    [AMStyles setWidth:[NSNumber numberWithFloat:32.0]];
    [AMStyles setHeight:[NSNumber numberWithFloat:32.0]];

    // hide only amenities
    if(![self.peopleMoverArray containsObject:
         thisIcon.description.lowercaseString])
    {
        // Hide all amenities by default
        [AMStyles setHidden:[NSNumber numberWithBool:YES]];
    }
    return AMStyles;
}


@end
