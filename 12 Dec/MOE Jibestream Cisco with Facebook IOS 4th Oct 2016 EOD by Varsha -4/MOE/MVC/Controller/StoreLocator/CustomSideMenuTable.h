//
//  CustomSideMenuTable.h
//  MOE
//
//  Created by webwerks on 7/8/16.
//  Copyright © 2016 Neosoft. All rights reserved.
//
#import <JMap/JMap.h>

#import <UIKit/UIKit.h>
@protocol CustomSideMenuTableDelegate<NSObject>
-(void)AmenitiesSelected:(NSArray *)array  ;
-(void)LevelSelected:(JMapFloor *)floor  ;

@end
@interface CustomSideMenuTable : UITableView
@property(nonatomic,strong)NSArray *amenitiesArray,*floorsArray;
@property(nonatomic,weak)id<CustomSideMenuTableDelegate>sideMenuDelegate;
-(void)setupTableView;
-(NSString *)getLevel:(NSNumber *)seq;
@end
