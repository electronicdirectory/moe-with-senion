//
//  SLMapView.h
//  SLDemo
//
//  Copyright (c) 2010-2016 Senion. All rights reserved.
//

#import <UIKit/UIKit.h>

#import <SLIndoorLocation/SLIndoorLocation.h>

static CFTimeInterval AnimationDuration = 0.4;

@class SLPixelPoint3D;
@class BuildingInfo;

@interface SLMapOverlayView : UIImageView

@property (nonatomic, readonly) FloorNr currentFloorNr;
@property (nonatomic, readonly) CGFloat pixelsPerMeter;

@property (nonatomic) BOOL animatePositionUpdate;
@property (nonatomic) BOOL showLocation;

@property (nonatomic, strong, readonly) SLPixelPoint3D *location;
@property (nonatomic, readonly) double uncertaintyRadius;
@property (nonatomic, readonly) double heading;

@property (nonatomic) BOOL showShortestPath;
@property (nonatomic, strong) NSArray *shortestPath;

- (void)setLocation:(SLPixelPoint3D *)location andUncertaintyRadius:(double)radius;
- (void)setHeading:(double)heading;
- (void)setCurrentFloorNr:(FloorNr)currentFloorNr andPixelsPerMeter:(CGFloat)pixelsPerMeter;
- (void)updateZoomScale:(CGFloat)zoomScale;
- (void)clearLocation;

@end
    