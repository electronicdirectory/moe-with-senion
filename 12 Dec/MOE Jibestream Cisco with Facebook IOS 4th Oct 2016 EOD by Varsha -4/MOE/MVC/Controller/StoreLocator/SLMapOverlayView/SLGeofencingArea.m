//
//  SLGeofencingArea.m
//  SLDemo
//
//  Copyright (c) 2010-2016 Senion. All rights reserved.
//

#import "SLGeofencingArea.h"

#import <SLIndoorLocation/SLCoordinate3D.h>
#import <SLIndoorLocation/SLRectangle.h>

@implementation SLGeofencingArea

- (id)initWithName:(NSString *)name andGeometry:(id<SLGeometry>)geometry
{
    self = [super init];
    if (self) {
        self.name = name;
        self.geometry = geometry;
    }
    return self;
}

@end
