//
//  SLMapView.m
//  SLDemo
//
//  Copyright (c) 2010-2016 Senion. All rights reserved.
//

#import "SLMapOverlayView.h"



static CGFloat MinimumRadiusOfPositionInPixels = 15.0;
static CGFloat PositionSizeInMeter             = 2.0;
static CGFloat UncertaintyLineWidthInPixels    = 2.0;
static CGFloat ShortestPathWidthInMeter        = 0.5;

@interface SLMapOverlayView ()

@property (nonatomic) BOOL showHeading;

@property (nonatomic, strong) CAShapeLayer *positionLayer;
@property (nonatomic, strong) CAShapeLayer *uncertaintyRadiusLayer;
@property (nonatomic, strong) CAShapeLayer *headingLayer;

@property (nonatomic, strong) CAShapeLayer *shortestPathLayer;

@property (nonatomic) CGFloat zoomScale;

@end

@implementation SLMapOverlayView

- (instancetype)init
{
    self = [super init];
    if (self) {
        [self baseInit];
    }
    return self;
}

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self baseInit];
    }
    return self;
}

- (instancetype)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self) {
        [self baseInit];
    }
    return self;
}

- (void)layoutSubviews
{
    [super layoutSubviews];
}

- (void)baseInit
{
    _uncertaintyRadius = 0;
    
    _pixelsPerMeter = 10;
    self.zoomScale = 1;
    
    [self createPositionLayer];
    
    self.showLocation = YES;
    self.showHeading = NO;
    self.showShortestPath = NO;
    
    self.animatePositionUpdate = YES;
}

- (void)createPositionLayer
{
    if (self.positionLayer) {
        [self.positionLayer removeFromSuperlayer];
    }
    if (self.headingLayer) {
        [self.headingLayer removeFromSuperlayer];
    }
    if (self.uncertaintyRadiusLayer) {
        [self.uncertaintyRadiusLayer removeFromSuperlayer];
    }
    
    if (self.shortestPathLayer) {
        [self.shortestPathLayer removeFromSuperlayer];
    }
    
    CGPoint locationPoint;
    if (self.location) {
        locationPoint = CGPointMake(self.location.x, self.location.y);
    } else {
        locationPoint = self.center;
    }
    
    CGFloat scaledRadius = self.pixelsPerMeter * PositionSizeInMeter/2;
    
    UIBezierPath *uncertaintyRadiusPath = [UIBezierPath bezierPathWithOvalInRect:CGRectMake(0, 0, 2.0 * self.uncertaintyRadius, 2.0 * self.uncertaintyRadius)];
    CGPoint uncertaintyRadiusPoint = CGPointMake(locationPoint.x-self.uncertaintyRadius, locationPoint.y-self.uncertaintyRadius);
    
    self.uncertaintyRadiusLayer = [CAShapeLayer layer];
    self.uncertaintyRadiusLayer.path = uncertaintyRadiusPath.CGPath;
    self.uncertaintyRadiusLayer.position = uncertaintyRadiusPoint;
    self.uncertaintyRadiusLayer.lineWidth = UncertaintyLineWidthInPixels;
//    self.uncertaintyRadiusLayer.strokeColor = [[UIColor senionBlue] CGColor];
//    self.uncertaintyRadiusLayer.fillColor = [[UIColor senionBlueWithAlpha:0.2] CGColor];
    
    UIBezierPath *positionPath = [UIBezierPath bezierPathWithOvalInRect:CGRectMake(0, 0, 2.0 * scaledRadius, 2.0 * scaledRadius)];
    
    self.positionLayer = [CAShapeLayer layer];
    self.positionLayer.frame = CGRectMake(0, 0, 2 * scaledRadius, 2 * scaledRadius);
    self.positionLayer.path = positionPath.CGPath;
    self.positionLayer.position = locationPoint;
    self.positionLayer.lineWidth = scaledRadius * 0.1;
    self.positionLayer.strokeColor = [[UIColor colorWithWhite:0.95 alpha:1.0] CGColor];
//    self.positionLayer.fillColor = [[UIColor darkSenionBlue] CGColor];
    self.positionLayer.masksToBounds = NO;
    self.positionLayer.shadowColor = [[UIColor blackColor] CGColor];
    self.positionLayer.shadowOffset = CGSizeMake(0, 0);
    self.positionLayer.shadowOpacity = 0.6f;
    self.positionLayer.shadowRadius = 5.0;
    self.positionLayer.shadowPath = positionPath.CGPath;
    
    double x = 2 * scaledRadius;
    double y = 0;
    double l = 1.25 * scaledRadius;
    
    UIBezierPath *path = [UIBezierPath bezierPath];
    [path moveToPoint:CGPointMake(x, y)];
    [path addLineToPoint:CGPointMake(x+l/2, y+l)];
    [path addLineToPoint:CGPointMake(x-l/2, y+l)];
    [path closePath];
    
    CGFloat angle = 0;
    if (self.headingLayer) {
        angle = [(NSNumber *)[((CAShapeLayer *)self.headingLayer.presentationLayer) valueForKeyPath:@"transform.rotation.z"] floatValue];
    }
    
    self.headingLayer = [CAShapeLayer layer];
    self.headingLayer.frame = CGRectMake(0, 0, x*2, x*2);
    self.headingLayer.path = [path CGPath];
    self.headingLayer.position = locationPoint;
    self.headingLayer.lineWidth = scaledRadius * 0.1;
    self.headingLayer.strokeColor = [[UIColor colorWithWhite:0.95 alpha:1.0] CGColor];
//    self.headingLayer.fillColor = [[UIColor darkSenionBlue] CGColor];
    self.headingLayer.masksToBounds = NO;
    self.headingLayer.shadowColor = [[UIColor blackColor] CGColor];
    self.headingLayer.shadowOffset = CGSizeMake(0, 0);
    self.headingLayer.shadowOpacity = 0.6f;
    self.headingLayer.shadowRadius = 5.0;
    self.headingLayer.shadowPath = path.CGPath;
    self.headingLayer.transform = CATransform3DMakeRotation(angle, 0, 0, 1);
    
    self.shortestPathLayer = [CAShapeLayer layer];
    self.shortestPathLayer.frame = CGRectMake(0, 0, 0, 0);
    self.shortestPathLayer.path = [[self createShortestPath] CGPath];
    self.shortestPathLayer.lineWidth = (ShortestPathWidthInMeter * self.pixelsPerMeter);
    self.shortestPathLayer.lineJoin = kCALineJoinRound;
    self.shortestPathLayer.lineCap = kCALineCapRound;
    self.shortestPathLayer.position = CGPointMake(0, 0);
    self.shortestPathLayer.fillColor = nil;
//    self.shortestPathLayer.strokeColor = [[UIColor darkSenionRed] CGColor];
    
    self.uncertaintyRadiusLayer.hidden = YES;
    self.positionLayer.hidden = YES;
    self.headingLayer.hidden = YES;
    self.shortestPathLayer.hidden = YES;
    
    [self.layer addSublayer:self.shortestPathLayer];
    [self.layer addSublayer:self.uncertaintyRadiusLayer];
    [self.layer addSublayer:self.headingLayer];
    [self.layer addSublayer:self.positionLayer];
    
    if (self.zoomScale != 0) {
        [self calculateSizeOfPosition];
    }
}

- (void)setLocation:(SLPixelPoint3D *)location andUncertaintyRadius:(double)uncertaintyRadius
{
    BOOL isFirstLocationUpdate = (self.location == nil || (self.location.floorNr != self.currentFloorNr));
    
    _location = location;
    _uncertaintyRadius = uncertaintyRadius;
    
    if (isFirstLocationUpdate) {
        [self updatePositionAnimated:NO];
    } else {
        [self updatePositionAnimated:self.animatePositionUpdate];
    }
    
    if (self.showLocation && self.showShortestPath) {
        self.shortestPathLayer.path = [[self createShortestPath] CGPath];
        self.shortestPathLayer.hidden = NO;
    } else {
        self.shortestPathLayer.hidden = YES;
    }
}

- (void)setShowLocation:(BOOL)showLocation
{
    _showLocation = showLocation;
    
    [self checkPositionVisibility];
}

- (void)clearLocation
{
    _location = nil;
    _uncertaintyRadius = 0;
    _heading = 0;
    
    self.showLocation = NO;
    self.showHeading = NO;
}

- (void)setHeading:(double)heading
{
    _heading = (heading+90) * M_PI / 180;
    _showHeading = YES;
    
    [self updateHeadingAnimated:self.animatePositionUpdate];
}

- (void)setCurrentFloorNr:(FloorNr)currentFloorNr andPixelsPerMeter:(CGFloat)pixelsPerMeter
{
    _currentFloorNr = currentFloorNr;
    _pixelsPerMeter = pixelsPerMeter;
    
    [self createPositionLayer];
    [self checkPositionVisibility];
}

- (void)checkPositionVisibility
{
    if (self.showLocation && self.location && self.location.floorNr == self.currentFloorNr) {
        self.positionLayer.hidden = NO;
        self.uncertaintyRadiusLayer.hidden = NO;
        if (self.showHeading) {
            self.headingLayer.hidden = NO;
        } else {
            self.headingLayer.hidden = YES;
        }
    } else {
        self.positionLayer.hidden = YES;
        self.uncertaintyRadiusLayer.hidden = YES;
        self.headingLayer.hidden = YES;
    }
}

- (void)updatePositionAnimated:(BOOL)animated
{
    if (!self.showLocation || self.location.floorNr != self.currentFloorNr) {
        self.positionLayer.hidden = YES;
        self.uncertaintyRadiusLayer.hidden = YES;
        return;
    }
    
    [self checkPositionVisibility];
    
    // Animate position location
    
    CGPoint locationPoint = CGPointMake(self.location.x, self.location.y);
    
    [CATransaction begin];
    
    if (!animated) {
        [CATransaction setDisableActions:YES];
    }
    
    if (animated) {
        CABasicAnimation *positionLocationAnimation = [CABasicAnimation animationWithKeyPath:@"position"];
        positionLocationAnimation.fromValue = [NSValue valueWithCGPoint:((CAShapeLayer *)self.positionLayer.presentationLayer).position];
        positionLocationAnimation.toValue = [NSValue valueWithCGPoint:locationPoint];
        positionLocationAnimation.duration = AnimationDuration;
        positionLocationAnimation.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseOut];
        
        self.positionLayer.position = locationPoint;
        
        [self.positionLayer addAnimation:positionLocationAnimation forKey:@"position"];
    } else {
        self.positionLayer.position = locationPoint;
    }
    
    // Animate heading location
    
    CGPoint headingPoint = CGPointMake(self.location.x, self.location.y);
    
    if (animated) {
        CABasicAnimation *headingLocationAnimation = [CABasicAnimation animationWithKeyPath:@"position"];
        headingLocationAnimation.fromValue = [NSValue valueWithCGPoint:((CAShapeLayer *)self.headingLayer.presentationLayer).position];
        headingLocationAnimation.toValue = [NSValue valueWithCGPoint:headingPoint];
        headingLocationAnimation.duration = AnimationDuration;
        headingLocationAnimation.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseOut];
        
        self.headingLayer.position = headingPoint;
        
        [self.headingLayer addAnimation:headingLocationAnimation forKey:@"position"];
    } else {
        self.headingLayer.position = headingPoint;
    }
    
    // Animate uncertainty circle location
    
    CGPoint radiusPoint = CGPointMake(self.location.x-self.uncertaintyRadius, self.location.y-self.uncertaintyRadius);
    
    if (animated) {
        CABasicAnimation *uncertaintyLocationAnimation = [CABasicAnimation animationWithKeyPath:@"position"];
        uncertaintyLocationAnimation.fromValue = [NSValue valueWithCGPoint:((CAShapeLayer *)self.uncertaintyRadiusLayer.presentationLayer).position];
        uncertaintyLocationAnimation.toValue = [NSValue valueWithCGPoint:radiusPoint];
        uncertaintyLocationAnimation.duration = AnimationDuration;
        uncertaintyLocationAnimation.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseOut];
        
        self.uncertaintyRadiusLayer.position = radiusPoint;
        
        [self.uncertaintyRadiusLayer addAnimation:uncertaintyLocationAnimation forKey:@"position"];
    } else {
        self.uncertaintyRadiusLayer.position = radiusPoint;
    }
    
    // Animate uncertainty circle size
    
    UIBezierPath *path = [UIBezierPath bezierPathWithOvalInRect:CGRectMake(0, 0, 2.0 * self.uncertaintyRadius, 2.0 * self.uncertaintyRadius)];
    
    if (animated) {
        CABasicAnimation *uncertaintySizeAnimation = [CABasicAnimation animationWithKeyPath:@"path"];
        uncertaintySizeAnimation.fromValue = (id) ((CAShapeLayer *)self.uncertaintyRadiusLayer.presentationLayer).path;
        uncertaintySizeAnimation.toValue = (id) [path CGPath];
        uncertaintySizeAnimation.duration = AnimationDuration;
        uncertaintySizeAnimation.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseOut];
        
        self.uncertaintyRadiusLayer.path = [path CGPath];
        
        [self.uncertaintyRadiusLayer addAnimation:uncertaintySizeAnimation forKey:@"size"];
    } else {
        self.uncertaintyRadiusLayer.path = [path CGPath];
    }
    
    [CATransaction commit];
}

- (void)updateHeadingAnimated:(BOOL)animated
{
    if (!self.showLocation || !self.showHeading || self.location.floorNr != self.currentFloorNr) {
        self.headingLayer.hidden = YES;
        return;
    }
    
    [self checkPositionVisibility];
    
    CGFloat angle = [(NSNumber *)[((CAShapeLayer *)self.headingLayer.presentationLayer) valueForKeyPath:@"transform.rotation.z"] floatValue];
    
    CGFloat headingDiff = self.heading - angle;
    
    CATransform3D transform = CATransform3DRotate(((CAShapeLayer *)self.headingLayer.presentationLayer).transform, headingDiff, 0, 0, 1);
    
    [CATransaction begin];
    
    if (!animated) {
        [CATransaction setDisableActions:YES];
    }
    
    if (animated) {
        CABasicAnimation *animation = [CABasicAnimation animationWithKeyPath:@"transform"];
        animation.fromValue = [NSValue valueWithCATransform3D:((CAShapeLayer *)self.headingLayer.presentationLayer).transform];
        animation.toValue = [NSValue valueWithCATransform3D:transform];
        animation.duration = AnimationDuration;
        animation.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseOut];
        
        self.headingLayer.transform = transform;
        
        [self.headingLayer addAnimation:animation forKey:@"transform"];
    } else {
        self.headingLayer.transform = transform;
    }
    
    [CATransaction commit];
}

- (void)updateZoomScale:(CGFloat)zoomScale
{
    self.zoomScale = zoomScale;
    
    [self calculateSizeOfPosition];
}

- (void)calculateSizeOfPosition
{
    CGFloat presentedSize = PositionSizeInMeter * self.pixelsPerMeter * self.zoomScale;
    CGFloat minimumSize = MinimumRadiusOfPositionInPixels * 2;
    
    if (presentedSize < minimumSize) {
        
        CGFloat currentSize = ((CAShapeLayer *)self.positionLayer.presentationLayer).frame.size.width * self.zoomScale;
        
        if (currentSize <= 0) {
            currentSize = presentedSize;
        }
        
        CGFloat diff = minimumSize / currentSize;
        
        if (diff <= 0 || diff == 1) {
            return;
        }
        
        [self.headingLayer removeAnimationForKey:@"transform"];
        
        CATransform3D positionTransform = CATransform3DScale(self.positionLayer.transform, diff, diff, 1);
        CATransform3D headingTransform = CATransform3DScale(self.headingLayer.transform, diff, diff, 1);
        
        [CATransaction begin];
        [CATransaction setDisableActions:YES];
        self.positionLayer.transform = positionTransform;
        self.headingLayer.transform = headingTransform;
        [CATransaction commit];
    }
}

- (UIBezierPath *)createShortestPath
{
    UIBezierPath *path = [UIBezierPath bezierPath];
    
    if (!self.shortestPath || self.shortestPath.count == 0) {
        return path;
    }
    
    SLPixelPoint2D *startPoint = nil;
    
    for (SLPixelPoint3D *p in self.shortestPath) {
        if (p.floorNr == self.currentFloorNr && !startPoint) {
            startPoint = p;
            [path moveToPoint:CGPointMake(p.x, p.y)];
        } else if (p.floorNr == self.currentFloorNr && startPoint) {
            [path addLineToPoint:CGPointMake(p.x, p.y)];
        }
    }
    
    return path;
}

@end
