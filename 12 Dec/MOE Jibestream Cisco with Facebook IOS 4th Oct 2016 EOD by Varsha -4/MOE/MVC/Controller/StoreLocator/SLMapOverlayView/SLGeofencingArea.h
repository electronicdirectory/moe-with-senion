//
//  SLGeofencingArea.h
//  SLDemo
//
//  Copyright (c) 2010-2016 Senion. All rights reserved.
//

#import <Foundation/Foundation.h>

#import <SLIndoorLocation/SLGeometry.h>

@class SLGeometry;
@class SLCoordinate3D;

@interface SLGeofencingArea : NSObject

@property (nonatomic, strong) NSString *name;
@property (nonatomic, strong) id<SLGeometry> geometry;
@property (nonatomic, strong) SLCoordinate3D *base;
@property (nonatomic, strong) SLCoordinate3D *edge1;
@property (nonatomic, strong) SLCoordinate3D *edge2;

- (id)initWithName:(NSString *)name andGeometry:(id<SLGeometry>)geometry;

@end
