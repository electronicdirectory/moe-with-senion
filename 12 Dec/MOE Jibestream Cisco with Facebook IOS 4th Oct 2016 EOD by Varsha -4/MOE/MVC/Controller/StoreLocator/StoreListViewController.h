//
//  StoreListViewController.h
//  MOE
//
//  Created by webwerks on 5/19/16.
//  Copyright © 2016 Neosoft. All rights reserved.
//

#import <UIKit/UIKit.h>
@protocol StoreListDelegate <NSObject>
-(void)storeSelectedValue:(id)value Type:(NSString *)strType;
@end

@interface StoreListViewController : UIViewController
@property(nonatomic,weak)id<StoreListDelegate>storeDelegate;
@property(nonatomic,strong)NSMutableArray *arrVenueData;
@property(nonatomic,strong)NSString *strType;
@property(nonatomic,strong)NSDictionary *selectedObject;
@end
