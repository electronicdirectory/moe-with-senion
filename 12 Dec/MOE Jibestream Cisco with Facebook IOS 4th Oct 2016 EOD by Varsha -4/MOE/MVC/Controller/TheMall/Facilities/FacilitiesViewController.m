//
//  FacilitiesViewController.m
//  MOE
//
//  Created by Neosoft on 6/26/15.
//  Copyright (c) 2015 Neosoft. All rights reserved.
//

#import "FacilitiesViewController.h"
#import "TheMallViewController.h"
#import "MallBottomView.h"

@interface FacilitiesViewController ()<UITextViewDelegate>
@end

@implementation FacilitiesViewController {
    TheMallModel *mallModel;
}

#pragma mark - View life cycle
- (void)viewDidLoad {
    [super viewDidLoad];
    [GoogleAnalyticsHandler sendScreenName:screenName_Facilities];
    [self.view setNeedsLayout];
    [self.view layoutIfNeeded];
    // Do any additional setup after loading the view.
    self.navigationItem.titleView = [CommanMethods getLabel:APP_FACILITIES];
    NSMutableArray *arr = [DBOpreation getTheMallData:TheMall_Facilities    ];
    self.webView.backgroundColor = [UIColor clearColor];
    
    MallBottomView *bView = [[MallBottomView alloc] initWithFrame:self.bottomView.bounds];
    bView.index=1;
    bView.showSelectedColor=YES;
    [bView setupBottomViewFields];
    [self.bottomView addSubview:bView];
    if([arr count]>0)
    {
        mallModel = [arr objectAtIndex:0];
        
        NSString *strDesc=[CommanMethods rePlaceTags:mallModel.theMall_PageDetail];
        NSString *strHtml=[CommanMethods getServiceHtml:strDesc];
        self.webView.opaque = NO;
        [self.webView setScalesPageToFit:YES];
        [self.webView loadHTMLString:strHtml baseURL:nil];
    }
    
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:YES];
    [[NSUserDefaults standardUserDefaults] setInteger:101 forKey:@"CurrentTagForBottomView"];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma mark - UIWebViewDelegate
- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType
{
    if (navigationType == UIWebViewNavigationTypeLinkClicked)
    {
        NSURL *url = request.URL;
        if ([url.scheme hasPrefix:@"http"])
        {
            WebViewController *webObj=[[WebViewController alloc]initWithNibName:@"WebViewController" bundle:nil];
            webObj.url=url;
            webObj.strType=APP_FACILITIES;
            [self.navigationController pushViewController:webObj    animated:YES];
            return  NO;
        }
    }
    return YES;
    
}
-(void)dealloc
{
    self.webView.delegate=nil;
}


@end