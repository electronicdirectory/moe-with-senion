//
//  OpeningHoursViewController.h
//  MOE
//
//  Created by Neosoft on 6/16/15.
//  Copyright (c) 2015 Neosoft. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface OpeningHoursViewController : UIViewController

@property (weak, nonatomic) IBOutlet UIView *bottomView;
@property (weak, nonatomic) IBOutlet UIWebView *webView;

@end
