//
//  OpeningHoursViewController.m
//  MOE
//
//  Created by Neosoft on 6/16/15.
//  Copyright (c) 2015 Neosoft. All rights reserved.
//

#import "TheMallViewController.h"
#import "MallBottomView.h"
#import "OpeningHoursViewController.h"
@interface OpeningHoursViewController ()<UITextViewDelegate>
@end

@implementation OpeningHoursViewController {
    TheMallModel *mallModel;
}
#pragma mark - View life cycle
- (void)viewDidLoad {
    [super viewDidLoad];
    [GoogleAnalyticsHandler sendScreenName:screenName_OpeningHours];
    self.navigationItem.titleView = [CommanMethods getLabel: APP_OPENING_HOURS];
    [self.view setNeedsLayout];
    [self.view layoutIfNeeded];
    MallBottomView *bView = [[MallBottomView alloc] initWithFrame:self.bottomView.bounds];
    bView.index=0;
    bView.showSelectedColor=YES;
    [bView setupBottomViewFields];
    [self.bottomView addSubview:bView];
    
    NSMutableArray *arr = [DBOpreation getTheMallData:TheMall_Opening_Hours];
    
    if([arr count]>0)
    {
        mallModel = [arr objectAtIndex:0];
        NSString *strDesc=[CommanMethods rePlaceTags:mallModel.theMall_PageDetail];
        NSString *strHTMLValue=[NSString stringWithFormat:@"<div style=\"margin-left:20px; width:90%%;\">%@ </div>",strDesc];
        NSString *strHtml=[CommanMethods getGettingHereHtml:strHTMLValue];
        
        self.webView.opaque = NO;
        self.webView.backgroundColor = [UIColor clearColor];
        [self.webView setScalesPageToFit:YES];
        [self.webView loadHTMLString:strHtml baseURL:nil];
    }
}
- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:YES];
    [[NSUserDefaults standardUserDefaults] setInteger:100 forKey:@"CurrentTagForBottomView"];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - UIWebViewDelegate
- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType
{
    if (navigationType == UIWebViewNavigationTypeLinkClicked)
    {
        NSURL *url = request.URL;
        if ([url.scheme hasPrefix:@"http"])
        {
            WebViewController *webObj=[[WebViewController alloc]initWithNibName:@"WebViewController" bundle:nil];
            webObj.url=url;
            webObj.strType=APP_OPENING_HOURS;
            [self.navigationController pushViewController:webObj    animated:YES];
            return  NO;
        }
    }
    return YES;
    
}
-(void)dealloc
{
    self.webView.delegate=nil;
}
@end
