
//  Created by Nivrutti  Patil on 27/06/15.
//  Copyright (c) 2015 Neosoft. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface GettinghereTableViewCell : UITableViewCell

@property(nonatomic , strong) IBOutlet UIWebView *mapWebView;
 @property(nonatomic,strong)IBOutlet NSLayoutConstraint *mapHeight;

@end
