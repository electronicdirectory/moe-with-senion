//
//  GettingHereViewController.m
//  MOE
//
//  Created by Neosoft on 6/26/15.
//  Copyright (c) 2015 Neosoft. All rights reserved.
//

#import "GettingHereViewController.h"
#import "TheMallViewController.h"
#import "MallBottomView.h"
#import "GettinghereTableViewCell.h"
@interface GettingHereViewController ()<UIWebViewDelegate>
@end

@implementation GettingHereViewController
{
    TheMallModel *mallModel;
    CGFloat cellHeight,webViewHeight;
    BOOL isWebViewLoading;
}

#pragma mark - View life cycle
- (void)viewDidLoad {
    [super viewDidLoad];
    
    [GoogleAnalyticsHandler sendScreenName:screenName_GettingHere];
    
    // Do any additional setup after loading the view.
    self.navigationItem.titleView = [CommanMethods getLabel: APP_GETTING_HERE];
    [self.view setNeedsLayout];
    [self.view layoutIfNeeded];
    [self setupTheMallModel];
    
    [self setupBottomView];
    
    
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    self.tableView.separatorStyle = UITableViewCellSelectionStyleNone;
    
    if(IS_IPHONE_4_OR_LESS)
        cellHeight = 220.0;
    else if (IS_IPHONE_5)
        cellHeight = 270.0;
    else if (IS_IPHONE_6)
        cellHeight = 330.0;
    else if (IS_IPHONE_6P)
        cellHeight = 370.0;
   
    
    [self.tableView registerNib:[UINib nibWithNibName:@"GettinghereTableViewCell" bundle:nil] forCellReuseIdentifier:@"GettinghereTableViewCell"];
    webViewHeight=self.view.frame.size.height-cellHeight;
    
}
-(void)viewWillAppear:(BOOL)animated
{
    [self.tableView reloadData];
    [super viewWillAppear:YES];
    [[NSUserDefaults standardUserDefaults] setInteger:103 forKey:@"CurrentTagForBottomView"];
}
-(void)willMoveToParentViewController:(UIViewController *)parent {
    [super willMoveToParentViewController:parent];
    if (!parent)
    {
        self.webView.delegate=nil;
        [self.webView removeFromSuperview];
        self.webView=nil;
    }
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma mark - SETUP
#pragma mark model
#pragma mark - Mall model
- (void)setupTheMallModel {
    NSMutableArray *arr = [DBOpreation getTheMallData:TheMall_Getting_Here];
    if([arr count]>0)
        mallModel = [arr objectAtIndex:0];
}
#pragma mark bottom view
- (void)setupBottomView {
    MallBottomView *bView = [[MallBottomView alloc] initWithFrame:self.bottomView.bounds];
    bView.index=3;
    bView.showSelectedColor=YES;
    [bView setupBottomViewFields];
    [self.bottomView addSubview:bView];
}
#pragma mark - Table view delegate/data source
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return (webViewHeight+cellHeight);
    
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (mallModel)
        return 1;
    else
        return  0;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    GettinghereTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"GettinghereTableViewCell" forIndexPath:indexPath];
    // Configure the cell...
    cell.selectionStyle=UITableViewCellSelectionStyleNone;
    if(!isWebViewLoading)
    {
        cell.mapHeight.constant =cellHeight;
         isWebViewLoading=YES;
        [cell.mapWebView loadHTMLString:TheMall_GettingHere_webView_URL baseURL:nil];
        cell.mapWebView.delegate=self;
     }
    if(!self.webView)
    {
        CGRect rect=CGRectZero;
        rect.size.width=cell.frame.size.width-8;
        rect.origin.x=4;
        rect.origin.y= cellHeight;
        rect.size.height=webViewHeight;
        
        self.webView=[[UIWebView alloc]initWithFrame:rect];
        [cell.contentView addSubview:self.webView];
        self.webView.dataDetectorTypes=UIDataDetectorTypePhoneNumber|UIDataDetectorTypeLink ;

        self.webView.opaque = NO;
        self.webView.backgroundColor = [UIColor clearColor];
        [self.webView setScalesPageToFit:YES];
        self.webView.scrollView.scrollEnabled=NO;
        NSString *strDesc=[CommanMethods rePlaceTags:mallModel.theMall_PageDetail];
        NSString *strHtml=[CommanMethods getServiceHtml:strDesc];
        [self.webView loadHTMLString:strHtml baseURL:nil];
    }
    self.webView.delegate=self;
    return cell;
}
- (void)webViewDidStartLoad:(UIWebView *)webView
{
    
}
- (void)webViewDidFinishLoad:(UIWebView *)webView
{
    if([self.webView isEqual:webView])
    {
        NSString *result = [webView stringByEvaluatingJavaScriptFromString:@"document.body.offsetHeight;"];
        float height = [result integerValue];
        webViewHeight=height+50;
        self.webView.frame=CGRectMake(4, cellHeight,self.view.frame.size.width-8, webViewHeight);
        self.webView.scrollView.scrollEnabled=NO;
        [self.tableView reloadData];
    }
 
}
- (void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error
{
}
#pragma mark - UIWebViewDelegate
- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType
{
    if (navigationType == UIWebViewNavigationTypeLinkClicked)
    {
        NSURL *url = request.URL;
        if ([url.scheme hasPrefix:@"http"])
        {
            WebViewController *webObj=[[WebViewController alloc]initWithNibName:@"WebViewController" bundle:nil];
            webObj.url=url;
            webObj.strType=APP_GETTING_HERE;
            [self.navigationController pushViewController:webObj animated:YES];
            return  NO;
        }
    }
    return YES;
}
@end