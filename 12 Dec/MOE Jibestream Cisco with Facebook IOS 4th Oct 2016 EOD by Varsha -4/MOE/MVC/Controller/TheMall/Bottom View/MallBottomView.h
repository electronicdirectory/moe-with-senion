//
//  MallBottomView.h
//  MOE
//
//  Created by Neosoft on 6/16/15.
//  Copyright (c) 2015 Neosoft. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MallBottomView : UIView
- (void)setupBottomViewFields;
@property NSUInteger index;
@property BOOL showSelectedColor;
@end