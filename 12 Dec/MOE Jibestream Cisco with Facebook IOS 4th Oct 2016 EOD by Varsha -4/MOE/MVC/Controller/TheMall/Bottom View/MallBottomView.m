//
//  MallBottomView.m
//  MOE
//
//  Created by Neosoft on 6/16/15.
//  Copyright (c) 2015 Neosoft. All rights reserved.
//
#import "MallBottomView.h"
#import "TheMallViewController.h"
@implementation MallBottomView {
    NSArray *arrButtonText;
    NSArray *arrButtonImage;
}

#pragma mark - init
- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        self.frame = frame;
        self.clipsToBounds = YES;
    }
    return self;
}

#pragma mark - bottom view fields
- (void)setupBottomViewFields {
    
    self.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"noti-bg-6"]];
    arrButtonText = [[NSArray alloc] initWithObjects:APP_OPENING_HOURS, APP_FACILITIES, APP_CONTACT_US, APP_GETTING_HERE, nil];
    arrButtonImage = [[NSArray alloc] initWithObjects:[UIImage imageNamed:@"ico-opening"], [UIImage imageNamed:@"ico-facilities"], [UIImage imageNamed:@"ico-contact"], [UIImage imageNamed:@"ico-gettinghere"], nil];
    for (int i = 0; i<4; i++)
    {
        UIView *view = [[UIView alloc] initWithFrame:CGRectMake((SCREEN_WIDTH/4) * i, 0, SCREEN_WIDTH/4, self.frame.size.height)];
        view.tag = 100+i;
        [self addSubview:view];
        UIImageView *img = [[UIImageView alloc] initWithFrame:CGRectMake(view.frame.size.width *0.5- (view.frame.size.width *0.3/2), view.frame.size.height*0.1, view.frame.size.width *0.3, view.frame.size.height*0.5)];
        img.contentMode = UIViewContentModeScaleAspectFit;
        [view addSubview:img];
        
        if (i<3)
        {
            UIImageView *divImg = [[UIImageView alloc] initWithFrame:CGRectMake(view.frame.size.width -1,view.frame.size.height*0.1, 1, view.frame.size.height*0.8)];
            divImg.image = [UIImage imageNamed:@"ico-div6"];
            [view addSubview:divImg];
        }
         UILabel *lbl = [[UILabel alloc] initWithFrame:CGRectMake(0, CGRectGetMaxY(img.frame)+2, view.frame.size.width  -3, view.frame.size.height*0.3)];
        lbl.text = [arrButtonText objectAtIndex:i];
        lbl.textAlignment = NSTextAlignmentCenter;
        lbl.font = [CommanMethods getHomeTextFont];
        [view addSubview:lbl];
        
        if(self.index ==i && self.showSelectedColor)
        {
            UIColor *color=[UIColor colorWithRed:101.0f/255.0f green:165.0f/255.0f blue:204.0f/255.0f alpha:1];
            lbl.textColor=color;
            UIImage *image=[arrButtonImage objectAtIndex:i];
            img.image = [image imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
            [img setTintColor:color];
        }
        else
        {
            lbl.textColor = [CommanMethods getTextColor];
            UIImage *image=[arrButtonImage objectAtIndex:i];
            img.image = [image imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
            [img setTintColor:[CommanMethods getTextColor]];
        }
        
        UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapMethod:)];
        [view addGestureRecognizer:tap];
    }
}

#pragma mark - tap gesture
- (void)tapMethod:(UITapGestureRecognizer *)gesture {
    
    
    switch ([gesture.view tag])
    {
         case 100: {
            [[NSNotificationCenter defaultCenter] postNotificationName:@"bottomViewActions" object:nil userInfo:@{@"tag":@100}];
        }
            break;
        case 101: {
            [[NSNotificationCenter defaultCenter] postNotificationName:@"bottomViewActions" object:nil userInfo:@{@"tag":@101}];
        }
            break;
        case 102: {
            [[NSNotificationCenter defaultCenter] postNotificationName:@"bottomViewActions" object:nil userInfo:@{@"tag":@102}];
        }
            break;
        case 103: {
            [[NSNotificationCenter defaultCenter] postNotificationName:@"bottomViewActions" object:nil userInfo:@{@"tag":@103}];
        }
            break;
        default:
            break;
    }
}

@end
