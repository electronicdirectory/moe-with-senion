//
//  TheMallViewController.m
//  MOE
//
//  Created by Neosoft on 6/13/15.
//  Copyright (c) 2015 Neosoft. All rights reserved.
//

#import "TheMallViewController.h"
#import "MallBottomView.h"
#import "OpeningHoursViewController.h"
#import "FacilitiesViewController.h"
#import "ContactUsViewController.h"
#import "GettingHereViewController.h"

#define BOTTOM_VIEW_HEIGHT self.bottomView.frame.size.height

@interface TheMallViewController ()
@end

@implementation TheMallViewController {
    MallBottomView *bView;
}
#pragma mark - View life cycle
- (void)viewDidLoad {
    [super viewDidLoad];
    
    
    // Do any additional setup after loading the view.
    [GoogleAnalyticsHandler sendScreenName:screenName_TheMall];
    [self setupNotificationCenter];
    [self setupNavigationBar];
    
    [self.view setNeedsLayout];
    [self.view layoutIfNeeded];
    
    NSMutableArray *arr = [DBOpreation getTheMallData:TheMall_TheMall];
    if([arr count]>0)
    {
        TheMallModel *mallModel = [arr objectAtIndex:0];
        NSString *strDesc=[CommanMethods rePlaceTags:mallModel.theMall_PageDetail];
        NSString *strHTMLValue=[NSString stringWithFormat:@"<img src=%@  style=\"width:100%%;height:250px;background-color:#ffffff;\"><div style=\"margin-left:20px; width:90%%;\">%@ </div>",[NSString stringWithFormat:@"%@%@",Web_Host_Url,mallModel.theMall_Image],strDesc];
        NSString *strHtml=[CommanMethods getGettingHereHtml:strHTMLValue];
        self.webView.opaque = NO;
        self.webView.backgroundColor = [UIColor clearColor];
        [self.webView setScalesPageToFit:YES];
        [self.webView loadHTMLString:strHtml baseURL:nil];
    }
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
    [self setupBottomView];

    [[NSUserDefaults standardUserDefaults] setInteger:99 forKey:@"CurrentTagForBottomView"];
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma mark Navigation bar
- (void)setupNavigationBar
{
    self.navigationItem.rightBarButtonItem=[CommanMethods getRightButton:self Action:@selector(search)];
    self.navigationItem.leftBarButtonItem = [CommanMethods getLeftButton:self Action:@selector(menuMethod)];
    self.navigationItem.titleView = [CommanMethods getLabel:APP_THE_MALL];
}
- (void)menuMethod
{
    [self.sideMenuViewController presentLeftMenuViewController];
}
- (void)search
{
    [[AppDelegate appDelegateInstance]openSearchScreen:self.navigationController];
}

#pragma mark - Notification center
- (void)setupNotificationCenter {
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(bottomViewActions:) name:@"bottomViewActions" object:nil];
}
#pragma mark - Bottom view
- (void)setupBottomView {
    if(!bView)
    {
        bView = [[MallBottomView alloc] initWithFrame:self.bottomView.bounds];
        [bView setupBottomViewFields];
        [self.bottomView addSubview:bView];
    }
}

- (void)bottomViewActions :(NSNotification *)dict
{
    int tag = [[[dict userInfo] valueForKey:@"tag"] intValue];
    if(tag == [[NSUserDefaults standardUserDefaults] integerForKey:@"CurrentTagForBottomView"])
        return;
    
    NSArray  *array=self.navigationController.viewControllers;
    NSMutableArray *arrayNav=[[NSMutableArray alloc]init];
    [arrayNav addObject:[array objectAtIndex:0]];
    
    switch (tag)
    {
        case 100: {
            OpeningHoursViewController *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"OpeningHoursViewController"];
            [arrayNav addObject:vc];
        }
            break;
        case 101: {
            FacilitiesViewController *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"FacilitiesViewController"];
            [arrayNav addObject:vc];
        }
            break;
        case 102: {
            ContactUsViewController *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"ContactUsViewController"];
            [arrayNav addObject:vc];
        }
            break;
        case 103: {
            GettingHereViewController *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"GettingHereViewController"];
            [arrayNav addObject:vc];
        }
            break;
        default:
            break;
     }
    [self.navigationController setViewControllers:arrayNav animated:NO];
}
#pragma mark - UIWebViewDelegate
- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType
{
    if (navigationType == UIWebViewNavigationTypeLinkClicked)
    {
        NSURL *url = request.URL;
        if ([url.scheme hasPrefix:@"http"])
        {
            WebViewController *webObj=[[WebViewController alloc]initWithNibName:@"WebViewController" bundle:nil];
            webObj.url=url;
            webObj.strType=APP_THE_MALL;
            [self.navigationController pushViewController:webObj    animated:YES];
            return  NO;
        }
    }
    return YES;
}
-(void)dealloc
{
    self.webView.delegate=nil;
}
@end


