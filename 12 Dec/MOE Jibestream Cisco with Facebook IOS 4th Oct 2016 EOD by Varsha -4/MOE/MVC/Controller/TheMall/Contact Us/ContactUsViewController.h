//
//  ContactUsViewController.h
//  MOE
//
//  Created by Neosoft on 6/26/15.
//  Copyright (c) 2015 Neosoft. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ContactUsViewController : UIViewController

@property (weak, nonatomic) IBOutlet UIView *bottomView;
@property (weak, nonatomic) IBOutlet UIWebView *webView;
@property(assign,nonatomic)BOOL isPushFromLeftSideContactUs;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *heightBottomView;
@end
