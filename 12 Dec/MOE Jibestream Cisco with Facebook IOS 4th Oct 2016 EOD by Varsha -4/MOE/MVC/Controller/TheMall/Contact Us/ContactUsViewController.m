//
//  ContactUsViewController.m
//  MOE
//
//  Created by Neosoft on 6/26/15.
//  Copyright (c) 2015 Neosoft. All rights reserved.
//

#import "ContactUsViewController.h"
#import "TheMallViewController.h"
#import "MallBottomView.h"

@interface ContactUsViewController ()
@end

@implementation ContactUsViewController {
    TheMallModel *mallModel;
}

#pragma mark - View life cycle
- (void)viewDidLoad {
    [super viewDidLoad];
    [GoogleAnalyticsHandler sendScreenName:screenName_ContactUS];
    
    // Do any additional setup after loading the view.
    self.navigationItem.titleView = [CommanMethods getLabel:@"CONTACT INFO"];
    [self.view setNeedsLayout];
    [self.view layoutIfNeeded];
    NSMutableArray *arr = [DBOpreation getTheMallData:TheMall_Contact_Details];
    if (_isPushFromLeftSideContactUs)
    {
        _heightBottomView.constant=0;
        self.bottomView.hidden = YES;
    }
    else
    {
        MallBottomView *bView = [[MallBottomView alloc] initWithFrame:self.bottomView.bounds];
        bView.index=2;
        bView.showSelectedColor=YES;
        [bView setupBottomViewFields];
        [self.bottomView addSubview:bView];
    }
  
    if([arr count]>0)
    {
        mallModel = [arr objectAtIndex:0];
        
        NSString *strDesc=[CommanMethods rePlaceTags:mallModel.theMall_PageDetail];
        NSString *strHtml=[CommanMethods getServiceHtml:strDesc];
        self.webView.opaque = NO;
        self.webView.backgroundColor = [UIColor clearColor];
        [self.webView setScalesPageToFit:YES];
        [self.webView loadHTMLString:strHtml baseURL:nil];
    }
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:YES];
    [[NSUserDefaults standardUserDefaults] setInteger:102 forKey:@"CurrentTagForBottomView"];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - UIWebViewDelegate
- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType
{
    if (navigationType == UIWebViewNavigationTypeLinkClicked)
    {
        NSURL *url = request.URL;
        if ([url.scheme hasPrefix:@"http"])
        {
            WebViewController *webObj=[[WebViewController alloc]initWithNibName:@"WebViewController" bundle:nil];
            webObj.url=url;
            webObj.strType=APP_CONTACT_US;
            [self.navigationController pushViewController:webObj    animated:YES];
            return  NO;
        }
    }
    return YES;
}
-(void)dealloc
{
    self.webView.delegate=nil;
}
@end