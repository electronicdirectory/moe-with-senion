//
//  AddMyParkingDetailViewController.h
//  AddparkingModule
//
//  Created by webwerks on 6/27/15.
//  Copyright (c) 2015 Neosoft. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AddMyParkingDetailViewController : UIViewController<UIImagePickerControllerDelegate,UINavigationControllerDelegate>

@property (nonatomic) MyParkingModel *myParkingModel;
@property (nonatomic) NSMutableArray *arrParkingData;
@property (nonatomic) BOOL isUpdate;

@property (weak, nonatomic) IBOutlet TPKeyboardAvoidingScrollView *scrollView;
@property (weak, nonatomic) IBOutlet UIButton *btnCapture;
@property (weak, nonatomic) IBOutlet UIButton *btnDelete;
@property (weak, nonatomic) IBOutlet UIButton *btnSave;

@property (weak, nonatomic) IBOutlet UIImageView *imgCapturedView;
@property (nonatomic)  UIImage *capturedImage;
@property (weak, nonatomic) IBOutlet UITextField *txtDesc;

- (IBAction)takePic:(id)sender;
- (IBAction)deletePic:(id)sender;
- (IBAction)savePic:(id)sender;

@end
