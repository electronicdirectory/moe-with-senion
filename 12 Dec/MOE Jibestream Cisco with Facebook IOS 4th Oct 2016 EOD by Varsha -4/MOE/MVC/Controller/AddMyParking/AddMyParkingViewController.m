//
//  AddMyParkingViewController.m
//  AddparkingModule
//
//  Created by webwerks on 6/27/15.
//  Copyright (c) 2015 Neosoft. All rights reserved.
//

#import "AddMyParkingViewController.h"
#import "AddMyParkingDetailViewController.h"
#import "AddMyParkingTableViewCell.h"

#define ADD_PARKING_CUSTOM_CELL_IDENTIFIER @"AddMyParkingTableViewCell"
#define CELL_HEIGHT 350.0f
#define documentsDirectory  [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,NSUserDomainMask,YES) lastObject]

@interface AddMyParkingViewController ()

@end

@implementation AddMyParkingViewController {
    MyParkingModel *parkingDetailModel;
    NSMutableArray *arrParkingData;
    BOOL isUpdateVal;
}

#pragma mark - view lifecycle
- (void)viewDidLoad {
    [super viewDidLoad];
    
    [GoogleAnalyticsHandler sendScreenName:screenName_MyParking];

     UIImageView *imageView=[[UIImageView alloc]initWithFrame:self.view.frame];
    imageView.image=[UIImage imageNamed:@"parking-bg"];
    [self.view addSubview:imageView];
    [self.view sendSubviewToBack:imageView];
    
    self.navigationItem.titleView = [CommanMethods getLabel:[screenHeader_MY_PARKING uppercaseString]];
    if(self.isROOT) {
        self.navigationItem.rightBarButtonItem=[CommanMethods getRightButton:self Action:@selector(search)];
        self.navigationItem.leftBarButtonItem =[CommanMethods getLeftButton:self Action:@selector(menuMethod)];
    }
    arrParkingData = [[NSMutableArray alloc] init];
    arrParkingData = [DBOpreation getMyParkingData];
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    [self.tableView setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    [self.tableView registerClass:[AddMyParkingTableViewCell class] forCellReuseIdentifier:ADD_PARKING_CUSTOM_CELL_IDENTIFIER];
    [self.tableView registerNib:[UINib nibWithNibName:@"AddMyParkingTableViewCell" bundle:[NSBundle mainBundle]] forCellReuseIdentifier:ADD_PARKING_CUSTOM_CELL_IDENTIFIER];
    [[AppDelegate appDelegateInstance] showFullScreenCiscoAd:self ScreenName:tag_ParkingScreen];
    
}
-(void)willMoveToParentViewController:(UIViewController *)parent {
    [super willMoveToParentViewController:parent];
    if (!parent)
    {
        [[AppDelegate appDelegateInstance]removeCustomApex];
    }
}


- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:YES];
    arrParkingData = [DBOpreation getMyParkingData];
    [self.tableView reloadData];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark navigation bar button methods
- (void)menuMethod {
    [self.sideMenuViewController presentLeftMenuViewController];
}
- (void)search
{
    [[AppDelegate appDelegateInstance]openSearchScreen:self.navigationController];
}
#pragma mark - table view delegate/data source
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return ([arrParkingData count]+1);
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.row==[arrParkingData count]) {
        return 140.0f;
    }
    else
    return CELL_HEIGHT;
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section {
    return [[UIView alloc] init];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    AddMyParkingTableViewCell *cell = nil;
    if(cell == nil) {
        cell = (AddMyParkingTableViewCell *)[tableView dequeueReusableCellWithIdentifier:ADD_PARKING_CUSTOM_CELL_IDENTIFIER];
    }
    cell.btn.userInteractionEnabled=NO;
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.backgroundColor = [UIColor clearColor];
    cell.btnDelete.tag=indexPath.row;
    [cell.btnDelete addTarget:self action:@selector(deleteParkingInfo:) forControlEvents:UIControlEventTouchUpInside];
    cell.addParkingLbl.font=[CommanMethods getHeaderFont];
    cell.dateLbl.font=[CommanMethods getDateFont];
    cell.descLbl.font=[CommanMethods getDetailsFont];
    cell.descLbl.textColor=[CommanMethods getTextColor];
    cell.dateLbl.textColor=[CommanMethods getTextColor];
    cell.img.contentMode=UIViewContentModeScaleToFill;
    if(indexPath.row == [arrParkingData count])
    {
        cell.btn.hidden = NO;
        cell.addParkingLbl.hidden=NO;
        cell.deleteImage.hidden=YES;
        cell.dateLbl.hidden=YES;
        cell.btnDelete.hidden=YES;
        cell.descLbl.hidden=YES;
        cell.img.hidden = YES;
        cell.addParkingLbl.text = ADD_PARKING_INFO;
        if ([arrParkingData count]==0) {
            cell.separtor.hidden=YES;
        }
    }
    else
    {
        MyParkingModel *model = [arrParkingData objectAtIndex:indexPath.row];
        NSURL *url=[NSURL fileURLWithPath:[NSString stringWithFormat:@"%@%@", documentsDirectory, model.strPath]];
         cell.dateLbl.hidden=NO;
        cell.descLbl.hidden = NO;
        cell.img.hidden = NO;
        cell.deleteImage.hidden=NO;
        cell.btnDelete.hidden=NO;
        [cell.img sd_setImageWithURL:url placeholderImage:nil];
        cell.dateLbl.text=model.strDate;
        cell.descLbl.text = model.strDesc;
        cell.addParkingLbl.hidden=YES;
        cell.btn.hidden = YES;
    }
    cell.btnDelete.tag=indexPath.row;
    return cell;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if(indexPath.row==[arrParkingData count] ) {
        parkingDetailModel=nil;
        isUpdateVal=NO;
        [self addNewParkingEntry:nil IsUpdate:isUpdateVal ArrParkingData:nil];
    }
    else {
        parkingDetailModel = [arrParkingData objectAtIndex:indexPath.row];
        isUpdateVal=YES;
        [self pushMyParkingDetailView:parkingDetailModel IsUpdate:isUpdateVal ArrParkingData:[arrParkingData objectAtIndex:indexPath.row ]];
    }
}
-(void)deleteParkingInfo:(UIButton *)btn
{
    MyParkingModel *parkingData=[arrParkingData objectAtIndex:btn.tag];
    [DBOpreation   deleteParkingDetail:parkingData.strPath];
    [arrParkingData removeObjectAtIndex:btn.tag];
    
    [self.tableView reloadData];
}
- (IBAction)btnAddParkingInfoMethod:(id)sender {
    [self pushMyParkingDetailView:nil IsUpdate:NO ArrParkingData:nil];
}
-(void)addNewParkingEntry:(MyParkingModel *)model IsUpdate:(BOOL)isUpdate ArrParkingData:(NSMutableArray *)arr
{
    AVAuthorizationStatus status = [AVCaptureDevice authorizationStatusForMediaType:AVMediaTypeVideo];
    if(status == AVAuthorizationStatusAuthorized) { // authorized
        dispatch_async(dispatch_get_main_queue(), ^{
            [self getCamera];
        });
    }
    else if(status == AVAuthorizationStatusDenied){ // denied
        dispatch_async(dispatch_get_main_queue(), ^{
            [CommanMethods displayAlert:error :camera_permission_error];
        });
    }
    else if(status == AVAuthorizationStatusRestricted){ // restricted
        dispatch_async(dispatch_get_main_queue(), ^{
            [CommanMethods displayAlert:error :camera_permission_error];
        });
    }
    else if(status == AVAuthorizationStatusNotDetermined)
    { // not determined
        
        [AVCaptureDevice requestAccessForMediaType:AVMediaTypeVideo completionHandler:^(BOOL granted) {
            if(granted){ // Access has been granted ..do something
                dispatch_async(dispatch_get_main_queue(), ^{
                    [self getCamera];
                });
            } else { // Access denied ..do something
                dispatch_async(dispatch_get_main_queue(), ^{
                    [CommanMethods displayAlert:error :camera_permission_error];
                });
            }
        }];
    }

}
- (void)pushMyParkingDetailView :(MyParkingModel *)model IsUpdate:(BOOL)isUpdate ArrParkingData:(MyParkingModel *)arr{
    
    AddMyParkingDetailViewController *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"AddMyParkingDetailViewController"];
    vc.myParkingModel= arr;
    vc.arrParkingData = arrParkingData;
    vc.isUpdate =isUpdate;
    [self.navigationController pushViewController:vc animated:YES];
}
- (void)getCamera {
    
    UIImagePickerController *picker = [[UIImagePickerController alloc] init];
    picker.delegate = self;
    picker.allowsEditing = YES;
    if([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera])
    {
        picker.sourceType = UIImagePickerControllerSourceTypeCamera;
    }
    else
    {
        picker.sourceType = UIImagePickerControllerSourceTypeSavedPhotosAlbum;
    }
    [self presentViewController:picker animated:YES completion:NULL];
}

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    UIImage *chosenImage = info[UIImagePickerControllerEditedImage];
    [picker dismissViewControllerAnimated:YES completion:NULL];
    AddMyParkingDetailViewController *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"AddMyParkingDetailViewController"];
    vc.myParkingModel= nil;
    vc.capturedImage =chosenImage;
    vc.arrParkingData = arrParkingData;
    vc.isUpdate =isUpdateVal;
    [self.navigationController pushViewController:vc animated:YES];
}
@end
