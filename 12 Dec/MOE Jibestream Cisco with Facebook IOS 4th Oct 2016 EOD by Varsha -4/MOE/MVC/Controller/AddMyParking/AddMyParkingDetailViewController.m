//
//  AddMyParkingDetailViewController.m
//  AddparkingModule
//
//  Created by webwerks on 6/27/15.
//  Copyright (c) 2015 Neosoft. All rights reserved.
//

#import <AVFoundation/AVFoundation.h>
#import "AddMyParkingDetailViewController.h"

#define documentsDirectory  [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,NSUserDomainMask,YES) lastObject]

@interface AddMyParkingDetailViewController ()

@end

@implementation AddMyParkingDetailViewController
@synthesize arrParkingData;
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    UIImageView *imageView=[[UIImageView alloc]initWithFrame:self.view.frame];
    imageView.image=[UIImage imageNamed:@"parking-bg"];
    [self.view addSubview:imageView];
    [self.view sendSubviewToBack:imageView];
    
    self.navigationItem.titleView = [CommanMethods getLabel:[screenHeader_MY_PARKING uppercaseString]];
    [self initializeParkingData];
    [self.scrollView contentSizeToFit];
   
    
     self.txtDesc.autocorrectionType = UITextAutocorrectionTypeNo;
    
    self.txtDesc.textColor = [CommanMethods getTextColor];
    self.txtDesc.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Tap to add Description" attributes:@{NSForegroundColorAttributeName: [CommanMethods getTextPlaceholderColor]}];
    
    [self.txtDesc setBorderStyle:UITextBorderStyleLine];
    self.txtDesc.layer.borderWidth = 1;
    self.txtDesc.layer.borderColor = [[CommanMethods getTextColor] CGColor];
    self.txtDesc.textColor=[CommanMethods getTextColor];
    self.btnSave.titleLabel.font=[CommanMethods getDetailsFont];
    self.btnCapture.titleLabel.font=[CommanMethods getDetailsFont];
    self.txtDesc.font=[CommanMethods getDetailsFont];
    
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (void)initializeParkingData
{
    if(self.myParkingModel != nil)
    {
        UIImage* image = [UIImage imageNamed:[NSString stringWithFormat:@"%@%@", documentsDirectory, self.myParkingModel.strPath]];
        self.imgCapturedView.image = image;
        self.txtDesc.text = self.myParkingModel.strDesc;
        self.btnDelete.hidden=NO;
    }
    else
    {
        self.imgCapturedView.image = self.capturedImage;
        self.btnDelete.hidden=YES;
    }
    [self.btnCapture setTitle:@"RETAKE" forState:UIControlStateNormal];
}
- (IBAction)takePic:(id)sender {
    
    AVAuthorizationStatus status = [AVCaptureDevice authorizationStatusForMediaType:AVMediaTypeVideo];
    if(status == AVAuthorizationStatusAuthorized) { // authorized
        dispatch_async(dispatch_get_main_queue(), ^{
            [self getCamera];
        });
    }
    else if(status == AVAuthorizationStatusDenied){ // denied
        dispatch_async(dispatch_get_main_queue(), ^{
            [CommanMethods displayAlert:error :camera_permission_error];
        });
    }
    else if(status == AVAuthorizationStatusRestricted){ // restricted
        dispatch_async(dispatch_get_main_queue(), ^{
            [CommanMethods displayAlert:error :camera_permission_error];
        });
    }
    else if(status == AVAuthorizationStatusNotDetermined)
    { // not determined
        
        [AVCaptureDevice requestAccessForMediaType:AVMediaTypeVideo completionHandler:^(BOOL granted) {
            if(granted){ // Access has been granted ..do something
                dispatch_async(dispatch_get_main_queue(), ^{
                    [self getCamera];
                });
            } else { // Access denied ..do something
                dispatch_async(dispatch_get_main_queue(), ^{
                    [CommanMethods displayAlert:error :camera_permission_error];
                });
            }
        }];
    }
}
- (void)getCamera {
    
    UIImagePickerController *picker = [[UIImagePickerController alloc] init];
    picker.delegate = self;
    picker.allowsEditing = YES;
    if([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera])
    {
        picker.sourceType = UIImagePickerControllerSourceTypeCamera;
    }
    else
    {
        picker.sourceType = UIImagePickerControllerSourceTypeSavedPhotosAlbum;
    }
    
    [self presentViewController:picker animated:YES completion:NULL];
}

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    
    UIImage *chosenImage = info[UIImagePickerControllerEditedImage];
    self.imgCapturedView.image = chosenImage;
    [picker dismissViewControllerAnimated:YES completion:NULL];
}
- (IBAction)deletePic:(id)sender {
    
    if(self.myParkingModel != nil) {
        if([DBOpreation deleteParkingDetail:self.myParkingModel.strPath]) {
            [APP_DELEGATE PlayProgressHudAsPerType:delete_picture View:self.view];
            [self.navigationController popViewControllerAnimated:YES];
        }
        else
        {
            [CommanMethods displayAlert:error :delete_picture_error];
        }
    }
}
- (IBAction)savePic:(id)sender {
    
    
    if(self.imgCapturedView.image!=nil)
    {
        NSMutableDictionary *dictForMyParking = [[NSMutableDictionary alloc] init];
        NSDateFormatter *formatorObj;
        NSDate *dateVal=[NSDate date];
        formatorObj=[[NSDateFormatter alloc]init];
        [formatorObj setTimeZone:[NSTimeZone localTimeZone]];
        [formatorObj setDateFormat:@"yyyyMMddHHmmssSSS"];
        NSString *strDate=[formatorObj stringFromDate:dateVal];
        
        NSString *parentPhotoFolder = [documentsDirectory stringByAppendingPathComponent:@"images"];
        
        // Create the folder if necessary
        BOOL isDir = NO;
        NSFileManager *fileManager = [[NSFileManager alloc] init];
        if (![fileManager fileExistsAtPath:parentPhotoFolder isDirectory:&isDir] && isDir == NO) {
            [fileManager createDirectoryAtPath:parentPhotoFolder
                   withIntermediateDirectories:NO
                                    attributes:nil
                                         error:nil];
        }
        
        NSString *imageName=[NSString stringWithFormat:@"/%@.png",strDate];
        NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
        formatter.dateFormat = @"dd-MMM-yyyy";
        NSDate *date=[NSDate date];
        NSString *stringFromDate = [formatter stringFromDate:date];
        [dictForMyParking setObject:[stringFromDate stringByReplacingOccurrencesOfString:@"-" withString:@" "] forKey:@"ParkingDate"];
        NSData *data=UIImagePNGRepresentation(self.imgCapturedView.image);
        BOOL write=[data writeToFile:[parentPhotoFolder stringByAppendingString:imageName] atomically:YES];
        if(write)
        {
            [dictForMyParking setObject:[NSString stringWithFormat:@"/images%@", imageName] forKey:@"path"];
            [dictForMyParking setObject:self.txtDesc.text forKey:@"desc"];
        }
        if(self.isUpdate) {
            [dictForMyParking setObject:self.myParkingModel.strPath forKey:@"path_old"];
            if([DBOpreation updateParkingDetail:dictForMyParking])
            {
                [self.navigationController popViewControllerAnimated:YES];
            }
            else {
                [CommanMethods displayAlert:error :save_error];
            }
        }
        else {
            if([DBOpreation insertParkingDetail:dictForMyParking])
            {
                [self.navigationController popViewControllerAnimated:YES];
            }
            else {
                [CommanMethods displayAlert:error :save_error];
            }
        }
    }
    else {
        
        [CommanMethods displayAlert:error :save_picture_error];
    }
}
@end




