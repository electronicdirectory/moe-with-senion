//
//  AddMyParkingViewController.h
//  AddparkingModule
//
//  Created by webwerks on 6/27/15.
//  Copyright (c) 2015 Neosoft. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AVFoundation/AVFoundation.h>
@interface AddMyParkingViewController : UIViewController <UITableViewDataSource, UITableViewDelegate,UIImagePickerControllerDelegate,UINavigationControllerDelegate>

@property (nonatomic) BOOL isROOT;

@property (weak, nonatomic) IBOutlet UITableView *tableView;


@end
