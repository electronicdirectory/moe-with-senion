//
//  AddMyParkingTableViewCell.h
//  MOE
//
//  Created by Neosoft on 6/29/15.
//  Copyright (c) 2015 Neosoft. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AddMyParkingTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIView *viewImg;
@property (weak, nonatomic) IBOutlet UIView *viewLbl;
@property (weak, nonatomic) IBOutlet UIImageView *img;
@property (weak, nonatomic) IBOutlet UIButton *btn;
@property (weak, nonatomic) IBOutlet UILabel *addParkingLbl;
@property (weak, nonatomic) IBOutlet UILabel *dateLbl;
@property (weak, nonatomic) IBOutlet UILabel *descLbl;
@property (weak, nonatomic) IBOutlet UIButton *btnDelete;
@property (weak, nonatomic) IBOutlet UIImageView *deleteImage;
@property (weak, nonatomic) IBOutlet UIImageView *separtor;

@end
