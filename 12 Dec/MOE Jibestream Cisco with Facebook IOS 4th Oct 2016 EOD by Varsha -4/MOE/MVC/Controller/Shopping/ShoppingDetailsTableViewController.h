//
//  ShoppingDetailsTableViewController.h
//  MOE
//
//  Created by Nivrutti on 6/25/15.
//  Copyright (c) 2015 Neosoft. All rights reserved.
//

#import <UIKit/UIKit.h>
@class ShoppingListModel;
@interface ShoppingDetailsTableViewController : UITableViewController<UIWebViewDelegate>
@property(nonatomic,strong)  ShopBaseModel  *shopBaseModelObj;
@property (nonatomic,strong)UIWebView *webView;

@end
