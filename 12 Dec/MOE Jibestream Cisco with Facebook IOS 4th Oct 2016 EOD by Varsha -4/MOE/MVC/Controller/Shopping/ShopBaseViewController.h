//
//  ShopBaseViewController.h
//
//
//  Created by Nivrutti on 6/24/15.
//
//

#import <UIKit/UIKit.h>
#import "ShopAllDataModel.h"

@interface ShopBaseViewController : UIViewController<UIPickerViewDataSource,UIPickerViewDelegate,UITextFieldDelegate,UITableViewDataSource,UITableViewDelegate>
{
    NSMutableArray *arrAllData,*arrNameData,*arrCategory,*arrSearch;
    NSMutableDictionary *dataDict;
    
    NSArray *indexArray;
    NSUserDefaults *pref;
    UIPickerView *pickerView;
    UIToolbar*mypickerToolbar;
    NSMutableArray *favArray;
    
    UIActivityIndicatorView *indicatorObj;
}
@property(nonatomic,strong) ShopAllDataModel *dataModelObj;
@property(nonatomic,weak)IBOutlet UITableView *tblView;
@property(nonatomic,weak)IBOutlet NSLayoutConstraint *searchWidthConstraint;
@property(nonatomic,weak)IBOutlet NSLayoutConstraint *categorywidthConstraint;
@property(nonatomic,weak)IBOutlet UITextField *txtSearchField;
@property(nonatomic,weak)IBOutlet UITextField *txtSearchCategory;
@property(nonatomic,weak)IBOutlet UISegmentedControl *segControl;

@property(nonatomic,strong) NSString *strType;
-(IBAction)segValueChanged:(id)sender;

@end
