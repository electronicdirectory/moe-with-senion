
//
//  ShopTableViewCell.h
//  MOE
//
//  Created by Nivrutti on 6/24/15.
//  Copyright (c) 2015 Neosoft. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ShopTableViewCell : UITableViewCell
@property(nonatomic,strong)IBOutlet UILabel *lblTitle;
@property(nonatomic,strong)IBOutlet UILabel *lblDesc;
@property(nonatomic,strong)IBOutlet UIButton *btnFav;
@property(nonatomic,strong)IBOutlet UIButton *btnMap;

@end
