//
//  ShopBaseViewController.m
//
//
//  Created by Nivrutti on 6/24/15.
//
//

#import "ShopBaseViewController.h"
#import "ShopTableViewCell.h"
#import "WebViewController.h"
#import "ShoppingDetailsTableViewController.h"
#import "CustomInfoView.h"
#import "StoreLocatorViewController.h"

@interface ShopBaseViewController ()
{
    UIView *sideView;
    bool hasSwipedLeft;
    NSIndexPath *swipeIndexPath;
    UIButton *btnFav,*btnCall;
    ShopTableViewCell *swipedCell;
    UIImageView *imgFavImageView;
    BOOL isCellActionPerformed;
    CategoryModel *selectedModel;

}
@end
@implementation ShopBaseViewController
#define Width 220
#define Height 52
#define SEARCH_CATEGORY @"Search By Category"
- (void)viewDidLoad
{
    [super viewDidLoad];
 
    [defaults synchronize];
    if (![defaults boolForKey:HasShoppingLaunchedOnce])
    {
        [defaults setBool:YES forKey:HasShoppingLaunchedOnce];
        CustomInfoView *info=[[CustomInfoView alloc]initWithFrame:[AppDelegate appDelegateInstance].window.frame];
        info.strImageName=@"shoppingCoach";
        [[AppDelegate appDelegateInstance].window addSubview:info];
    }
    [defaults synchronize];
    self.navigationItem.titleView =[CommanMethods getLabel:@"SHOP & DINE"];
    
    isCellActionPerformed=NO;
    [self.tblView registerNib:[UINib nibWithNibName:@"ShopTableViewCell" bundle:nil] forCellReuseIdentifier:@"ShopTableViewCell"];
    self.navigationItem.rightBarButtonItem=[CommanMethods getRightButton:self Action:@selector(search)];
    self.navigationItem.leftBarButtonItem =[CommanMethods getLeftButton:self Action:@selector(menuMethod)];
    
    UIColor *color = [CommanMethods getTextPlaceholderColor];
    UIFont *font = [CommanMethods getDateFont];
    
    [self.segControl setTitleTextAttributes:@{NSFontAttributeName: [CommanMethods getDateFont]} forState:UIControlStateNormal];
    
    self.txtSearchField.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Search By Store" attributes:@{NSForegroundColorAttributeName: color, NSFontAttributeName: font}];
    
    UIImageView *left = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 10, 20)];
    left.contentMode=UIViewContentModeScaleAspectFit;
    self.txtSearchField.leftView = left;
    self.txtSearchField.leftViewMode = UITextFieldViewModeAlways;
    self.txtSearchField.font=font;
    self.txtSearchField.layer.borderColor = [UIColor clearColor].CGColor;
    self.txtSearchCategory.layer.borderColor = [UIColor clearColor].CGColor;
    [self.txtSearchCategory setReturnKeyType:UIReturnKeyDone];
    [self.txtSearchField setReturnKeyType:UIReturnKeyDone];
    
    self.txtSearchField.backgroundColor = [UIColor clearColor] ;
    self.txtSearchField.layer.borderColor = [[CommanMethods getTextColor] colorWithAlphaComponent:0.5f].CGColor;
    self.txtSearchField.layer.borderWidth = 1.0f;
    [self.txtSearchField setReturnKeyType:UIReturnKeyDone];
    
    UIImageView *leftViewForCategory = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 10, 20)];
    leftViewForCategory.contentMode=UIViewContentModeScaleAspectFit;
    self.txtSearchCategory.leftView = leftViewForCategory;
    self.txtSearchCategory.leftViewMode = UITextFieldViewModeAlways;
    self.txtSearchCategory.font=font;
    self.txtSearchCategory.attributedPlaceholder = [[NSAttributedString alloc] initWithString:SEARCH_CATEGORY attributes:@{NSForegroundColorAttributeName: color, NSFontAttributeName: font}];
    
    
    self.txtSearchCategory.backgroundColor = [UIColor clearColor] ;
    self.txtSearchCategory.layer.borderColor = [[CommanMethods getTextColor] colorWithAlphaComponent:0.5f].CGColor;
    self.txtSearchCategory.layer.borderWidth = 1.0f;
    [self.txtSearchCategory setReturnKeyType:UIReturnKeyDone];
    
    
    [self.tblView setSectionIndexColor:[CommanMethods getTextColor]];
    [[self tblView] setSectionIndexBackgroundColor:[UIColor clearColor]];
    
    
    if ([self.tblView respondsToSelector:@selector(setSectionIndexColor:)])
    {
        self.tblView.sectionIndexTrackingBackgroundColor =[UIColor clearColor];
    }
    [self.tblView setSectionIndexColor:[CommanMethods getTextColor]];
    [self.tblView setSeparatorInset:UIEdgeInsetsZero];
    [self.tblView setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    
    [self setPadding:_txtSearchCategory];
    
    CGFloat startpoint=[UIScreen mainScreen].bounds.size.width;
    
    sideView = [[UIView alloc] initWithFrame:CGRectMake(startpoint,0, Width, Height)];
    sideView.backgroundColor = [UIColor clearColor];
    [self.tblView addSubview:sideView];
    
    
    UIView *callView=[[UIView alloc]initWithFrame:CGRectMake(0, 0, Width/2, Height)];
    callView.backgroundColor=[UIColor clearColor];
    UIImageView *imgCallBgImageView=[[UIImageView alloc]initWithFrame:callView.frame];
    imgCallBgImageView.image=[UIImage imageNamed:@"SwipeRed"];
    [callView addSubview:imgCallBgImageView];
    [sideView addSubview:callView];
    
    btnCall = [[UIButton alloc] initWithFrame:CGRectMake(0,0, Width/2, Height)];
    [btnCall setTitle:@"Call" forState:UIControlStateNormal];
    [btnCall addTarget:self action:@selector(call:) forControlEvents:UIControlEventTouchUpInside];
    btnCall.backgroundColor=[UIColor clearColor];
    btnCall.titleLabel.font=[CommanMethods getDateFont];
    
    UIImageView *imgCallImageView=[[UIImageView alloc]initWithFrame:CGRectMake(15,14,20,20)];
    imgCallImageView.image=[UIImage imageNamed:@"swipe-call6"];
    imgCallImageView.contentMode=UIViewContentModeScaleToFill;
    [callView addSubview:imgCallBgImageView];
    [callView addSubview:imgCallImageView];
    [callView addSubview:btnCall];
    [sideView addSubview:callView];
    
    UIView *favView=[[UIView alloc]initWithFrame:CGRectMake(Width/2,0, Width/2+5, Height)];
    UIImageView *imgFavBgImageView=[[UIImageView alloc]initWithFrame:CGRectMake(0,0,Width/2+5, Height)];
    UIImage *img=[UIImage imageNamed:@"swipeBlack"];
    imgFavBgImageView.image=img;
    
    [favView addSubview:imgFavBgImageView];
    btnFav = [[UIButton alloc] initWithFrame:CGRectMake(0,0, Width/2, Height)];
    [btnFav setTitle:@"Favorite" forState:UIControlStateNormal];
    [btnFav addTarget:self action:@selector(favIconClickedFromSwipeView:) forControlEvents:UIControlEventTouchUpInside];
    btnFav.backgroundColor=[UIColor clearColor];
    btnFav.titleLabel.font=[CommanMethods getDateFont];
    btnFav.titleLabel.textAlignment = NSTextAlignmentRight;
    
    imgFavImageView=[[UIImageView alloc]initWithFrame:CGRectMake(5,16,16,16)];
    imgFavImageView.image=[UIImage imageNamed:@"shop-whiteFav"];
    imgFavImageView.contentMode=UIViewContentModeScaleAspectFit;
    
    [favView addSubview:imgFavImageView];
    [favView addSubview:btnFav];
    [sideView addSubview:favView];
    
    sideView .frame = CGRectMake(startpoint,0, SCREEN_WIDTH, Height);
    sideView.center = CGPointMake(sideView.center.x, sideView.center.y);
    
    UISwipeGestureRecognizer *swipeRight = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(swipeRightMethod:)];
    swipeRight.direction = UISwipeGestureRecognizerDirectionRight;
    [sideView addGestureRecognizer:swipeRight];
    hasSwipedLeft = false;
    
    
    indicatorObj=[[UIActivityIndicatorView alloc]initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    [self.view addSubview:indicatorObj];
    [self.view bringSubviewToFront:indicatorObj];
    indicatorObj.center=self.view.center;
    indicatorObj.hidesWhenStopped=YES;
    indicatorObj.hidden=NO;
    hasSwipedLeft = false;
    
    
    indexArray=[CommanMethods getAlphabets];
    arrAllData=[[NSMutableArray alloc]init];
    arrNameData=[[NSMutableArray alloc]init];
    arrCategory=[[NSMutableArray alloc]init];
    arrSearch=[[NSMutableArray alloc]init];
    favArray=[[NSMutableArray alloc]init];
    
    dataDict=[[NSMutableDictionary alloc]init];
    
    if([self.strType isEqualToString:APP_SHOP])
    {
        self.segControl.selectedSegmentIndex=1;
    }
    else if([self.strType isEqualToString:APP_DINE])
    {
        self.segControl.selectedSegmentIndex=2;
    }
    else if([self.strType isEqualToString:APP_ENTERTAINMENT])
    {
        self.segControl.selectedSegmentIndex=3;
    }
    else
    {
        self.segControl.selectedSegmentIndex=0;
    }
    [indicatorObj startAnimating];
    dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
    dispatch_async(queue, ^{
        self.dataModelObj=[DBOpreation getStoreData];
        [self segValueChanged:self.segControl];
    });
}
-(void)viewWillAppear:(BOOL)animated
{
    hasSwipedLeft = false;
    
    CGFloat startpoint =  [UIScreen mainScreen].bounds.size.width;
    sideView .frame = CGRectMake(startpoint,0, SCREEN_WIDTH, Height);
    sideView.center = CGPointMake(sideView.center.x, sideView.center.y);
    
    [favArray removeAllObjects];
    favArray=[DBOpreation   FavouriteList];
    [self.tblView reloadData];
}
-(IBAction)segValueChanged:(id)sender
{
    dispatch_async(dispatch_get_main_queue(), ^{
        
        CGRect rect=self.view.frame;
        float diff=(rect.size.width-30)/2;
        
        [self.txtSearchCategory setBackground:[UIImage imageNamed:@"shop-filed-bg6"]];
        [self.txtSearchField setBackground:[UIImage imageNamed:@"shop-filed-bg6"]];
        if(self.segControl.selectedSegmentIndex==1)
        {
            _txtSearchCategory.hidden=NO;
            self.strType=APP_SHOP;
            [GoogleAnalyticsHandler sendScreenName:screenName_Shopping];
            self.searchWidthConstraint.constant=diff;
            self.categorywidthConstraint.constant=diff;
        }
        else if(self.segControl.selectedSegmentIndex==2)
        {
            _txtSearchCategory.hidden=NO;
            
            self.strType=APP_DINE;
            [GoogleAnalyticsHandler sendScreenName:screenName_Dining];
            self.searchWidthConstraint.constant=diff;
            self.categorywidthConstraint.constant=diff;
        }
        else if(self.segControl.selectedSegmentIndex==3)
        {
            self.strType=APP_ENTERTAINMENT;
            
            [GoogleAnalyticsHandler sendScreenName:screenName_Entertainment];
            _txtSearchCategory.hidden=YES;
            self.searchWidthConstraint.constant=self.view.frame.size.width-20;
            self.categorywidthConstraint.constant=0;
            [self.txtSearchField setBackground:[UIImage imageNamed:@"search-field-bg6"]];
        }
        else
        {
            self.strType=APP_ALLSTORE;
            [GoogleAnalyticsHandler sendScreenName:@"Stores"];
            _txtSearchCategory.hidden=YES;
            self.searchWidthConstraint.constant=self.view.frame.size.width-20;
            self.categorywidthConstraint.constant=0;
            [self.txtSearchField setBackground:[UIImage imageNamed:@"search-field-bg6"]];
        }
    });
    [arrNameData removeAllObjects];
    [arrSearch removeAllObjects];
    [arrAllData removeAllObjects];
    [arrCategory removeAllObjects];
    
    [arrCategory addObject:SEARCH_CATEGORY];
    _txtSearchCategory.text = @"";
    selectedModel=nil;

    
    if(pickerView) {
        [self.view endEditing:YES];
    }
    
    if(self.segControl.selectedSegmentIndex==1)
    {
        [arrNameData addObjectsFromArray:self.dataModelObj.arrShopsName];
        [arrAllData addObjectsFromArray:self.dataModelObj.arrShopsAll];
        [arrCategory addObjectsFromArray:self.dataModelObj.arrshopsCategory];
    }
    else if(self.segControl.selectedSegmentIndex==2)
    {
        [arrNameData addObjectsFromArray:self.dataModelObj.arrDineName];
        [arrAllData addObjectsFromArray:self.dataModelObj.arrDineAll];
        [arrCategory addObjectsFromArray:self.dataModelObj.arrDineCategory];
    }
    else if(self.segControl.selectedSegmentIndex==3)
    {
        [arrNameData addObjectsFromArray:self.dataModelObj.arrEntertainmentsName];
        [arrAllData addObjectsFromArray:self.dataModelObj.arrEntertainmentsAll];
    }
    else
    {
        [arrNameData addObjectsFromArray:self.dataModelObj.arrShopsName];
        [arrAllData addObjectsFromArray:self.dataModelObj.arrShopsAll];
        
        [arrNameData addObjectsFromArray:self.dataModelObj.arrDineName];
        [arrAllData addObjectsFromArray:self.dataModelObj.arrDineAll];
        
        [arrNameData addObjectsFromArray:self.dataModelObj.arrEntertainmentsName];
        [arrAllData addObjectsFromArray:self.dataModelObj.arrEntertainmentsAll];
    }
    
    //filterTextData
    [arrSearch addObjectsFromArray:arrNameData];
    NSString *newText =self.txtSearchField.text;
    newText=[newText stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    [self filterTextData:newText];
}
#pragma mark FilterArray
/******************************************
 Method name - getFilterData
 Parameter   - NSMutableArray
 Return      - nil
 Desc        - This method sorts the data as per  in table view
 ****************************************/
-(void)getFilterData:(NSArray *)array
{
    dispatch_async(dispatch_get_main_queue(), ^{
        [dataDict removeAllObjects];
        [self.tblView reloadData];
        NSMutableArray *dummyArray=[[NSMutableArray alloc]initWithArray:array];
        if(!dataDict)
        {
            dataDict=[[NSMutableDictionary alloc]init];
        }
        NSPredicate *predicate;
        for (NSString *strAlpha in indexArray )
        {
            predicate=[NSPredicate predicateWithFormat:@"strTitle beginswith[cd] %@",strAlpha];
            NSArray *dataArray=[dummyArray filteredArrayUsingPredicate:predicate];
            if([dataArray count]>0)
            {
                [dataDict setObject:dataArray forKey:strAlpha];
            }
            if(![strAlpha isEqualToString:@"#"])
            {
                [dummyArray removeObjectsInArray:dataArray];
            }
        }
        [dataDict setObject:dummyArray forKey:@"#"];
        
        [self resetSwipedCell];
        [self.tblView reloadData];
        if([indicatorObj isAnimating])
        {
            [indicatorObj stopAnimating];
        }
    });
}
#pragma mark- Selector Method
/******************************************
 Method name - menuMethod
 Parameter   - nil
 Desc        - Method for opening Left Menu
 ****************************************/
- (void)menuMethod
{
    [self.sideMenuViewController presentLeftMenuViewController];
}
- (void)search
{
    [[AppDelegate appDelegateInstance]openSearchScreen:self.navigationController];
}
#pragma mark- Memory Worning Method
/******************************************
 Method name - didReceiveMemoryWarning
 Parameter   - nil
 Desc        - Method for Memory Worning
 ****************************************/
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}


#pragma mark - Table view data source
/******************************************
 Method name - numberOfSectionsInTableView
 Parameter   - UITableView
 Return      - NSInteger
 Desc        - Method for retuurning number of section in table view
 ****************************************/
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    if([[dataDict allKeys]count]>0)
    {
        return [indexArray count];
    }
    return  0;
}
/******************************************
 Method name - sectionIndexTitlesForTableView
 Parameter   - UITableView
 Return      - NSArray
 Desc        - Method for returning array for table indexes in table view
 ****************************************/
- (NSArray *)sectionIndexTitlesForTableView:(UITableView *)tableView
{
    if([[dataDict allKeys]count]>0)
        return indexArray;
    return nil;
}
/******************************************
 Method name - sectionForSectionIndexTitle
 Parameter   - UITableView
 Return      - NSInteger
 Desc        - Method for returning title for section index
 ****************************************/
- (NSInteger)tableView:(UITableView *)tableView sectionForSectionIndexTitle:(NSString *)title atIndex:(NSInteger)index
{
    if([[dataDict allKeys]count]>0)
    {
        return [indexArray indexOfObject:title];
    }
    
    return -1;
}
/******************************************
 Method name - heightForRowAtIndexPath
 Parameter   - UITableView, NSIndexPath
 Return      - CGFloat
 Desc        - Method for returning size of row in table view
 ****************************************/
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 52.0f;
}
/******************************************
 Method name - numberOfRowsInSection
 Parameter   - UITableView, NSInteger
 Return      - NSInteger
 Desc        - Method for returning number of rows in table view
 ****************************************/
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    NSString *strSection=[indexArray objectAtIndex:section];
    NSArray *array=[dataDict objectForKey:strSection];
    return [array count];
}
/******************************************
 Method name - cellForRowAtIndexPath
 Parameter   - UITableView, NSIndexPath
 Return      - UITableViewCell
 Desc        - Method for returning cell of table
 ****************************************/
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier=nil;
    ShopTableViewCell   *cell;
    CellIdentifier = @"ShopTableViewCell";
    cell =[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    NSString *strSection=[indexArray objectAtIndex:indexPath.section];
    NSArray *array=[dataDict objectForKey:strSection];
    ShopBaseModel *dbModel=[array objectAtIndex:indexPath.row];
    cell.lblTitle.text=[dbModel.strTitle uppercaseString];
    cell.lblDesc.text=dbModel.strFloor;
    
    cell.lblTitle.font=[CommanMethods getDetailsFont];
    cell.lblDesc.font=[CommanMethods getDateFont];
    
    
    
    cell.backgroundColor=[UIColor clearColor];
    cell.contentView.backgroundColor=[UIColor clearColor];
    cell.backgroundView.backgroundColor=[UIColor clearColor];
    [cell.btnMap addTarget:self action:@selector(mapIconClick:) forControlEvents:UIControlEventTouchUpInside];
    [cell.btnFav addTarget:self action:@selector(favIconClicked:) forControlEvents:UIControlEventTouchUpInside];
    
    
    UISwipeGestureRecognizer *swipeRight = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(swipeRightMethod:)];
    swipeRight.direction = UISwipeGestureRecognizerDirectionRight;
    [cell addGestureRecognizer:swipeRight];
    [self setImageforFavButton:indexPath Button:cell.btnFav Type:@"table"];
    
    
    UISwipeGestureRecognizer *swipeLeft = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(swipeLeftMethod:)];
    swipeLeft.direction = UISwipeGestureRecognizerDirectionLeft;
    [cell addGestureRecognizer:swipeLeft];
    
    
    return  cell;
}
-(void)setImageforFavButton:(NSIndexPath *)indexPath Button:(UIButton *)btn Type:(NSString *)strTypeVal
{
    NSString *strSection=[indexArray objectAtIndex:indexPath.section];
    NSArray *array=[dataDict objectForKey:strSection];
    ShopBaseModel *dbModel=[array objectAtIndex:indexPath.row];
    FavouriteListModel *isFav=[self isRecordisFav:dbModel.numID Type:dbModel.strType];
    UIImage *favImage;
    
    
    if([strTypeVal isEqualToString:@"table"])
    {
        if(isFav)
        {
            favImage=[UIImage imageNamed:@"shopico-fav-active6"];
        }
        else
        {
            favImage=[UIImage imageNamed:@"shop-ico-fav-a6"];
        }
        [btn setImage:favImage forState:UIControlStateNormal];
    }
    else
    {
        if(isFav)
        {
            favImage=[UIImage imageNamed:@"shop-whiteSelectedFav"];
            [btnFav setTitle:@"    Unfavorite" forState:UIControlStateNormal];
        }
        else
        {
            favImage=[UIImage imageNamed:@"shop-whiteFav"];
            [btnFav setTitle:@"Favorite" forState:UIControlStateNormal];
        }
        [imgFavImageView setImage:favImage ];
    }
}
- (void)swipeLeftMethod:(UISwipeGestureRecognizer *)gesture {
    isCellActionPerformed=YES;
    if(!hasSwipedLeft)
    {
        swipedCell = (ShopTableViewCell *)gesture.view;
        CGPoint location = [gesture locationInView:self.tblView];
        NSIndexPath *indexPath = [self.tblView indexPathForRowAtPoint:location];
        [self setImageforFavButton:indexPath Button:btnFav Type:@"swipe"];
        
        // sideView.backgroundColor = [UIColor clearColor];
        swipeIndexPath=indexPath;
        //Get location of the swipe
        UITableViewCell *cell = [self.tblView  cellForRowAtIndexPath:indexPath];
        sideView.center = CGPointMake(sideView.center.x,cell.center.y);
        [UIView animateWithDuration:0.6 animations:^{
            gesture.view.center=CGPointMake(gesture.view.center.x- Width+15, gesture.view.center.y);
            sideView.center = CGPointMake(sideView.center.x - Width, sideView.center.y);
        }completion:^(BOOL finished) {
            if(finished) {
                hasSwipedLeft = !hasSwipedLeft;
                isCellActionPerformed=!isCellActionPerformed;
                sideView.tag = cell.tag;
            }
        }];
    }
    
}
/******************************************
 Method name - swipeRightMethod
 Parameter   - UISwipeGestureRecognizer
 Return      - void
 Desc        - swipe Right Method
 ****************************************/
- (void)swipeRightMethod:(UISwipeGestureRecognizer *)gesture {
    
    CGPoint location = [gesture locationInView:self.tblView];
    NSIndexPath *indexPath = [self.tblView indexPathForRowAtPoint:location];
    if(hasSwipedLeft && [indexPath isEqual:swipeIndexPath])
    {
        [self setImageforFavButton:indexPath Button:btnFav Type:@"swipe"];
        UITableViewCell *cell = [self.tblView cellForRowAtIndexPath:indexPath];
        swipeIndexPath=indexPath;
        sideView.center = CGPointMake(sideView.center.x, cell.center.y);
        [UIView animateWithDuration:0.6 animations:^{
            
            cell.center = CGPointMake(cell.center.x + Width-15, cell.center.y);
            sideView.center = CGPointMake(sideView.center.x + Width, sideView.center.y);
            
        }completion:^(BOOL finished) {
            if(finished) {
                hasSwipedLeft = !hasSwipedLeft;
                //sideView.backgroundColor = [UIColor clearColor];
            }
        }];
        
    }
}

/******************************************
 Method name - resetSwipedCell
 Parameter   - nil
 Return      - void
 Desc        - To Reset Cell of original Position/Origin
 ****************************************/
- (void)resetSwipedCell {
    
    if(sideView.frame.origin.x>SCREEN_WIDTH/2){
        
        return;
    }
    
    NSIndexPath *indexPath = [self.tblView indexPathForCell:swipedCell];
    [self setImageforFavButton:indexPath Button:btnFav Type:@"swipe"];
    UITableViewCell *cell = [self.tblView cellForRowAtIndexPath:indexPath];
    swipeIndexPath=nil;
    sideView.center = CGPointMake(sideView.center.x, cell.center.y);
    [UIView animateWithDuration:0.6 animations:^{
        cell.center = CGPointMake(cell.center.x + Width-15, cell.center.y);
        sideView.center = CGPointMake(sideView.center.x + Width, sideView.center.y);
    }completion:^(BOOL finished) {
        if(finished) {
            hasSwipedLeft = false;
            //sideView.backgroundColor = [UIColor clearColor];
        }
    }];
}
/******************************************
 Method name - btn1Method
 Parameter   - id
 Return      - void
 Desc        - btn 1 Method
 ****************************************/
- (void)btn1Method:(id)sender {
    
}
/******************************************
 Method name - btn1Method
 Parameter   - id
 Return      - void
 Desc        - btn 2 Method
 ****************************************/


-(FavouriteListModel *)isRecordisFav:(NSNumber *)recordID Type:(NSString *)strTypeVal
{
    if([favArray count]>0)
    {
        NSString *strPredicate=[NSString stringWithFormat:@"favID == %@ AND favType == '%@'",recordID,strTypeVal];
        NSPredicate *predicate = [NSPredicate predicateWithFormat:strPredicate];
        NSArray *array=[favArray filteredArrayUsingPredicate:predicate];
        if ([array count]>0)
        {
            return  [array objectAtIndex:0];
        }
    }
    return  nil;
}
-(void)call:(id)sender
{
    UIButton *button = (UIButton*)sender;
    CGPoint buttonOrigin = [sender frame].origin;
    CGPoint originInTableView = [self.tblView convertPoint:buttonOrigin fromView:[button superview]];
    NSIndexPath *indexPath = [self.tblView  indexPathForRowAtPoint:originInTableView];
    NSString *strSection=[indexArray objectAtIndex:indexPath.section];
    NSArray *array=[dataDict objectForKey:strSection];
    ShopBaseModel *dbModel=[array objectAtIndex:indexPath.row];
    [[AppDelegate appDelegateInstance]callViaTelephone:dbModel.strTelephone];
}
-(void)mapIconClick:(id)sender
{
    UIButton *button = (UIButton*)sender;
    CGPoint buttonOrigin = [sender frame].origin;
    CGPoint originInTableView = [self.tblView convertPoint:buttonOrigin fromView:[button superview]];
    NSIndexPath *indexPath = [self.tblView  indexPathForRowAtPoint:originInTableView];
    NSString *strSection=[indexArray objectAtIndex:indexPath.section];
    NSArray *array=[dataDict objectForKey:strSection];
    ShopBaseModel *dbModel=[array objectAtIndex:indexPath.row];
    
    NSString *strScreenValue;
    if([dbModel.strType isEqualToString:APP_SHOP])
    {
        strScreenValue=[NSString stringWithFormat:@"%@ - %@ - %@",screenName_Shopping,dbModel.categoryName,screenName_StoreLocator];
    }
    else if([dbModel.strType isEqualToString:APP_DINE])
    {
        strScreenValue=[NSString stringWithFormat:@"%@ - %@ - %@",screenName_Dining,dbModel.categoryName,screenName_StoreLocator];
    }
    else
    {
        strScreenValue=[NSString stringWithFormat:@"%@ - %@ - %@",screenName_Entertainment,dbModel.categoryName,screenName_StoreLocator];
    }
    [GoogleAnalyticsHandler sendScreenName:strScreenValue];
    

    StoreLocatorViewController *storeObj=[[StoreLocatorViewController alloc]initWithNibName:@"StoreLocatorViewController" bundle:nil];
    storeObj.strStoreId=dbModel.strDestination_ID;
    [self.navigationController pushViewController:storeObj animated:YES];
 }
-(void)favIconClicked:(id)sender
{
    isCellActionPerformed=YES;
    [self resetSwipedCell];
    UIButton *button = (UIButton*)sender;
    CGPoint buttonOrigin = [sender frame].origin;
    CGPoint originInTableView = [self.tblView convertPoint:buttonOrigin fromView:[button superview]];
    NSIndexPath *indexPath = [self.tblView  indexPathForRowAtPoint:originInTableView];
    [self performFav:indexPath];
    isCellActionPerformed=NO;
}

-(void)favIconClickedFromSwipeView:(id)sender
{
    [self resetSwipedCell];
    NSIndexPath *indexPath = [self.tblView  indexPathForCell:swipedCell];
    [self performSelector:@selector(performFav:) withObject:indexPath afterDelay:0.3];
}

-(void)performFav:(NSIndexPath *)indexPath
{
    NSString *strSection=[indexArray objectAtIndex:indexPath.section];
    NSArray *array=[dataDict objectForKey:strSection];
    ShopBaseModel *dbModel=[array objectAtIndex:indexPath.row];
    FavouriteListModel *isFav=[self isRecordisFav:dbModel.numID Type:dbModel.strType];
    if(isFav)
    {
        BOOL delete=[DBOpreation deleteFav:dbModel.strType ID:dbModel.numID];
        if(delete)
        {
            [favArray removeObject:isFav];
            [self.tblView reloadRowsAtIndexPaths:[NSArray arrayWithObjects:indexPath, nil] withRowAnimation:UITableViewRowAnimationAutomatic];
            
        }
    }
    else
    {
        BOOL insert=[DBOpreation insertFavData:dbModel.strType ID:dbModel.numID title:dbModel.strTitle];
        if(insert)
        {
            FavouriteListModel *model=[[FavouriteListModel alloc]init];
            model.favID=dbModel.numID;
            model.strTitle=dbModel.strTitle;
            model.favType=dbModel.strType;
            [favArray addObject:model];
            [self.tblView reloadRowsAtIndexPaths:[NSArray arrayWithObjects:indexPath, nil] withRowAnimation:UITableViewRowAnimationAutomatic];
            
            [[WebServiceCommunicator sharedInstance]updateOfferCategoryHits:[[AppDelegate appDelegateInstance].strSiteID intValue] CategoryID:dbModel.catID];
        }
    }
    [self setImageforFavButton:indexPath Button:btnFav Type:@"swipe"];
    
}
/******************************************
 Method name - didSelectRowAtIndexPath
 Parameter   - UITableView, NSIndexPath
 Return      - nil
 Desc        - Method Called on selection of row of table view
 ****************************************/
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [self resetSwipedCell];
    
    ShoppingDetailsTableViewController   *shop=[self.storyboard instantiateViewControllerWithIdentifier:@"ShoppingDetailsTableViewController"];
    NSString *strSection=[indexArray objectAtIndex:indexPath.section];
    NSArray *array=[dataDict objectForKey:strSection];
    ShopBaseModel *dbModel=[array objectAtIndex:indexPath.row];
    shop.shopBaseModelObj=nil;
    shop.shopBaseModelObj=dbModel;
    [self.navigationController pushViewController:shop animated:YES];
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (!isCellActionPerformed && !hasSwipedLeft)
    {
        
        CATransition *transition = [CATransition animation];
        [transition setType:kCATransitionPush];
        [transition setSubtype:kCATransitionFromLeft];
        cell.layer.shadowColor = [[CommanMethods  getTextColor]CGColor];
        cell.layer.shadowOffset = CGSizeMake(10, 10);
        cell.alpha = 0;
        CATransform3D rotation;
        rotation = CATransform3DMakeRotation( 0, 0.0, 0, 0);
        rotation.m34 = 1.0/ -600;
        cell.layer.transform = rotation;
        [cell.layer addAnimation:transition forKey:@"rotation1"];
        [UIView beginAnimations:@"rotation" context:NULL];
        [UIView setAnimationDuration:0.3];
        cell.layer.transform = CATransform3DIdentity;
        cell.alpha = 1;
        cell.layer.shadowOffset = CGSizeMake(0, 0);
        [UIView commitAnimations];
    }
}
#pragma mark- textField Delegate
/******************************************
 Method name - textFieldShouldReturn
 Parameter   - UITextField
 Return      - BOOL
 Desc        - Method Called on selection of textField
 ****************************************/
-(BOOL) textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}

/******************************************
 Method name - shouldChangeCharactersInRange
 Parameter   - UITextField, NSRange, NSString
 Return      - BOOL
 Desc        - Method Called when text entered in the text field changes
 ****************************************/
-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    if([textField isEqual:_txtSearchField])
    {
        NSString *newText = [textField.text stringByReplacingCharactersInRange:range withString:string];
        newText=[newText stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
        [self filterTextData:newText];
    }
    return YES;
}

/******************************************
 Method name - textFieldDidBeginEditing
 Parameter   - UITextField
 Return      - nil
 Desc        - Method Called when text is entered in text filed
 ****************************************/
- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    if ([textField isEqual:_txtSearchCategory])
    {
        if(!pickerView)
        {
            pickerView = [[UIPickerView alloc] initWithFrame:CGRectMake(0, 0, 0, 0)];
            pickerView.dataSource = self;
            pickerView.delegate = self;
            
            NSMutableArray *barItems;
            
            mypickerToolbar = [[UIToolbar alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 44)];
            mypickerToolbar.barStyle = UIBarStyleBlack;
            [mypickerToolbar sizeToFit];
            
            barItems = [[NSMutableArray alloc] init];
            
            UIBarButtonItem *cancelBtn = [[UIBarButtonItem alloc]initWithTitle:@"CANCEL" style:UIBarButtonItemStyleDone target:self action:@selector(cancelTouched:)];
            cancelBtn.tintColor=[CommanMethods getTextColor];
            [barItems addObject:cancelBtn];
            
            UIBarButtonItem *flexSpace = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:self action:nil];
            [barItems addObject:flexSpace];
            
            
            UILabel *lblTitle=[[UILabel alloc]initWithFrame:CGRectMake(0,2,220,44)];
            lblTitle.text=@"SEARCH BY CATEGORY";
            lblTitle.textColor=[CommanMethods getTextColor];
            lblTitle.font=[CommanMethods getHeaderFont];
            lblTitle.textAlignment=NSTextAlignmentCenter;
            lblTitle.center=mypickerToolbar.center;
            UIBarButtonItem *btnSearch = [[UIBarButtonItem alloc]initWithCustomView:lblTitle];
            [barItems addObject:btnSearch];
            
            UIBarButtonItem *flexSpace1 = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:self action:nil];
            [barItems addObject:flexSpace1];
            
            
            UIBarButtonItem *doneBtn = [[UIBarButtonItem alloc]initWithTitle:@"DONE" style:UIBarButtonItemStyleDone target:self action:@selector(pickerDoneClicked)];
            doneBtn.tintColor=[CommanMethods getTextColor];
            
            [barItems addObject:doneBtn];
            [mypickerToolbar setItems:barItems animated:YES];
            
            [textField setTintColor:[UIColor lightGrayColor]];
            [textField setTintColor:[UIColor clearColor]];
        }
        
        
        pickerView.hidden=NO;
        [ pickerView  setShowsSelectionIndicator:YES];
        textField.inputView =  pickerView;
        textField.inputAccessoryView = mypickerToolbar;
        [pickerView reloadAllComponents];
        if([arrCategory count]>0)
        {
            [pickerView selectRow:0 inComponent:0 animated:YES];
        }
    }
}

/******************************************
 Method name - pickerDoneClicked
 Parameter   - nil
 Return      - nil
 Desc        -  Selector method Called on click of done button of toolbar
 ****************************************/
-(void)pickerDoneClicked
{
    [self.view endEditing:YES];
    _txtSearchCategory.text = @"";
    NSInteger row = [pickerView selectedRowInComponent:0];
    [self filterRecord:row];
}
/******************************************
 Method name - filterRecord
 Parameter   - NSUInteger
 Return      - nil
 Desc        - Method for filtering records at row
 ****************************************/
-(void)filterRecord:(NSUInteger)row
{
    selectedModel=nil;
    if([[arrCategory objectAtIndex:row]isKindOfClass:[CategoryModel class]])
    {
        selectedModel=[arrCategory objectAtIndex:row];
        NSString *strName=nil;
        strName=selectedModel.categoryName;
        _txtSearchCategory.text=strName;
        
        NSString *newText =self.txtSearchField.text;
        newText=[newText stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
        
        NSString *strPredicate=[NSString stringWithFormat:@"catID == %d",selectedModel.catID];
        NSPredicate *predicate = [NSPredicate predicateWithFormat:strPredicate];
        NSArray *array=[arrAllData filteredArrayUsingPredicate:predicate];
        [arrSearch removeAllObjects];
        
        //search by text
        if([newText length]>0)
        {
            NSPredicate *predicateText;
            predicateText=[NSPredicate predicateWithFormat:@"strTitle beginswith[cd] %@",newText];
            NSArray *arrayData=[array filteredArrayUsingPredicate:predicateText];
            arrSearch=[NSMutableArray arrayWithArray:arrayData];
        }
        else
        {
            arrSearch=[NSMutableArray arrayWithArray:array];
        }
        [self getFilterData:(NSMutableArray *)arrSearch];
    }
    else
    {
        _txtSearchCategory.text=@"";
        [arrSearch removeAllObjects];
        [arrSearch addObjectsFromArray:arrNameData];
        [self filterTextData:self.txtSearchField.text];
    }
}
-(void)filterTextData:(NSString *)newText
{
    NSPredicate *predicate;
    if(selectedModel!=nil)
    {
        NSString *strPredicate=[NSString stringWithFormat:@"catID == %d",selectedModel.catID];
        predicate=[NSPredicate predicateWithFormat:strPredicate];
    }
    
    if ([newText length] == 0)
    {
        [arrSearch removeAllObjects];
        if(predicate)
        {
            NSArray *array=[arrNameData filteredArrayUsingPredicate:predicate];
            [arrSearch addObjectsFromArray:array];
        }
        else
        {
            [arrSearch addObjectsFromArray:arrNameData];
        }
        [self getFilterData:arrSearch];
    }
    else
    {
        NSArray *array;
        if(predicate)
        {
            array=[arrSearch filteredArrayUsingPredicate:predicate];
        }
        else
        {
            array=[NSArray  arrayWithArray:arrSearch];
        }
        NSPredicate *predicatetext;
        predicatetext=[NSPredicate predicateWithFormat:@"strTitle beginswith[cd] %@",newText];
        NSArray *arrayData=[array filteredArrayUsingPredicate:predicatetext];
        [self getFilterData:(NSMutableArray *)arrayData];
    }
}

/******************************************
 Method name - cancelTouched
 Parameter   - nil
 Return      - nil
 Desc        - Selector method Called on click of cancel button of toolbar
 ****************************************/
- (void)cancelTouched:(id)sender
{
    [self.view endEditing:YES];
}
#pragma mark - UIPickerViewDataSource
/******************************************
 Method name - numberOfComponentsInPickerView
 Parameter   - UIPickerView
 Return      - NSInteger
 Desc        - Delegate method called for number of records in the picker view
 ****************************************/
- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    return 1;
}

/******************************************
 Method name - numberOfRowsInComponent
 Parameter   - UIPickerView, NSInteger
 Return      - NSInteger
 Desc        - Delegate method called for number of rows in component in the picker view
 ****************************************/
- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    return[arrCategory count];
}
#pragma mark - UIPickerViewDelegate
/******************************************
 Method name - titleForRow
 Parameter   - UIPickerView, NSInteger
 Return      - NSString
 Desc        - Delegate method called for title of rows in component of the picker view
 ****************************************/
-(NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    NSString *strName=nil;
    
    if([[arrCategory objectAtIndex:row]isKindOfClass:[CategoryModel class]])
    {
        CategoryModel *dbModel=[arrCategory  objectAtIndex:row];
        strName=dbModel.categoryName;
    }
    else
    {
        strName=[arrCategory objectAtIndex:row];
    }
    return strName;
}
- (UIView *)pickerView:(UIPickerView *)pickerView viewForRow:(NSInteger)row forComponent:(NSInteger)component reusingView:(UIView *)view
{
    UILabel *lblTitle;
    if(!view)
    {
        view=[[UIView alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 44)];
        lblTitle=[[UILabel alloc]initWithFrame:view.frame];
        lblTitle.backgroundColor=[UIColor clearColor];
        lblTitle.tag=111;
        [view addSubview:lblTitle];
    }
    
    lblTitle=(UILabel *)[view viewWithTag:111];
    lblTitle.textAlignment=NSTextAlignmentCenter;
    lblTitle.font=[CommanMethods getHeaderFont];
    
    NSString *strName=nil;
    if([[arrCategory objectAtIndex:row]isKindOfClass:[CategoryModel class]])
    {
        CategoryModel *dbModel=[arrCategory  objectAtIndex:row];
        strName=dbModel.categoryName;
    }
    else
    {
        strName=[arrCategory objectAtIndex:row];
    }
    view.backgroundColor=[UIColor clearColor];
    lblTitle.text=strName;
    return  view;
}
-(void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    [self resetSwipedCell];
}

/******************************************
 Method name - setPadding
 Parameter   - UITextField
 Return      - nil
 Desc        - method for giving padding to text
 ****************************************/
- (void)setPadding:(UITextField*)textField
{
    if(textField != _txtSearchCategory)
    {
        UIView *paddingView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 10, textField.frame.size.height)];
        textField.leftView = paddingView;
        textField.leftViewMode = UITextFieldViewModeAlways;
    }
}
-(NSArray *)sortArrayAlphabetically :(NSArray *)Array
{
    NSSortDescriptor *sortDescriptor =
    [NSSortDescriptor sortDescriptorWithKey:@"strTitle"
                                  ascending:YES
                                   selector:@selector(caseInsensitiveCompare:)];
    return [Array sortedArrayUsingDescriptors:@[sortDescriptor]];
}
-(void)dealloc
{
    arrSearch=nil;
    arrAllData=nil;
    arrNameData=nil;
    arrCategory=nil;
    dataDict=nil;
    pickerView=nil;
    mypickerToolbar=nil;
    favArray=nil;
}
@end
