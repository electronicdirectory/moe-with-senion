//
//  ShopAllDataModel.h
//  CityCenter
//
//  Created by webwerks on 4/9/16.
//  Copyright © 2016 Nivrutti. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ShopAllDataModel : NSObject
@property(nonatomic,strong)NSArray *arrShopsAll;
@property(nonatomic,strong)NSArray *arrEntertainmentsAll;
@property(nonatomic,strong)NSArray *arrDineAll;

@property(nonatomic,strong)NSArray *arrShopsName;
@property(nonatomic,strong)NSArray *arrEntertainmentsName;
@property(nonatomic,strong)NSArray *arrDineName;

@property(nonatomic,strong)NSArray *arrshopsCategory;
@property(nonatomic,strong)NSArray *arrDineCategory;
@end
