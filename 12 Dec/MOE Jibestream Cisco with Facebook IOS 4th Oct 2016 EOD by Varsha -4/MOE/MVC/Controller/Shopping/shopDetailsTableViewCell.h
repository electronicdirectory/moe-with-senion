//
//  shopDetailsTableViewCell.h
//  MOE
//
//  Created by Nivrutti on 6/25/15.
//  Copyright (c) 2015 Neosoft. All rights reserved.
//

#import <UIKit/UIKit.h>
@interface shopDetailsTableViewCell : UITableViewCell
@property(nonatomic,strong)IBOutlet UIScrollView *scrollView;
@property(nonatomic,strong)IBOutlet UIPageControl *pageControl;
@property(nonatomic,strong)IBOutlet UIButton *btnFav;
@property(nonatomic,strong)IBOutlet UIButton *btnMap;
@property(nonatomic,strong)IBOutlet UIButton *btnShare;
@property(nonatomic,strong)IBOutlet UIButton *btnCall;
@property(nonatomic,strong)IBOutlet UIImageView *imgFav;
@property(nonatomic,strong)IBOutlet UIView *bottomView;
 @property(nonatomic,strong)IBOutlet UIButton *btnMap1;
@property(nonatomic,strong)IBOutlet UIButton *btnShare1;
@property(nonatomic,strong)IBOutlet UIView *bottomView1;
@property(nonatomic,strong)IBOutlet NSLayoutConstraint *scrollViewHeight;

@end
