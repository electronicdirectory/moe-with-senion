//
//  ShoppingDetailsTableViewController.m
//  MOE
//
//  Created by Nivrutti on 6/25/15.
//  Copyright (c) 2015 Neosoft. All rights reserved.
//

#import "ShoppingDetailsTableViewController.h"
#import "OfferTableViewCell.h"
#import "shopDetailsTableViewCell.h"
#import "shopDetailsTableViewCell.h"
#import "WebViewController.h"
#import "offerDetailViewController.h"
#import "FavouriteListModel.h"
#import "BaseModel.h"
#import "offerDetailDescCell.h"
#import "GTMNSString+HTML.h"
#import "StoreLocatorViewController.h"
@interface ShoppingDetailsTableViewController ()
{
    BOOL isScrollAdded;
    UIImage *placeHolder;
    float imageHeight;
    
    float webViewHeight;
    BOOL isWebViewLoding;
    BOOL isAnimated,istranfarm;
    
    
    
}
@property(nonatomic,strong)NSArray *arrayFeatureProduct;
@property(nonatomic,strong)NSArray *arrOffer;
@property(nonatomic,strong)FavouriteListModel *favModel;
@property(nonatomic,strong)NSString *strHtml;
@property(nonatomic,strong)NSMutableArray *imgData;


@end

@implementation ShoppingDetailsTableViewController
@synthesize arrayFeatureProduct,arrOffer,favModel;
#define Height 36
#define DESCCELL @"offerDetailDescCell"

- (void)viewDidLoad {
    [super viewDidLoad];
    
    if(IS_IPHONE_4_OR_LESS)
        imageHeight = IPHONE4_DETAILS_IMAGE_HEIGHT;
    else if (IS_IPHONE_5)
        imageHeight = IPHONE5_DETAILS_IMAGE_HEIGHT;
    else if (IS_IPHONE_6)
        imageHeight = IPHONE6_DETAILS_IMAGE_HEIGHT;
    else if (IS_IPHONE_6P)
        imageHeight = IPHONE6PLUS_DETAILS_IMAGE_HEIGHT;
    
    UIImageView *backgroundImage=[[UIImageView alloc]initWithImage:[UIImage imageNamed:@"background6"]];
    backgroundImage.frame=CGRectMake(0, 0, self.tableView.contentSize.width, self.tableView.contentSize.height);
    self.tableView.backgroundView=backgroundImage;
     istranfarm=YES;
    
    placeHolder=[UIImage imageNamed:@"place-holder-entertainment1-6p"];
    arrayFeatureProduct=self.shopBaseModelObj.arrayFeatureProduct;
    arrOffer=[DBOpreation getOfferByStoreID:self.shopBaseModelObj.numID];
    self.navigationItem.titleView = [CommanMethods getLabel:[self.shopBaseModelObj.strType uppercaseString]];
    [self.tableView registerNib:[UINib nibWithNibName:@"shopDetailsTableViewCell" bundle:nil] forCellReuseIdentifier:@"shopDetailsTableViewCell"];
    [self.tableView registerNib:[UINib nibWithNibName:@"OfferTableViewCell" bundle:nil] forCellReuseIdentifier:@"OfferTableViewCell"];
    [self.tableView registerNib:[UINib nibWithNibName:@"HotelTableViewCell" bundle:nil] forCellReuseIdentifier:@"HotelTableViewCell"];
    [self.tableView registerNib:[UINib nibWithNibName:DESCCELL bundle:nil] forCellReuseIdentifier:DESCCELL];
    
    if([self.shopBaseModelObj.arrImageList count]>0)
    {
        self.imgData=[[NSMutableArray alloc]initWithArray:self.shopBaseModelObj.arrImageList];
        NSURL *url =[CommanMethods getImageURL:self.shopBaseModelObj.strImage];
        if(url)
        {
            NSDictionary *dict=[NSDictionary dictionaryWithObjectsAndKeys:self.shopBaseModelObj.strImage,@"Image", nil];
            [self.imgData addObject:dict];
        }
    }
    
    
    if(([arrayFeatureProduct count]==0 && [arrOffer count] ==0))
    {
        NSString *strTitle=[CommanMethods rePlaceTags:[self.shopBaseModelObj.strTitle uppercaseString]];
        NSString *strDesc = [CommanMethods rePlaceTags:self.shopBaseModelObj.strDescription];
        strDesc=[NSString stringWithString:[strDesc gtm_stringByUnescapingFromHTML]];
        self.strHtml=[CommanMethods getHtml:strTitle Date:@" " Desc:strDesc];
    }
    
    NSString *strScreenValue;
    if([self.shopBaseModelObj.strType isEqualToString:APP_SHOP])
    {
        strScreenValue=[NSString stringWithFormat:@"%@ - %@",screenName_Shopping,self.shopBaseModelObj.categoryName];
    }
    else if([self.shopBaseModelObj.strType isEqualToString:APP_DINE])
    {
        strScreenValue=[NSString stringWithFormat:@"%@ - %@",screenName_Dining,self.shopBaseModelObj.categoryName];
    }
    else
    {
        strScreenValue=[NSString stringWithFormat:@"%@ - %@",screenName_Entertainment,self.shopBaseModelObj.categoryName];
    }
    [GoogleAnalyticsHandler sendScreenName:strScreenValue];
    
    
    favModel=[DBOpreation getFav:self.shopBaseModelObj.numID Type:self.shopBaseModelObj.strType];
    [[WebServiceCommunicator sharedInstance]updateOfferCategoryHits:[[AppDelegate appDelegateInstance].strSiteID intValue] CategoryID:self.shopBaseModelObj.catID];
}
-(void)viewWillAppear:(BOOL)animated
{
    if (isAnimated) {
            [self.tableView reloadData];
    }

}
-(void)viewWillDisappear:(BOOL)animated
{
    self.webView.delegate=nil;
    [self.webView removeFromSuperview];
    self.webView=nil;
}
#pragma mark- Selector Method
/******************************************
 Method name - menuMethod
 Parameter   - nil
 Desc        - Method for opening Left Menu
 ****************************************/
- (void)menuMethod
{
    [self.sideMenuViewController presentLeftMenuViewController];
}
- (void)search
{
    [[AppDelegate appDelegateInstance]openSearchScreen:self.navigationController];
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma mark - Table view data source
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    if([arrayFeatureProduct count]>0 && [arrOffer count]>0)
    {
        return  3;
    }
    return 2;
}
- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    if(([arrayFeatureProduct count]>0 || [arrOffer count]>0) && section>0)
    {
        UIView *view=[[UIView alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, Height)];
        UIImage *navimg=[CommanMethods getNavigationBarImage];
        view.backgroundColor=[UIColor colorWithPatternImage:navimg];
        UILabel *lblHeader=[[UILabel alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, Height)];
        lblHeader.textAlignment=NSTextAlignmentCenter;
        lblHeader.textColor=[CommanMethods getTextColor];
        lblHeader.backgroundColor=[UIColor clearColor];
        lblHeader.font=[CommanMethods getDetailsFont];
        
        [view addSubview:lblHeader];
        if([arrOffer count]>0 && section==1)
        {
            lblHeader.text=[@"Offers" uppercaseString];
        }
        else
        {
            lblHeader.text=[screenHeader_Featured_Products uppercaseString];
        }
        return  view;
    }
    
    return  nil;
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    if(([arrayFeatureProduct count]>0 || [arrOffer count]>0) && section>0)
    {
        return Height;
    }
    return 0;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(indexPath.section==0)
    {
        float height=imageHeight+50;
        return   height;
    }
    else  if([arrayFeatureProduct count]>0 || [arrOffer count]>0)
    {
        return  117;
    }
    return  webViewHeight;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if(section==0)
    {
        return  1;
    }
    else if([arrayFeatureProduct count]>0 || [arrOffer count]>0)
    {
        if([arrOffer count]>0 && section==1)
        {
            return  [arrOffer count];
        }
        else
        {
            return  [arrayFeatureProduct count];
        }
    }
    return 1;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(indexPath.section==0)
    {
        shopDetailsTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"shopDetailsTableViewCell" forIndexPath:indexPath];
        [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
        
        if(!isScrollAdded)
        {
            UIScrollView *scrollView=cell.scrollView;
            scrollView.delegate=self;
            scrollView.pagingEnabled=YES;
            float height=scrollView.frame.size.height;
            height=imageHeight;
            cell.scrollViewHeight.constant=height;
            isScrollAdded=YES;
            if([self.imgData count]>0)
            {
                //Image
                float width =0;
                for (NSDictionary *dict in self.imgData)
                {
                    NSURL *url;
                    CustomImageView *imageView;
                    imageView=[[CustomImageView alloc]initWithFrame:CGRectMake(width,0,self.view.frame.size.width,height)];
                    width+=self.view.frame.size.width;
                    url = [CommanMethods getImageURL:[dict objectForKey:@"Image"]];
                    if(url)
                    {
                        [imageView sd_setImageWithURL:url placeholderImage:placeHolder];
                    }
                    else
                    {
                        [imageView setImage:placeHolder];
                    }
                    imageView.contentMode=UIViewContentModeScaleToFill;
                    imageView.backgroundColor=[UIColor clearColor];
                    [scrollView addSubview:imageView];
                    
                }
                scrollView.contentSize=CGSizeMake(width,height);
                cell.pageControl.numberOfPages=[self.imgData count];
            }
            else
            {
                float width =self.view.frame.size.width;
                CustomImageView *imageView=[[CustomImageView alloc]initWithFrame:CGRectMake(0,0,self.view.frame.size.width,height)];
                [cell.scrollView addSubview:imageView];
                imageView.backgroundColor=[UIColor clearColor];
                
                NSURL *url = [CommanMethods getImageURL:self.shopBaseModelObj.strImage];
                if(url)
                {
                    [imageView sd_setImageWithURL:url placeholderImage:placeHolder];
                }
                else
                {
                    [imageView setImage:placeHolder];
                }
                imageView.contentMode=UIViewContentModeScaleToFill;
                scrollView.contentSize=CGSizeMake(width,height);
                cell.pageControl.numberOfPages=0;
                cell.pageControl.hidden=YES;
            }
            [cell.btnCall addTarget:self action:@selector(call) forControlEvents:UIControlEventTouchUpInside];
            [cell.btnFav addTarget:self action:@selector(fav:) forControlEvents:UIControlEventTouchUpInside];
            [cell.btnMap addTarget:self action:@selector(map) forControlEvents:UIControlEventTouchUpInside];
            [cell.btnShare addTarget:self action:@selector(share) forControlEvents:UIControlEventTouchUpInside];
        }
        if(favModel)
        {
            [cell.imgFav setImage:[UIImage imageNamed:@"ico-fav-active6p"]];
        }
        else
        {
            [cell.imgFav setImage:[UIImage imageNamed:@"shop-whiteFav"]];
        }
        [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
        cell.bottomView1.hidden=YES;
        cell.bottomView.hidden=NO;
        
        return  cell;
    }
    else  if([arrayFeatureProduct count]>0 || [arrOffer count]>0)
    {
        NSString *strTitle=nil;
        NSURL *url=nil;
        NSString *strDesc=nil;
        NSString *strDate=nil;
        NSString *strIdentifier=@"OfferTableViewCell";
        
        if([arrOffer count]>0 && indexPath.section==1)
        {
            BaseModel *baseModel=[arrOffer objectAtIndex:indexPath.row];
            strTitle=baseModel.strTitle;
            
            url=[CommanMethods getImageURL:baseModel.strImageURL];
            strDesc=baseModel.strDesc;
            strDate=[NSString stringWithFormat:@"%@ - %@", [CommanMethods changeDateFormat:baseModel.strStartDate upperCase:YES],[CommanMethods changeDateFormat:baseModel.strEndDate upperCase:YES]];
        }
        else
        {
            strIdentifier=@"HotelTableViewCell";
            NSDictionary *dict=[arrayFeatureProduct objectAtIndex:indexPath.row];
            strTitle=[dict objectForKey:@"Title"];
            url = [CommanMethods getImageURL:[dict objectForKey:@"Thumbnail"]];
            strDesc=[dict objectForKey:@"Brief_Description"];
        }
        
        OfferTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:strIdentifier forIndexPath:indexPath];
        cell.selectionStyle=UITableViewCellSelectionStyleNone;
        
        cell.lblTitle.font=[CommanMethods getHeaderFont];
        if([arrOffer count]>0 && indexPath.section==1)
        {
            cell.lblDesc.font=[CommanMethods getHomeTextFont];
        }
        else
        {
            cell.lblDesc.font=[CommanMethods getDateFont];
            cell.lblDate.font=[CommanMethods getDateFont];
        }
        cell.lblTitle.textColor=[CommanMethods getTextColor];
        cell.lblDesc.textColor=[CommanMethods getTextColor];
        cell.lblDate.textColor=[CommanMethods getTextColor];
        strTitle=[CommanMethods rePlaceTagsWithPlainText: strTitle];
        cell.lblTitle.text=[strTitle uppercaseString];
        NSString *strValue=[CommanMethods rePlaceTagsWithPlainText:strDesc];
        cell.lblDesc.text=strValue;
        cell.lblDate.text=strDate;
        if(url)
        {
            [cell.imgView sd_setImageWithURL:url placeholderImage:PLACE_HODLER_IMAGE];
        }
        else
        {
            [cell.imgView setImage:PLACE_HODLER_IMAGE];
        }
        [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
        return cell;
    }
    static offerDetailDescCell *cell=nil;
    cell = (offerDetailDescCell *)[self.tableView dequeueReusableCellWithIdentifier:DESCCELL];
    if(!self.webView)
    {
        self.webView=[[UIWebView alloc]initWithFrame:cell.frame];
        [cell.contentView addSubview:self.webView];
        self.webView.dataDetectorTypes=UIDataDetectorTypePhoneNumber|UIDataDetectorTypeLink;

        self.webView.opaque = NO;
        self.webView.backgroundColor = [UIColor clearColor];
        [self.webView setScalesPageToFit:YES];
        self.webView.scrollView.scrollEnabled=NO;
        [self.webView loadHTMLString:self.strHtml baseURL:nil];
    }
    self.webView.delegate=self;
    return  cell;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if([arrayFeatureProduct count]>0 || [arrOffer count]>0)
    {
        if([arrOffer count]>0 && indexPath.section==1)
        {
            BaseModel *baseModel=[arrOffer objectAtIndex:indexPath.row];
            offerDetailViewController *offerdetailview = [self.storyboard instantiateViewControllerWithIdentifier:@"offerDetailViewController"];
            offerdetailview.offerInfo=baseModel;
            [self.navigationController pushViewController:offerdetailview animated:YES];
        }
        else
        {
            NSDictionary *dict=[arrayFeatureProduct objectAtIndex:indexPath.row];
            WebViewController *webObj=[[WebViewController alloc]initWithNibName:@"WebViewController" bundle:nil];
            webObj.url=[CommanMethods getImageURL:[dict objectForKey:@"Product_PDF"]];
            webObj.strType=screenHeader_Featured_Products;
            
            NSString *strScreenValue;
            if([self.shopBaseModelObj.strType isEqualToString:APP_SHOP])
            {
                strScreenValue=[NSString stringWithFormat:@"%@ - %@ - %@",screenName_Shopping,self.shopBaseModelObj.categoryName,screenName_Featured_Products];
            }
            else if([self.shopBaseModelObj.strType isEqualToString:APP_DINE])
            {
                strScreenValue=[NSString stringWithFormat:@"%@ - %@ - %@",screenName_Dining,self.shopBaseModelObj.categoryName,screenName_Featured_Products];
            }
            else
            {
                strScreenValue=[NSString stringWithFormat:@"%@ - %@ - %@",screenName_Entertainment,self.shopBaseModelObj.categoryName,screenName_Featured_Products];
            }
            [GoogleAnalyticsHandler sendScreenName:strScreenValue];
            [self.navigationController pushViewController:webObj animated:YES];
        }
    }
}
-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath{
    if(!isAnimated)
    {
        isAnimated=YES;
        istranfarm=YES;
        if (indexPath.row==0)
        {
            shopDetailsTableViewCell *imgcell = (shopDetailsTableViewCell *)cell;
            imgcell.scrollView.transform = CGAffineTransformMakeScale(0.5, 0.5);
            [UIView animateWithDuration:0.5
                             animations:^{
                                 imgcell.scrollView.transform  =CGAffineTransformIdentity ;
                             }completion:^(BOOL finished)
             {
                 istranfarm=NO;
                 [self.tableView reloadData];
             }];
            
        }
    }
}

-(void)call
{
    [[AppDelegate appDelegateInstance]callViaTelephone:self.shopBaseModelObj.strTelephone];
}
-(void)share
{
    NSURL *url = [CommanMethods getImageURL:self.shopBaseModelObj.strImage];
    SDWebImageManager *manager = [SDWebImageManager sharedManager];
    [manager downloadImageWithURL:url
                          options:0
                         progress:^(NSInteger receivedSize, NSInteger expectedSize) {
                             // progression tracking code
                         }
                        completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, BOOL finished, NSURL *imageURL) {
                            if (!image)
                            {
                                image=PLACE_HODLER_IMAGE;
                            }
                            
                            dispatch_async(dispatch_get_main_queue(), ^{
                                NSString *texttoshare = self.shopBaseModelObj.strTitle;
                                UIImage *imagetoshare = image;
                                NSArray *activityItems = @[texttoshare, imagetoshare];
                                UIActivityViewController *activityVC = [[UIActivityViewController alloc] initWithActivityItems:activityItems applicationActivities:nil];
                                activityVC.excludedActivityTypes = @[UIActivityTypeAssignToContact, UIActivityTypePrint];
                                [self presentViewController:activityVC animated:TRUE completion:nil];
                            });
                        }];
    
}
-(void)map
{
    NSString *strScreenValue;
    if([self.shopBaseModelObj.strType isEqualToString:APP_SHOP])
    {
        strScreenValue=[NSString stringWithFormat:@"%@ - %@ - %@",screenName_Shopping,self.shopBaseModelObj.categoryName,screenName_StoreLocator];
    }
    else if([self.shopBaseModelObj.strType isEqualToString:APP_DINE])
    {
        strScreenValue=[NSString stringWithFormat:@"%@ - %@ - %@",screenName_Dining,self.shopBaseModelObj.categoryName,screenName_StoreLocator];
    }
    else
    {
        strScreenValue=[NSString stringWithFormat:@"%@ - %@ - %@",screenName_Entertainment,self.shopBaseModelObj.categoryName,screenName_StoreLocator];
    }
    [GoogleAnalyticsHandler sendScreenName:strScreenValue];
    
    StoreLocatorViewController *storeObj=[[StoreLocatorViewController alloc]initWithNibName:@"StoreLocatorViewController" bundle:nil];
    storeObj.strStoreId=self.shopBaseModelObj.strDestination_ID;
    [self.navigationController pushViewController:storeObj animated:YES];
    
 }
-(void)fav:(id)sender
{
    UIButton *button = (UIButton*)sender;
    CGPoint buttonOrigin = [sender frame].origin;
    CGPoint originInTableView = [self.tableView convertPoint:buttonOrigin fromView:[button superview]];
    NSIndexPath *indexPath = [self.tableView  indexPathForRowAtPoint:originInTableView];
    shopDetailsTableViewCell *cell =(shopDetailsTableViewCell *) [self.tableView  cellForRowAtIndexPath:indexPath];
    if(favModel)
    {
        BOOL delete=[DBOpreation deleteFav:self.shopBaseModelObj.strType ID:self.shopBaseModelObj.numID];
        if(delete)
        {
            self.favModel=nil;
        }
        
    }
    else
    {
        BOOL insert=[DBOpreation insertFavData:self.shopBaseModelObj.strType ID:self.shopBaseModelObj.numID title:self.shopBaseModelObj.strTitle];
        if(insert)
        {
            [[WebServiceCommunicator sharedInstance]updateOfferCategoryHits:[[AppDelegate appDelegateInstance].strSiteID intValue] CategoryID:self.shopBaseModelObj.catID];
            FavouriteListModel *model=[[FavouriteListModel alloc]init];
            model.favID=self.shopBaseModelObj.numID;
            model.favType=self.shopBaseModelObj.strType;
            model.strTitle=self.shopBaseModelObj.strTitle;
            self.favModel=model;
        }
    }
    if(favModel)
    {
        [cell.imgFav setImage:[UIImage imageNamed:@"ico-fav-active6p"]];
    }
    else
    {
        [cell.imgFav setImage:[UIImage imageNamed:@"shop-whiteFav"]];
    }
}

#pragma mark - UIWebViewDelegate
- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType
{
    if (navigationType == UIWebViewNavigationTypeLinkClicked)
    {
        NSURL *url = request.URL;
        if ([url.scheme hasPrefix:APP_HTPP_KEYWORD])
        {
            WebViewController *webObj=[[WebViewController alloc]initWithNibName:@"WebViewController" bundle:nil];
            webObj.url=url;
            webObj.strType=self.shopBaseModelObj.strType;
            [self.navigationController pushViewController:webObj animated:YES];
            return  NO;
        }
    }
    return YES;
}
- (void)webViewDidStartLoad:(UIWebView *)webView
{
    
}
- (void)webViewDidFinishLoad:(UIWebView *)webView
{
    NSString *result = [webView stringByEvaluatingJavaScriptFromString:@"document.body.offsetHeight;"];
    float height = [result integerValue];
    webViewHeight=height+50;
    self.webView.frame=CGRectMake(0, 10,self.view.frame.size.width, webViewHeight);
    self.webView.scrollView.scrollEnabled=NO;
    if(!istranfarm)
        [self.tableView reloadData];
}

- (void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error
{
}

-(void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    if([scrollView.superview.superview isKindOfClass:[shopDetailsTableViewCell class]])
    {
        shopDetailsTableViewCell *cell = (shopDetailsTableViewCell *) scrollView.superview.superview;
        NSInteger currentPage = scrollView.contentOffset.x/scrollView.frame.size.width;
        cell.pageControl.currentPage = currentPage;
    }
}
@end
