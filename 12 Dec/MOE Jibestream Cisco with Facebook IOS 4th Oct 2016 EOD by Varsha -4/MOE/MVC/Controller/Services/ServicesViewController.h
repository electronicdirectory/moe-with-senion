//
//  ServicesViewController.h
//  MOE
//
//  Created by Nivrutti on 6/26/15.
//  Copyright (c) 2015 Neosoft. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ServicesViewController : UIViewController
{
    NSMutableArray *array;
}
@property(nonatomic,strong)IBOutlet UILabel *lblPlaceHolder;
@property(nonatomic, strong) IBOutlet UICollectionView *collectionView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *collectionViewBottomDistance;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *collectionViewTopDistance;
@end
