//
//  ServicesViewController.m

//
//  Created by Nivrutti on 6/26/15.
//  Copyright (c) 2015 Neosoft. All rights reserved.
//

#import "ServicesViewController.h"
#import "ServicesCollectionViewCell.h"
#import "ServiceDetailsViewController.h"
#import "Constant.h"
#import "ATMListViewController.h"
@interface ServicesViewController ()<UICollectionViewDelegateFlowLayout, UICollectionViewDelegate,UICollectionViewDataSource>
{
    NSArray *serviceImageArray;
    NSUInteger indexForATM;
    float cellWidth;
}

@end

@implementation ServicesViewController
//icon-atm6
//icon-car-wash6
//icon-disable6
//icon-kids-cart6
//icon-lockers6
//icon-lost-found6
//icon-metro6
//icon-parking6
//icon-shuttle-bus6
//icon-taxi6
//icon-valet6
//icon-wifi6

#define ATM     @""
#define CARWASH     @"Car Wash Services"
#define DISABLE     @"Wheel Chair"
#define KIDSCART     @"Kid Karts"
#define LOCKER     @"Lockers"
#define LOSTFOUND     @"Lost & Found"
#define METRO     @"Metro"
#define PARKING     @"Parking"
#define SHUTTLEBUS     @"Shuttle Bus Services"
#define TAXI     @"Taxi Bays"
#define VALET     @"Valet Parking"
#define WIFI     @"Free WiFi"
#define MALL_CONNECT     @"Mall Connect"


#define ATMIMAGE     [UIImage imageNamed:@"icon-atm6"]
#define CARWASHIMAGE     [UIImage imageNamed:@"icon-car-wash6"]
#define DISABLEIMAGE     [UIImage imageNamed:@"icon-disable6"]
#define KIDSCARTIMAGE     [UIImage imageNamed:@"icon-kids-cart6"]
#define LOCKERIMAGE     [UIImage imageNamed:@"icon-lockers6"]
#define LOSTFOUNDIMAGE     [UIImage imageNamed:@"icon-lost-found6"]
#define METROIMAGE     [UIImage imageNamed:@"icon-metro6"]
#define PARKINGIMAGE     [UIImage imageNamed:@"icon-parking6"]
#define SHUTTLEBUSIMAGE     [UIImage imageNamed:@"icon-shuttle-bus6"]
#define TAXIIMAGE     [UIImage imageNamed:@"icon-taxi6"]
#define VALETIMAGE     [UIImage imageNamed:@"icon-valet6"]
#define WIFIIMAGE     [UIImage imageNamed:@"icon-wifi6"]

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [GoogleAnalyticsHandler sendScreenName:screenName_Services];
    
    self.lblPlaceHolder=[[UILabel alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 44)];
    [self.view addSubview:self.lblPlaceHolder];
    self.lblPlaceHolder.text=APP_NODATA;
    self.lblPlaceHolder.font=[CommanMethods getHeaderFont];
    self.lblPlaceHolder.textColor=[CommanMethods getTextColor];
    self.lblPlaceHolder.center=self.view.center;
    [self.lblPlaceHolder setTextAlignment:NSTextAlignmentCenter];
    
    
    self.navigationItem.rightBarButtonItem=[CommanMethods getRightButton:self Action:@selector(search)];
    self.navigationItem.leftBarButtonItem =[CommanMethods getLeftButton:self Action:@selector(menuMethod)];
    self.navigationItem.titleView = [CommanMethods getLabel:[APP_SERVICES uppercaseString]];
    array=[[NSMutableArray alloc]initWithArray:[DBOpreation getServiceListing]];
    
    //insert record for ATM data
    indexForATM=[array count];
    [array insertObject:@"ATM Data" atIndex:indexForATM];
    
    [self.collectionView registerNib:[UINib nibWithNibName:@"ServicesCollectionViewCell" bundle:nil] forCellWithReuseIdentifier:@"ServicesCollectionViewCell"];
    
    if (IS_IPHONE_4_OR_LESS) {
        self.collectionViewTopDistance.constant=self.collectionViewBottomDistance.constant=8;
    }
    
    // Do any additional setup after loading the view.
}
-(void)viewWillAppear:(BOOL)animated
{
}
- (void)menuMethod
{
    [self.sideMenuViewController presentLeftMenuViewController];
}

- (void)search
{
    [[AppDelegate appDelegateInstance]openSearchScreen:self.navigationController];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    CGFloat width=collectionView.bounds.size.width/3.0;
    return CGSizeMake(width,120);
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    if([array count]==0)
    {
        self.lblPlaceHolder.text=APP_NODATA;
        self.lblPlaceHolder.font=[CommanMethods getHeaderFont];
        self.lblPlaceHolder.textColor=[CommanMethods getTextColor];
        self.lblPlaceHolder.hidden=NO;
        [self.view bringSubviewToFront:self.lblPlaceHolder];
    }
    else
    {
        self.lblPlaceHolder.hidden=YES;
    }
    return [array count] ;
}
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    ServicesCollectionViewCell *cell=(ServicesCollectionViewCell *)[collectionView dequeueReusableCellWithReuseIdentifier:@"ServicesCollectionViewCell" forIndexPath:indexPath];
    cell.lblTitle.font=[CommanMethods getHomeTextFont];
    cell.lblTitle.adjustsFontSizeToFitWidth = YES;
    
    NSUInteger count=[array count];
    NSUInteger position=indexPath.row;
    
    if(count%3==0)
    {
        if(position==count-1)
        {
            cell.horizontalBar.hidden=YES;
            cell.verticalBar.hidden=YES;
        }
        else if(position >= count-3)
        {
            cell.horizontalBar.hidden=YES;
            cell.verticalBar.hidden=NO;
        }
        else
        {
            cell.horizontalBar.hidden=NO;
            cell.verticalBar.hidden=NO;
        }
    }
    else if(count%3==1 )
    {
        if(position==count-1)
        {
            cell.horizontalBar.hidden=YES;
            cell.verticalBar.hidden=NO;
        }
        else
        {
            cell.horizontalBar.hidden=NO;
            cell.verticalBar.hidden=NO;
        }
        
    }
    else if(count%3==2 )
    {
        if(position >= count-2)
        {
            cell.horizontalBar.hidden=YES;
            cell.verticalBar.hidden=NO;
        }
        else
        {
            cell.horizontalBar.hidden=NO;
            cell.verticalBar.hidden=NO;
        }
    }
    
    if((position+1)%3==0 && position!=count-1)
    {
        cell.horizontalBar.hidden=NO;
        cell.verticalBar.hidden=YES;
    }
    
    if (indexForATM==indexPath.row)
    {
        [cell.imgViewForIcon setImage:[UIImage imageNamed:@"icon-atm6"]];
        cell.lblTitle.text=@"ATM'S";
    }
    else
    {
        ServicesModel *model=[array objectAtIndex:indexPath.row];
        NSString *strTitle=[model.strTitle stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
        if([[model.strTitle lowercaseString]  isEqual:@"free wifi"])
        {
            strTitle=@"MallConnect";
        }
        cell.lblTitle.text=[strTitle uppercaseString];
        cell.imgViewForIcon.image=[self setImageForCell:cell forModel:model];
    }
    if(IS_IPHONE_4_OR_LESS)
        cell.imgViewForIcon.contentMode=UIViewContentModeCenter;
    else
    {
        cell.imgViewForIcon.contentMode=UIViewContentModeScaleAspectFit;
    }
    
    if (IS_GREATER_THAN_IOS8)
    {
        CGRect frmCell = cell.frame;
        frmCell.origin.x = self.view.frame.size.width;
        cell.frame = frmCell;
        cell.alpha = 0.0f;
        cellWidth = cell.frame.size.width;
        
    }
    else if(cell.alpha==1)
    {
        CGRect frmCell = cell.frame;
        frmCell.origin.x = self.view.frame.size.width;
        cell.frame = frmCell;
        cell.alpha = 0.0f;
        cellWidth = cell.frame.size.width;
        
        if(indexPath.row%3 == 0)
            frmCell.origin.x = 0*cellWidth;
        else if(indexPath.row%3 == 1)
            frmCell.origin.x = 1*cellWidth;
        else if(indexPath.row%3 == 2)
            frmCell.origin.x = 2*cellWidth;
        
        [UIView animateWithDuration:0.3 delay:0.1*indexPath.row options:UIViewAnimationOptionCurveEaseInOut  animations:^{
            cell.frame = frmCell;
            cell.alpha = 1.0f;
        } completion:nil];
    }
    
    
    
    return cell;
}


-(UIImage *)setImageForCell :(ServicesCollectionViewCell *)cell forModel:(ServicesModel *)model
{
    UIImage *imgIcon;
    if ([model.strTitle isEqualToString:ATM])
    {
        imgIcon=ATMIMAGE;
    }
    else if ([model.strTitle isEqualToString:CARWASH])
    {
        imgIcon=CARWASHIMAGE;
    }
    else if ([model.strTitle isEqualToString:DISABLE])
    {
        imgIcon=DISABLEIMAGE;
    }
    else if ([model.strTitle isEqualToString:KIDSCART])
    {
        imgIcon=KIDSCARTIMAGE;
    }
    else if ([model.strTitle isEqualToString:LOCKER])
    {
        imgIcon=LOCKERIMAGE;
    }
    else if ([model.strTitle isEqualToString:LOSTFOUND])
    {
        imgIcon=LOSTFOUNDIMAGE;
    }
    else if ([model.strTitle isEqualToString:METRO])
    {
        imgIcon=METROIMAGE;
    }
    else if ([model.strTitle isEqualToString:PARKING])
    {
        imgIcon=PARKINGIMAGE;
    }
    else if ([model.strTitle isEqualToString:SHUTTLEBUS])
    {
        imgIcon=SHUTTLEBUSIMAGE;
    }
    else if ([model.strTitle isEqualToString:TAXI])
    {
        imgIcon=TAXIIMAGE;
    }
    else if ([model.strTitle isEqualToString:VALET])
    {
        imgIcon=VALETIMAGE;
    }
    else if ([model.strTitle isEqualToString:WIFI])
    {
        imgIcon=WIFIIMAGE;
    }
    else if ([model.strTitle isEqualToString:MALL_CONNECT])
    {
        imgIcon=WIFIIMAGE;
    }
    return imgIcon;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexForATM==indexPath.row) {
        ATMListViewController *obj=[self.storyboard instantiateViewControllerWithIdentifier:@"ATMListViewController"];
        [self.navigationController pushViewController:obj animated:YES];
        return ;
    }
    ServicesModel *model=[array objectAtIndex:indexPath.row];
    NSString *strTitle=[model.strTitle stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    if([[SHUTTLEBUS lowercaseString]isEqualToString:[strTitle lowercaseString]])
    {
        WebViewController *webObj=[[WebViewController alloc]initWithNibName:@"WebViewController" bundle:nil];
        NSURL *url=[NSURL URLWithString:[CommanMethods getShuttleBusURL]];
        webObj.url=url;
        webObj.hideBottom=YES;
        webObj.strType=[SHUTTLEBUS uppercaseString];
        [self.navigationController pushViewController:webObj animated:YES];
    }
    else
    {
        ServiceDetailsViewController *obj= [self.storyboard instantiateViewControllerWithIdentifier:@"ServiceDetailsViewController"];
        obj.model=model;
        obj.titleText=[APP_SERVICES uppercaseString];
        [self.navigationController pushViewController:obj animated:YES];
    }
}
-(void)collectionView:(UICollectionView *)collectionView willDisplayCell:(UICollectionViewCell *)cell forItemAtIndexPath:(NSIndexPath *)indexPath
{
    
    
    CGRect frmCell = cell.frame;
    if(indexPath.row%3 == 0)
        frmCell.origin.x = 0*cellWidth;
    else if(indexPath.row%3 == 1)
        frmCell.origin.x = 1*cellWidth;
    else if(indexPath.row%3 == 2)
        frmCell.origin.x = 2*cellWidth;
    
    
    [UIView animateWithDuration:0.3 delay:0.1*indexPath.row options:UIViewAnimationOptionCurveEaseInOut  animations:^{
        
        cell.frame = frmCell;
        cell.alpha = 1.0f;
        
    } completion:nil];
    
}
-(void)dealloc
{
    array=nil;
}
@end
