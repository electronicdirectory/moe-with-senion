//
//  ServicesCollectionViewCell.h
//  MOE
//
//  Created by Nivrutti  Patil on 26/06/15.
//  Copyright (c) 2015 Neosoft. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ServicesCollectionViewCell : UICollectionViewCell
@property(nonatomic, strong) IBOutlet UILabel *lblTitle;
@property (weak, nonatomic) IBOutlet UIImageView *horizontalBar;
@property(nonatomic, strong) IBOutlet UIImageView *imgViewForIcon;
@property(nonatomic, strong) IBOutlet UIImageView *verticalBar;
@end
