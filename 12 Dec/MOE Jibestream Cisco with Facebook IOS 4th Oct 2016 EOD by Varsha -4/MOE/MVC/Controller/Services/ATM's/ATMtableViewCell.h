//
//  ATMtableViewCell.h
//  MOE
//
//  Created by webwerks on 7/6/15.
//  Copyright (c) 2015 Neosoft. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ATMtableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *lblATMTitle;

@end
