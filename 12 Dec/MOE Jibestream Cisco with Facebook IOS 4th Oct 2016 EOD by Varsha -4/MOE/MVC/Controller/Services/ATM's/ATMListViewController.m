//
//  ATMListViewController.m
//  MOE
//
//  Created by Nivrutti  Patil on 29/06/15.
//  Copyright (c) 2015 Neosoft. All rights reserved.
//

#import "ATMListViewController.h"
#import "ServiceDetailsViewController.h"
#import "ATMtableViewCell.h"
#import "ServicesModel.h"
@interface ATMListViewController ()
{
    NSArray  *indexArray;
    NSMutableArray *arrATMList;
    NSMutableDictionary *dataDict;
}
@end
@implementation ATMListViewController
- (void)viewDidLoad
{
    [super viewDidLoad];

  
    [GoogleAnalyticsHandler sendScreenName:screenName_ATMList];

    // Do any additional setup after loading the view.
     self.navigationItem.titleView = [CommanMethods getLabel:screenHeader_ATMS];
    [self.tblView registerNib:[UINib nibWithNibName:@"ATMtableViewCell" bundle:nil] forCellReuseIdentifier:@"ATMtableViewCell"];
    indexArray=[[CommanMethods getAlphabets]copy];
    dataDict=[[NSMutableDictionary alloc]init];
    arrATMList=[[NSMutableArray alloc]initWithArray:[DBOpreation getATMListing]];
    [self getFilterData:arrATMList];
}
-(void)getFilterData:(NSMutableArray *)array
{
    dispatch_async(dispatch_get_main_queue(),^{
        
        if ([self.tblView respondsToSelector:@selector(setSectionIndexColor:)]) {
            self.tblView.sectionIndexTrackingBackgroundColor =[UIColor clearColor];
        }
        [self.tblView setSectionIndexColor:[CommanMethods getTextColor]];
        [self.tblView setSectionIndexBackgroundColor:[UIColor clearColor]];
        [self.tblView setSeparatorInset:UIEdgeInsetsZero];
        [self.tblView setSeparatorStyle:UITableViewCellSeparatorStyleNone];

        [dataDict removeAllObjects];
        [self.tblView reloadData];
        @autoreleasepool
        {
            NSMutableArray *dummyArray=[[NSMutableArray alloc]initWithArray:array];
            if(!dataDict)
            {
                dataDict=[[NSMutableDictionary alloc]init];
            }
            NSPredicate *predicate;
            for (NSString *strAlpha in indexArray )
            {
                predicate=[NSPredicate predicateWithFormat:@"strTitle beginswith[cd] %@",strAlpha];
                NSArray *dataArray=[dummyArray filteredArrayUsingPredicate:predicate];
                if([dataArray count]>0)
                {
                    [dataDict setObject:dataArray forKey:strAlpha];
                }
                if(![strAlpha isEqualToString:@"#"])
                {
                    [dummyArray removeObjectsInArray:dataArray];
                }
            }
            [dataDict setObject:dummyArray forKey:@"#"];
             [self.tblView reloadData];
        }
    });
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma mark - Table view data source
/******************************************
 Method name - numberOfSectionsInTableView
 Parameter   - UITableView
 Return      - NSInteger
 Desc        - Method for returning number of section in table view
 ****************************************/
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    if([[dataDict allKeys]count]>0)
        return [indexArray count];
    
    return  0;
}
/******************************************
 Method name - sectionIndexTitlesForTableView
 Parameter   - UITableView
 Return      - NSArray
 Desc        - Method for returning array for table indexes in table view
 ****************************************/
- (NSArray *)sectionIndexTitlesForTableView:(UITableView *)tableView
{
    if([[dataDict allKeys]count]>0)
        return indexArray;
    return nil;
}
/******************************************
 Method name - sectionForSectionIndexTitle
 Parameter   - UITableView
 Return      - NSInteger
 Desc        - Method for returning title for section index
 ****************************************/
- (NSInteger)tableView:(UITableView *)tableView sectionForSectionIndexTitle:(NSString *)title atIndex:(NSInteger)index
{
    if([[dataDict allKeys]count]>0)
        return [indexArray indexOfObject:title];
    return -1;
}
/******************************************
 Method name - heightForRowAtIndexPath
 Parameter   - UITableView, NSIndexPath
 Return      - CGFloat
 Desc        - Method for returning size of row in table view
 ****************************************/
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 45.0f;
}
/******************************************
 Method name - numberOfRowsInSection
 Parameter   - UITableView, NSInteger
 Return      - NSInteger
 Desc        - Method for returning number of rows in table view
 ****************************************/
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    NSString *strSection=[indexArray objectAtIndex:section];
    NSArray *array=[dataDict objectForKey:strSection];
    return [array count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier = @"ATMtableViewCell";
    ATMtableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    cell.backgroundColor=[UIColor clearColor];
    NSString *strSection=[indexArray objectAtIndex:indexPath.section];
    NSArray *array=[dataDict objectForKey:strSection];
    ServicesModel *model=[array objectAtIndex:indexPath.row];
    cell.lblATMTitle.text=[model.strTitle uppercaseString];
    cell.lblATMTitle.font=[CommanMethods getHeaderFont];

    [cell.lblATMTitle setTextColor:[CommanMethods getTextColor]];
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *strSection=[indexArray objectAtIndex:indexPath.section];
    NSArray *array=[dataDict objectForKey:strSection];
    ServicesModel *model=[array objectAtIndex:indexPath.row];
    ServiceDetailsViewController *obj= [self.storyboard instantiateViewControllerWithIdentifier:@"ServiceDetailsViewController"];
    obj.model=model;
    obj.titleText=[model.strTitle uppercaseString];
    [self.navigationController pushViewController:obj animated:YES];
}
/******************************************
 Method name - willDisplayCell
 Parameter   - UITableView, NSIndexPath
 Return      - nil
 Desc        - Method Called before row getting displayed in table view (Used for animation)
 ****************************************/
- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    CATransition *transition = [CATransition animation];
    [transition setType:kCATransitionPush];
    [transition setSubtype:kCATransitionFromLeft];
    cell.layer.shadowColor = [[CommanMethods getblackColor]CGColor];
    cell.layer.shadowOffset = CGSizeMake(10, 10);
    cell.alpha = 0;
    CATransform3D rotation;
    rotation = CATransform3DMakeRotation( 0, 0.0, 0, 0);
    rotation.m34 = 1.0/ -600;
    cell.layer.transform = rotation;
    [cell.layer addAnimation:transition forKey:@"rotation1"];
    [UIView beginAnimations:@"rotation" context:NULL];
    [UIView setAnimationDuration:0.3];
    cell.layer.transform = CATransform3DIdentity;
    cell.alpha = 1;
    cell.layer.shadowOffset = CGSizeMake(0, 0);
    [UIView commitAnimations];
}
@end
