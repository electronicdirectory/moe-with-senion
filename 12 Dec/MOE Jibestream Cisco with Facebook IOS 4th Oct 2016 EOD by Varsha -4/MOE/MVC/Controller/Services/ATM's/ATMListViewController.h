//
//  ATMListViewController.h
//  MOE
//
//  Created by Nivrutti  Patil on 29/06/15.
//  Copyright (c) 2015 Neosoft. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ATMListViewController : UIViewController

@property(nonatomic, strong) IBOutlet UITableView *tblView;
@end
