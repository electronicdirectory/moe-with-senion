//
//  ServiceDetailsViewController.m
//  MOE
//
//  Created by Nivrutti  Patil on 27/06/15.
//  Copyright (c) 2015 Neosoft. All rights reserved.
//

#import "ServiceDetailsViewController.h"
#import "shopDetailsTableViewCell.h"
#import "offerDetailDescCell.h"
@interface ServiceDetailsViewController ()
{
    float imageHeight;
    BOOL isScrollAdded;
    UIImage *placeHolderImage;
    float webViewHeight;
    BOOL isWebViewLoding;
    BOOL isAnimated,istranfarm;
}
@property(nonatomic,strong)NSString *strHtml;
@end

@implementation ServiceDetailsViewController
@synthesize model;
#define IMAGECELL @"shopDetailsTableViewCell"
#define DESCCELL @"offerDetailDescCell"

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    if(IS_IPHONE_4_OR_LESS)
        imageHeight = IPHONE4_DETAILS_IMAGE_HEIGHT;
    else if (IS_IPHONE_5)
        imageHeight = IPHONE5_DETAILS_IMAGE_HEIGHT;
    else if (IS_IPHONE_6)
        imageHeight = IPHONE6_DETAILS_IMAGE_HEIGHT;
    else if (IS_IPHONE_6P)
        imageHeight = IPHONE6PLUS_DETAILS_IMAGE_HEIGHT;
    
    placeHolderImage=[UIImage imageNamed:@"place-holder-offer1-6p"];
    
     NSString *strTitle=[model.strTitle stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    if([[model.strTitle lowercaseString]  isEqual:@"free wifi"])
    {
        strTitle=@"MallConnect";
    }
 
    // Do any additional setup after loading the view from its nib.
    NSString *strScreenValue;
    if([model.strType isEqual:APP_ATM])
    {
        strScreenValue =[NSString stringWithFormat:@"%@ - %@ - %@",screenName_Services,screenName_ATM,strTitle];
    }
    else
    {
        strScreenValue =[NSString stringWithFormat:@"%@ - %@",screenName_Services,strTitle];
    }
    [GoogleAnalyticsHandler sendScreenName:strScreenValue];
    istranfarm=YES;
    [self.tblView registerNib:[UINib nibWithNibName:IMAGECELL bundle:nil] forCellReuseIdentifier:IMAGECELL];
    [self.tblView registerNib:[UINib nibWithNibName:DESCCELL bundle:nil] forCellReuseIdentifier:DESCCELL];
    self.tblView.separatorStyle=UITableViewCellSeparatorStyleNone;
    
 
    self.navigationItem.titleView = [CommanMethods getLabel:[strTitle uppercaseString]];
    
    
    NSString *strDesc = [CommanMethods rePlaceTags:model.strPageDetail];
    self.strHtml=[CommanMethods getHtml:@"" Date:@" " Desc:strDesc];
    webViewHeight=self.view.frame.size.height-imageHeight;
    
}
-(void)viewWillAppear:(BOOL)animated
{
    [self.tblView reloadData];
}
-(void)viewWillDisappear:(BOOL)animated
{
    self.webView.delegate=nil;
    [self.webView removeFromSuperview];
    self.webView=nil;
}
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
-(NSInteger)tableView:(UITableView *)tablView numberOfRowsInSection:(NSInteger)section
{
    return 2;
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return [self heightForCellAtIndexPath:indexPath ];
}
- (CGFloat)heightForCellAtIndexPath:(NSIndexPath * )indexPath
{
    if(indexPath.row==0)
    {
        return imageHeight;
    }
    return webViewHeight;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(indexPath.row==0)
    {
        shopDetailsTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"shopDetailsTableViewCell" forIndexPath:indexPath];
        [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
        
        cell.bottomView.hidden=YES;
        cell.bottomView1.hidden=YES;
        
        if(!isScrollAdded)
        {
            isScrollAdded=YES;
            UIScrollView *scrollView=cell.scrollView;
            float height= cell.scrollView.frame.size.height;
            height=imageHeight;
            cell.scrollViewHeight.constant=height;
            
            float width =self.view.frame.size.width;
            CustomImageView *imageView=[[CustomImageView alloc]initWithFrame:CGRectMake(0,0,self.view.frame.size.width,height)];
            [cell.scrollView addSubview:imageView];
            imageView.backgroundColor=[UIColor clearColor];
            
            NSURL *url = [CommanMethods getImageURL:model.strImage_URL];
            if(url)
            {
                [imageView sd_setImageWithURL:url placeholderImage:placeHolderImage];
            }
            else
            {
                [imageView setImage:placeHolderImage];
            }
            imageView.contentMode=UIViewContentModeScaleToFill;
            scrollView.contentSize=CGSizeMake(width,height);
            cell.pageControl.hidden=YES;
        }
        return  cell;
    }
    static offerDetailDescCell *cell=nil;
    cell = (offerDetailDescCell *)[self.tblView dequeueReusableCellWithIdentifier:DESCCELL];
    if(!self.webView)
    {
        CGRect rect=cell.frame;
        rect.size.width-=8;
        rect.origin.x-=4;
        self.webView=[[UIWebView alloc]initWithFrame:rect];
        [cell.contentView addSubview:self.webView];
        self.webView.dataDetectorTypes=UIDataDetectorTypePhoneNumber|UIDataDetectorTypeLink;

        self.webView.opaque = NO;
        [self.webView setScalesPageToFit:YES];
        self.webView.backgroundColor = [UIColor clearColor];
        [self.webView loadHTMLString:self.strHtml baseURL:nil];
    }
    self.webView.delegate=self;
    return  cell;
}
-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath{
    if(!isAnimated)
    {
        isAnimated=YES;
        istranfarm=YES;
        if (indexPath.row==0)
        {
            shopDetailsTableViewCell *imgcell = (shopDetailsTableViewCell *)cell;
            imgcell.scrollView.transform = CGAffineTransformMakeScale(0.5, 0.5);
            [UIView animateWithDuration:0.5
                             animations:^{
                                 imgcell.scrollView.transform  =CGAffineTransformIdentity ;
                             }completion:^(BOOL finished)
             {
                 istranfarm=NO;
                 [self.tblView reloadData];
             }];
            
        }
    }
}

#pragma mark - UIWebViewDelegate
- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType
{
    if (navigationType == UIWebViewNavigationTypeLinkClicked)
    {
        NSURL *url = request.URL;
        if ([url.scheme hasPrefix:@"http"])
        {
            WebViewController *webObj=[[WebViewController alloc]initWithNibName:@"WebViewController" bundle:nil];
            webObj.url=url;
            if(self.titleText!=nil)
            {
                webObj.strType=[self.titleText uppercaseString];
            }
            else
            {
                webObj.strType=[APP_SERVICES uppercaseString];
            }
            
            [self.navigationController pushViewController:webObj animated:YES];
            return  NO;
        }
    }
    return YES;
}
- (void)webViewDidStartLoad:(UIWebView *)webView
{
    
}
- (void)webViewDidFinishLoad:(UIWebView *)webView
{
    NSString *result = [webView stringByEvaluatingJavaScriptFromString:@"document.body.offsetHeight;"];
    float height = [result integerValue];
    webViewHeight=height+50;
    self.webView.frame=CGRectMake(4, 10,self.view.frame.size.width-8, webViewHeight);
    self.webView.scrollView.scrollEnabled=NO;
    if(!istranfarm)
        [self.tblView reloadData];
}
- (void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error
{
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
