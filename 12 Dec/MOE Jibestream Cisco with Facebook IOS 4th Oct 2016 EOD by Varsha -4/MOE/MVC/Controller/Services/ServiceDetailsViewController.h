//
//  ServiceDetailsViewController.h
//  MOE
//
//  Created by Nivrutti  Patil on 27/06/15.
//  Copyright (c) 2015 Neosoft. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ServiceDetailsViewController : UIViewController<UIWebViewDelegate>
{
    NSMutableArray *array;
}
@property(nonatomic,strong) IBOutlet UITableView *tblView;
@property(nonatomic,strong) ServicesModel *model;
@property (nonatomic,strong)UIWebView *webView;
@property(nonatomic,strong) NSString *titleText;
@end
