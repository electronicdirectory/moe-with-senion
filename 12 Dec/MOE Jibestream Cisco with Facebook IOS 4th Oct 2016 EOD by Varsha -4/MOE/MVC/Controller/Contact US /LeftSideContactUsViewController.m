//
//  LeftSideContactUsViewController.m
//  MOE
//
//  Created by webwerks on 4/7/16.
//  Copyright © 2016 Neosoft. All rights reserved.
//

#import "LeftSideContactUsViewController.h"
#import "ContactUsViewController.h"
#import "FacilitiesViewController.h"
#import "WebViewController.h"
#import <AddressBook/AddressBook.h>
#import "settingsTableViewCell.h"

@interface LeftSideContactUsViewController ()
{
    NSString *strName;
}
@end

@implementation LeftSideContactUsViewController
{
    NSMutableArray *arrData;
}
#define SettingCell @"settingsTableViewCell"

static NSString * const contact_add_error = @"Cannot Add Contact";
static NSString * const contact_add_error_permission = @"You must give the app permission to add the contact first.";
static NSString * const contact_added = @"Contact added successfully.";
static NSString * const contact_added_to_addressBook = @"Add this contact to address book?";
- (void)viewDidLoad {
    [super viewDidLoad];
    
    [GoogleAnalyticsHandler sendScreenName:screenName_Contact_Info];
    // Do any additional setup after loading the view.
    
    [CommanMethods getBgImage:self.view];
    self.navigationItem.titleView =[CommanMethods getLabel:APP_CONTACT_US];
    
    self.navigationItem.leftBarButtonItem =[CommanMethods getLeftButton:self Action:@selector(menuMethod)];
    
    arrData = [[NSMutableArray alloc] initWithObjects:screenHeader_Contact_Info, screenHeader_Feedback, nil];
    
   
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.tableView.scrollEnabled = NO;
    [self.tableView registerNib:[UINib nibWithNibName:SettingCell bundle:nil] forCellReuseIdentifier:SettingCell];

}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark- Selector Method
/******************************************
 Method name - menuMethod
 Parameter   - nil
 Desc        - Method for opening Left Menu
 ****************************************/
- (void)menuMethod
{
    [self.sideMenuViewController presentLeftMenuViewController];
}
- (void)search
{
    [[AppDelegate appDelegateInstance]openSearchScreen:self.navigationController];
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [arrData count];
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 68;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    settingsTableViewCell   *cell = [self.tableView dequeueReusableCellWithIdentifier:SettingCell];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.lblTitle.text = [arrData objectAtIndex:indexPath.row];
    cell.lblTitle.backgroundColor = [UIColor clearColor];
    cell.lblTitle.textColor = [CommanMethods  getTextColor];
    cell.lblTitle.font = [CommanMethods getHeaderFont];
    return cell;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if(indexPath.row == 0)
    {
        ContactUsViewController *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"ContactUsViewController"];
        vc.isPushFromLeftSideContactUs = YES;
        [self.navigationController pushViewController:vc animated:YES];
    }
    else if(indexPath.row ==1)
    {
        NSString *strURL=[AppDelegate appDelegateInstance].strFeedback;
        NSURL *url=[NSURL URLWithString:strURL];
        WebViewController *webObj=[[WebViewController alloc]initWithNibName:@"WebViewController" bundle:nil];
        webObj.url=url;
        webObj.hideBottom=YES;
        webObj.strType=@"Feedback";
        [self.navigationController pushViewController:webObj animated:YES];
    }
   
}
- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section {
    return [[UIView alloc] init];
}
@end
