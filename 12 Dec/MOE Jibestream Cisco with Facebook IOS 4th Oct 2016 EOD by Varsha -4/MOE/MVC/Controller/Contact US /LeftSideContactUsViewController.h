//
//  LeftSideContactUsViewController.h
//  MOE
//
//  Created by webwerks on 4/7/16.
//  Copyright © 2016 Neosoft. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LeftSideContactUsViewController : UIViewController<UITableViewDataSource,UITableViewDelegate>
@property (weak, nonatomic) IBOutlet UITableView *tableView;

@end
