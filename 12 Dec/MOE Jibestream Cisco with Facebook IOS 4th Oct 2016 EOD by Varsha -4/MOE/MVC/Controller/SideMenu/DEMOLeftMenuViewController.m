//
//  DEMOMenuViewController.m
//  RESideMenuExample
//
//  Created by Roman Efimov on 10/10/13.
//  Copyright (c) 2013 Roman Efimov. All rights reserved.
//

#import "DEMOLeftMenuViewController.h"

#import "UtilitiesViewController.h"
#import "TheMallViewController.h"
#import "SettingsViewController.h"

#define INDEX_HOME 0
#define INDEX_SHOPPING 1
#define INDEX_DINING 2
#define INDEX_ENTERTAINMENT 3
#define INDEX_HOTELS 4
#define INDEX_STORE_LOCATER 5
#define INDEX_OFFERS 6
#define INDEX_EVENTS 7
#define INDEX_THE_MALL 8
#define INDEX_GIFT_CARDS 9
#define INDEX_SOCIAL_WALL 10
#define INDEX_SPIN_AND_WIN 11
#define INDEX_UTILITIES 12
#define INDEX_SERVICES 13


@interface DEMOLeftMenuViewController ()

@property (strong, readwrite, nonatomic) UITableView *tableView;

@end

@implementation DEMOLeftMenuViewController {
    UIStoryboard *storyBoard;
    NSArray *titles;
    NSArray *images;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    storyBoard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];

    titles = [CommanMethods getSideMenuFields];
    
    
    images = @[@"IconHome", @"IconCalendar", @"IconProfile", @"IconSettings", @"IconEmpty"];

    self.tableView = ({
        UITableView *tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, (self.view.frame.size.height * 0.2) / 2.0f, self.view.frame.size.width, self.view.frame.size.height - (self.view.frame.size.width*0.3)) style:UITableViewStylePlain];
        tableView.autoresizingMask = UIViewAutoresizingFlexibleTopMargin | UIViewAutoresizingFlexibleBottomMargin | UIViewAutoresizingFlexibleWidth;
        tableView.delegate = self;
        tableView.dataSource = self;
        tableView.opaque = NO;
        tableView.backgroundColor = [UIColor clearColor];
        tableView.backgroundView = nil;
        tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        tableView.bounces = NO;
        tableView;
    });
    [self.view addSubview:self.tableView];
}

#pragma mark -
#pragma mark UITableView Delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    // TheMallTabBarController
    
    if(indexPath.row == INDEX_THE_MALL) {
        
        TheMallViewController *vc = [storyBoard instantiateViewControllerWithIdentifier:@"TheMallViewController"];
        [self.sideMenuViewController setContentViewController:[[UINavigationController alloc] initWithRootViewController:vc]
                                                     animated:YES];
        [self.sideMenuViewController hideMenuViewController];
    }
    else if(indexPath.row == INDEX_UTILITIES) {
        
        UtilitiesViewController *vc = [storyBoard instantiateViewControllerWithIdentifier:@"UtilitiesViewController"];
        [self.sideMenuViewController setContentViewController:[[UINavigationController alloc] initWithRootViewController:vc]
                                                     animated:YES];
        [self.sideMenuViewController hideMenuViewController];
    }
    

}

#pragma mark -
#pragma mark UITableView Datasource

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return (SCREEN_HEIGHT/18);
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)sectionIndex
{
    return [titles count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
        cell.backgroundColor = [UIColor clearColor];
        cell.textLabel.font = [UIFont fontWithName:@"HelveticaNeue" size:14];
        cell.textLabel.textColor = [UIColor whiteColor];
        cell.textLabel.highlightedTextColor = [UIColor lightGrayColor];
        cell.selectedBackgroundView = [[UIView alloc] init];
    }
    
    cell.textLabel.text = titles[indexPath.row];
//    cell.imageView.image = [UIImage imageNamed:images[indexPath.row]];
    
    return cell;
}

- (NSString *)tableView:(UITableView *)tableView titleForFooterInSection:(NSInteger)section {
    return @" ";
}


- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section {
    
    UIView *footerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, tableView.frame.size.width, 100)];
//    footerView.backgroundColor = [UIColor redColor];
    
    UIButton *btn = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 200, 50)];
    [btn setTitle:@"SETTINGS" forState:UIControlStateNormal];
//    btn.backgroundColor = [UIColor yellowColor];
    [btn addTarget:self action:@selector(btnSettingMethod:) forControlEvents:UIControlEventTouchUpInside];
    [footerView addSubview:btn];
    
    return footerView;
    
}

- (void)btnSettingMethod:(id)sender {

    SettingsViewController *vc = [storyBoard instantiateViewControllerWithIdentifier:@"SettingsViewController"];
    [self.sideMenuViewController setContentViewController:[[UINavigationController alloc] initWithRootViewController:vc]
                                                 animated:YES];
    [self.sideMenuViewController hideMenuViewController];

}
@end












