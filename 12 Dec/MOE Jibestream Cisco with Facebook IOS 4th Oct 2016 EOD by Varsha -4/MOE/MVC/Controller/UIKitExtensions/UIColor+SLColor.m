//
//  SLColor.m
//  SLDemo
//
//  Copyright (c) 2010-2016 Senion. All rights reserved.
//

#import "UIColor+SLColor.h"

@implementation UIColor (SLColor)

+ (UIColor *)senionBlue
{
    return [UIColor senionBlueWithAlpha:1.0];
}

+ (UIColor *)senionBlueWithAlpha:(CGFloat)alpha
{
    return [UIColor colorWithRed:106.0/255.0 green:172.0/255.0 blue:223.0/255.0 alpha:alpha];
}

+ (UIColor *)darkSenionBlue
{
    return [UIColor darkSenionBlueWithAlpha:1.0];
}

+ (UIColor *)darkSenionBlueWithAlpha:(CGFloat)alpha
{
    return [UIColor colorWithRed:52.0/255.0 green:148.0/255.0 blue:224.0/255.0 alpha:alpha];
}

+ (UIColor *)senionTurquoise
{
    return [UIColor senionTurquoiseWithAlpha:1.0];
}

+ (UIColor *)senionTurquoiseWithAlpha:(CGFloat)alpha
{
    return [UIColor colorWithRed:95.0/255.0 green:210.0/255.0 blue:211.0/255.0 alpha:alpha];
}

+ (UIColor *)darkSenionTurquoise
{
    return [UIColor darkSenionTurquoiseWithAlpha:1.0];
}

+ (UIColor *)darkSenionTurquoiseWithAlpha:(CGFloat)alpha
{
    return [UIColor colorWithRed:47.0/255.0 green:197.0/255.0 blue:213.0/255.0 alpha:alpha];
}

+ (UIColor *)senionYellow
{
    return [UIColor darkSenionYellowWithAlpha:1.0];
}

+ (UIColor *)senionYellowWithAlpha:(CGFloat)alpha
{
    return [UIColor colorWithRed:248.0/255.0 green:209.0/255.0 blue:125.0/255.0 alpha:alpha];
}

+ (UIColor *)darkSenionYellow
{
    return [UIColor darkSenionYellowWithAlpha:1.0];
}

+ (UIColor *)darkSenionYellowWithAlpha:(CGFloat)alpha
{
    return [UIColor colorWithRed:244.0/255.0 green:196.0/255.0 blue:130.0/255.0 alpha:alpha];
}

+ (UIColor *)senionRed
{
    return [UIColor senionRedWithAlpha:1.0];
}

+ (UIColor *)senionRedWithAlpha:(CGFloat)alpha
{
    return [UIColor colorWithRed:255.0/255.0 green:129.0/255.0 blue:108.0/255.0 alpha:alpha];
}

+ (UIColor *)darkSenionRed
{
    return [UIColor darkSenionRedWithAlpha:1.0];
}

+ (UIColor *)darkSenionRedWithAlpha:(CGFloat)alpha;
{
    return [UIColor colorWithRed:255.0/255.0 green:93.0/255.0 blue:113.0/255.0 alpha:alpha];
}

+ (UIColor *)senionGreen
{
    return [UIColor senionGreenWithAlpha:1.0];
}

+ (UIColor *)senionGreenWithAlpha:(CGFloat)alpha
{
    return [UIColor colorWithRed:96.0/255.0 green:211.0/255.0 blue:115.0/255.0 alpha:alpha];
}

+ (UIColor *)darkSenionGreen
{
    return [UIColor darkSenionGreenWithAlpha:1.0];
}

+ (UIColor *)darkSenionGreenWithAlpha:(CGFloat)alpha
{
    return [UIColor colorWithRed:47.0/255.0 green:198.0/255.0 blue:120.0/255.0 alpha:alpha];
}

@end
