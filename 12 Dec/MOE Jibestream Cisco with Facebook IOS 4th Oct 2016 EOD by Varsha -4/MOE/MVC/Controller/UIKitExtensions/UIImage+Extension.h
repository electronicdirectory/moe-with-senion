//
//  UIImage+Custom.h
//  SLDemo
//
//  Copyright (c) 2010-2016 Senion. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface UIImage (Extension)

+ (UIImage *)imageWithColor:(UIColor *)color;

@end
