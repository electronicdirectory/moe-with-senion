//
//  SLColor.h
//  SLDemo
//
//  Copyright (c) 2010-2016 Senion. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface UIColor (SLColor)

+ (UIColor *)senionBlue;
+ (UIColor *)senionBlueWithAlpha:(CGFloat)alpha;

+ (UIColor *)darkSenionBlue;
+ (UIColor *)darkSenionBlueWithAlpha:(CGFloat)alpha;

+ (UIColor *)senionTurquoise;
+ (UIColor *)senionTurquoiseWithAlpha:(CGFloat)alpha;

+ (UIColor *)darkSenionTurquoise;
+ (UIColor *)darkSenionTurquoiseWithAlpha:(CGFloat)alpha;

+ (UIColor *)senionYellow;
+ (UIColor *)senionYellowWithAlpha:(CGFloat)alpha;

+ (UIColor *)darkSenionYellow;
+ (UIColor *)darkSenionYellowWithAlpha:(CGFloat)alpha;

+ (UIColor *)senionRed;
+ (UIColor *)senionRedWithAlpha:(CGFloat)alpha;

+ (UIColor *)darkSenionRed;
+ (UIColor *)darkSenionRedWithAlpha:(CGFloat)alpha;

+ (UIColor *)senionGreen;
+ (UIColor *)senionGreenWithAlpha:(CGFloat)alpha;

+ (UIColor *)darkSenionGreen;
+ (UIColor *)darkSenionGreenWithAlpha:(CGFloat)alpha;

@end
