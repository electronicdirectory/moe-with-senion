//
//  UIButton+Custom.h
//  SLDemo
//
//  Copyright (c) 2010-2016 Senion. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface UIButton (Extension)

- (void)setupButtonWithBackgroundRed:(CGFloat)red green:(CGFloat)green andBlue:(CGFloat)blue;

@end
