//
//  UIButton+Custom.m
//  SLDemo
//
//  Copyright (c) 2010-2016 Senion. All rights reserved.
//

#import "UIButton+Extension.h"

#import "UIImage+Extension.h"

@implementation UIButton (Extension)

- (void)setupButtonWithBackgroundRed:(CGFloat)red green:(CGFloat)green andBlue:(CGFloat)blue
{
    [self setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [self setTitleColor:[UIColor whiteColor] forState:UIControlStateHighlighted];
    
    self.titleLabel.font = [UIFont boldSystemFontOfSize:16.0];
    
    [self setBackgroundImage:[UIImage imageWithColor:[UIColor colorWithRed:red green:green blue:blue alpha:1.0]] forState:UIControlStateNormal];
    [self setBackgroundImage:[UIImage imageWithColor:[UIColor colorWithRed:red green:green blue:blue alpha:0.5]] forState:UIControlStateHighlighted];
    
    [self setBackgroundImage:[UIImage imageWithColor:[UIColor lightGrayColor]] forState:UIControlStateDisabled];
    
    self.layer.cornerRadius = 5.0;
    self.clipsToBounds = YES;
}

@end
