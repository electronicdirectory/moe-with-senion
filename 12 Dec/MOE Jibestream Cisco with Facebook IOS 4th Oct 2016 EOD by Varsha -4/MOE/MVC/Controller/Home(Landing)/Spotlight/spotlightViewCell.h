//
//  spotlightViewCell.h
//  MOE
//
//  Created by webwerks on 6/29/15.
//  Copyright (c) 2015 Neosoft. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface spotlightViewCell : UICollectionViewCell
@property(nonatomic, strong) IBOutlet UIImageView *spotlightImageView;
@end
