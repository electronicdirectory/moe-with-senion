//
//  offerImageViewViewController.h
//  MOE
//
//  Created by webwerks on 6/27/15.
//  Copyright (c) 2015 Neosoft. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface spotlightImageViewController : UIViewController<UICollectionViewDataSource,UICollectionViewDelegate,UIScrollViewDelegate>
@property(nonatomic,strong)NSMutableArray *arrData;
@property (weak, nonatomic)IBOutlet UIPageControl *imagePageControl;
@property(nonatomic,strong)IBOutlet UICollectionView *imageCollection;

@end
