//
//  offerImageViewViewController.m
//  MOE
//
//  Created by webwerks on 6/27/15.
//  Copyright (c) 2015 Neosoft. All rights reserved.
//

#import "spotlightImageViewController.h"
#import "spotlightViewCell.h"
#import "offerDetailViewController.h"
#import "ShoppingDetailsTableViewController.h"
#import "ServiceDetailsViewController.h"
@interface spotlightImageViewController ()
{
    UIImage *placeHolderImage;
}
@end

@implementation spotlightImageViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    [GoogleAnalyticsHandler sendScreenName:screenName_Whats_Happening];

    placeHolderImage= [ UIImage imageNamed:@"place-holder-spotlight6p"];
    UICollectionViewFlowLayout *flowLayout = [[UICollectionViewFlowLayout alloc] init];
    [flowLayout setScrollDirection:UICollectionViewScrollDirectionHorizontal];
    [flowLayout setMinimumInteritemSpacing:0.0f];
    [flowLayout setMinimumLineSpacing:0.0f];
    [self.imageCollection setPagingEnabled:YES];
    [self.imageCollection setCollectionViewLayout:flowLayout];
    self.imageCollection.showsHorizontalScrollIndicator = false;
    [self.imageCollection registerNib:[UINib nibWithNibName:@"spotlightViewCell" bundle:nil] forCellWithReuseIdentifier:@"spotlightViewCell"];
     self.navigationItem.titleView = [CommanMethods getLabel:screenHeader_Whats_Happening];
    self.imagePageControl.numberOfPages=[self.arrData count];

}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
     return 1;
}
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return [self.arrData count] ;
}
- (void)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout cellCenteredAtIndexPath:(NSIndexPath *)indexPath {
    self.imagePageControl.currentPage = indexPath.row;
}
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    spotlightViewCell *cell=(spotlightViewCell *)[collectionView dequeueReusableCellWithReuseIdentifier:@"spotlightViewCell" forIndexPath:indexPath];
    [cell.spotlightImageView setContentMode:UIViewContentModeScaleAspectFit];
        [cell.spotlightImageView sd_setImageWithURL:[CommanMethods getImageURL:[[self.arrData objectAtIndex:indexPath.row]objectForKey:@"Image"]] placeholderImage:placeHolderImage];
    
    return cell;
}
- (CGSize)collectionView:(UICollectionView *)collectionView
                  layout:(UICollectionViewLayout *)collectionViewLayout
  sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    return CGSizeMake(self.view.frame.size.width, self.imageCollection.frame.size.height);
}
-(void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    NSInteger currentIndex = self.imageCollection.contentOffset.x / self.imageCollection
    .frame.size.width;
    self.imagePageControl.currentPage=currentIndex;
}
- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    if ([[[self.arrData objectAtIndex:indexPath.row]objectForKey:@"Mode"]isEqualToString:@"Media"])
    {
        
    }
    else if ([[[self.arrData objectAtIndex:indexPath.row]objectForKey:@"Mode"]isEqualToString:@"URL"])
    {
        [self displayDataInWebView:[NSURL URLWithString:[[self.arrData objectAtIndex:indexPath.row]objectForKey:@"URL"]]];
    }
    else if ([[[self.arrData objectAtIndex:indexPath.row]objectForKey:@"Mode"]isEqualToString:@"Content"])
    {
        SearchModel *searchModel= [DBOpreation getSearchDataById:[NSNumber numberWithInteger:[[[self.arrData objectAtIndex:indexPath.row]objectForKey:@"ID"]integerValue ]] ];
        if (searchModel)
        {
            if ([[searchModel search_Type]isEqualToString:APP_DINE])
            {
                [self moveToShopDetails:[DBOpreation getDineById:[NSNumber numberWithInteger:[searchModel.search_ID integerValue]]]];
            }
            else  if ([[searchModel search_Type]isEqualToString:APP_ENTERTAINMENT])
            {
                [self moveToShopDetails:[DBOpreation getEntListById:[NSNumber numberWithInteger:[searchModel.search_ID integerValue]]]];
            }
            else if ([[searchModel search_Type]isEqualToString:APP_SHOP])
            {
                [self moveToShopDetails:[DBOpreation getShopById:[NSNumber numberWithInteger:[searchModel.search_ID integerValue]]]];
            }
            else if ([[searchModel search_Type]isEqualToString:APP_EVENTS])
            {
                [self moveToOffer:[DBOpreation getEventById:[NSNumber numberWithInteger:[searchModel.search_ID integerValue]]]];
            }
            else if ([[searchModel search_Type]isEqualToString:APP_OFFER])
            {
                [self moveToOffer:[DBOpreation getOfferData:searchModel.search_ID]];
            }
            else if ([[searchModel search_Type]isEqualToString:APP_HOTEL])
            {
                [self moveToOffer:[DBOpreation getHotelsById:[NSNumber numberWithInteger:[searchModel.search_ID integerValue]]]];
            }
            else if ([[searchModel search_Type]isEqualToString:APP_HOTEL])
            {
                ServiceDetailsViewController *obj= [self.storyboard instantiateViewControllerWithIdentifier:@"ServiceDetailsViewController"];
                obj.model=[DBOpreation getServicesById:[NSNumber numberWithInteger:[searchModel.search_ID integerValue]]];
                [self.navigationController pushViewController:obj animated:YES];
            }
        }
        else
        {
            [self displayDataInWebView: [NSURL URLWithString:[[self.arrData objectAtIndex:indexPath.row]objectForKey:@"URL"]]];
        }
    }
}

-(void)moveToShopDetails:(ShopBaseModel *)model
{
    ShoppingDetailsTableViewController   *shop=[self.storyboard instantiateViewControllerWithIdentifier:@"ShoppingDetailsTableViewController"];
    shop.shopBaseModelObj=model;
    [self.navigationController pushViewController:shop animated:YES];
}
-(void)moveToOffer:(BaseModel *)model
{
    offerDetailViewController *offerdetailview = [self.storyboard instantiateViewControllerWithIdentifier:@"offerDetailViewController"];
    offerdetailview.offerInfo=model;
    [self.navigationController pushViewController:offerdetailview animated:YES];
}
-(void)displayDataInWebView:(NSURL*)url
{
    NSString *strUrl=[url.absoluteString stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    if(strUrl.length==0)
    {
        url=[NSURL URLWithString:DEFAULT_WEBSITE_URL];
    }
    WebViewController *webObj=[[WebViewController alloc]initWithNibName:@"WebViewController" bundle:nil];
    webObj.url=url;
    webObj.strType=screenHeader_Whats_Happening;
    [self.navigationController pushViewController:webObj animated:YES];
}
- (void)search
{
    [[AppDelegate appDelegateInstance]openSearchScreen:self.navigationController];
}

@end
