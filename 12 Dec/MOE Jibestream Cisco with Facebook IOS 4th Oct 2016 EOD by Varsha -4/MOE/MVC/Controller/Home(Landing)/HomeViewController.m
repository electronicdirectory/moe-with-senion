//
//  HomeViewController.m
//  MOE
//
//  Created by Neosoft on 6/12/15.
//  Copyright (c) 2015 Neosoft. All rights reserved.
//

#import "HomeViewController.h"
#import "spotlightImageViewController.h"
#import "ShoppingDetailsTableViewController.h"
#import "ServiceDetailsViewController.h"
#import "offerDetailViewController.h"
#import "UtilitiesViewController.h"


#import "Spin&WinViewController.h"
#import "AddMyParkingViewController.h"
#import "SpinAndWinLandingPageViewController.h"
#import "ShopBaseViewController.h"
#import "bookTaxiViewController.h"
#import "GiftCardViewController.h"
#import "GettingHereViewController.h"
#import "CustomInfoView.h"
#import "OpeningHoursViewController.h"
#import "FacilitiesViewController.h"
#import "ContactUsViewController.h"
#import "StoreLocatorViewController.h"
#import "offerDetailViewController.h"
@interface HomeViewController ()
{
    NSUInteger currentAnimationState;
    CustomImageView *spotLightImage;
    
    UIScrollView *mainScrollView;
    BOOL isPushDownAdShown;
    UIView *adView;
    float y;
    CGRect mainFrame;
    CGSize spotLightSize;
 }
@property (nonatomic,strong) IBOutlet  UILabel *lblDine,*lblEntertainment,*lblShop,*lblSpinWin,*lblLocateStore,*lblGift,*lblSocial,*lblUtilties,*lblFind;
@property (nonatomic,strong) IBOutlet UIView *bottomView ;

@end

@implementation HomeViewController

#define GIFTCARDSPAGETAG 100
#define SOCIALWALLPAGETAG 101
#define UTILITIESPAGETAG 102
#define SPINANDWINESPAGETAG 103
#define FINDMYCARTAG 104
#define LOCATESTORETAG 105
#define SHOPTAG 106
#define DINETAG 107
#define ENTERTAINMENTTAG 108


#define Navheight 64
#define TopViewHeight 64
#define BottomViewHeight 64
#define ANIMATION_DURATION 8.0
#define ZERO 0.0

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [defaults synchronize];
    if (![defaults boolForKey:HasLandingLaunchedOnce])
    {
        [defaults setBool:YES forKey:HasLandingLaunchedOnce];
        CustomInfoView *info=[[CustomInfoView alloc]initWithFrame:[AppDelegate appDelegateInstance].window.frame];
        info.strImageName=@"dashboardCoach";
        [[AppDelegate appDelegateInstance].window addSubview:info];
    }
    [defaults synchronize];
    
    
    // Do any additional setup after loading the view.
    self.navigationItem.rightBarButtonItem=[CommanMethods getRightButton:self Action:@selector(search)];
    self.navigationItem.leftBarButtonItem =[CommanMethods getLeftButton:self Action:@selector(menuMethod)];
    self.navigationItem.titleView = [CommanMethods getLabel:[screenHeader_LandingPage uppercaseString]];
    
    mainFrame=self.view.frame;
    NSUserDefaults *pref = [NSUserDefaults standardUserDefaults];
    [pref synchronize];
    float height=mainFrame.size.height-Navheight-TopViewHeight-BottomViewHeight;
    CGRect rect;
    if([[pref objectForKey:mall_Checker]isEqual:INSIDE_MALL])
    {
        [GoogleAnalyticsHandler sendScreenName:screenName_Inside_Mall];
        rect=CGRectMake(ZERO,TopViewHeight+BottomViewHeight+1, mainFrame.size.width, height-1);
    }
    else
    {
        [GoogleAnalyticsHandler sendScreenName:screenName_Outside_Mall];
        rect=CGRectMake(ZERO,Navheight+TopViewHeight+1, mainFrame.size.width, height-2);
    }
    
    
    
    mainScrollView=[[UIScrollView alloc]initWithFrame:rect];
    mainScrollView.backgroundColor=[UIColor clearColor];
    [self.view addSubview:mainScrollView];
    
    spotLightSize=[self getSpotLightSize];
    CGSize offerSize=[self getOfferSize];
    
    CGRect topViewFrame=CGRectMake(ZERO,ZERO,mainFrame.size.width,spotLightSize.height-1);
    topScrollView=[[UIScrollView alloc]initWithFrame:topViewFrame];
    topScrollView.backgroundColor=[UIColor clearColor];
    [mainScrollView addSubview:topScrollView];
    
    
    // add spotLight image
    // add spotLight image
    spotLightImage=[[CustomImageView alloc]initWithFrame:CGRectMake(ZERO,ZERO,spotLightSize.width,spotLightSize.height)];
    spotLightImage.tag = 111;
    NSURL *spotLightUrl;
    if([[HomeDataModel   sharedInstance].arrSpotLightBanner count]>ZERO)
    {
        NSDictionary *dict=[[HomeDataModel   sharedInstance].arrSpotLightBanner objectAtIndex:ZERO];
        if([dict isKindOfClass:[NSDictionary class]])
        {
            spotLightImage.imageInfo=dict;
            spotLightUrl= [CommanMethods getImageURL:[dict objectForKey:hotelsListing_Image]];
        }
    }
    spotLightImage.strType=APP_SPOTLIGHT;
    UIImage *imgPlaceHolder=[UIImage imageNamed:@"place-holder-spotlight6p"];
    if(spotLightUrl)
    {
        [spotLightImage sd_setImageWithURL:spotLightUrl placeholderImage:imgPlaceHolder];
    }
    else
    {
        [spotLightImage setImage:imgPlaceHolder];
    }
    [self setTapEvent:spotLightImage action:@selector(moveToList:)];
    [topScrollView addSubview:spotLightImage];
    [topScrollView sendSubviewToBack:spotLightImage];
    
    UIImageView *middleVerticaleImage=[self getSmallIcon:CGRectMake(spotLightSize.width,ZERO,1,spotLightSize.height) ImageName:@"icon-divider"];
    [topScrollView addSubview:middleVerticaleImage];
    
    
    NSUInteger eventCount=[[HomeDataModel   sharedInstance].arrEvent count];
    NSUInteger offerCount=[[HomeDataModel   sharedInstance].arrOffer count];
    NSMutableArray *arrayData=[[NSMutableArray alloc]init];
    
    if(offerCount>=eventCount)
    {
        NSUInteger diff=0;
        if((offerCount+eventCount)>12)
        {
            diff=12-eventCount;
        }
        else
        {
            diff=offerCount;
        }
        
        for (int i=0;i<[[HomeDataModel   sharedInstance].arrOffer count];i++)
        {
            if(i>diff)
            {
                break;
            }
            NSMutableDictionary *dataDict=[[NSMutableDictionary alloc]init];
            [dataDict setObject:[[HomeDataModel   sharedInstance].arrOffer  objectAtIndex:i] forKey:@"info"];
            [dataDict setObject:APP_OFFER forKey:@"type"];
            [arrayData addObject:dataDict];
        }
        for (int i=0;i<[[HomeDataModel   sharedInstance].arrEvent count];i++)
        {
            if(i+diff>12)
            {
                break;
            }
            NSMutableDictionary *dataDict=[[NSMutableDictionary alloc]init];
            [dataDict setObject:[[HomeDataModel   sharedInstance].arrEvent  objectAtIndex:i] forKey:@"info"];
            [dataDict setObject:APP_EVENTS forKey:@"type"];
            [arrayData addObject:dataDict];
        }
    }
    else if(eventCount>=offerCount)
    {
        NSUInteger diff=0;
        if((offerCount+eventCount)>12)
        {
            diff=12-offerCount;
        }
        else
        {
            diff=eventCount;
        }
        for (int i=0;i<[[HomeDataModel   sharedInstance].arrOffer count];i++)
        {
            NSMutableDictionary *dataDict=[[NSMutableDictionary alloc]init];
            [dataDict setObject:[[HomeDataModel   sharedInstance].arrOffer  objectAtIndex:i] forKey:@"info"];
            [dataDict setObject:APP_OFFER forKey:@"type"];
            [arrayData addObject:dataDict];
        }
        for (int i=0;i<[[HomeDataModel   sharedInstance].arrEvent count];i++)
        {
            if(i>diff)
            {
                break;
            }
            NSMutableDictionary *dataDict=[[NSMutableDictionary alloc]init];
            [dataDict setObject:[[HomeDataModel   sharedInstance].arrEvent  objectAtIndex:i] forKey:@"info"];
            [dataDict setObject:APP_EVENTS forKey:@"type"];
            [arrayData addObject:dataDict];
        }
    }
    if([arrayData count]>0)
    {
        if([arrayData count]%2!=0)
        {
            [arrayData addObject:[NSNull class]];
        }
    }
    
    NSUInteger maxCount=round([arrayData count]/2);
    float offerwidth = spotLightSize.width+1;
    BOOL isNextRow=NO;
    NSUInteger recordCounter=0;
    for (id obj in arrayData)
    {
        float y1=0;
        UIImage *offerPlaceHolder;
        NSString *iconOfLeft;
        if(recordCounter>=maxCount)
        {
            y1=offerSize.height+1;
            if(!isNextRow)
            {
                isNextRow=YES;
                offerwidth=spotLightSize.width+1;
            }
        }
        
        CustomImageView *offerImageView=[[CustomImageView alloc]initWithFrame:CGRectMake(offerwidth,y1,offerSize.width,offerSize.height)];
        
        if([obj isKindOfClass:[NSDictionary class]])
        {
            if([[obj objectForKey:@"type"] isEqualToString:APP_EVENTS])
            {
                offerPlaceHolder=[UIImage imageNamed:@"place-holder-event1-6p"];
                iconOfLeft=@"ico-event6";
            }
            else
            {
                offerPlaceHolder =[UIImage imageNamed:@"place-holder-offer1-6p"];
                iconOfLeft=@"ico-offers6";
            }
            
            NSDictionary *offerDict=(NSDictionary *)obj;
            NSDictionary *infoDict=[offerDict objectForKey:@"info"];
            offerImageView.strType=[offerDict objectForKey:@"type"];
            NSURL *url;
            if([[offerDict objectForKey:@"type"] isEqualToString:APP_EVENTS])
            {
                url = [CommanMethods getImageURL:[infoDict objectForKey:event_Image]];
            }
            else
            {
                url = [CommanMethods getImageURL:[infoDict objectForKey:offerListing_Image_URL]];
            }
            offerImageView.imageInfo=infoDict;
            if(url)
            {
                [offerImageView sd_setImageWithURL:url placeholderImage:offerPlaceHolder];
            }
            else
            {
                [offerImageView setImage:offerPlaceHolder];
            }
            [self setTapEvent:offerImageView action:@selector(moveToList:)];
        }
        else
        {
            offerPlaceHolder=[UIImage imageNamed:@"place-holder-event1-6p"];
            iconOfLeft=nil;
            [offerImageView setImage:offerPlaceHolder];
        }
        [topScrollView addSubview:offerImageView];
        [topScrollView sendSubviewToBack:offerImageView];
        
        offerwidth+=1;
        //add offer and Event div
        UIImageView *offerIcon=[self getSmallIcon:CGRectMake(offerwidth,y1,28,28) ImageName:iconOfLeft];
        [topScrollView addSubview:offerIcon];
        [topScrollView bringSubviewToFront:offerIcon];
        offerwidth+=offerSize.width;
        recordCounter++;
    }
    topScrollView.contentSize=CGSizeMake((offerwidth),spotLightSize.height-1);
    
    /// Ad view for Push down
    y=spotLightSize.height;
    if([[pref objectForKey:mall_Checker]isEqual:INSIDE_MALL])
    {
        CGSize adViewCGSize = [self getAdViewSize];
        adView=[[UIView  alloc]initWithFrame:CGRectMake(0, spotLightSize.height,adViewCGSize.width,0)];
        [adView setBackgroundColor:[UIColor clearColor]];
        [mainScrollView addSubview:adView ];
        adView.clipsToBounds=YES;
        
        //PUSHDOWN
        NSMutableDictionary *params = [NSMutableDictionary dictionary];
        [params setValue:[[AppDelegate appDelegateInstance]getBundelID] forKey:@"myKey"];
        NSString *strTag=nil;
        if([AppDelegate appDelegateInstance].showLocationAds && [[AppDelegate appDelegateInstance].currentLocationId length]>0)
        {
            strTag=[NSString stringWithFormat:@"%@_%@_%@",[AppDelegate appDelegateInstance].currentLocationId,tag_HomeScreen,tag_PushDownLocation];
        }
        else
        {
            strTag=[NSString stringWithFormat:@"%@_%@_%@",cisco_mall_code,tag_HomeScreen,tag_PushDown];
        }
       AppXPushDown* appXPushDownObj = [[AppXPushDown alloc] initWithFrame:CGRectMake(0, 0, adView.frame.size.width, adView.frame.size.height)tag:strTag contextParams:params controller:self delegate:self];
        [adView addSubview:appXPushDownObj];
        y+=adView.frame.size.height;
    }
    //==================Bottom scrollview
    CGSize movieSize=[self getMovieSize];
    CGSize entertainmentSize=[self getEntertainmentSize];
    CGRect bottomViewFrame=CGRectMake(ZERO,y,mainFrame.size.width,rect.size.height-(spotLightSize.height));
    
    bottomScollView   =[[UIScrollView alloc]initWithFrame:bottomViewFrame];
    bottomScollView.backgroundColor=[UIColor clearColor];
    [mainScrollView addSubview:bottomScollView];
    
    [mainScrollView setContentSize:CGSizeMake(self.view.frame.size.width,bottomViewFrame.size.height+y)];
    
    
    NSMutableArray *bottomViewArray=[[NSMutableArray alloc]init];
    if([[HomeDataModel   sharedInstance].arrMovie count]>ZERO)
    {
        [bottomViewArray addObject:[[HomeDataModel   sharedInstance].arrMovie objectAtIndex:ZERO]];
    }
    else
    {
        [bottomViewArray addObject:[NSNull  class]];
    }
    
    if([[HomeDataModel   sharedInstance].arrEntList count]>ZERO)
    {
        NSDictionary *dict=[[HomeDataModel   sharedInstance].arrEntList objectAtIndex:ZERO];
        if([[dict objectForKey:shop_List_key]  isKindOfClass:[NSArray class]])
        {
            NSArray *Shops_list=[dict objectForKey:shop_List_key];
            [bottomViewArray addObjectsFromArray:Shops_list];
        }
    }
    
    NSUInteger counter=1;
    float entertainment = movieSize.width;
    for (id value in bottomViewArray)
    {
        NSString *strPlaceHolder=nil;
        CustomImageView *imageView;
        NSURL *url;
        if(counter==1)
        {
            strPlaceHolder=@"place-holder-movie1-6p";
            imageView=[[CustomImageView alloc]initWithFrame:CGRectMake(ZERO,ZERO,movieSize.width,bottomViewFrame.size.height)];
            imageView.strType=APP_MOVIE;
            if([value isKindOfClass:[NSDictionary class]])
            {
                NSDictionary *dict=(NSDictionary *)value;
                imageView.imageInfo=dict;
                NSString *strURLVal=[dict objectForKey:hotelsListing_Image];
                strURLVal=[strURLVal stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding ];
                url =[NSURL URLWithString:strURLVal];

            }
            UIImageView *movieIcon=[self getSmallIcon:CGRectMake(1,ZERO,28,28) ImageName:@"ico-entertainment6"];
            [bottomScollView addSubview:movieIcon];
        }
        else
        {
            strPlaceHolder=@"place-holder-entertainment1-6p";
            imageView=[[CustomImageView alloc]initWithFrame:CGRectMake(entertainment,ZERO,entertainmentSize.width,bottomViewFrame.size.height)];
            imageView.strType=APP_ENTERTAINMENT;
            
            UIImageView *entIcon=[self getSmallIcon:CGRectMake(entertainment+1,ZERO,28,28) ImageName:@"ico-entertainment6"];
            [bottomScollView addSubview:entIcon];
            
            entertainment+=entertainmentSize.width;
            if([value isKindOfClass:[NSDictionary class]])
            {
                NSDictionary *dict=(NSDictionary *)value;
                imageView.imageInfo=dict;
                NSString *strURL=[dict objectForKey:dining_Thumbnail];
                url = [CommanMethods getImageURL:strURL];
            }
            
        }
        [self setTapEvent:imageView action:@selector(moveToList:)];
        imageView.tag=counter;
        UIImage *imgPlaceHolder=[UIImage imageNamed:strPlaceHolder];
        if(url)
        {
            [imageView sd_setImageWithURL:url placeholderImage:imgPlaceHolder];
        }
        else
        {
            [imageView setImage:imgPlaceHolder];
        }
        [bottomScollView addSubview:imageView];
        [bottomScollView sendSubviewToBack:imageView];
        entertainment+=1;
        counter++;
    }
    bottomScollView.contentSize=CGSizeMake(entertainment, bottomViewFrame.size.height);
    
    [self addGes:GIFTCARDSPAGETAG];
    [self addGes:SOCIALWALLPAGETAG];
    [self addGes:UTILITIESPAGETAG];
    [self addGes:SPINANDWINESPAGETAG];
    [self addGes:FINDMYCARTAG];
    [self addGes:LOCATESTORETAG];
    [self addGes:SHOPTAG];
    [self addGes:DINETAG];
    [self addGes:ENTERTAINMENTTAG];
    
    self.lblDine.font=[CommanMethods getHomeTextFont];
    self.lblEntertainment.font=[CommanMethods getHomeTextFont];
    self.lblFind.font=[CommanMethods getHomeTextFont];
    self.lblGift.font=[CommanMethods getHomeTextFont];
    self.lblLocateStore.font=[CommanMethods getHomeTextFont];
    self.lblShop.font=[CommanMethods getHomeTextFont];
    self.lblSocial.font=[CommanMethods getHomeTextFont];
    self.lblSpinWin.font=[CommanMethods getHomeTextFont];
    self.lblUtilties.font=[CommanMethods getHomeTextFont];
    [self setupNotificationCenter];
    [self setupBottomViewFields];
    [[AppDelegate appDelegateInstance]bringHudToFront];
    [[AppDelegate appDelegateInstance] showFullScreenCiscoAd:self ScreenName:tag_HomeScreen];
    
}
- (void)appXPushDownDidFailToLoad:(AppXPushDown *)appXPushDown withError:(NSError *)error
{
    isPushDownAdShown= NO;
}
- (void)appXPushDownDidFinishLoad:(AppXPushDown *)appXPushDown
{
    isPushDownAdShown= YES;
}
- (void)appXPushDownDidChangeLayout:(AppXPushDown *)appXPushDown{
 
    CGRect rect = adView.frame;
    rect.size.height =140;
    adView.frame = rect;
    y=spotLightSize.height+rect.size.height;
    CGRect bottomViewFrame=CGRectMake(ZERO,y,mainFrame.size.width, bottomScollView.frame.size.height);
    bottomScollView.frame=bottomViewFrame;
    y+=bottomScollView.frame.size.height;
    [mainScrollView setContentSize:CGSizeMake(self.view.frame.size.width,y)];
}

#pragma mark - bottom view fields
- (void)setupBottomViewFields
{
    NSMutableArray *arrMenus=[[NSMutableArray alloc]init];
    NSMutableArray *arrMenusImages=[[NSMutableArray alloc]init];
    
    
    [arrMenus addObject:@"GIFT CARDS"];
    [arrMenus addObject:screenHeader_SocialWall];
    [arrMenus addObject:screenHeader_Utilities];
    
    [arrMenusImages addObject:[UIImage imageNamed:@"icon-giftcard6"]];
    [arrMenusImages addObject:[UIImage imageNamed:@"icon-socialwall6"]];
    [arrMenusImages addObject:[UIImage imageNamed:@"icon-utilities6"]];
    if([[HomeDataModel   sharedInstance].mallMenus containsObject:@"SPIN & WIN"])
    {
        [arrMenus addObject:@"SPIN & WIN"];
        [arrMenusImages addObject:[UIImage imageNamed:@"icon-spinandwin6"]];
    }
    for (int i = 0; i<[arrMenus count]; i++)
    {
        UIView *view = [[UIView alloc] initWithFrame:CGRectMake((SCREEN_WIDTH/[arrMenus count]) * i, 0, SCREEN_WIDTH/[arrMenus count], 64)];
        view.tag = 1100+i;
        [self.bottomView addSubview:view];
        
        UIImageView *img = [[UIImageView alloc] initWithFrame:CGRectMake(view.frame.size.width *0.5- (view.frame.size.width *0.3/2), view.frame.size.height*0.15, view.frame.size.width *0.3, view.frame.size.height*0.5)];
        img.image = [arrMenusImages objectAtIndex:i];
        img.contentMode = UIViewContentModeScaleAspectFit;
        [view addSubview:img];
        if (i<3)
        {
            UIImageView *divImg = [[UIImageView alloc] initWithFrame:CGRectMake(view.frame.size.width -1,view.frame.size.height*0.1, 1, view.frame.size.height*0.8)];
            divImg.image = [UIImage imageNamed:@"div"];
            [view addSubview:divImg];
        }
        UILabel *lbl = [[UILabel alloc] initWithFrame:CGRectMake(0, CGRectGetMaxY(img.frame)+2, view.frame.size.width , view.frame.size.height*0.3)];
        lbl.text = [arrMenus objectAtIndex:i];
        lbl.textAlignment = NSTextAlignmentCenter;
        lbl.font = [CommanMethods getHomeTextFont];
        lbl.textColor = [UIColor whiteColor];
        [view addSubview:lbl];
        lbl.minimumScaleFactor =0.5;
        UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapMethod:)];
        [view addGestureRecognizer:tap];
    }
    [self.view bringSubviewToFront:self.bottomView];
}

#pragma mark - tap gesture
- (void)tapMethod:(UITapGestureRecognizer *)gesture {
    switch ([gesture.view tag]) {
        case 1100: {
            [self pushGiftCarView];
        }
            break;
        case 1101: {
            [self openSocialWall];
            
        }
            break;
        case 1102: {
            [self pushUtilityView];
            
        }
            break;
        case 1103: {
            [self pushSpinAndWinView];
        }
            break;
        default:
            break;
    }
}

//=====================5 th april added by IDEV ===================
-(void)willMoveToParentViewController:(UIViewController *)parent {
    [super willMoveToParentViewController:parent];
    if (!parent)
    {
        [[AppDelegate appDelegateInstance]removeCustomApex];
        [[NSNotificationCenter defaultCenter]removeObserver:self name:@"bottomViewActions" object:nil];
    }
}
#pragma mark - Notification center
- (void)setupNotificationCenter {
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(bottomViewActions:) name:@"bottomViewActions" object:nil];
}
- (void)bottomViewActions :(NSNotification *)dict
{
    int tag = [[[dict userInfo] valueForKey:@"tag"] intValue];
    if(tag == [[NSUserDefaults standardUserDefaults] integerForKey:@"CurrentTagForBottomView"])
        return;
    
    NSArray  *array=self.navigationController.viewControllers;
    NSMutableArray *arrayNav=[[NSMutableArray alloc]init];
    [arrayNav addObject:[array objectAtIndex:0]];
    
    switch (tag)
    {
        case 100: {
            OpeningHoursViewController *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"OpeningHoursViewController"];
            [arrayNav addObject:vc];
        }
            break;
        case 101: {
            FacilitiesViewController *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"FacilitiesViewController"];
            [arrayNav addObject:vc];
        }
            break;
        case 102: {
            ContactUsViewController *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"ContactUsViewController"];
            [arrayNav addObject:vc];
        }
            break;
        case 103: {
            GettingHereViewController *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"GettingHereViewController"];
            [arrayNav addObject:vc];
        }
            break;
        default:
            break;
    }
    [self.navigationController setViewControllers:arrayNav animated:NO];
}

-(void )addGes:(int)tag
{
    UITapGestureRecognizer* singleTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(jumpToPage:)];
    [[self.view viewWithTag:tag] addGestureRecognizer:singleTap];
}
-(UIImageView *)getSmallIcon:(CGRect)rect ImageName:(NSString *)strName
{
    UIImage *img=[UIImage imageNamed:strName];
    UIImageView *imgView=[[UIImageView alloc]initWithFrame:rect];
    imgView.image=img;
    if(![strName isEqualToString:@"icon-divider"]  ||! [strName isEqualToString:@"hor-div"])
        imgView.contentMode=UIViewContentModeScaleAspectFit;
    else
        imgView.contentMode=UIViewContentModeScaleAspectFill;
    return  imgView;
}
-(void)viewWillAppear:(BOOL)animated
{
    self.navigationController.navigationBar.translucent=YES;
    [self performSelector:@selector(flipImages) withObject:nil afterDelay:ANIMATION_DURATION];
}
-(void)viewWillDisappear:(BOOL)animated
{
    self.navigationController.navigationBar.translucent=NO;
    
    [NSObject cancelPreviousPerformRequestsWithTarget:self selector:@selector(flipImages) object:nil];
}
-(void)jumpToPage:(UITapGestureRecognizer *)sender
{
    UIView *view=(UIView *)sender.view;
    switch (view.tag)
    {
        case GIFTCARDSPAGETAG:
            [self pushGiftCarView];
            break;
        case SOCIALWALLPAGETAG:
            [self openSocialWall];
            break;
        case UTILITIESPAGETAG:
            [self pushUtilityView];
            break;
        case SPINANDWINESPAGETAG:
            [self pushSpinAndWinView];
            break;
        case FINDMYCARTAG:
            [self pushFindMyCar];
            break;
        case LOCATESTORETAG:
            [self openStoreLocater];
            break;
        case SHOPTAG:
            [self pushView:APP_SHOP];
            break;
        case DINETAG:
            [self pushView:APP_DINE];
            break;
        case ENTERTAINMENTTAG:
            [self pushView:APP_ENTERTAINMENT];
            break;
    }
}
-(void)pushView:(NSString *)strType
{
    ShopBaseViewController *shopObj=[self.storyboard instantiateViewControllerWithIdentifier:@"ShopBaseViewController"];
    shopObj.strType=strType;
    [self pushToView:shopObj];
}
-(void)pushGiftCarView
{
    GiftCardViewController *giftObj=[self.storyboard instantiateViewControllerWithIdentifier:@"GiftCardViewController"];
    [self pushToView:giftObj];
    
}
- (void)setupWebViewController :(NSURL *)url Title:(NSString *)strTitle{
    
    WebViewController *webObj=[[WebViewController alloc]initWithNibName:@"WebViewController" bundle:nil];
    webObj.url=url;
    webObj.strType=strTitle;
    [self pushToView:webObj];
}
- (void)openStoreLocater
{
    StoreLocatorViewController *storeObj=[[StoreLocatorViewController alloc]initWithNibName:@"StoreLocatorViewController" bundle:nil];
    [self pushToView:storeObj];
}
- (void)openSocialWall {
    
    NSURL *url=[NSURL URLWithString:[CommanMethods getSocialURL]];
    [self setupWebViewController:url Title:screenHeader_SocialWall];
}
-(void)pushUtilityView
{
    UtilitiesViewController *utilityView=[self.storyboard instantiateViewControllerWithIdentifier:@"UtilitiesViewController"];
    [self pushToView:utilityView];
}
-(void)pushSpinAndWinView
{
    Spin_WinViewController *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"Spin&WinViewController"];
    vc.dictCampaign =[NSDictionary dictionaryWithDictionary:[HomeDataModel   sharedInstance].dictCampaign];
    [self pushToView:vc];
    
}
- (void)pushFindMyCar
{
    AddMyParkingViewController *vc=[self.storyboard instantiateViewControllerWithIdentifier:@"AddMyParkingViewController"];
    [self.navigationController pushViewController:vc animated:YES];
}
-(void)setImage
{
    NSUInteger count=[[HomeDataModel   sharedInstance].arrSpotLightBanner count];
    if(currentAnimationState==count)
    {
        currentAnimationState=ZERO;
    }
    UIImage *image=[UIImage imageNamed:@"place-holder-spotlight6p"];
    id value=[[HomeDataModel   sharedInstance].arrSpotLightBanner objectAtIndex:currentAnimationState];
    if([value isKindOfClass:[NSDictionary class]])
    {
        NSDictionary *dict=(NSDictionary *)value;
        
        spotLightImage.imageInfo=dict;
        if([CommanMethods getImageURL:[dict objectForKey:hotelsListing_Image]])
        {
            [spotLightImage sd_setImageWithURL:[CommanMethods getImageURL:[dict objectForKey:hotelsListing_Image]] placeholderImage:image];
        }
        else
        {
            [spotLightImage setImage:image];
        }
    }
    currentAnimationState++;
}
-(void)flipImages{
    if([[HomeDataModel   sharedInstance].arrSpotLightBanner count]>1)
    {
        [self setImage];
        [NSObject cancelPreviousPerformRequestsWithTarget:self selector:@selector(flipImages) object:nil];
        [self performSelector:@selector(flipImages) withObject:nil afterDelay:ANIMATION_DURATION];
    }
}
-(void )setTapEvent:(CustomImageView *)imageObj action:(SEL)actionEvent
{
    UITapGestureRecognizer *tapObj=[[UITapGestureRecognizer alloc]initWithTarget:self action:actionEvent];
    [imageObj addGestureRecognizer:tapObj];
    [imageObj setUserInteractionEnabled:YES];
}

-(CGSize)getSpotLightSize
{
    CGSize size;
    CGRect mainFrame1=self.view.frame;
    if(mainFrame1.size.height==480)
    {
        size=CGSizeMake(210, 194);
    }
    else if(mainFrame1.size.height==568)
    {
        size=CGSizeMake(210, 246);
    }
    else if(mainFrame1.size.height==667)
    {
        size=CGSizeMake(245, 290);
    }
    else
    {
        size=CGSizeMake(270,316);
    }
    return  size;
}
-(CGSize)getOfferSize
{
    CGSize size;
    CGRect mainFrame2=self.view.frame;
    if(mainFrame2.size.height==480)
    {
        size=CGSizeMake(150, 97);
    }
    else if(mainFrame2.size.height==568)
    {
        size=CGSizeMake(150, 123);
    }
    else if(mainFrame2.size.height==667)
    {
        size=CGSizeMake(180, 145);
    }
    else
    {
        size=CGSizeMake(210, 157);
    }
    return  size;
}
-(CGSize)getMovieSize
{
    CGSize size;
    CGRect mainFrame3=self.view.frame;
    if(mainFrame3.size.height==480)
    {
        size=CGSizeMake(90, 90);
    }
    else if(mainFrame3.size.height==568)
    {
        size=CGSizeMake(90, 145);
    }
    else if(mainFrame3.size.height==667)
    {
        size=CGSizeMake(110, 160);
    }
    else
    {
        size=CGSizeMake(130, 180);
    }
    return  size;
}
-(CGSize)getEntertainmentSize
{
    CGSize size;
    mainFrame=self.view.frame;
    if(mainFrame.size.height==480)
    {
        size=CGSizeMake(150, 90);
    }
    else if(mainFrame.size.height==568)
    {
        size=CGSizeMake(170, 140);
    }
    else if(mainFrame.size.height==667)
    {
        size=CGSizeMake(220, 160);
    }
    else
    {
        size=CGSizeMake(270, 180);
    }
    return  size;
}
//get Ad view size
-(CGSize)getAdViewSize
{
    CGSize size;
    mainFrame=self.view.frame;
    if(mainFrame.size.height==480)//1
    {
        size=CGSizeMake(320, 146);
    }
    else if(mainFrame.size.height==568 )//2
    {
        size=CGSizeMake(320, 220);
    }
    else if( mainFrame.size.height==667)//2
    {
        size=CGSizeMake(375, 220);
    }
    
    else//6plus
    {
        size=CGSizeMake(414, 440);
    }
    
    return  size;
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)moveToList :(UITapGestureRecognizer *)sender
{
    CustomImageView *view=(CustomImageView *)sender.view;
    
    if([view.imageInfo isKindOfClass:[NSDictionary class]])
    {
        NSDictionary *dict=(NSDictionary *)view.imageInfo;
        NSNumber *number=[dict objectForKey:@"ID"];
        if([view.strType isEqualToString:APP_ENTERTAINMENT])
        {
            [self moveToShopDetails:[DBOpreation getEntListById:number]];
        }
        else  if([view.strType isEqualToString:APP_OFFER])
        {
            [self moveToOffer:[DBOpreation getOfferData:[NSString stringWithFormat:@"%@",[dict objectForKey:@"ID"]]]];
        }
        else  if([view.strType isEqualToString:APP_EVENTS])
        {
            [self moveToOffer:[DBOpreation getEventById:[dict objectForKey:@"ID"]]];
        }
        else  if([view.strType isEqualToString:APP_MOVIE])
        {
            NSURL *movieURL= [CommanMethods getImageURL:[dict objectForKey:@"URL"]];
            if (movieURL )
            {
                [self selectMovie:movieURL];
            }
            else
            {
                
                [self selectMovie:[NSURL URLWithString:[CommanMethods getMovieURL]]];
            }
        }
        else  if([view.strType isEqualToString:APP_SPOTLIGHT])
        {
            spotlightImageViewController *spotLightView=[self.storyboard instantiateViewControllerWithIdentifier:@"spotlightImageViewController"];
            spotLightView.arrData=[[HomeDataModel   sharedInstance] arrSpotLightBanner];
            [self.navigationController pushViewController:spotLightView animated:YES];
        }
    }
    else
    {
        if([view.strType isEqualToString:APP_MOVIE])
        {
            [self selectMovie:[NSURL URLWithString:[CommanMethods getMovieURL]]];
        }
    }
}
-(void)selectMovie:(NSURL *)url
{
    WebViewController *webObj=[[WebViewController alloc]initWithNibName:@"WebViewController" bundle:nil];
    webObj.url=url;
    webObj.strType=screenHeader_Movie;
    [self.navigationController pushViewController:webObj animated:YES];
}
-(void)pushToView:(UIViewController *)controller
{
    UINavigationController   *navigationController=(UINavigationController *)self.sideMenuViewController.contentViewController;
    NSMutableArray *array=[[NSMutableArray alloc]initWithObjects:controller, nil];
    navigationController.viewControllers=array;
    [self.sideMenuViewController setContentViewController:navigationController animated:YES];
    [self.sideMenuViewController hideMenuViewController];
}

-(void)moveToShopDetails:(ShopBaseModel *)model
{
    ShoppingDetailsTableViewController   *shop=[self.storyboard instantiateViewControllerWithIdentifier:@"ShoppingDetailsTableViewController"];
    shop.shopBaseModelObj=model;
    [self.navigationController pushViewController:shop animated:YES];
}
-(void)moveToOffer:(BaseModel *)model
{
    offerDetailViewController *offerdetailview = [self.storyboard instantiateViewControllerWithIdentifier:@"offerDetailViewController"];
    offerdetailview.offerInfo=model;
    [self.navigationController pushViewController:offerdetailview animated:YES];
}
- (void)menuMethod {
    [self.sideMenuViewController presentLeftMenuViewController];
}
- (void)search
{
    [[AppDelegate appDelegateInstance]openSearchScreen:self.navigationController];
}
-(IBAction)btnTaxiClicked:(id)sender
{
    bookTaxiViewController   *taxiObj=[self.storyboard instantiateViewControllerWithIdentifier:@"bookTaxiViewController"];
    [self.navigationController pushViewController:taxiObj animated:YES];
}
-(IBAction)btnGettingHere:(id)sender
{
    GettingHereViewController   *gettingObj=[self.storyboard instantiateViewControllerWithIdentifier:@"GettingHereViewController"];
    [self.navigationController pushViewController:gettingObj animated:YES];
    
}@end
