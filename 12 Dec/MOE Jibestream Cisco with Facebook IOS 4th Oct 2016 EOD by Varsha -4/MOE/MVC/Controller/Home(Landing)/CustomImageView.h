//
//  CustomImageView.h
//  MOE
//
//  Created by Nivrutti on 20/03/15.
//  Copyright (c) 2015 Neosoft. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CustomImageView : UIImageView
@property(nonatomic,strong)NSDictionary *imageInfo;
@property(nonatomic,strong)NSString *strType;

@end
