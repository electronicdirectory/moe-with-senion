//
//  HomeViewController.h
//  MOE
//
//  Created by Neosoft on 6/12/15.
//  Copyright (c) 2015 Neosoft. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HomeViewController : UIViewController
{
    UIScrollView *topScrollView,*bottomScollView;
}
-(IBAction)btnTaxiClicked:(id)sender;
-(IBAction)btnGettingHere:(id)sender;

 @end
