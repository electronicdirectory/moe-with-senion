//
//  CustomAppXView.h
//  CityCenter
//
//  Created by webwerks on 5/13/16.
//  Copyright © 2016 Nivrutti. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <EMSP/EMSP.h>
@class CustomAppXView;
@protocol RemoveCustomApexDelegate<NSObject>
-(void)removeCustomApexView:(CustomAppXView *)view;
 @end
@interface CustomAppXView : UIScrollView{
    AppXView * appxViewObj;
}
-(instancetype)initWithFrame:(CGRect)frame tag:(NSString *)aTag;
-(IBAction)close:(id)sender;
@property(weak)id<RemoveCustomApexDelegate>apexDelegate;
 @end
