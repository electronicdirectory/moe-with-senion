
#import "CustomAppXView.h"
@implementation CustomAppXView
#define BUNDEL_ID [[NSBundle mainBundle] bundleIdentifier]

- (instancetype)initWithFrame:(CGRect)frame tag:(NSString *)aTag
{
    self = [super initWithFrame:frame];
    if (self) {
        self.hidden=YES;
        self.backgroundColor=[UIColor whiteColor];
        NSMutableDictionary *params = [NSMutableDictionary dictionary];
        [params setValue:BUNDEL_ID forKey:@"myKey"];
        appxViewObj=[[AppXView alloc]initWithFrame:[AppDelegate appDelegateInstance].window.frame tag:aTag   contextParams:params controller:nil delegate:self];
        [self addSubview:appxViewObj];
        [self sendSubviewToBack:appxViewObj];
        
        UIButton *btnCustom=[UIButton buttonWithType:UIButtonTypeCustom];
        [btnCustom addTarget:self action:@selector(close:) forControlEvents:UIControlEventTouchUpInside];
        [btnCustom setImage:[UIImage imageNamed:@"closeButton"] forState:UIControlStateNormal];
        [btnCustom setImage:[UIImage imageNamed:@"closeButton"] forState:UIControlStateSelected];
        [btnCustom setImage:[UIImage imageNamed:@"closeButton"] forState:UIControlStateHighlighted];
        btnCustom.frame=CGRectMake(self.frame.size.width-40, 20, 40, 40);
        [self addSubview:btnCustom];
        [self bringSubviewToFront:btnCustom];
    }
    return self;
}

-(IBAction)close:(id)sender
{
    appxViewObj.delegate=nil;
    [appxViewObj removeFromSuperview];
    appxViewObj=nil;
    [self removeFromSuperview];
    if([self.apexDelegate respondsToSelector:@selector(removeCustomApexView:)])
    {
        [self.apexDelegate removeCustomApexView:self];
    }
 }
- (void)appXViewDidFailToLoad:(AppXView *)appXView withError:(NSError *)error{
    self.hidden=YES;
    [self close:nil];
 }
- (void)appXViewDidFinishLoad:(AppXView *)appXView{
    self.hidden=NO;
    [[AppDelegate appDelegateInstance].window bringSubviewToFront:self];
}

- (void)didReceiveCustomView:(NSString *)viewName withJsonData:(NSDictionary *)customViewDic{
}
- (void)didReceiveCustomView:(NSString *)viewName withInfo:(NSString *)viewNodeValue{
}
@end
