//
//  ManageNotificationsViewController.m
//  MOE
//
//  Created by Neosoft on 6/16/15.
//  Copyright (c) 2015 Neosoft. All rights reserved.
//

#import "ManageNotificationsViewController.h"
#import "CustomInfoView.h"

#define SWITCH_ON_TINT_COLOR [UIColor colorWithRed:0.8510 green:0.2471 blue:0.3020 alpha:1.0]
#define SWITCH_THUMB_COLOR [UIColor colorWithPatternImage:[UIImage imageNamed:@"switch-thumb"]]
#define SWITCH_ON_COLOR [UIColor colorWithPatternImage:[UIImage imageNamed:@"switch-on"]]
#define SWITCH_OFF_COLOR [UIColor colorWithPatternImage:[UIImage imageNamed:@"switch-off"]]
#define VIEW_BG [UIColor colorWithPatternImage:[UIImage imageNamed:@"noti-bg-6"]]
#define TEXTFIELD_BG [UIImage imageNamed:@"noti-field-bg"]
#define imgGiftCard_PLACEHOLDER [UIImage imageNamed:@"ad-banner-notification"]
#define SEL_CHECK [UIImage imageNamed:@"spin-reg-field-sel-chk"]
#define SEL_UNCHECK [UIImage imageNamed:@"spin-reg-field-sel-unchk"]

@interface ManageNotificationsViewController ()
{
    int maleVal;
    BOOL isAlredyRegistered;

}
@end

@implementation ManageNotificationsViewController
{
    UIImage *img_checked;
    UIImage *img_unchecked;
}

#pragma mark - view life cycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    isAlredyRegistered=NO;
    [self.imgGiftCard setImage:[UIImage imageNamed:@"NotificationPlaceHolder"]];
    
    [CommanMethods getBgImage:self.view];
    self.navigationItem.titleView = [CommanMethods getLabel:screenHeader_Notification];
    [self designObjects];
    [self getImageBanner];
    img_checked = SEL_CHECK;
    img_unchecked = SEL_UNCHECK;
    [self.btnMr setBackgroundImage:img_unchecked forState:UIControlStateNormal];
    [self.btnMs setBackgroundImage:img_unchecked forState:UIControlStateNormal];
    self.txtName.delegate = self;
    self.txtEmail.delegate = self;
    self.txtMobile.delegate = self;
    self.txtName.tag = 101;
    self.txtEmail.tag = 102;
    self.txtMobile.tag = 103;
    
    
    
    self.txtEmail.autocorrectionType = UITextAutocorrectionTypeNo;
    self.txtMobile.autocorrectionType = UITextAutocorrectionTypeNo;
    self.txtName.autocorrectionType = UITextAutocorrectionTypeNo;
    [self designTextFields];
    
    self.topView.backgroundColor = [UIColor clearColor];
    
    [defaults synchronize];
    if ([defaults boolForKey:HasLaunchedOnce])
    {
        self.skipViewHeight.constant=0;
        self.topView.hidden = YES;
        [GoogleAnalyticsHandler sendScreenName:screenName_Notification_FirstLaunch];
    }
    else
    {
        CustomInfoView *info=[[CustomInfoView alloc]initWithFrame:[AppDelegate appDelegateInstance].window.frame];
        info.strImageName=@"notificationCoach";
        [[AppDelegate appDelegateInstance].window addSubview:info];
        [GoogleAnalyticsHandler sendScreenName:screenName_SettingsNotification];
    }
    
    [self setSwitchs];
    [self.scrollView contentSizeToFit];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
    
}

#pragma mark - set switchs

- (void)setSwitchs {
    
    if([defaults boolForKey:NOTIFICATION_FASHION_LADIES])
        [self.switch_fashionLadies setOn:YES];
    else
        [self.switch_fashionLadies setOn:NO];
    
    if([defaults boolForKey:NOTIFICATION_FASHION_MEN])
        [self.switch_fashionMen setOn:YES];
    else
        [self.switch_fashionMen setOn:NO];
    
    if([defaults boolForKey:NOTIFICATION_DINNING])
        [self.switch_dinning setOn:YES];
    else
        [self.switch_dinning setOn:NO];
    
    if([defaults boolForKey:NOTIFICATION_OFFERS])
        [self.switch_offers setOn:YES];
    else
        [self.switch_offers setOn:NO];
    
    if([defaults boolForKey:NOTIFICATION_EVENTS])
        [self.switch_events setOn:YES];
    else
        [self.switch_events setOn:NO];
    
    if([defaults boolForKey:NOTIFICATION_WEEKLY_6_12ALERTS])
        self.lblWeeklyUpdate.text = @"6 - 12";
    else
        self.lblWeeklyUpdate.text = @"1 - 6";
    
    
    if([[defaults objectForKey:NOTIFICATION_USER_NAME] isEqual:@"null"]|| ![defaults objectForKey:NOTIFICATION_USER_NAME])
    {
        isAlredyRegistered=NO;
    }
    else
    {
        self.txtName.text = [defaults objectForKey:NOTIFICATION_USER_NAME];
        isAlredyRegistered=YES;
    }
    if([[defaults objectForKey:NOTIFICATION_USER_EMAIL] isEqual:@"null"] || ![defaults objectForKey:NOTIFICATION_USER_EMAIL])
    {
    }
    else
    {
        self.txtEmail.text = [defaults objectForKey:NOTIFICATION_USER_EMAIL];
    }
    if([[defaults objectForKey:NOTIFICATION_USER_MOBILE] isEqual:@"null"]|| ![defaults objectForKey:NOTIFICATION_USER_MOBILE])
    {
    }
    else
    {
        self.txtMobile.text = [defaults objectForKey:NOTIFICATION_USER_MOBILE];
    }
    
    NSNumber *mrVal=[defaults objectForKey:NOTIFICATION_IS_MR];
    if(!mrVal)
    {
        maleVal=-1;
    }
    maleVal=[mrVal intValue];
    if(maleVal==-1)
    {
        [self.btnMr setBackgroundImage:img_unchecked forState:UIControlStateNormal];
        [self.btnMs setBackgroundImage:img_unchecked forState:UIControlStateNormal];
    }
    else if(maleVal==1)
    {
        [self.btnMr setBackgroundImage:img_checked forState:UIControlStateNormal];
        [self.btnMs setBackgroundImage:img_unchecked forState:UIControlStateNormal];
    }
    else
    {
        [self.btnMr setBackgroundImage:img_unchecked forState:UIControlStateNormal];
        [self.btnMs setBackgroundImage:img_checked forState:UIControlStateNormal];
    }
}
#pragma mark -
#pragma mark switch actions
-(void)setUpSwitchData
{
    [defaults synchronize];
    
    if(self.switch_fashionLadies.isOn)
        [defaults setBool:YES forKey:NOTIFICATION_FASHION_LADIES];
    else
        [defaults setBool:NO forKey:NOTIFICATION_FASHION_LADIES];
    
    if(self.switch_fashionMen.isOn)
        [defaults setBool:YES forKey:NOTIFICATION_FASHION_MEN];
    else
        [defaults setBool:NO forKey:NOTIFICATION_FASHION_MEN];
    
    if(self.switch_dinning.isOn)
        [defaults setBool:YES forKey:NOTIFICATION_DINNING];
    else
        [defaults setBool:NO forKey:NOTIFICATION_DINNING];
    
    if(self.switch_offers.isOn)
        [defaults setBool:YES forKey:NOTIFICATION_OFFERS];
    else
        [defaults setBool:NO forKey:NOTIFICATION_OFFERS];
    
    if(self.switch_events.isOn)
        [defaults setBool:YES forKey:NOTIFICATION_EVENTS];
    else
        [defaults setBool:NO forKey:NOTIFICATION_EVENTS];
    
    [defaults setObject:[NSNumber numberWithInt:maleVal] forKey:NOTIFICATION_IS_MR];
    
    if ([self.lblWeeklyUpdate.text isEqualToString:@"1 - 6"])
    {
        [defaults setBool:YES forKey:NOTIFICATION_WEEKLY_1_6ALERTS];
        [defaults setBool:NO forKey:NOTIFICATION_WEEKLY_6_12ALERTS];
    }
    else
    {
        [defaults setBool:NO forKey:NOTIFICATION_WEEKLY_1_6ALERTS];
        [defaults setBool:YES forKey:NOTIFICATION_WEEKLY_6_12ALERTS];
    }
    [defaults setObject:self.txtName.text forKey:NOTIFICATION_USER_NAME];
    
    NSString *emailid = self.txtEmail.text;
    NSString *emailRegex = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
    NSPredicate *emailTest =[NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    BOOL myStringMatchesRegEx=[emailTest evaluateWithObject:emailid];
    if(myStringMatchesRegEx)
        [defaults setObject:self.txtEmail.text forKey:NOTIFICATION_USER_EMAIL];
    else
    {
        if(![[defaults objectForKey:NOTIFICATION_USER_EMAIL] isEqualToString:@"null"])
            self.txtEmail.text = [defaults objectForKey:NOTIFICATION_USER_EMAIL];
    }
    [defaults setObject:self.txtMobile.text forKey:NOTIFICATION_USER_MOBILE];
    
    [defaults synchronize];
}
- (IBAction)switch_fashionWomen:(id)sender
{}
- (IBAction)switch_fashionMen:(id)sender
{}
- (IBAction)switch_dinning:(id)sender
{}
- (IBAction)switch_offers:(id)sender {}
- (IBAction)switch_events:(id)sender {}

#pragma mark button actions
- (IBAction)btnSkipMethod:(id)sender {
    [self call_updateUserPushNotifications];
    [defaults setBool:YES forKey:HasLaunchedOnce];
    [defaults synchronize];
    [[AppDelegate appDelegateInstance]setupRootView];
}
- (IBAction)btnWeeklyUpdate:(id)sender
{
    UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:nil delegate:self cancelButtonTitle:nil destructiveButtonTitle:nil otherButtonTitles:@"1 - 6", @"6 - 12",@"Cancel", nil];
    [actionSheet showInView:self.view];
}
- (IBAction)btnMrMethod:(id)sender
{
    maleVal = 1;
    [self.btnMr setBackgroundImage:img_checked forState:UIControlStateNormal];
    [self.btnMs setBackgroundImage:img_unchecked forState:UIControlStateNormal];
}
- (IBAction)btnMsMethod:(id)sender
{
    maleVal = 0;
    [self.btnMr setBackgroundImage:img_unchecked forState:UIControlStateNormal];
    [self.btnMs setBackgroundImage:img_checked forState:UIControlStateNormal];
}
-(IBAction)btnSubmit:(id)sender
{
    //check and set Root view as per requirment
    if([self validateFields])
    {
        [self setUpSwitchData];
        [self call_updateUserPushNotifications];
        
        [[AppDelegate appDelegateInstance]PlayProgressHudAsPerType:error_registerProfile View:nil];

        if ([defaults boolForKey:HasLaunchedOnce])
        {
            [self.navigationController popViewControllerAnimated:YES];
        }
        else
        {
            [defaults setBool:YES forKey:HasLaunchedOnce];
            [defaults synchronize];
            [[AppDelegate appDelegateInstance]setupRootView];
        }
    }
}
#pragma mark - design objects
- (void)designObjects
{
    //switchs
    [self designSwitch:self.switch_fashionLadies];
    [self designSwitch:self.switch_fashionMen];
    [self designSwitch:self.switch_dinning];
    [self designSwitch:self.switch_offers];
    [self designSwitch:self.switch_events];
    //views
    [self designViews:self.view_fashionLadies];
    [self designViews:self.view_fashionMen];
    [self designViews:self.view_dinning];
    [self designViews:self.view_offers];
    [self designViews:self.view_events];
    [self designViews:self.view_weekly];
    //labels
    [self designLabels:self.lbl_pageHeader];
    [self designLabels:self.lbl_interestedCategories];
    [self designLabels:self.lbl_fashionLadies];
    [self designLabels:self.lbl_fashionMen];
    [self designLabels:self.lbl_dinning];
    [self designLabels:self.lbl_offers];
    [self designLabels:self.lbl_events];
    [self designLabels:self.lbl_alertsToRecieve];
    [self designLabels:self.lbl_weekly];
    [self designLabels:self.lblMr];
    [self designLabels:self.lblMs];
    self.lblWeeklyUpdate.font = [CommanMethods  getHeaderFont];
}

- (UISwitch *)designSwitch :(UISwitch *)sender {
    [sender setOnTintColor:[UIColor colorWithRed:0.8510 green:0.2471 blue:0.3020 alpha:1.0]];
    [sender setBackgroundColor:[CommanMethods getblackColor]];
    sender.layer.cornerRadius = 16.0;
    return sender;
}

- (UIView *)designViews :(UIView *)view{
    view.backgroundColor = VIEW_BG;
    return view;
}

- (UILabel *)designLabels :(UILabel *)lbl{
    lbl.font = [CommanMethods getDetailsFont];
    return lbl;
}

- (void)designTextFields
{
    UIColor *color = [CommanMethods getTextPlaceholderColor];
    UIFont *font = [CommanMethods getDetailsFont];
    NSArray *subviews = [self.textFieldView subviews];
    
    if ([subviews count] == 0)
        return;
    
    for (UITextField *txt in subviews) {
        if([txt isKindOfClass:[UITextField class]]) {
        
            txt.delegate=self;
            if([txt tag] == 101)
                txt.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Name*" attributes:@{NSForegroundColorAttributeName: color, NSFontAttributeName: font}];
            else if([txt tag] == 102)
                txt.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Email Address*" attributes:@{NSForegroundColorAttributeName: color, NSFontAttributeName: font}];
            else if([txt tag] == 103)
                txt.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"+971 55 0000000" attributes:@{NSForegroundColorAttributeName: color, NSFontAttributeName: font}];
            
            UIView *paddingView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 15, 20)];
            txt.leftView = paddingView;
            txt.leftViewMode = UITextFieldViewModeAlways;
            [txt setBackground:TEXTFIELD_BG];
        }
    }
}
- (BOOL)validateFields
{
    BOOL isValidated = true;
    
    NSString *strName=[self.txtName.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    
    NSString *strEmail=[self.txtEmail.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    
    NSString *strPhone=[self.txtMobile.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    
    if (maleVal==-1)
    {
        isValidated = false;
        [CommanMethods displayAlert:error_title_MOE :error_invalid_gender];
    }
    else if ([strName length]==0)
    {
        isValidated = false;
        [CommanMethods displayAlert:error_title_MOE :error_invalid_name];
    }
    else if([strEmail length]==0 || ![CommanMethods validateEmail:strEmail])
    {
        isValidated = false;
        [CommanMethods displayAlert:error_title_MOE :error_invalid_email];
    }
    else if([strPhone length]<8)
    {
        isValidated = false;
        [CommanMethods displayAlert:error_title_MOE :error_invalid_valid_mobile];
    }
    return isValidated;
}

#pragma mark - web service methods
- (void)call_updateUserPushNotifications
{
    
    [[WebServiceCommunicator sharedInstance] updateUserPushNotifications:Service_UpdateUserPushNotifications ResultRespose:Service_UpdateUserPushNotifications_Response_Key ResultValKey:Service_UpdateUserPushNotifications_Resultkey Block:^(id response, NSError *error)
     {
     }];
}
#pragma mark image banner
- (void)getImageBanner
{
    [[WebServiceCommunicator sharedInstance] GetNotificationImage:Service_GetNotificationImage ResultRespose:Service_GetNotificationImage_Response_Key ResultValKey:Service_GetNotificationImage_Resultkey Block:^(id response, NSError *error)
     {
         NSURL *url=nil;
         if([response isKindOfClass:[NSArray class]])
         {
             if([response count]>0)
             {
                 if([[[response objectAtIndex:0] objectForKey:@"ImageURL"] isKindOfClass:[NSString class]])
                 {
                     NSString *imgURL = [[[response objectAtIndex:0] objectForKey:@"ImageURL"] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
                     if([imgURL length]>0)
                     {
                         url = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@", Web_Host_Url, imgURL]];
                     }
                 }
             }
         }
         if(url)
         {
             [self.imgGiftCard sd_setImageWithURL:url placeholderImage:[UIImage imageNamed:@"NotificationPlaceHolder"]];
         }
         else
         {
             [self.imgGiftCard setImage:[UIImage imageNamed:@"NotificationPlaceHolder"]];
         }
     }];
}
#pragma mark - action sheet delegate
- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if(buttonIndex == 0)
    {
        self.lblWeeklyUpdate.text = @"1 - 6";
    }
    else if(buttonIndex==1)
    {
        self.lblWeeklyUpdate.text = @"6 - 12";
    }
}
#pragma mark - text field delegate
- (void)textFieldDidEndEditing:(UITextField *)textField
{
}
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [self.view endEditing:YES];
    return  YES;
}
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    if(textField == _txtMobile)
    {
        NSCharacterSet *invalidCharSet = [[NSCharacterSet characterSetWithCharactersInString:@"0123456789"] invertedSet];
        NSString *filtered = [[string componentsSeparatedByCharactersInSet:invalidCharSet] componentsJoinedByString:@""];
        BOOL  result=[string isEqualToString:filtered];
        return result;
    }
    else
        return YES;
}

@end
