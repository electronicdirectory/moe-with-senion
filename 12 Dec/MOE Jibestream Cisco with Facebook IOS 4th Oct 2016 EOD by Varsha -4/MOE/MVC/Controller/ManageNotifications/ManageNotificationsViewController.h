//
//  ManageNotificationsViewController.h
//  MOE
//
//  Created by Neosoft on 6/16/15.
//  Copyright (c) 2015 Neosoft. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ManageNotificationsViewController : UIViewController <UIActionSheetDelegate, UITextFieldDelegate>


@property (weak, nonatomic) IBOutlet TPKeyboardAvoidingScrollView *scrollView;
@property (weak, nonatomic) IBOutlet UIView *topView;
@property (weak, nonatomic) IBOutlet UIView *baseView;

@property (weak, nonatomic) IBOutlet UIButton *btnSkip;

@property (weak, nonatomic) IBOutlet UIView *view_fashionLadies;
@property (weak, nonatomic) IBOutlet UIView *view_fashionMen;
@property (weak, nonatomic) IBOutlet UIView *view_dinning;
@property (weak, nonatomic) IBOutlet UIView *view_offers;
@property (weak, nonatomic) IBOutlet UIView *view_events;
@property (weak, nonatomic) IBOutlet UIView *view_weekly;


@property (weak, nonatomic) IBOutlet UILabel *lbl_pageHeader;
@property (weak, nonatomic) IBOutlet UILabel *lbl_interestedCategories;
@property (weak, nonatomic) IBOutlet UILabel *lbl_fashionLadies;
@property (weak, nonatomic) IBOutlet UILabel *lbl_fashionMen;
@property (weak, nonatomic) IBOutlet UILabel *lbl_dinning;
@property (weak, nonatomic) IBOutlet UILabel *lbl_offers;
@property (weak, nonatomic) IBOutlet UILabel *lbl_events;
@property (weak, nonatomic) IBOutlet UILabel *lbl_alertsToRecieve;
@property (weak, nonatomic) IBOutlet UILabel *lbl_weekly;



@property (weak, nonatomic) IBOutlet UISwitch *switch_fashionLadies;
@property (weak, nonatomic) IBOutlet UISwitch *switch_fashionMen;
@property (weak, nonatomic) IBOutlet UISwitch *switch_dinning;
@property (weak, nonatomic) IBOutlet UISwitch *switch_offers;
@property (weak, nonatomic) IBOutlet UISwitch *switch_events;

@property (weak, nonatomic) IBOutlet UILabel *lblWeeklyUpdate;
@property (weak, nonatomic) IBOutlet UIButton *btnWeeklyUpdate;
@property (weak, nonatomic) IBOutlet UIImageView *imgGiftCard;

@property (weak, nonatomic) IBOutlet UIView *textFieldView;
@property (weak, nonatomic) IBOutlet UIButton *btnMr;
@property (weak, nonatomic) IBOutlet UIButton *btnMs;
@property (weak, nonatomic) IBOutlet UILabel *lblMr;
@property (weak, nonatomic) IBOutlet UILabel *lblMs;
@property (weak, nonatomic) IBOutlet UITextField *txtName;
@property (weak, nonatomic) IBOutlet UITextField *txtEmail;
@property (weak, nonatomic) IBOutlet UITextField *txtMobile;


- (IBAction)btnSkipMethod:(id)sender;
- (IBAction)switch_fashionWomen:(id)sender;
- (IBAction)switch_fashionMen:(id)sender;
- (IBAction)switch_dinning:(id)sender;
- (IBAction)switch_offers:(id)sender;
- (IBAction)switch_events:(id)sender;

- (IBAction)btnWeeklyUpdate:(id)sender;

- (IBAction)btnMrMethod:(id)sender;
- (IBAction)btnMsMethod:(id)sender;

- (IBAction)btnSubmit:(id)sender;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *skipViewHeight;

@end
