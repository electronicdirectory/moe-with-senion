//
//  UtilitiesTableViewCell.h
//  MOE
//
//  Created by Neosoft on 6/27/15.
//  Copyright (c) 2015 Neosoft. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UtilitiesTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *img;
@property (weak, nonatomic) IBOutlet UILabel *lbl;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *topViewHeightConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *middleViewHeightConstraint;
@property (weak, nonatomic) IBOutlet UIImageView *separator;

@end
