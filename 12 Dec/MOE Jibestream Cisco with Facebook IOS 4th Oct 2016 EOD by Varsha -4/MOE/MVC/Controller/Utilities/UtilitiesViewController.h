//
//  UtilitiesViewController.h
//  MOE
//
//  Created by Neosoft on 6/11/15.
//  Copyright (c) 2015 Neosoft. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ZBarSDK.h"
#import <AddressBook/AddressBook.h>
#import <MessageUI/MessageUI.h>

@interface UtilitiesViewController : UIViewController <UITableViewDelegate, UITableViewDataSource, ZBarReaderDelegate, MFMessageComposeViewControllerDelegate,UIAlertViewDelegate,MFMailComposeViewControllerDelegate>

@property (weak, nonatomic) IBOutlet UITableView *tableView;


@end
