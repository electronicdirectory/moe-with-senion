//
//  FashionWearViewController.h
//  MOE
//
//  Created by Neosoft on 6/12/15.
//  Copyright (c) 2015 Neosoft. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FashionWearViewController : UIViewController <UITableViewDataSource, UITableViewDelegate>

@property(nonatomic,retain)NSMutableDictionary *data;
@property(nonatomic,retain)NSString *type;
@property(nonatomic) UIImage *imgHeader;

@property (weak, nonatomic) IBOutlet UITableView *tableView;

@end
