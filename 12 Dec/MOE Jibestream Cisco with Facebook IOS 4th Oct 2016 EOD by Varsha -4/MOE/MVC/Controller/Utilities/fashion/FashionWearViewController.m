//
//  FashionWearViewController.m
//  MOE
//
//  Created by Neosoft on 6/12/15.
//  Copyright (c) 2015 Neosoft. All rights reserved.
//

#import "FashionWearViewController.h"
#import "ShoeFashionSizeConverterViewController.h"

@interface FashionWearViewController ()
{
    NSArray *arrKeys;

}
@end

@implementation FashionWearViewController
@synthesize data;
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [GoogleAnalyticsHandler sendScreenName:screenName_FashionConverter];

    [CommanMethods getBgImage:self.view];
    self.navigationItem.titleView = [CommanMethods getLabel:[self.type uppercaseString]];

    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    
    self.tableView.backgroundColor = [UIColor clearColor];
    self.tableView.scrollEnabled = NO;
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    
    NSMutableDictionary *dict=[CommanMethods getFashionSizes];
    data=[dict objectForKey:self.type];
    arrKeys = [[data allKeys] sortedArrayUsingSelector:@selector(localizedCaseInsensitiveCompare:)];

}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return ([data count] + ([data count]-1));
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if(indexPath.row%2 == 0) {
        return 60;
    }
    else {
        return 3;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    
    cell.selectionStyle=UITableViewCellSelectionStyleNone;
    
    if(indexPath.row%2 == 0) {
        cell.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"fashion-list-bg"]];
        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
        cell.textLabel.textColor=[CommanMethods getTextColor];
        cell.textLabel.font=[CommanMethods getHeaderFont];
        cell.textLabel.backgroundColor=[UIColor clearColor];
        cell.textLabel.textAlignment=NSTextAlignmentLeft;
        cell.textLabel.text=[[arrKeys objectAtIndex:(indexPath.row/2)] uppercaseString];
    }
    else {
        cell.backgroundColor = [UIColor clearColor];
    }
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    ShoeFashionSizeConverterViewController *detailViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"ShoeFashionSizeConverterViewController"];
    detailViewController.personType = self.type;
    detailViewController.SHOEorFASHION = @"fashion";
    detailViewController.fashionType = [arrKeys objectAtIndex:(indexPath.row/2)];
    detailViewController.selectedArray=[data objectForKey:[arrKeys objectAtIndex:(indexPath.row/2)]];
    detailViewController.title = [[arrKeys objectAtIndex:(indexPath.row/2)] uppercaseString];
    [self.navigationController pushViewController:detailViewController animated:YES];
    
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section {
    return [[UIView alloc] init];
}

@end




