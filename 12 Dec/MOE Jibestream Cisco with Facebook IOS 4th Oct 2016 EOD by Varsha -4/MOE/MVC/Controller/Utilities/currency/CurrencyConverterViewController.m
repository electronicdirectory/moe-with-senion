//
//  CurrencyConverterViewController.m
//  MOE
//
//  Created by Nivrutti on 10/04/15.
//  Copyright (c) 2015 Neosoft. All rights reserved.
//

#import "CurrencyConverterViewController.h"
//#import "CustomMethods.h"
#import "WebServiceCommunicator.h"
#import "Language.h"
//#import "WebServiceResponseKeys.h"
#import "Constant.h"
#import "ErrorMessages.h"
@interface CurrencyConverterViewController ()
{
    NSMutableDictionary *dictForCurrencyList;
    NSArray *arrList;
    UITableView *tblViewForList;
    NSString *fromString, *toString;
    WebServiceCommunicator *webObj;
    NSMutableArray *arrResponce;
    NSUserDefaults *pref;
    UIToolbar* numberToolbar;
    BOOL hideCursor;
}
@end

@implementation CurrencyConverterViewController

#define  TBLFROM 100
#define  TBLTO 200

#define TABLE_HEIGHT 145

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [CommanMethods getBgImage:self.view];

    [GoogleAnalyticsHandler sendScreenName:screenName_CurrencyConverter];

   
    self.navigationItem.titleView = [CommanMethods getLabel:screenHeader_Utilities_Currency];

    self.txtInput.autocorrectionType = UITextAutocorrectionTypeNo;
    self.txtInputUnderline.autocorrectionType = UITextAutocorrectionTypeNo;
    self.txtOutput.autocorrectionType = UITextAutocorrectionTypeNo;
    
    pref=[NSUserDefaults standardUserDefaults];
    [pref synchronize];
    
    tblViewForList=[[UITableView alloc]init];
    tblViewForList.dataSource=self;
    tblViewForList.delegate=self;
    NSData *data=[NSData dataWithContentsOfFile:[[NSBundle mainBundle]pathForResource:@"currency" ofType:@"json"]];
    id response=[JsonBuilder parseJsonObject:data];

     arrList=[[NSMutableArray alloc]initWithArray:response];

    
    self.txtOutput.text=@"";
    self.txtInput.keyboardType = UIKeyboardTypeNumberPad;
    self.btnFirstValue.titleLabel.font = [CommanMethods getFont:18.0];
    self.btnSecondValue.titleLabel.font = [CommanMethods getFont:18.0];
    self.txtInput.font=[CommanMethods getHeaderFont];
    self.txtOutput.font=[CommanMethods getHeaderFont];
    self.txtInput.textColor=[CommanMethods getTextColor];
    self.txtOutput.textColor=[CommanMethods getTextColor];

//    NSDictionary *attrs = @{ NSForegroundColorAttributeName : [CommanMethods getTextColor], NSFontAttributeName : [CommanMethods getDetailsFont] };
    NSDictionary *underlineAttribute = @{NSUnderlineStyleAttributeName: @(NSUnderlineStyleSingle), NSForegroundColorAttributeName : [CommanMethods getTextPlaceholderColor], NSFontAttributeName : [CommanMethods getDetailsFont]};
    self.txtInput.attributedText = [[NSAttributedString alloc] initWithString:@"" attributes:underlineAttribute];
    self.txtInput.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Enter Amount Here" attributes:underlineAttribute];

    UIView *paddingView_input = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 15, 20)];
    self.txtInput.rightView = paddingView_input;
    self.txtInput.rightViewMode = UITextFieldViewModeAlways;
    UIView *paddingView_inputUnderline = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 15, 20)];
    self.txtInputUnderline.rightView = paddingView_inputUnderline;
    self.txtInputUnderline.rightViewMode = UITextFieldViewModeAlways;
    UIView *paddingView_output = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 15, 20)];
    self.txtOutput.rightView = paddingView_output;
    self.txtOutput.rightViewMode = UITextFieldViewModeAlways;

    webObj=[WebServiceCommunicator sharedInstance];
    arrResponce=[[NSMutableArray alloc]init];
    toString=@"AED";
    fromString=@"USD";
    
    numberToolbar = [[UIToolbar alloc]initWithFrame:CGRectMake(0, 0, 320, 50)];
    numberToolbar.barStyle = UIBarStyleBlack;

    numberToolbar.items = [NSArray arrayWithObjects:
                           [[UIBarButtonItem alloc]initWithTitle:@"CANCEL" style:UIBarButtonItemStylePlain target:self action:@selector(cancelNumberPad)],
                           [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil],
                           [[UIBarButtonItem alloc]initWithTitle:@"DONE" style:UIBarButtonItemStylePlain target:self action:@selector(doneWithNumberPad)],
                           nil];
    
    numberToolbar.barTintColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"currency-main-country-bg"]];

    [numberToolbar sizeToFit];
    [numberToolbar setTintColor:[CommanMethods getblackColor]];
    _txtInput.inputAccessoryView = numberToolbar;
    
    self.cursorView = [[UIView alloc] initWithFrame:CGRectMake(0.0, self.txtInput.frame.size.width/2, 1, 20.0)];
    self.cursorView.backgroundColor = [CommanMethods getblackColor];
    self.cursorView.alpha = 0.0f;
    self.cursorView.center = CGPointMake(self.txtInput.center.x-10, self.cursorView.center.y);
    [self.txtInput addSubview:self.cursorView];
    
    [self cursorBlinking];
}
-(void)cursorBlinking{
    
    if(hideCursor){
        return;
    }
    [UIView animateWithDuration:0.3 animations:^{
        
        self.cursorView.alpha = 1.0f;
        
    }completion:^(BOOL finish){
        
        if(finish){
            
            [UIView animateWithDuration:0.3 delay:0.5 options:UIViewAnimationOptionCurveLinear animations:^{
                
                self.cursorView.alpha = 0.0f;
                
            }completion:^(BOOL finish){
                
                if(finish){
                    
                    if([[self.txtInput text] length] == 0){
                        
                        [NSObject cancelPreviousPerformRequestsWithTarget:self selector:@selector(cursorBlinking) object:nil];
                        [self performSelector:@selector(cursorBlinking) withObject:nil afterDelay:0.0];
                    }
                }
            }];
        }
    }];
}

-(void)cancelNumberPad{
    [_txtInput resignFirstResponder];
    
    if([[_txtInput text] length] == 0){
    
        hideCursor = NO;
        self.cursorView.hidden = NO;
    }
    _txtInput.text = @"";
}

-(void)doneWithNumberPad{
    
    if([[_txtInput text] length] == 0){
        
        hideCursor = NO;
        self.cursorView.hidden = NO;
    }
     if([[_txtInput text] length] != 0){
         [self callWebService];
     }
    [_txtInput resignFirstResponder];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(IBAction)btnFirstValueClicked:(id)sender
{
    tblViewForList.frame=CGRectMake(0, TABLE_HEIGHT, self.view.frame.size.width , self.view.frame.size.height-TABLE_HEIGHT);
    [tblViewForList reloadData];
    tblViewForList.tag=TBLFROM;
    [_txtInput resignFirstResponder];
    if([[_txtInput text] length] == 0){
        
        hideCursor = NO;
        self.cursorView.hidden = NO;
    }
    [UIView animateWithDuration:0.1 animations:^{
        [self.view addSubview:tblViewForList];
    } completion:nil];
}

-(IBAction)btnSecondValueClicked:(id)sender
{
    tblViewForList.frame=CGRectMake(0, TABLE_HEIGHT, self.view.frame.size.width , self.view.frame.size.height-TABLE_HEIGHT);
    [tblViewForList reloadData];
    [_txtInput resignFirstResponder];
    if([[_txtInput text] length] == 0)
    {
        hideCursor = NO;
        self.cursorView.hidden = NO;
    }
    tblViewForList.tag=TBLTO;
    [self.view addSubview:tblViewForList];
}

-(IBAction)switch_clicked:(id)sender
{
    [_txtInput resignFirstResponder];
    NSString *tempString=fromString;
    fromString=toString;
    toString=tempString;
    [[self.view viewWithTag:TBLFROM] removeFromSuperview];
    [[self.view viewWithTag:TBLTO] removeFromSuperview];
    [self.btnFirstValue setTitle:fromString forState:UIControlStateNormal];
    [self.btnSecondValue setTitle:toString forState:UIControlStateNormal];
    [self.txtInput resignFirstResponder];
     if([[_txtInput text] length] == 0){
         hideCursor = NO;
        self.cursorView.hidden = NO;
    }
    else
    {
        [self callWebService];
    }
}

#pragma mark- Table view Delegate

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return  1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    return [arrList count];
}


-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 40;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell"];
    if (cell == nil)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"Cell"];
        cell.accessoryType = UITableViewCellAccessoryNone;
    }
    
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];   
    cell.textLabel.text=[arrList objectAtIndex:indexPath.row];
    cell.textLabel.font=[CommanMethods getDetailsFont];
    cell.textLabel.textColor=[CommanMethods getblackColor];
    
    return cell;
}

/******************************************
 Method name - didSelectRowAtIndexPath
 Parameter   - UITableView, NSIndexPath
 Return      - nil
 Desc        - Method Called on selection of row of table view
 ****************************************/
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    [_txtInput resignFirstResponder];
    NSString *strVal;
    NSArray *array=[[arrList objectAtIndex:indexPath.row] componentsSeparatedByString:@"("];
    if([array count]>1)
    {
        strVal=[array objectAtIndex:1];
        strVal=[strVal stringByReplacingOccurrencesOfString:@")" withString:@""];
    }
    
    if (tableView.tag==TBLFROM)
    {
        fromString=strVal;
        
        [self.btnFirstValue setTitle:fromString forState:UIControlStateNormal];
        
        if (!toString)
        {
            UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"MOE" message:@"Select Currency " delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            [alert show];
        }
        
        if (!fromString) {
            UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"MOE" message:@"Select Currency " delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            [alert show];
        }
        
        if ([[_txtInput text] length]>0) {
            [self callWebService];
            
        }
    }
    if (tableView.tag==TBLTO)
    {
        toString=strVal;
        
        [self.btnSecondValue setTitle:toString forState:UIControlStateNormal];
        
        
        if (!toString) {
            UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"MOE" message:@"Select Currency " delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            [alert show];
        }
        
        if (!fromString) {
            UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"MOE" message:@"Select Currency " delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            [alert show];
        }
        
        if ([[_txtInput text] length]>0) {
            [self callWebService];
            
        }
        
    }
    
    [tableView removeFromSuperview];
    
}

-(void)callWebService
{
    if (fromString && toString)
    {
        if ([APP_DELEGATE CheckNetWorkConnection]) {
            [APP_DELEGATE showHud:nil Title:nil];
            [webObj getCurrencyValue:fromString toString:toString CompletionBlock:^(id response, NSError *error)
             {
                 [APP_DELEGATE closeHud];
                 [arrResponce removeAllObjects];
                 if(!error && [response isKindOfClass:[NSDictionary class]] )
                 {
                    [arrResponce addObject:[[[[response valueForKey:@"query"] valueForKey:@"results"] valueForKey:@"row"] valueForKey:@"col1"]];
                 }
                 if ([arrResponce count]>0)
                 {
                     
                     float rate=[[arrResponce objectAtIndex:0] floatValue];
                     rate=rate *[_txtInput.text integerValue];
                     self.txtOutput.text=[NSString stringWithFormat:@"%.4f",rate];
                 }
             }];
        }
        else
        {
            [APP_DELEGATE PlayProgressHudAsPerType:error_message View:nil];
        }
    }
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    UITouch *touch = [[event allTouches] anyObject];
    if ([_txtInput isFirstResponder] && [touch view] != _txtInput) {
        [_txtInput resignFirstResponder];
        
        if([[_txtInput text] length] == 0){
            
            hideCursor = NO;
            self.cursorView.hidden = NO;
        }

    }
    [super touchesBegan:touches withEvent:event];
    
    if([[_txtInput text] length] == 0){
        
        hideCursor = NO;
        self.cursorView.hidden = NO;
    }
}

#pragma mark- TextField Delegate
/******************************************
 Method name - textFieldShouldReturn
 Parameter   - UITextField
 Return      - BOOL
 Desc        - Method Called on selection of textField
 ****************************************/
-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
     [self callWebService];
    return YES;
}

/******************************************
 Method name - textFieldDidBeginEditing
 Parameter   - UITextField
 Return      - nil
 Desc        - Method Called when text is entered in text filed
 ****************************************/
-(void)textFieldDidBeginEditing:(UITextField *)textField
{
    
    hideCursor = YES;
    self.cursorView.hidden = YES;
    
    if (textField.tag==110) {
    }
    self.txtInput.delegate=self;
}
@end
