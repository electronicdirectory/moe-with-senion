//
//  CurrencyConverterViewController.h
//  MOE
//
//  Created by Nivrutti on 10/04/15.
//  Copyright (c) 2015 Neosoft. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CurrencyConverterViewController : UIViewController<UITableViewDataSource,UITableViewDelegate,UITextFieldDelegate>

-(IBAction)btnFirstValueClicked:(id)sender;
-(IBAction)btnSecondValueClicked:(id)sender;
-(IBAction)switch_clicked:(id)sender;

@property(nonatomic, strong) IBOutlet UIButton *btnFirstValue;
@property(nonatomic, strong) IBOutlet UIButton *btnSecondValue;
@property(nonatomic, strong) IBOutlet UIButton *btnSwtich;
@property(nonatomic,strong) IBOutlet UILabel *lblTitle;



@property(nonatomic, strong) IBOutlet UITextField *txtInput;
@property(nonatomic, strong) IBOutlet UITextField *txtInputUnderline;
@property(nonatomic, strong) IBOutlet UITextField *txtOutput;
@property(nonatomic,strong) UIView *cursorView;
@end
