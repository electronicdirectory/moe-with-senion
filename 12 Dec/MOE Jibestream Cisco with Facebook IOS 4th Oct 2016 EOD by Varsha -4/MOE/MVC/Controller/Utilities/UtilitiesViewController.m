//
//  UtilitiesViewController.m
//  MOE
//
//  Created by Neosoft on 6/11/15.
//  Copyright (c) 2015 Neosoft. All rights reserved.
//

#import "UtilitiesViewController.h"
#import "ShoeFashionPersonSelectorViewController.h"
#import "CurrencyConverterViewController.h"
#import "UtilitiesTableViewCell.h"

#define INDEXPATH_ROW_SHOESIZE 0
#define INDEXPATH_ROW_FASHION 1
#define INDEXPATH_ROW_CURRENCY 2
#define INDEXPATH_ROW_QR 3

@interface UtilitiesViewController ()

@end

@implementation UtilitiesViewController {
    NSArray *arrTableData;
    NSString *finalData;
    NSArray *arrIcons;
    shoeFashionPersonSelectorViewController *shoeVC;
    CurrencyConverterViewController *currencyVC;
    RESideMenu *sideMenuViewController;
    NSString *linkToURL;
}

#pragma mark - View life cycle

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [GoogleAnalyticsHandler sendScreenName:screenName_UtilitiesListing];
    
    // Do any additional setup after loading the view.
    [CommanMethods getBgImage:self.view];
    
    
    self.navigationItem.titleView =[CommanMethods getLabel:screenHeader_Utilities];
    
    self.navigationItem.rightBarButtonItem=[CommanMethods getRightButton:self Action:@selector(search)];
    self.navigationItem.leftBarButtonItem =[CommanMethods getLeftButton:self Action:@selector(menuMethod)];
    
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    
    self.tableView.scrollEnabled = NO;
    self.tableView.backgroundColor = [UIColor clearColor];
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    
    [self.tableView registerClass:[UtilitiesTableViewCell class] forCellReuseIdentifier:@"UtilitiesTableViewCell"];
    
    
    arrTableData = [[NSArray alloc] initWithObjects:screenHeader_Utilities_ShoeSize, screenHeader_Utilities_Fashion, screenHeader_Utilities_Currency, screenHeader_Utilities_QRCODE, nil];
    arrIcons = [[NSArray alloc] initWithObjects:[UIImage imageNamed:@"icon-shoe"], [UIImage imageNamed:@"icon-fashion"], [UIImage imageNamed:@"icon-currecny"], [UIImage imageNamed:@"icon-qr"], nil];
}
-(void)viewWillAppear:(BOOL)animated
{
}
#pragma mark memory warning
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma mark navigation bar button methods
- (void)menuMethod {
    [self.sideMenuViewController presentLeftMenuViewController];
}
- (void)search
{
    [[AppDelegate appDelegateInstance]openSearchScreen:self.navigationController];
}

#pragma mark - Table view delegates

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [arrTableData count];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return (self.view.frame.size.height/4);
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    if(indexPath.row == INDEXPATH_ROW_SHOESIZE) {
        shoeVC = [self.storyboard instantiateViewControllerWithIdentifier:@"ShoeFashionPersonSelectorViewController"];
        shoeVC.SHOEorFASHION = @"shoe";
        [self.navigationController pushViewController:shoeVC animated:YES];
    }
    else if(indexPath.row == INDEXPATH_ROW_FASHION) {
        shoeVC = [self.storyboard instantiateViewControllerWithIdentifier:@"ShoeFashionPersonSelectorViewController"];
        shoeVC.SHOEorFASHION = @"fashion";
        [self.navigationController pushViewController:shoeVC animated:YES];
    }
    else if(indexPath.row == INDEXPATH_ROW_CURRENCY) {
        currencyVC = [self.storyboard instantiateViewControllerWithIdentifier:@"CurrencyConverterViewController"];
        [self.navigationController pushViewController:currencyVC animated:YES];
    }
    else if (indexPath.row == INDEXPATH_ROW_QR ) {
        [self startScanner];
    }
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section {
    return [[UIView alloc] init];
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    [self performSelector:@selector(playAniamtion:) withObject:indexPath afterDelay:0.2*indexPath.row];
}

-(void)playAniamtion:(NSIndexPath *)indexPath{
    
    UITableViewCell *cell = (UITableViewCell *)[self.tableView cellForRowAtIndexPath:indexPath];
    CGRect frmCell;
    if (indexPath.row%2==1) {
        frmCell=CGRectMake(cell.frame.origin.x-self.view.frame.size.width, cell.frame.origin.y, cell.frame.size.width, cell.frame.size.height);
    }
    else
        frmCell=CGRectMake(cell.frame.origin.x+self.view.frame.size.width, cell.frame.origin.y, cell.frame.size.width, cell.frame.size.height);
    
    [UIView animateWithDuration:0.5 animations:^{
        cell.frame = frmCell;
        cell.alpha = 1.0f;
        
    }];
    
}
- (UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    [tableView registerNib:[UINib nibWithNibName:@"UtilitiesTableViewCell" bundle:[NSBundle mainBundle]] forCellReuseIdentifier:@"UtilitiesTableViewCell"];
    UtilitiesTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"UtilitiesTableViewCell"];
    cell.backgroundColor = [UIColor clearColor];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.img.image = [arrIcons objectAtIndex:indexPath.row];
    cell.lbl.text = [arrTableData objectAtIndex:indexPath.row];
    cell.lbl.font = [CommanMethods getFont:12.0];
    cell.lbl.adjustsFontSizeToFitWidth = YES;
    [self performSelector:@selector(playBeforeAniamtion:) withObject:indexPath afterDelay:0.0*indexPath.row];
    if (indexPath.row == [arrTableData count]-1) {
        cell.separator.hidden=YES;
    }
    
    return cell;
}
-(void)playBeforeAniamtion:(NSIndexPath *)indexPath{
    UITableViewCell *cell = (UITableViewCell *)[self.tableView cellForRowAtIndexPath:indexPath];
    if (indexPath.row%2==0) {
        cell.frame=CGRectMake(cell.frame.origin.x-self.view.frame.size.width, cell.frame.origin.y, cell.frame.size.width, cell.frame.size.height);
    }
    else
        cell.frame=CGRectMake(cell.frame.origin.x+self.view.frame.size.width, cell.frame.origin.y, cell.frame.size.width, cell.frame.size.height);
    
}

#pragma mark - QR SCANNER

- (void)startScanner {
    
    ZBarReaderViewController *reader = [ZBarReaderViewController new];
    reader.readerDelegate = self;
    reader.supportedOrientationsMask = ZBarOrientationMaskAll;
    ZBarImageScanner *scanner = reader.scanner;
    [scanner setSymbology: ZBAR_I25
                   config: ZBAR_CFG_ENABLE
                       to: 0];
    [self presentViewController:reader animated:YES completion:nil];
}

#pragma mark Perform action on QR code data


-(void)getVcardInfo:(NSString *)infoString
{
    if (ABAddressBookGetAuthorizationStatus() == kABAuthorizationStatusDenied ||
        ABAddressBookGetAuthorizationStatus() == kABAuthorizationStatusRestricted)
    {
        UIAlertView *cantAddContactAlert = [[UIAlertView alloc] initWithTitle: @"Cannot Add Contact" message: @"You must give the app permission to add the contact first." delegate:nil cancelButtonTitle: @"OK" otherButtonTitles: nil];
        [cantAddContactAlert show];
    }
    else if (ABAddressBookGetAuthorizationStatus() == kABAuthorizationStatusAuthorized)
    {
        [self addToContact:infoString];
    }
    else
    {
        ABAddressBookRequestAccessWithCompletion(ABAddressBookCreateWithOptions(NULL, nil), ^(bool granted, CFErrorRef error) {
            dispatch_async(dispatch_get_main_queue(), ^{
                if (!granted)
                {
                    UIAlertView *cantAddContactAlert = [[UIAlertView alloc] initWithTitle: @"Cannot Add Contact" message: @"You must give the app permission to add the contact first." delegate:nil cancelButtonTitle: @"OK" otherButtonTitles: nil];
                    [cantAddContactAlert show];
                    return;
                }
                [self addToContact:infoString];
            });
        });
    }
}
-(void)addToContact:(NSString *)strCon
{
    NSString *vCardString = strCon;
    CFDataRef vCardData = (__bridge_retained CFDataRef)[vCardString dataUsingEncoding:NSUTF8StringEncoding];
    CFErrorRef err;
    ABAddressBookRef book = ABAddressBookCreateWithOptions(NULL, &err);
    if (ABAddressBookGetAuthorizationStatus() == kABAuthorizationStatusAuthorized)
    {
        ABAddressBookRequestAccessWithCompletion(book, ^(bool granted, CFErrorRef error)
                                                 {
                                                     ABRecordRef defaultSource = ABAddressBookCopyDefaultSource(book);
                                                     CFArrayRef vCardPeople = ABPersonCreatePeopleInSourceWithVCardRepresentation(defaultSource, vCardData);
                                                     for (CFIndex index = 0; index < CFArrayGetCount(vCardPeople); index++) {
                                                         ABRecordRef person = CFArrayGetValueAtIndex(vCardPeople, index);
                                                         ABAddressBookAddRecord(book, person, NULL);
                                                     }
                                                     CFRelease(vCardPeople);
                                                     CFRelease(defaultSource);
                                                     ABAddressBookSave(book, NULL);
                                                     
                                                     dispatch_async(dispatch_get_main_queue(), ^{
                                                         UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil message:@"Contact added successfully." delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil,nil];
                                                         [alert show];
                                                     });
                                                 });
    }
}
- (void)sendSMS: (NSString *)SMS_NUM :(NSString *)SMS_MSG {
    
    MFMessageComposeViewController *controller = [[MFMessageComposeViewController alloc] init];
    if([MFMessageComposeViewController canSendText])
    {
        controller.body = SMS_MSG;
        controller.recipients = [NSArray arrayWithObjects:SMS_NUM, nil];
        controller.messageComposeDelegate = self;
        [self presentViewController:controller animated:NO completion:nil];
    }
    else
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Can not send SMS, Text Messaging not available" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil,nil];
        [alert show];
    }
}

#pragma mark  QR Image picker delegate

- (void) imagePickerController: (UIImagePickerController*) reader
 didFinishPickingMediaWithInfo: (NSDictionary*) info
{
    
    id<NSFastEnumeration> results =
    [info objectForKey: ZBarReaderControllerResults];
    ZBarSymbol *symbol = nil;
    finalData=@"";
    for(symbol in results)
    {
        finalData=symbol.data;
    }
    NSArray *tempSMSArray=[finalData componentsSeparatedByString:@":"];
    
    [reader dismissViewControllerAnimated:YES completion:nil];
    
    if ([[finalData lowercaseString] hasPrefix:APP_HTPP_KEYWORD])
    {
        for(symbol in results)
        {
            break;
        }
        linkToURL = symbol.data;
        linkToURL=[linkToURL stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        BOOL result = [[linkToURL lowercaseString] hasPrefix:APP_HTPP_KEYWORD];
        NSURL *urlAddress = nil;
        if (result) {
            urlAddress = [NSURL URLWithString: linkToURL];
        }
        else {
            NSString *good = [NSString stringWithFormat:@"http://%@", linkToURL];
            urlAddress = [NSURL URLWithString: good];
        }
        WebViewController *dvController=[[WebViewController alloc]initWithNibName:@"WebViewController" bundle:nil];
        dvController.url=urlAddress;
        dvController.strType=@"QR SCANNER";
        [self.navigationController pushViewController:dvController animated:NO];
    }
    else if ([[finalData lowercaseString] hasPrefix:@"tel"])
    {
        NSString *telNo = [finalData substringFromIndex:4];
        [[AppDelegate appDelegateInstance]callViaTelephone:telNo];
    }
    else if ([finalData rangeOfString:@"BEGIN:VCARD" options:(NSCaseInsensitiveSearch|NSDiacriticInsensitiveSearch)].location != NSNotFound)
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil message:@"Add this contact to address book?" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:@"Cancel",nil];
        alert.tag=100;
        alert.delegate=self;
        [alert show];
    }
    else if ([[[tempSMSArray objectAtIndex:0] uppercaseString] isEqualToString:@"SMSTO"])
    {
        NSString *SMS_NUM = [tempSMSArray objectAtIndex:1];
        NSString *SMS_MSG = [tempSMSArray objectAtIndex:2];
        [self sendSMS:SMS_NUM:SMS_MSG];
    }
    else if ([[finalData uppercaseString] hasPrefix:@"MATMSG"])
    {
        if([MFMailComposeViewController canSendMail])
        {
            NSArray *mailValues=[finalData componentsSeparatedByString:@";"];
            MFMailComposeViewController *composeViewController = [[MFMailComposeViewController alloc] initWithNibName:nil bundle:nil];
            [composeViewController setMailComposeDelegate:self];
            NSArray *recipients=[NSArray arrayWithArray:[[[[mailValues objectAtIndex:0] componentsSeparatedByString:@":"]objectAtIndex:2 ]componentsSeparatedByString:@"," ]];
            [composeViewController setToRecipients:recipients];
            [composeViewController setSubject:[[[mailValues objectAtIndex:1]componentsSeparatedByString:@":"]objectAtIndex:1]];
            [composeViewController setMessageBody:[[[mailValues objectAtIndex:2]componentsSeparatedByString:@":"]objectAtIndex:1] isHTML:NO];
            [self presentViewController:composeViewController animated:NO completion:nil];
        }
        else
        {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error" message:@"No mail account found.Please configure mail account from iPhone setting app" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil,nil];
            [alert show];
        }
    }
    else
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil message:finalData delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alert show];
        
    }
}
#pragma mark Message composer delegate
- (void) mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error
{
    [self dismissViewControllerAnimated:YES completion:NULL];
}
- (void)messageComposeViewController:(MFMessageComposeViewController *)controller didFinishWithResult:(MessageComposeResult)result
{
    switch (result)
    {
        case MessageComposeResultCancelled:
            break;
        case MessageComposeResultFailed: {
            
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"MyApp" message:@"Unknown Error"
                                                           delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
            [alert show];
        }
            break;
        case MessageComposeResultSent:
            
            break;
        default:
            break;
    }
    [self dismissViewControllerAnimated:YES completion:nil];
}
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (alertView.tag==100 &&buttonIndex==0)
    {
        [self getVcardInfo:finalData];
    }
}

@end
