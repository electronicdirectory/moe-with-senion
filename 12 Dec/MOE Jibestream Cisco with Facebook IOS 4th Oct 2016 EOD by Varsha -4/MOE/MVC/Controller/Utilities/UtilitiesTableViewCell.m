//
//  UtilitiesTableViewCell.m
//  MOE
//
//  Created by Neosoft on 6/27/15.
//  Copyright (c) 2015 Neosoft. All rights reserved.
//

#import "UtilitiesTableViewCell.h"

@implementation UtilitiesTableViewCell

- (void)awakeFromNib {
    // Initialization code
    
    if(IS_IPHONE_4_OR_LESS) {
        self.topViewHeightConstraint.constant = self.topViewHeightConstraint.constant - 10.0;
        self.middleViewHeightConstraint.constant = self.middleViewHeightConstraint.constant - 10.0;
    }
    
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
