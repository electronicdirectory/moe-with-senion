//
//  shoesizeViewController.h
//  MOE
//
//  Created by Neosoft on 6/11/15.
//  Copyright (c) 2015 Neosoft. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface shoeFashionPersonSelectorViewController : UIViewController

@property(nonatomic,retain)NSString *SHOEorFASHION;

@property(nonatomic,retain) IBOutlet UIButton *btnMen;
@property(nonatomic,retain) IBOutlet UIButton *btnWomen;
@property(nonatomic,retain) IBOutlet UIButton *btnKids;

- (IBAction)btnMenMethod:(id)sender;
- (IBAction)btnWomenMethod:(id)sender;
- (IBAction)btnKidsMethod:(id)sender;

@end
