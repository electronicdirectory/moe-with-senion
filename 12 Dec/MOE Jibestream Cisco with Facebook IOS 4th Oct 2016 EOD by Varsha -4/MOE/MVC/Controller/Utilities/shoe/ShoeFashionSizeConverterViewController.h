//
//  ShowSizeConverterViewController.h
//  MOE
//
//  Created by Neosoft on 6/11/15.
//  Copyright (c) 2015 Neosoft. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ShoeFashionSizeConverterViewController : UIViewController <UIPickerViewDataSource, UIPickerViewDelegate>

@property(nonatomic,retain)NSString *SHOEorFASHION;
@property(nonatomic,retain)NSString *personType;
@property(nonatomic,retain)NSString *selectedCountryType;
@property(nonatomic,retain)NSString *fashionType;
@property(nonatomic,retain)NSMutableArray *selectedArray;
@property(nonatomic,retain)NSMutableDictionary *currentDict;
@property(nonatomic)NSInteger selectedIndex;
@property(nonatomic) UIImage *imgHeader;

@property (weak, nonatomic) IBOutlet UIButton *btnHeader;
@property (weak, nonatomic) IBOutlet UILabel *lblTitle;

@property (weak, nonatomic) IBOutlet UIPickerView *pickerRegion;

@property (weak, nonatomic) IBOutlet UIView *viewBottom;

@property (weak, nonatomic) IBOutlet UILabel *lblRegion_UK;
@property (weak, nonatomic) IBOutlet UILabel *lblRegion_Europe;
@property (weak, nonatomic) IBOutlet UILabel *lblRegion_US;
@property (weak, nonatomic) IBOutlet UILabel *lblRegion_Japan;
@property (weak, nonatomic) IBOutlet UILabel *lblSize_UK;
@property (weak, nonatomic) IBOutlet UILabel *lblSize_Europe;
@property (weak, nonatomic) IBOutlet UILabel *lblSize_US;
@property (weak, nonatomic) IBOutlet UILabel *lblSize_Japan;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *pickerViewHeightConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *bottomViewHeightConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *middleSpaceHeightConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *bottomSpaceHeightConstraint;

@end
