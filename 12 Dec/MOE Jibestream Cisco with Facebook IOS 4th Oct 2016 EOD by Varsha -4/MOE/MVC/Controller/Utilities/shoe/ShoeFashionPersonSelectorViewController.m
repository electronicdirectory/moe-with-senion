//
//  shoesizeViewController.m
//  MOE
//
//  Created by Neosoft on 6/11/15.
//  Copyright (c) 2015 Neosoft. All rights reserved.
//

#import "shoeFashionPersonSelectorViewController.h"
#import "ShoeFashionSizeConverterViewController.h"
#import "FashionWearViewController.h"



@interface shoeFashionPersonSelectorViewController ()
{

    CGRect btnMenFrame;
    CGRect btnWomenFrame;
    CGRect btnKidsFrame;

}
@end

@implementation shoeFashionPersonSelectorViewController {
    
    ShoeFashionSizeConverterViewController *shoeConverterVC;
    NSMutableDictionary *dict;
    NSString *type;
    UIImage *selectedBtnImg;
}

#pragma mark - view lifecycle
- (void)viewDidLoad {
    [super viewDidLoad];


    [GoogleAnalyticsHandler sendScreenName:screenName_UtilitiesListing];



    self.btnMen.titleLabel.font = [CommanMethods getHeaderFont];
    self.btnWomen.titleLabel.font = [CommanMethods getHeaderFont];
    self.btnKids.titleLabel.font = [CommanMethods getHeaderFont];
    
   
    
    if([self.SHOEorFASHION isEqualToString:@"shoe"]) {
        self.navigationItem.titleView = [CommanMethods getLabel:screenHeader_Utilities_ShoeSize];
        self.view.backgroundColor = [UIColor colorWithPatternImage:SHOE_BG_IMAGE];
        [self.btnMen setImage:SHOE_BTN_IMAGE_MEN forState:UIControlStateNormal];
        [self.btnWomen setImage:SHOE_BTN_IMAGE_WOMEN forState:UIControlStateNormal];
        [self.btnKids setImage:SHOE_BTN_IMAGE_KIDS forState:UIControlStateNormal];
         [CommanMethods getShoeSizeConvertorBgImage:self.view];
    }
    else {
        self.navigationItem.titleView = [CommanMethods getLabel:screenHeader_Utilities_Fashion];
        self.view.backgroundColor = [UIColor colorWithPatternImage:FASHION_BG_IMAGE];
        [self.btnMen setImage:FASHION_BTN_IMAGE_MEN forState:UIControlStateNormal];
        [self.btnWomen setImage:FASHION_BTN_IMAGE_WOMEN forState:UIControlStateNormal];
        [self.btnKids setImage:FASHION_BTN_IMAGE_KIDS forState:UIControlStateNormal];
         [CommanMethods getFashionBgImage:self.view];
    }
    [UIView animateWithDuration:0
                     animations:^{
                         self.btnKids.transform = CGAffineTransformMakeScale(0.5, 0.5);
                          self.btnMen.transform = CGAffineTransformMakeScale(0.5, 0.5);
                          self.btnWomen.transform = CGAffineTransformMakeScale(0.5, 0.5);
                     }
                     completion:^(BOOL finished) {
                         [UIView animateWithDuration:0.6
                                          animations:^{
                                              self.btnKids.transform  =CGAffineTransformIdentity ;
                                              self.btnMen.transform  = CGAffineTransformIdentity;
                                              self.btnWomen.transform  = CGAffineTransformIdentity;
                                              
                                          }];
                     }];
    
    
}

#pragma mark - Button actions

- (IBAction)btnMenMethod:(id)sender {
    type=@"Men";
    [self presentCalculator];
}

- (IBAction)btnWomenMethod:(id)sender {
    type=@"Women";
    [self presentCalculator];
}

- (IBAction)btnKidsMethod:(id)sender {
    type=@"Kids";
    [self presentCalculator];
}

#pragma mark - Present calculator
- (void)presentCalculator {
    
    if([self.SHOEorFASHION isEqualToString:@"shoe"]) {
        shoeConverterVC = [self.storyboard instantiateViewControllerWithIdentifier:@"ShoeFashionSizeConverterViewController"];
        shoeConverterVC.personType = type;
        shoeConverterVC.SHOEorFASHION = self.SHOEorFASHION;
        dict=[CommanMethods getShoeSizes];
        shoeConverterVC.selectedArray=[dict objectForKey:type];
        [self.navigationController pushViewController:shoeConverterVC animated:YES];
    }
    else {
        FashionWearViewController *fashion = [self.storyboard instantiateViewControllerWithIdentifier:@"FashionWearViewController"];
        fashion.type=type;
        fashion.title=[type uppercaseString];
        dict=[CommanMethods getFashionSizes];
        [self.navigationController pushViewController:fashion animated:YES];
    }
}

#pragma mark - memory warning
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
