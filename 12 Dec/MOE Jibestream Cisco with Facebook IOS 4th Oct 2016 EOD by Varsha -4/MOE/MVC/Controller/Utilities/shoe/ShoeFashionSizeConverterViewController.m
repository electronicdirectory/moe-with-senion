//
//  ShowSizeConverterViewController.m
//  MOE
//
//  Created by Neosoft on 6/11/15.
//  Copyright (c) 2015 Neosoft. All rights reserved.
//

#import "ShoeFashionSizeConverterViewController.h"

#define TABLE_BG [UIColor colorWithPatternImage:[UIImage imageNamed:@"table-converter"]]

@interface ShoeFashionSizeConverterViewController ()

@end

@implementation ShoeFashionSizeConverterViewController

#pragma mark - synthesize
@synthesize pickerRegion;
@synthesize selectedCountryType, personType, selectedIndex;
@synthesize selectedArray, currentDict;
@synthesize lblRegion_Europe, lblRegion_Japan, lblRegion_UK, lblRegion_US;
@synthesize lblSize_Europe, lblSize_Japan, lblSize_UK, lblSize_US;

#pragma mark - view life cycle
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.


    pickerRegion.dataSource = self;
    pickerRegion.delegate = self;
    selectedIndex=0;
    selectedCountryType=@"UK";
    self.btnHeader.titleLabel.font = [CommanMethods getHeaderFont];
    self.lblTitle.font = [CommanMethods getHeaderFont];
    self.lblRegion_UK.font = [CommanMethods getHeaderFont];
    self.lblRegion_Europe.font = [CommanMethods getHeaderFont];
    self.lblRegion_US.font = [CommanMethods getHeaderFont];
    self.lblRegion_Japan.font = [CommanMethods getHeaderFont];
    self.lblSize_UK.font = [CommanMethods getHeaderFont];
    self.lblSize_Europe.font = [CommanMethods getHeaderFont];
    self.lblSize_US.font = [CommanMethods getHeaderFont];
    self.lblSize_Japan.font = [CommanMethods getHeaderFont];
    self.lblSize_UK.text = @"-";
    self.lblSize_Europe.text = @"-";
    self.lblSize_US.text = @"-";
    self.lblSize_Japan.text = @"-";
    if([self.SHOEorFASHION isEqualToString:@"shoe"])
    {
        [CommanMethods getShoeSizeConvertorBgImage:self.view];
        currentDict = [selectedArray objectAtIndex:selectedIndex];
        self.navigationItem.titleView = [CommanMethods getLabel:[self.personType uppercaseString]];
        self.view.backgroundColor = [UIColor colorWithPatternImage:SHOE_BG_IMAGE];
        if([self.personType isEqualToString:@"Men"])
        {
            [GoogleAnalyticsHandler sendScreenName:screenName_MenShoeSizeConverter];
            [self.btnHeader setImage:SHOE_BTN_IMAGE_MEN forState:UIControlStateNormal];
        }
        else if([self.personType isEqualToString:@"Women"])
        {
            [GoogleAnalyticsHandler sendScreenName:screenName_KidsShoeSizeConverter];
            [self.btnHeader setImage:SHOE_BTN_IMAGE_WOMEN forState:UIControlStateNormal];
        }
       else if([self.personType isEqualToString:@"Kids"])
        {
            [GoogleAnalyticsHandler sendScreenName:screenName_WomenShoeSizeConverter];
            [self.btnHeader setImage:SHOE_BTN_IMAGE_KIDS forState:UIControlStateNormal];
         }
    }
    else {
        [CommanMethods getFashionBgImage:self.view];
        currentDict=[selectedArray objectAtIndex:selectedIndex];
        self.navigationItem.titleView = [CommanMethods getLabel:[self.fashionType uppercaseString]];
        self.view.backgroundColor = [UIColor colorWithPatternImage:FASHION_BG_IMAGE];
        
        if([self.personType isEqualToString:@"Men"])
        {
            [GoogleAnalyticsHandler sendScreenName:screenName_MenFashionConverter];
            [self.btnHeader setImage:FASHION_BTN_IMAGE_MEN forState:UIControlStateNormal];
        }
        else if([self.personType isEqualToString:@"Women"])
        {
            [GoogleAnalyticsHandler sendScreenName:screenName_KidsFashionConverter];
            [self.btnHeader setImage:FASHION_BTN_IMAGE_WOMEN forState:UIControlStateNormal];
        }
        else if([self.personType isEqualToString:@"Kids"])
        {
            [GoogleAnalyticsHandler sendScreenName:screenName_WomenFashionConverter];
            [self.btnHeader setImage:FASHION_BTN_IMAGE_KIDS forState:UIControlStateNormal];
        }
    }
    
    self.viewBottom.backgroundColor = TABLE_BG;

    if(IS_IPHONE_4_OR_LESS) {
        self.pickerViewHeightConstraint.constant = self.pickerViewHeightConstraint.constant - 60.0;
        self.bottomViewHeightConstraint.constant = self.bottomViewHeightConstraint.constant - 60.0;
        self.middleSpaceHeightConstraint.constant = self.middleSpaceHeightConstraint.constant - 15.0;
        self.bottomSpaceHeightConstraint.constant = self.bottomSpaceHeightConstraint.constant - 15.0;
    }
    if(IS_IPHONE_5) {
        self.pickerViewHeightConstraint.constant = self.pickerViewHeightConstraint.constant - 40.0;
        self.bottomViewHeightConstraint.constant = self.bottomViewHeightConstraint.constant - 30.0;
        self.middleSpaceHeightConstraint.constant = self.middleSpaceHeightConstraint.constant - 10.0;
        self.bottomSpaceHeightConstraint.constant = self.bottomSpaceHeightConstraint.constant - 10.0;
    }
}

- (void)viewDidAppear:(BOOL)animated {
    
    [super viewDidAppear:YES];
    [self updateSizeLabels];
}

#pragma mark - update size
- (void)updateSizeLabels {
    lblSize_UK.text=[currentDict objectForKey:@"UK"];
    lblSize_Europe.text=[currentDict objectForKey:@"Europe"];
    lblSize_US.text=[currentDict objectForKey:@"US"];
    lblSize_Japan.text=[currentDict objectForKey:@"Japan"];
}

#pragma mark - Picker view delegates

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView {
    return 2;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component {
    NSUInteger numRows = 4;
    switch (component) {
        case 0:
        {
            numRows = 4;
        }
            break;
        case 1:
        {
            numRows = selectedArray.count;
        }
            break;
        default:
            break;
    }
    return numRows;
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow: (NSInteger)row inComponent:(NSInteger)component {
    switch (component) {
        case 0:
        {
            switch (row) {
                case 0:
                {
                    selectedCountryType=@"UK";
                }
                    break;
                case 1:
                {
                    selectedCountryType=@"Europe";
                }
                    break;
                    
                case 2:
                {
                    selectedCountryType=@"US";
                }
                    break;
                    
                case 3:
                {
                    selectedCountryType=@"Japan";
                }
                    break;
                default:
                    break;
            }
            [self.pickerRegion reloadComponent:1];
        }
            break;
        case 1:
        {
            selectedIndex=row;
            currentDict=[selectedArray objectAtIndex:selectedIndex];
            [self updateSizeLabels];
        }
            break;
        default:
            break;
    }
}

- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component {
    NSString *title;
    title = [@"" stringByAppendingFormat:@"%ld",(long)row];
    switch (component) {
        case 0:
        {
            switch (row) {
                case 0:
                {
                    title=@"UK";
                }
                    break;
                case 1:
                {
                    title=@"EUROPE";
                }
                    break;
                case 2:
                {
                    title=@"US";
                }
                    break;
                case 3:
                {
                    title=@"JAPAN";
                }
                    break;
                default:
                    break;
            }
        }
            break;
        case 1:
        {
            NSDictionary *tempDict=[selectedArray objectAtIndex:row];
            title=[tempDict objectForKey:selectedCountryType];
        }
            break;
        default:
            break;
    }
    return title;
}
- (UIView *)pickerView:(UIPickerView *)pickerView viewForRow:(NSInteger)row forComponent:(NSInteger)component reusingView:(UIView *)view
{
    UILabel *lblTitle;
    if(!view)
    {
        view=[[UIView alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 44)];
        lblTitle=[[UILabel alloc]initWithFrame:view.frame];
        lblTitle.backgroundColor=[UIColor clearColor];
        lblTitle.tag=111;
        [view addSubview:lblTitle];
    }
    
    lblTitle=(UILabel *)[view viewWithTag:111];
    lblTitle.textAlignment=NSTextAlignmentCenter;
    lblTitle.font=[CommanMethods getHeaderFont];
    view.backgroundColor=[UIColor clearColor];
    NSString *title;
    title = [@"" stringByAppendingFormat:@"%ld",(long)row];
    switch (component) {
        case 0:
        {
            switch (row) {
                case 0:
                {
                    title=@"UK";
                }
                    break;
                case 1:
                {
                    title=@"EUROPE";
                }
                    break;
                case 2:
                {
                    title=@"US";
                }
                    break;
                case 3:
                {
                    title=@"JAPAN";
                }
                    break;
                default:
                    break;
            }
        }
            break;
        case 1:
        {
            NSDictionary *tempDict=[selectedArray objectAtIndex:row];
            title=[tempDict objectForKey:selectedCountryType];
        }
            break;
        default:
            break;
    }
    lblTitle.text=title;
    return  view;
}
#pragma mark - memory warning
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
@end




















