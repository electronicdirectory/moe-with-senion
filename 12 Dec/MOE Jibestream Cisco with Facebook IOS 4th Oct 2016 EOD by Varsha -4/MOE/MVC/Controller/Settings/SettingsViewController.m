//
//  SettingsViewController.m
//  MOE
//
//  Created by Neosoft on 6/16/15.
//  Copyright (c) 2015 Neosoft. All rights reserved.
//

#import "SettingsViewController.h"
#import "ManageNotificationsViewController.h"
#import "MyFavViewController.h"
#import "settingsTableViewCell.h"

@interface SettingsViewController ()

@end

@implementation SettingsViewController {
    
    NSArray *arrData;
}
#define SettingCell @"settingsTableViewCell"

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [GoogleAnalyticsHandler sendScreenName:screenName_SettingsList];

    // Do any additional setup after loading the view.

    
    [CommanMethods getBgImage:self.view];
    self.navigationItem.titleView = [CommanMethods getLabel:screenHeader_Settings];

    self.navigationItem.rightBarButtonItem=[CommanMethods getRightButton:self Action:@selector(search)];
    self.navigationItem.leftBarButtonItem =[CommanMethods getLeftButton:self Action:@selector(menuMethod)];

    arrData = [NSArray arrayWithObjects:screenHeader_NotificationManagement, screenHeader_Favorites, nil];
    
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.tableView.scrollEnabled = NO;
    [self.tableView registerNib:[UINib nibWithNibName:SettingCell bundle:nil] forCellReuseIdentifier:SettingCell];

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark- Selector Method
/******************************************
 Method name - menuMethod
 Parameter   - nil
 Desc        - Method for opening Left Menu
 ****************************************/
- (void)menuMethod
{
    [self.sideMenuViewController presentLeftMenuViewController];
}
- (void)search
{
    [[AppDelegate appDelegateInstance]openSearchScreen:self.navigationController];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [arrData count];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 64;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    settingsTableViewCell   *cell = [self.tableView dequeueReusableCellWithIdentifier:SettingCell];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.lblTitle.text = [arrData objectAtIndex:indexPath.row];
    cell.lblTitle.backgroundColor = [UIColor clearColor];
    cell.lblTitle.textColor = [CommanMethods  getTextColor];
    cell.lblTitle.font = [CommanMethods getHeaderFont];
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if(indexPath.row == 0) {
     
        ManageNotificationsViewController *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"ManageNotificationsViewController"];
        [self.navigationController pushViewController:vc animated:YES];
    }
    else if(indexPath.row ==1)
    {
        MyFavViewController *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"MyFavTableViewController"];
        [self.navigationController pushViewController:vc animated:YES];
    }
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section {
    return [[UIView alloc] init];
}

@end


