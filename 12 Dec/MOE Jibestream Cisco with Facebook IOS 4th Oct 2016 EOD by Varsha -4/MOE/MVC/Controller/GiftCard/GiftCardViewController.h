//
//  GiftCardViewController.h
//  MOE
//
//  Created by webwerks on 6/29/15.
//  Copyright (c) 2015 Neosoft. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface GiftCardViewController : UIViewController<UITextViewDelegate>
@property (weak, nonatomic) IBOutlet UIImageView *giftCardImage;
@property (weak, nonatomic) IBOutlet UITextView *giftCardDesc;
 - (IBAction)CheckBalance:(id)sender;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *cardViewHeight;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *cardViewBGWidth;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *balanceViewWidth;
@property (weak, nonatomic) IBOutlet UIButton *btnCheckBalance,*btnKnowMore;
- (IBAction)btnKnowMoreClicked:(id)sender;

@end
