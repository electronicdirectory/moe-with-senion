//
//  GiftCardViewController.m
//  MOE
//
//  Created by webwerks on 6/29/15.
//  Copyright (c) 2015 Neosoft. All rights reserved.
//

#import "GiftCardViewController.h"

@interface GiftCardViewController ()

@end

@implementation GiftCardViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [GoogleAnalyticsHandler sendScreenName:screenName_GiftCards];
    
    UITapGestureRecognizer* singleTapOnImage = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(showImageWebView)];
    [self.giftCardImage addGestureRecognizer:singleTapOnImage];
    self.giftCardImage.userInteractionEnabled = YES;
    self.giftCardDesc.scrollEnabled=NO;
    self.giftCardDesc.editable=NO;
    self.giftCardDesc.delegate = self;
    self.giftCardDesc.textColor = [UIColor  darkGrayColor];
    [self.giftCardDesc setValue:[CommanMethods rePlaceTags:GIFTCARDDESCRIPTION] forKey:@"contentToHTMLString" ];
    self.giftCardDesc.font=[CommanMethods getDetailsFont];
    self.navigationItem.rightBarButtonItem=[CommanMethods getRightButton:self Action:@selector(search)];
    self.navigationItem.leftBarButtonItem=[CommanMethods getLeftButton:self Action:@selector(menuMethod)];
    self.navigationItem.titleView=[CommanMethods getLabel:MALLGIFTCARDTITLE];
    
    self.btnCheckBalance.titleLabel.font = [CommanMethods getHeaderFont];
    self.btnKnowMore.titleLabel.font = [CommanMethods getHeaderFont];

    if (IS_IPHONE_4_OR_LESS)
    {
        self.cardViewHeight.constant-=110;
        self.cardViewBGWidth.constant-=25;
    }
    if (IS_IPHONE_5)
    {
        self.cardViewHeight.constant-=35;
        self.cardViewBGWidth.constant-=15;
    }
    else if(IS_IPHONE_6)
    {
        self.cardViewHeight.constant+=40;
        self.cardViewBGWidth.constant+=30;
        self.balanceViewWidth.constant+=30;
    }
    else if(IS_IPHONE_6P)
    {
        self.cardViewHeight.constant+=60;
        self.cardViewBGWidth.constant+=50;
        self.balanceViewWidth.constant+=60;
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)showImageWebView
{
    [self displayDataInWebView:[NSURL URLWithString:IMAGEACTIONURL]];
}

- (IBAction)CheckBalance:(id)sender {
    [self displayDataInWebView:[NSURL URLWithString:[CommanMethods getCheckBalanceURL]]];
}
- (IBAction)btnKnowMoreClicked:(id)sender
{
    [self displayDataInWebView:[NSURL URLWithString:GIFTCARD_KNOW_MORE]];
}
//displayDataInWebView
-(void)displayDataInWebView:(NSURL*)url
{
    WebViewController *webObj=[[WebViewController alloc]initWithNibName:@"WebViewController" bundle:nil];
    webObj.url=url;
    webObj.strType=MALLGIFTCARDTITLE;
    [self.navigationController pushViewController:webObj animated:YES];
}
-(BOOL)textView:(UITextView *)textView shouldInteractWithURL:(NSURL *)URL inRange:(NSRange)characterRange
{
    if ([URL.scheme hasPrefix:APP_HTPP_KEYWORD])
    {
        WebViewController *webObj=[[WebViewController alloc]initWithNibName:@"WebViewController" bundle:nil];
        webObj.url=URL;
        webObj.strType=MALLGIFTCARDTITLE;
        [self.navigationController pushViewController:webObj animated:YES];
        return  NO;
    }
    return YES;
    
}
- (void)menuMethod {
    [self.sideMenuViewController presentLeftMenuViewController];
}
- (void)search
{
    [[AppDelegate appDelegateInstance]openSearchScreen:self.navigationController];
}
@end
