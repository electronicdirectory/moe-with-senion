//
//  SearchViewController.m
//  MOE
//
//  Created by Nivrutti on 6/16/15.
//  Copyright (c) 2015 Neosoft. All rights reserved.
//

#import "SearchViewController.h"
#import "SearchViewCell.h"
#import "DBOpreation.h"
#import "offerDetailViewController.h"
#import "ShoppingDetailsTableViewController.h"
#import "ServiceDetailsViewController.h"

@interface SearchViewController ()
{
    BOOL isFetching;
    BOOL isPagination;
    
}

@property(nonatomic,strong)UIImage *imgEvent,*imgServices,*imgOffer,*imgHotels,*imgDine,*imgEnt,*imgshop,*imgATM;

@end

@implementation SearchViewController
#define pagination 50
@synthesize  imgEvent,imgServices,imgOffer,imgHotels,imgDine,imgEnt,imgshop,imgATM;

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [GoogleAnalyticsHandler sendScreenName:screenName_Search];

    arrResult=[[NSMutableArray alloc]init];
    [self.tblView registerNib:[UINib nibWithNibName:@"SearchViewCell" bundle:nil] forCellReuseIdentifier:@"SearchViewCell"];
    
     self.txtSearch.autocorrectionType = UITextAutocorrectionTypeNo;
    
    [self.txtSearch becomeFirstResponder];
    self.txtSearch.frame=CGRectMake(5, 5, self.view.frame.size.width-95,34);
    self.txtSearch.layer.borderColor = [UIColor clearColor].CGColor;

    if(IS_IPHONE_6P)
        self.txtSearch.frame=CGRectMake(5, 5, self.view.frame.size.width-105,34);
    
    self.txtSearch.placeholder=@"Search";
    self.txtSearch.delegate=self;
    UIColor *color = [CommanMethods getTextPlaceholderColor];
    UIFont *font = [CommanMethods getDetailsFont];
    
     [self.txtSearch setReturnKeyType:UIReturnKeyDone];
    self.txtSearch.autocorrectionType = UITextAutocorrectionTypeNo;

    self.txtSearch.font=font;
    self.txtSearch.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Search " attributes:@{NSForegroundColorAttributeName: color, NSFontAttributeName: font}];
    
    
    UIView *leftView=[[UIView alloc]initWithFrame:CGRectMake(0, 0, 30, 20)];
    leftView.backgroundColor=[UIColor clearColor];
    
    UIImageView *left = [[UIImageView alloc] initWithFrame:CGRectMake(5, 0, 25, 20)];
    [left setImage:[UIImage imageNamed:@"search-icon6"]];
    left.contentMode=UIViewContentModeScaleAspectFit;
    [leftView addSubview:left];
    self.txtSearch.leftView = leftView;
    self.txtSearch.leftViewMode = UITextFieldViewModeAlways;
    
    UITapGestureRecognizer   *tapGes=[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(Tap:)];
    UIImageView *rightView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 30, 20)];
    [rightView setImage:[UIImage imageNamed:@"search-icon-close6"]];
    rightView.contentMode=UIViewContentModeScaleAspectFit;
    rightView.userInteractionEnabled=YES;
    [rightView addGestureRecognizer:tapGes];
    self.lblPlaceHolder.font=[CommanMethods getNavigationTitleFont];
    
    self.txtSearch.rightView = rightView;
    self.txtSearch.rightViewMode = UITextFieldViewModeAlways;
    [self.txtSearch setBackground:[UIImage imageNamed:@"search-field-bg6"]];
    
    
    
    self.txtSearch.backgroundColor = [UIColor clearColor] ;
    self.txtSearch.layer.borderColor = [[CommanMethods getTextColor] colorWithAlphaComponent:0.5f].CGColor;
    self.txtSearch.layer.borderWidth = 1.0f;


    
    
    
    self.navigationItem.leftBarButtonItem=[[UIBarButtonItem alloc]initWithCustomView: self.txtSearch];
    
    UIBarButtonItem *btnCancel=[[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemCancel target:self action:@selector(Cancel)];
    self.navigationItem.rightBarButtonItem=btnCancel;
    imgEvent=[UIImage imageNamed:@"search-icon-event6p"];
    imgServices=[UIImage imageNamed:@"search-icon-services6p"];
    imgOffer=[UIImage imageNamed:@"search-icon-offer6p"];
    imgHotels=[UIImage imageNamed:@"search-icon-hotlel6p"];
    imgDine=[UIImage imageNamed:@"search-icon-dine6p"];
    imgEnt=[UIImage imageNamed:@"search-icon-entertaint6p"];
    imgshop=[UIImage imageNamed:@"search-icon-shopping6p"];
    imgATM=[UIImage imageNamed:@"icon-atm6p"];
}
-(void)Tap:(UITapGestureRecognizer *)tapObj
{
    isFetching=NO;
    isPagination=NO;
    [arrResult removeAllObjects];
    self.txtSearch.text=@"";
    [self.tblView reloadData];
}
-(void)Cancel
{
    [self.navigationController popViewControllerAnimated:YES];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - TableView Delegate Method
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if([arrResult count]==0)
    {
        self.lblPlaceHolder.text=APP_NODATA;
        self.lblPlaceHolder.font=[CommanMethods getHeaderFont];
        self.lblPlaceHolder.textColor=[CommanMethods getTextColor];
        self.lblPlaceHolder.hidden=NO;
        [self.view bringSubviewToFront:self.lblPlaceHolder];

    }
    else
    {
        self.lblPlaceHolder.hidden=YES;
    }
    return [arrResult count];
}
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *identifier = @"SearchViewCell";
    SearchViewCell *cell = (SearchViewCell *)[self.tblView dequeueReusableCellWithIdentifier:identifier];
    SearchModel *searchModel=[arrResult objectAtIndex:indexPath.row];
    cell.lbl.text =[CommanMethods rePlaceTagsWithPlainText:searchModel.search_Title];
    cell.lbl.font =[CommanMethods getDetailsFont];
    cell.contentMode=UIViewContentModeScaleAspectFit;
    UIImage *img;
    if ([[searchModel search_Type]isEqualToString:APP_DINE])
    {
        img=imgDine;
    }
    else  if ([[searchModel search_Type]isEqualToString:APP_ENTERTAINMENT])
    {
        img=imgEnt;
    }
    else if ([[searchModel search_Type]isEqualToString:APP_SHOP])
    {
        img=imgshop;
    }
    else if ([[searchModel search_Type]isEqualToString:APP_EVENTS])
    {
        img=imgEvent;
    }
    else if ([[searchModel search_Type]isEqualToString:APP_OFFER])
    {
        img=imgOffer;
    }
    else if ([[searchModel search_Type]isEqualToString:APP_SERVICES])
    {
        img=imgServices;
    }
    else if ([[searchModel search_Type]isEqualToString:APP_HOTEL])
    {
        img=imgHotels;
    }
    else if ([[searchModel search_Type]isEqualToString:APP_ATM])
    {
        img=imgATM;
    }
    
    cell.imageView.image=img;
    cell.deleteBtn.hidden=YES;
    cell.deletebuttonWidth.constant=5;
    cell.selectionStyle=UITableViewCellEditingStyleNone;
    
    
    if (indexPath.row==[arrResult count]-15 && isPagination && !isFetching)
    {
        isFetching=YES;
        isPagination=NO;
        dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
        dispatch_async(queue, ^{
            NSArray *array = [DBOpreation getSearchData:self.txtSearch.text index:[arrResult count] noOfRecord:pagination];
            if([array count]==pagination)
            {
                isPagination=YES;
            }
            
            if([array count]>0)
            {
                NSMutableArray *arrayData=[[NSMutableArray alloc]init];
                for (int i=0;i<[array count];i++)
                {
                    NSIndexPath *path=[NSIndexPath indexPathForRow:[arrResult count]inSection:0];
                    [arrayData addObject:path];
                    [arrResult addObject:[array objectAtIndex:i]];
                }
                dispatch_async(dispatch_get_main_queue(),^{
                    [tableView beginUpdates];
                    [tableView insertRowsAtIndexPaths:arrayData withRowAnimation:UITableViewRowAnimationBottom];
                    [tableView endUpdates];
                    isFetching=NO;
                });
            }
        });
    }
    return cell;
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 49;
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    SearchModel *searchModel=[arrResult objectAtIndex:indexPath.row];
    if ([[searchModel search_Type]isEqualToString:APP_DINE])
    {
        [self moveToShopDetails:[DBOpreation getDineById:[NSNumber numberWithInteger:[searchModel.search_ID integerValue]]]];
    }
    else  if ([[searchModel search_Type]isEqualToString:APP_ENTERTAINMENT])
    {
        [self moveToShopDetails:[DBOpreation getEntListById:[NSNumber numberWithInteger:[searchModel.search_ID integerValue]]]];
    }
    else if ([[searchModel search_Type]isEqualToString:APP_SHOP])
    {
        [self moveToShopDetails:[DBOpreation getShopById:[NSNumber numberWithInteger:[searchModel.search_ID integerValue]]]];
    }
    else if ([[searchModel search_Type]isEqualToString:APP_EVENTS])
    {
        [self moveToOffer:[DBOpreation getEventById:[NSNumber numberWithInteger:[searchModel.search_ID integerValue]]]];
    }
    else if ([[searchModel search_Type]isEqualToString:APP_OFFER])
    {
        [self moveToOffer:[DBOpreation getOfferData:searchModel.search_ID]];
    }
    else if ([[searchModel search_Type]isEqualToString:APP_HOTEL])
    {
        [self moveToOffer:[DBOpreation getHotelsById:[NSNumber numberWithInteger:[searchModel.search_ID integerValue]]]];
    }
    else if ([[searchModel search_Type]isEqualToString:APP_SERVICES])
    {
        ServiceDetailsViewController *obj= [self.storyboard instantiateViewControllerWithIdentifier:@"ServiceDetailsViewController"];
        obj.model=[DBOpreation getServicesById:[NSNumber numberWithInteger:[searchModel.search_ID integerValue]]];
        [self.navigationController pushViewController:obj animated:YES];
    }
    else if ([[searchModel search_Type]isEqualToString:APP_ATM])
    {
        ServiceDetailsViewController *obj= [self.storyboard instantiateViewControllerWithIdentifier:@"ServiceDetailsViewController"];
        obj.model=[DBOpreation getATMById:[NSNumber numberWithInteger:[searchModel.search_ID integerValue]]];
        obj.titleText=[searchModel.search_Title uppercaseString];
        [self.navigationController pushViewController:obj animated:YES];
    }
}

/******************************************
 Method name - willDisplayCell
 Parameter   - UITableView, NSIndexPath
 Return      - nil
 Desc        - Method Called before row getting displayed in table view (Used for animation)
 ****************************************/
- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    CATransition *transition = [CATransition animation];
    [transition setType:kCATransitionPush];
    [transition setSubtype:kCATransitionFromLeft];
    cell.layer.shadowColor = [[CommanMethods getblackColor]CGColor];
    cell.layer.shadowOffset = CGSizeMake(10, 10);
    cell.alpha = 0;
    CATransform3D rotation;
    rotation = CATransform3DMakeRotation( 0, 0.0, 0, 0);
    rotation.m34 = 1.0/ -600;
    cell.layer.transform = rotation;
    [cell.layer addAnimation:transition forKey:@"rotation1"];
    [UIView beginAnimations:@"rotation" context:NULL];
    [UIView setAnimationDuration:0.3];
    cell.layer.transform = CATransform3DIdentity;
    cell.alpha = 1;
    cell.layer.shadowOffset = CGSizeMake(0, 0);
    [UIView commitAnimations];
}

-(void)moveToShopDetails:(ShopBaseModel *)model
{
    ShoppingDetailsTableViewController   *shop=[self.storyboard instantiateViewControllerWithIdentifier:@"ShoppingDetailsTableViewController"];
    shop.shopBaseModelObj=model;
    [self.navigationController pushViewController:shop animated:YES];
}
-(void)moveToOffer:(BaseModel *)model
{
    offerDetailViewController *offerdetailview = [self.storyboard instantiateViewControllerWithIdentifier:@"offerDetailViewController"];
    offerdetailview.offerInfo=model;
    [self.navigationController pushViewController:offerdetailview animated:YES];
}
- (BOOL)textFieldShouldClear:(UITextField *)textField
{
    return  YES;
}
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [self.txtSearch resignFirstResponder];
    return  YES;
}
-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    NSString *searchText = [textField.text stringByReplacingCharactersInRange:range withString:string];
    if (searchText.length > 0)
    {
        if(!isFetching)
        {
            [arrResult removeAllObjects];
            isFetching=YES;
            isPagination=NO;
            NSArray *array = [DBOpreation getSearchData:searchText index:0 noOfRecord:pagination];
            if([array count]==pagination)
            {
                isPagination=YES;
            }
            isFetching=NO;
            [arrResult addObjectsFromArray:array];
        }
    }
    else{
        isFetching=NO;
        isPagination=NO;
        [arrResult removeAllObjects];
    }
    [self.tblView reloadData];
    return YES;
}
-(void)dealloc
{
    arrResult=nil;
    self.tblView .delegate=nil;
    self.tblView =nil;
    imgEvent=nil;
    imgServices=nil;
    imgOffer=nil;
    imgHotels=nil;
    imgDine=nil;
    imgEnt=nil;
    imgshop=nil;
    imgATM=nil;
    
}
@end
