//
//  SearchViewController.h
//  MOE
//
//  Created by Nivrutti on 6/16/15.
//  Copyright (c) 2015 Neosoft. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SearchViewController : UIViewController <UITableViewDelegate,UITableViewDataSource,UITextFieldDelegate>
{
    NSMutableArray *arrResult;

 }
@property(nonatomic,weak)IBOutlet UILabel *lblPlaceHolder;
@property(nonatomic,weak)IBOutlet UITableView *tblView;
@property(nonatomic,weak)IBOutlet UITextField *txtSearch;
@end