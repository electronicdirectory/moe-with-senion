//
//  SearchViewCell.h
//  MOE
//
//  Created by Atul on 6/17/15.
//  Copyright (c) 2015 Neosoft. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SearchViewCell : UITableViewCell
@property (nonatomic,strong)IBOutlet UIButton *deleteBtn;
@property (nonatomic,strong)IBOutlet NSLayoutConstraint *deletebuttonWidth;
@property (nonatomic,strong)IBOutlet UILabel *lbl;
@property (nonatomic,strong) IBOutlet UIImageView *imageView;
@end
