//
//  bookTaxiViewController.h
//  MOE
//
//  Created by webwerks on 6/30/15.
//  Copyright (c) 2015 Neosoft. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface bookTaxiViewController : UIViewController<UITableViewDataSource,UITableViewDelegate,UIScrollViewDelegate>
{
    UITableView *tblViewForFirstSection;
    UITableView *tblViewForSecondSection;
    NSMutableArray *arrData;
}
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property ( nonatomic) BOOL isROOT;
@end
