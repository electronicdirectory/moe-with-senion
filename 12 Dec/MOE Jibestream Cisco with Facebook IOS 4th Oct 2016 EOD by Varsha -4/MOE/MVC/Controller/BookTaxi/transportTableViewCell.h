//
//  transportTableViewCell.h
//  MOE
//
//  Created by webwerks on 6/30/15.
//  Copyright (c) 2015 Neosoft. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface transportTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *lblTrasportTitle;
@property (weak, nonatomic) IBOutlet UIImageView *callImage;

@end
