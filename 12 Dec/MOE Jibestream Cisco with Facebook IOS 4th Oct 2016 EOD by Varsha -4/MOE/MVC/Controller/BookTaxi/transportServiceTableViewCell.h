//
//  transportServiceTableViewCell.h
//  MOE
//
//  Created by webwerks on 6/30/15.
//  Copyright (c) 2015 Neosoft. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface transportServiceTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *transportImage;
@property (weak, nonatomic) IBOutlet UILabel *lblTransportType;
@property (weak, nonatomic) IBOutlet UIButton *metroTimingBtn;
@property (weak, nonatomic) IBOutlet UIButton *metroMapBtn;
@property (weak, nonatomic) IBOutlet UIButton *moreInfo;
@property (weak, nonatomic) IBOutlet UIImageView *separatorImage;

@end
