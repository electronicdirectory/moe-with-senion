//
//  MapViewController.h
//  MOE
//
//  Created by webwerks on 7/1/15.
//  Copyright (c) 2015 Neosoft. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MapViewController : UIViewController<UIScrollViewDelegate>
{
    UIScrollView *scrollView;
}
@end
