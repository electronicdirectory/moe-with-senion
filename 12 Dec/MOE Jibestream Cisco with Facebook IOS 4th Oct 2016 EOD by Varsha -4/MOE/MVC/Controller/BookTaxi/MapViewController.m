//
//  MapViewController.m
//  MOE
//
//  Created by webwerks on 7/1/15.
//  Copyright (c) 2015 Neosoft. All rights reserved.
//

#import "MapViewController.h"

@interface MapViewController ()
{
    UIImageView *imageView;
}
@end

@implementation MapViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationItem.titleView = [CommanMethods getLabel:screenHeader_MetroMAp];

    [GoogleAnalyticsHandler sendScreenName:screenName_MetroMap];
    UIImage *image=[UIImage imageNamed:@"Map2"];
  
    scrollView=[[UIScrollView alloc]initWithFrame:CGRectMake(0,0,self.view.frame.size.width, self.view.frame.size.height-64)];
    
    imageView=[[UIImageView alloc]initWithFrame:scrollView.frame];
    imageView.image=image;
    imageView.contentMode=UIViewContentModeScaleAspectFit;
    [scrollView addSubview:imageView];
    [self.view  addSubview:scrollView];
    scrollView.delegate=self;
    scrollView.minimumZoomScale=1;
    scrollView.maximumZoomScale=2.0;
    
   
}
- (UIView*)viewForZoomingInScrollView:(UIScrollView *)scrollView {
    // Return the view that we want to zoom
    return imageView;
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
