//
//  bookTaxiViewController.m
//  MOE
//
//  Created by webwerks on 6/30/15.
//  Copyright (c) 2015 Neosoft. All rights reserved.
//

#import "bookTaxiViewController.h"
#import "ServiceDetailsViewController.h"
#import "transportServiceTableViewCell.h"
#import "transportTableViewCell.h"
#import "MapViewController.h"
#import "Constant.h"
@interface bookTaxiViewController ()
{
    BOOL isShuttleBusAvailable;
    NSArray *transArray,*transportServiceArr;
    BOOL isSetshuttleBusIndex,isSetMetroBusIndex;
}
@end

@implementation bookTaxiViewController


#define TRANSPORTARRAY @[@"Dubai TAXI ",@"City Taxi (Sharjah)",@"Al Ghazal Taxi  (Abu Dhabi)"]
#define TRANSPORTSERVICEARRAY @[@"BY DUBAI METRO",@"BY BIG BUS",@"SHUTTLE BUS"]
#define IMAGEARRAY @[@"icon-metro6",@"icon-bus6",@"icon-shuttle-bus6"]
#define SHUTTLEBUSSERVICE @"Shuttle Bus Services"
#define TRANSPORTTABLEVIEWCELL @"transportTableViewCell"
#define TRANSPORTSERVICETABLEVIEWCELL @"transportServiceTableViewCell"

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [GoogleAnalyticsHandler sendScreenName:screenName_BookATaxi];

    
    self.navigationItem.rightBarButtonItem=[CommanMethods getRightButton:self Action:@selector(search)];
    self.navigationItem.titleView = [CommanMethods getLabel:screenHeader_BOOK_A_TAXI];
    if(self.isROOT) {
        self.navigationItem.rightBarButtonItem=[CommanMethods getRightButton:self Action:@selector(search)];
        self.navigationItem.leftBarButtonItem =[CommanMethods getLeftButton:self Action:@selector(menuMethod)];
    }

    
    isSetshuttleBusIndex=isSetMetroBusIndex=NO;
    transArray=TRANSPORTARRAY;
    transportServiceArr=TRANSPORTSERVICEARRAY;
    
    isShuttleBusAvailable=YES;
    isSetshuttleBusIndex=YES;
    
    tblViewForSecondSection=[[UITableView alloc]init];
    tblViewForFirstSection=[[UITableView alloc]init];
    [tblViewForSecondSection registerNib:[UINib nibWithNibName:TRANSPORTSERVICETABLEVIEWCELL bundle:nil] forCellReuseIdentifier:TRANSPORTSERVICETABLEVIEWCELL];
    [tblViewForFirstSection registerNib:[UINib nibWithNibName:TRANSPORTTABLEVIEWCELL bundle:nil] forCellReuseIdentifier:TRANSPORTTABLEVIEWCELL];
    [tblViewForSecondSection setScrollEnabled:NO];
    [tblViewForFirstSection setScrollEnabled:NO];
    
    tblViewForFirstSection.delegate=self;
    tblViewForFirstSection.dataSource=self;
    
    tblViewForSecondSection.delegate=self;
    tblViewForSecondSection.dataSource=self;
    
    [tblViewForFirstSection setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    [tblViewForSecondSection setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    
    [tblViewForSecondSection reloadData];
    [tblViewForFirstSection reloadData];
    tblViewForFirstSection.frame=CGRectMake(0, 0, self.view.frame.size.width, tblViewForFirstSection.contentSize.height);
    tblViewForSecondSection.frame=CGRectMake(0,CGRectGetMaxY(tblViewForFirstSection.frame), self.view.frame.size.width,tblViewForSecondSection.contentSize.height);
    UIImageView *tempImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"overlay-bg6"]];
    [tempImageView setFrame:tblViewForSecondSection.frame];
    
    tblViewForSecondSection.backgroundView = tempImageView;
    tblViewForFirstSection.backgroundColor=[UIColor clearColor];
    
    CGFloat width=tblViewForFirstSection.contentSize.width;
    CGFloat height=tblViewForFirstSection.contentSize.height+tblViewForSecondSection.contentSize.height;
    
    [self.scrollView setContentSize:CGSizeMake(width, height)];
    self.scrollView.clipsToBounds=YES;
    self.scrollView.userInteractionEnabled=YES;
    self.scrollView.scrollEnabled=YES;
    
    [self.scrollView addSubview:tblViewForSecondSection];
    [self.scrollView addSubview:tblViewForFirstSection];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Return the number of sections.
    return 1;
}
-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    if (tableView==tblViewForSecondSection) {
        return 75;
    }
    else
        return 0;
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    if (tableView==tblViewForSecondSection)
    {
        UIView *view=[[UIView alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 75)];
        UILabel *lblHeader=[[UILabel alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 74)];
        UIImageView *separatorView=[[UIImageView alloc]initWithFrame:CGRectMake(0, CGRectGetMaxY(view.frame)-1, self.view.frame.size.width, 1)];
        separatorView.image=[UIImage imageNamed:@"taxi-div.png"];
        separatorView.alpha=0.5f;
        lblHeader.backgroundColor=view.backgroundColor=[UIColor clearColor ];
        lblHeader.textAlignment=NSTextAlignmentCenter;
        lblHeader.text=@"OTHER SERVICES";
        lblHeader.font=[CommanMethods getHeaderFont];
        lblHeader.textColor=[CommanMethods getTextColor];
        [view addSubview:separatorView];
        [view addSubview:lblHeader];
        return  view;
    }
    return nil;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (tableView==tblViewForSecondSection)
    {
        return 204.0f;
    }
    else
        return 45.0f;
    
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (tableView==tblViewForSecondSection) {
        return 3;
     }
    else
    {
        return 3;
    }
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (tableView==tblViewForFirstSection)
    {
        transportTableViewCell *cell=[tableView dequeueReusableCellWithIdentifier:TRANSPORTTABLEVIEWCELL forIndexPath:indexPath];
        cell.selectionStyle=UITableViewCellSelectionStyleNone;
        cell.contentView.backgroundColor   =[UIColor clearColor];
        cell.lblTrasportTitle.font=[CommanMethods getHeaderFont];
        cell.lblTrasportTitle.text=[[transArray objectAtIndex:indexPath.row]uppercaseString];
        return cell;
    }
    else
    {
        transportServiceTableViewCell *cell=[tableView dequeueReusableCellWithIdentifier:TRANSPORTSERVICETABLEVIEWCELL  forIndexPath:indexPath];
        cell.selectionStyle=UITableViewCellSelectionStyleNone;
        cell.contentView.backgroundColor   =[UIColor clearColor];
        cell.lblTransportType.text=[transportServiceArr objectAtIndex:indexPath.row];
        cell.lblTransportType.font=[CommanMethods getHeaderFont];
        cell.moreInfo.tag=indexPath.row;
        [cell.metroMapBtn addTarget:self action:@selector(metroMapAction:) forControlEvents:UIControlEventTouchUpInside];
        [cell.metroTimingBtn addTarget:self action:@selector(metroTimingAction:) forControlEvents: UIControlEventTouchUpInside];
        cell.transportImage.image=[UIImage imageNamed:[IMAGEARRAY objectAtIndex:indexPath.row]];
        cell.transportImage.contentMode=UIViewContentModeScaleAspectFit;
        cell.moreInfo.hidden=YES;
        if (indexPath.row==1 || indexPath.row==2)
        {
            cell.metroMapBtn.hidden=YES;
            cell.metroTimingBtn.hidden=YES;
            cell.moreInfo.hidden=NO;
            [cell.moreInfo addTarget:self action:@selector(moreInfoAction:) forControlEvents:UIControlEventTouchUpInside];
        }
        if (isShuttleBusAvailable)
        {
            if (indexPath.row==2)
                cell.separatorImage.hidden=YES;
        }
        else
        {
            if (indexPath.row==1)
                cell.separatorImage.hidden=YES;
        }
        return cell;
    }
}
- (void)tableView:(UITableView *)tableView   willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    [cell setBackgroundColor:[UIColor clearColor]];
}

-(void)moreInfoAction: (id)sender
{
    UIButton *btn=(UIButton *)sender;
    WebViewController *webObj=[[WebViewController alloc]initWithNibName:@"WebViewController" bundle:nil];
    if (btn.tag==1)
    {
        webObj.url=[NSURL URLWithString:[CommanMethods getBigBusURL]];
        webObj.strType=screenHeader_BigBus;
    }
    else
    {
        NSURL *url=[NSURL URLWithString:[CommanMethods getShuttleBusURL]];
        webObj.url=url;
        webObj.hideBottom=YES;
        webObj.strType=[@"Shuttle Bus Services" uppercaseString];
    }
    [self.navigationController pushViewController:webObj animated:YES];
}
-(void)metroTimingAction : (id)sender
{
    NSURL *url=[NSURL URLWithString:[CommanMethods getMetroTimingURL]];
    WebViewController *webObj=[[WebViewController alloc]initWithNibName:@"WebViewController" bundle:nil];
    webObj.url=url;
    webObj.hideBottom=YES;
    webObj.strType=screenHeader_MetroTiming;
    [self.navigationController pushViewController:webObj animated:YES];
}
-(void)metroMapAction: (id)sender
{
    MapViewController *mapView = [self.storyboard instantiateViewControllerWithIdentifier:@"MapViewController"];
    [self.navigationController pushViewController:mapView animated:YES];
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (tableView==tblViewForFirstSection)
    {
        switch (indexPath.row)
        {
            case 0:
                [self showAlert:indexPath.row PhoneNo:DUBAITANSPORT];
                break;
            case 1:
                [self showAlert:indexPath.row PhoneNo:CITYTAXISHARJAH];
                break;
            case 2:
                [self showAlert:indexPath.row PhoneNo:ALGHAZALTAXI];
                break;
            default:
                break;
        }
    }
}
-(void)showAlert:(NSUInteger)tag PhoneNo:(NSString *)strNumber
{
    UIAlertView *alert=[[UIAlertView alloc]initWithTitle:nil message:strNumber delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"Call", nil];
    alert.tag=tag;
    [alert show];
}
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if(buttonIndex==1)
    {
        switch (alertView.tag)
        {
            case 0:
                [self call:DUBAITANSPORT];
                break;
            case 1:
                [self call:CITYTAXISHARJAH];
                break;
            case 2:
                [self call:ALGHAZALTAXI];
                break;
            default:
                break;
        }

    }
}
-(void)call:(NSString *)telephone
{
    [[AppDelegate appDelegateInstance]callViaTelephone:telephone];
}
- (void)menuMethod {
    [self.sideMenuViewController presentLeftMenuViewController];
}
- (void)search
{
    [[AppDelegate appDelegateInstance]openSearchScreen:self.navigationController];
}
@end
