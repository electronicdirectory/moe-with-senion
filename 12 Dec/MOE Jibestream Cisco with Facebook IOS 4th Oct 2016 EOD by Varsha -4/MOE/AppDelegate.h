//
//  AppDelegate.h
//  MOE
//
//  Created by Nivrutti on 6/11/15.
//  Copyright (c) 2015 Neosoft. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MBProgressHUD.h"
#import "CurrentCLController.h"
#import "RESideMenu.h"
#import <EMSP/EMSP.h>
#import "CustomAppXView.h"
@interface AppDelegate : UIResponder <UIApplicationDelegate,MBProgressHUDDelegate,CurrentCLControllerDelegate, RESideMenuDelegate,EMSPRegistrationDelegate,EMSPLocationDelegate,AppXOverlayDelegate,UIGestureRecognizerDelegate,RemoveCustomApexDelegate>
{
    MBProgressHUD *hud;
    CustomAppXView *customAppX;
    CustomAppXView *locationAppX;
    
    AppXOverlay *normalOverlay;
    AppXOverlay *locationOverlay;
}
@property(nonatomic,strong)NSString *strStoreLocatorURL;
@property(nonatomic,strong)NSString *strBigBusURL;
@property(nonatomic,strong)NSString *strMetroTiming;
@property(nonatomic,strong)NSString *strSocialWall;
@property(nonatomic,strong)NSString *strSiteID;
@property(nonatomic,strong)NSString *strFeedback;
@property(nonatomic,strong)NSString *strWhatsApp;



@property (strong, nonatomic) UIWindow *window;
@property (strong, nonatomic) FMDatabaseQueue *DBQueue;

@property (nonatomic, strong)  NSString *currentLocationId;
@property (nonatomic, assign) BOOL showLocationAds;

-(void)removeCustomApex;

-(void)bringHudToFront;
-(void)PlayProgressHudAsPerType:(NSString *)text View:(UIView*)inputView;
-(void)showHud:(UIView*)inputView Title:(NSString *)strTitle;
-(void)PlayProgressHudForStoreLocator:(NSString *)text View:(UIView*)inputView;

-(void)closeHud;
-(BOOL)CheckNetWorkConnection;
-(void)getCurrentLoation;
-(NSString *)dbPathAddress;
-(NSString *)dbPath:(NSString *)fileName;
+(AppDelegate *)appDelegateInstance;
-(void)setupRootView;
-(void)openSearchScreen:(UINavigationController *)navObj;
-(void)callViaTelephone:(NSString *)strTel;
-(void)setNavigationBarValues:(UINavigationController *)navigationController;


//CISCO
-(void)showFullScreenCiscoAd:(UIViewController *)viewController ScreenName:(NSString *)strScreenName;
-(NSString *)getBundelID;
@end

