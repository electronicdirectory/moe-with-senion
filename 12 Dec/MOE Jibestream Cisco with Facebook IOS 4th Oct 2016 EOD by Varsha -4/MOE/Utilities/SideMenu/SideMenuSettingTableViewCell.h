//
//  SideMenuSettingTableViewCell.h
//  MOE
//
//  Created by webwerks on 4/14/16.
//  Copyright © 2016 Neosoft. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SideMenuSettingTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIButton *btnChangeMallClicked;
@property (weak, nonatomic) IBOutlet UIButton *btnSettingClicked;
@property (weak, nonatomic) IBOutlet UIButton *btnWhatsappClicked;
@property (weak, nonatomic) IBOutlet UILabel *lblChangeMall;

@end
