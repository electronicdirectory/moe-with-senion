//
//  SideMenuSettingTableViewCell.m
//  MOE
//
//  Created by webwerks on 4/14/16.
//  Copyright © 2016 Neosoft. All rights reserved.
//

#import "SideMenuSettingTableViewCell.h"

@implementation SideMenuSettingTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
