

#import "DEMOLeftMenuViewController.h"
#import "WebViewController.h"

#import "UtilitiesViewController.h"
#import "TheMallViewController.h"
#import "SettingsViewController.h"
#import "HomeViewController.h"
#import "ShopBaseViewController.h"
#import "BaseTableViewController.h"
#import "ServicesViewController.h"
#import "Spin&WinViewController.h"
#import "GiftCardViewController.h"
#import "AddMyParkingViewController.h"
#import "bookTaxiViewController.h"
#import "HomeDataModel.h"
#import <AddressBook/AddressBook.h>
#import "SideMenuSettingTableViewCell.h"
#import "StoreLocatorViewController.h"
@interface DEMOLeftMenuViewController ()

@property (strong, readwrite, nonatomic) UITableView *tableView;

@end
#define  DIFF 80
static NSString * const contact_add_error = @"Cannot Add Contact";
static NSString * const contact_add_error_permission = @"You must give the app permission to add the contact first.";
static NSString * const contact_added = @"Contact added successfully.";
static NSString * const contact_added_to_addressBook = @"Add this contact to address book?";
@implementation DEMOLeftMenuViewController {
    UIStoryboard *storyBoard;
    
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    storyBoard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    
    self.tableView = ({
        
        CGRect rect1=[UIScreen mainScreen].bounds;
        float height=rect1.size.height-DIFF;
        float y=(DIFF/2)+20;
        CGRect rect=CGRectMake(15, y,rect1.size.width,height);
        
        
        UITableView *tableView=[[UITableView alloc] initWithFrame:rect style:UITableViewStylePlain];
        tableView.autoresizingMask = UIViewAutoresizingFlexibleTopMargin | UIViewAutoresizingFlexibleBottomMargin | UIViewAutoresizingFlexibleWidth;
        tableView.delegate = self;
        tableView.dataSource = self;
        tableView.opaque = NO;
        tableView.backgroundColor = [UIColor clearColor];
        tableView.backgroundView = nil;
        tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        tableView.bounces = NO;
        tableView.scrollEnabled = NO;
        tableView;
    });
    [self.view addSubview:self.tableView];
    [self.tableView registerNib:[UINib nibWithNibName:@"SideMenuSettingTableViewCell" bundle:nil] forCellReuseIdentifier:@"SideMenuSettingTableViewCell"];
}

#pragma mark -
#pragma mark UITableView Datasource
 
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    float height=(self.view.frame.size.height-DIFF)/([[HomeDataModel   sharedInstance].mallMenus count]+2);
    if (indexPath.row==[[HomeDataModel   sharedInstance].mallMenus count])
    {
        return (height*2);
    }
    return height;
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)sectionIndex
{
    return [[HomeDataModel   sharedInstance].mallMenus count]+1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row==[[HomeDataModel   sharedInstance].mallMenus count])
    {
        SideMenuSettingTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"SideMenuSettingTableViewCell"];
        cell.lblChangeMall.font = [CommanMethods getDetailsFont];
        cell.lblChangeMall.textColor = [CommanMethods getTextColor];
        cell.backgroundColor = [UIColor clearColor];
        cell.contentView.backgroundColor = [UIColor clearColor];
        [cell.btnWhatsappClicked addTarget:self action:@selector(whatsAppCliked) forControlEvents:UIControlEventTouchUpInside ];
        [cell.btnSettingClicked addTarget:self action:@selector(btnSettingMethod:) forControlEvents:UIControlEventTouchUpInside ];
        
        return  cell;
    }
    static NSString *cellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    if (cell == nil)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
        cell.backgroundColor = [UIColor clearColor];
        cell.textLabel.font = [CommanMethods getDetailsFont];
        cell.textLabel.textColor = [CommanMethods getTextColor];
        cell.textLabel.highlightedTextColor = [UIColor lightGrayColor];
        cell.selectedBackgroundView = [[UIView alloc] init];
    }
    cell.textLabel.text =[NSString stringWithFormat:@"%@", [HomeDataModel   sharedInstance].mallMenus[indexPath.row]];
    return cell;
}

#pragma mark - UITableView Delegate
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    NSUserDefaults *pref = [NSUserDefaults standardUserDefaults];
    [pref synchronize];
    
    if (indexPath.row==[[HomeDataModel   sharedInstance].mallMenus count])
    {
        return;
    }
    NSString *strTitle=[HomeDataModel   sharedInstance].mallMenus[indexPath.row];
    if([[strTitle lowercaseString]isEqualToString:[@"HOME" lowercaseString]])
    {
        if([[pref objectForKey:mall_Checker]isEqual:INSIDE_MALL])
            [self setUpContentView:@"HomeViewControllerInSide"];
        else
            [self setUpContentView:@"HomeViewControllerOutSide"];
    }
    else if([[strTitle lowercaseString]isEqualToString:[@"SHOP & DINE" lowercaseString]])
    {
        [self   openShopsScreens:APP_ALLSTORE];
    }
    else if([[strTitle lowercaseString]isEqualToString:[@"STORE LOCATOR" lowercaseString]])
    {
        [self openStoreLocater];
    }
    else if([[strTitle lowercaseString]isEqualToString:[@"HOTELS" lowercaseString]])
    {
        BaseTableViewController *baseObj=[storyBoard instantiateViewControllerWithIdentifier:@"BaseTableViewController"];
        baseObj.strType=@"hotels";
        [self pushToView:baseObj];
    }
    else if([[strTitle lowercaseString]isEqualToString:[@"OFFERS" lowercaseString]])
    {
        BaseTableViewController *baseObj=[storyBoard instantiateViewControllerWithIdentifier:@"BaseTableViewController"];
        baseObj.strType=@"offers";
        [self pushToView:baseObj];
    }
    else if([[strTitle lowercaseString]isEqualToString:[@"EVENTS" lowercaseString]])
    {
        BaseTableViewController *baseObj=[storyBoard instantiateViewControllerWithIdentifier:@"BaseTableViewController"];
        baseObj.strType=@"events";
        [self pushToView:baseObj];
    }
    else if([[strTitle lowercaseString]isEqualToString:[@"THE MALL" lowercaseString]])
    {
        [self setUpContentView:@"TheMallViewController"];
    }
    else if([[strTitle lowercaseString]isEqualToString:[@"GIFT CARDS" lowercaseString]])
    {
        [self setUpContentView:@"GiftCardViewController"];
    }
    else if([[strTitle lowercaseString]isEqualToString:[@"SOCIAL WALL" lowercaseString]])
    {
        [self openSocialWall];
    }
    else if([[strTitle lowercaseString]isEqualToString:[@"MY PARKING" lowercaseString]])
    {
        [self openMyParking];
    }
    else if([[strTitle lowercaseString]isEqualToString:[@"UTILITIES" lowercaseString]])
    {
        [self setUpContentView:@"UtilitiesViewController"];
    }
    else if([[strTitle lowercaseString]isEqualToString:[@"CALL A TAXI" lowercaseString]])
    {
        [self openBookATaxi];
    }
    else if([[strTitle lowercaseString]isEqualToString:[@"SERVICES" lowercaseString]])
    {
        [self setUpContentView:@"ServicesViewController"];
    }
    else if([[strTitle lowercaseString]isEqualToString:[@"CONTACT US" lowercaseString]])
    {
        [self setUpContentView:@"LeftSideContactUsViewController"];
        return;
    }
    else
    {
        [self openSpinAndWin];
    }
}

#pragma mark -

-(void)openShopsScreens:(NSString *)strType
{
    ShopBaseViewController *shopObj=[storyBoard instantiateViewControllerWithIdentifier:@"ShopBaseViewController"];
    shopObj.strType=strType;
    [self pushToView:shopObj];
}
-(void)setUpContentView:(NSString *)strID
{
    [self pushToView:[storyBoard instantiateViewControllerWithIdentifier:strID]];
}
- (void)setupWebViewController :(NSURL *)url Title:(NSString *)strTitle{
    
    WebViewController *webObj=[[WebViewController alloc]initWithNibName:@"WebViewController" bundle:nil];
    webObj.url=url;
    webObj.strType=strTitle;
    webObj.hideBottom=YES;
    [self pushToView:webObj];
}
- (void)openStoreLocater
{
    StoreLocatorViewController *storeObj=[[StoreLocatorViewController alloc]initWithNibName:@"StoreLocatorViewController" bundle:nil];
    [self pushToView:storeObj];
}
- (void)openSocialWall {
    
    NSURL *url=[NSURL URLWithString:[CommanMethods getSocialURL]];
    [self setupWebViewController:url Title:screenHeader_SocialWall];
}
- (void)openSpinAndWin
{
    Spin_WinViewController *vc = [storyBoard instantiateViewControllerWithIdentifier:@"Spin&WinViewController"];
    vc.dictCampaign = [HomeDataModel   sharedInstance].dictCampaign;
    [self pushToView:vc];
}
- (void)openMyParking {
    AddMyParkingViewController *vc = [storyBoard instantiateViewControllerWithIdentifier:@"AddMyParkingViewController"];
    vc.isROOT = YES;
    [self pushToView:vc];
}
- (void)openBookATaxi {
    bookTaxiViewController *vc = [storyBoard instantiateViewControllerWithIdentifier:@"bookTaxiViewController"];
    vc.isROOT = YES;
    [self pushToView:vc];
}
- (void)btnSettingMethod:(id)sender
{
    SettingsViewController *vc = [storyBoard instantiateViewControllerWithIdentifier:@"SettingsViewController"];
    [self pushToView:vc];
}

-(void)pushToView:(UIViewController *)controller
{
    UINavigationController   *navigationController=(UINavigationController *)self.sideMenuViewController.contentViewController;
    NSMutableArray *array=[[NSMutableArray alloc]initWithObjects:controller, nil];
    navigationController.viewControllers=array;
    [self.sideMenuViewController setContentViewController:navigationController animated:YES];
    [self.sideMenuViewController hideMenuViewController];
}
//=====
-(void)whatsAppCliked
{
    NSString *strPhone=[AppDelegate appDelegateInstance].strWhatsApp;
    if([strPhone length]>0)
    {
        [self addContactToAddressBook];
    }
    else
    {
        [self openWhatsApp];
    }
}
-(void)addContactToAddressBook
{
    if (ABAddressBookGetAuthorizationStatus() == kABAuthorizationStatusDenied ||
        ABAddressBookGetAuthorizationStatus() == kABAuthorizationStatusRestricted)
    {
        UIAlertView *cantAddContactAlert = [[UIAlertView alloc] initWithTitle: contact_add_error message: contact_add_error_permission delegate:nil cancelButtonTitle: @"Ok" otherButtonTitles: nil];
        [cantAddContactAlert show];
    }
    else if (ABAddressBookGetAuthorizationStatus() == kABAuthorizationStatusAuthorized)
    {
        [self addToContact];
    }
    else
    {
        ABAddressBookRequestAccessWithCompletion(ABAddressBookCreateWithOptions(NULL, nil), ^(bool granted, CFErrorRef error) {
            dispatch_async(dispatch_get_main_queue(), ^{
                if (!granted)
                {
                    UIAlertView *cantAddContactAlert = [[UIAlertView alloc] initWithTitle: contact_add_error message: contact_add_error_permission delegate:nil cancelButtonTitle: @"Ok" otherButtonTitles: nil];
                    [cantAddContactAlert show];
                    return;
                }
                [self addToContact];
            });
        });
    }
}
-(void)addToContact
{
    
    CFErrorRef err;
    ABAddressBookRef book = ABAddressBookCreateWithOptions(NULL, &err);
    if (ABAddressBookGetAuthorizationStatus() == kABAuthorizationStatusAuthorized)
    {
        ABAddressBookRequestAccessWithCompletion(book, ^(bool granted, CFErrorRef error) {
            
            NSString * strName=[NSString stringWithFormat:@"Mall Of Emirates CS Desk"];
            
            
            CFErrorRef errorObj = NULL;
            NSString *strPhone=[AppDelegate appDelegateInstance].strWhatsApp;
            NSArray *allContacts = (__bridge NSArray *) ABAddressBookCopyArrayOfAllPeople(book);
            for (id record in allContacts)
            {
                ABRecordRef contactPerson = (__bridge ABRecordRef)record;
                NSString *firstName = CFBridgingRelease(ABRecordCopyValue(contactPerson, kABPersonFirstNameProperty));
                ABMultiValueRef phonesRef=ABRecordCopyValue(contactPerson, kABPersonPhoneProperty);
                NSString *strUserPhone=[self getMobilePhoneProperty:phonesRef];
                if(phonesRef)
                {
                    CFRelease(phonesRef);
                }
                if([strUserPhone isEqualToString:strPhone] && [firstName isEqualToString:strName])
                {
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [self openWhatsApp];
                    });
                    return ;
                }
            }
            ABRecordRef defaultSource = ABAddressBookCopyDefaultSource(book);
            ABRecordRef person = ABPersonCreate();
            //add
            //Phone number is a list of phone number, so create a multivalue
            ABMutableMultiValueRef phoneNumberMultiValue  = ABMultiValueCreateMutable(kABMultiStringPropertyType);
            ABMultiValueAddValueAndLabel(phoneNumberMultiValue, (__bridge CFTypeRef)(strPhone), kABPersonPhoneMobileLabel, NULL);
            ABRecordSetValue(person, kABPersonFirstNameProperty, (__bridge CFTypeRef)(strName) , nil); // first name of the new person
            ABRecordSetValue(person, kABPersonPhoneProperty, phoneNumberMultiValue, &errorObj); // set the phone number property
            ABAddressBookAddRecord(book, person, &errorObj);
            ABAddressBookSave(book, &errorObj);
            CFRelease(person);
            CFRelease(defaultSource);
            
            if(errorObj!=nil)
            {
                dispatch_async(dispatch_get_main_queue(), ^{
                    UIAlertView *cantAddContactAlert = [[UIAlertView alloc] initWithTitle: contact_add_error message: contact_add_error_permission delegate:nil cancelButtonTitle: @"Ok" otherButtonTitles: nil];
                    [cantAddContactAlert show];
                });
            }
            else
            {
                dispatch_async(dispatch_get_main_queue(), ^{
                    [self openWhatsApp];
                });
            }
        });
    }
}
- (NSString *)getMobilePhoneProperty:(ABMultiValueRef)phonesRef
{
    for (int i=0; i < ABMultiValueGetCount(phonesRef); i++)
    {
        CFStringRef currentPhoneLabel = ABMultiValueCopyLabelAtIndex(phonesRef, i);
        CFStringRef currentPhoneValue = ABMultiValueCopyValueAtIndex(phonesRef, i);
        
        if(currentPhoneLabel)
        {
            if (CFStringCompare(currentPhoneLabel, kABPersonPhoneMobileLabel, 0) == kCFCompareEqualTo) {
                return (__bridge NSString *)currentPhoneValue;
            }
            
            if (CFStringCompare(currentPhoneLabel, kABHomeLabel, 0) == kCFCompareEqualTo) {
                return (__bridge NSString *)currentPhoneValue;
            }
        }
        if(currentPhoneLabel) {
            CFRelease(currentPhoneLabel);
        }
        if(currentPhoneValue) {
            CFRelease(currentPhoneValue);
        }
    }
    
    return nil;
}
-(void)openWhatsApp
{
    NSString * urlWhats = [NSString stringWithFormat:@"whatsapp://"];
    NSURL * whatsappURL = [NSURL URLWithString:[urlWhats stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
    if ([[UIApplication sharedApplication] canOpenURL: whatsappURL])
    {
        [[UIApplication sharedApplication] openURL: whatsappURL];
    }
    else
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil message:@"Whats App application not installed on your device" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil,nil];
        [alert show];
    }
}
@end