//
//  GoogleAnalyicsHandler.m
//  MOE
//
//  Created by Nivrutti on 7/16/15.
//  Copyright (c) 2015 Neosoft. All rights reserved.
//

#import "GoogleAnalyticsHandler.h"
#import "GAI.h"
#import "GAIFields.h"
#import "GAIDictionaryBuilder.h"
@implementation GoogleAnalyticsHandler
+(void)setupGoggleAnalytics
{
    //************ GOOGLE ANALYTICS ****************//
    // Optional: automatically send uncaught exceptions to Google Analytics.
    [GAI sharedInstance].trackUncaughtExceptions = YES;
    // Optional: set Google Analytics dispatch interval
    [GAI sharedInstance].dispatchInterval = 120; //every to 2 mintues
    // Optional: set Logger to VERBOSE for debug information.
    [[[GAI sharedInstance] logger] setLogLevel:kGAILogLevelNone];
    // Initialize tracker. Replace with your tracking ID.
    [[GAI sharedInstance] trackerWithTrackingId:GOOGLE_ANALYTICS_ID];
    //************ GOOGLE ANALYTICS INITIALIZATION END ****************//
}
+(void)sendScreenName:(NSString *)strName
{
    id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
    [tracker set:kGAIScreenName value:strName];
    [tracker send:[[GAIDictionaryBuilder createScreenView] build]];
}
+(void)sendEventName:(NSString *)strEventName Category:(NSString *)strCategory Label:(NSString *)strLabel
{
    [[GAI sharedInstance].defaultTracker send:
    [[GAIDictionaryBuilder createEventWithCategory:strEventName
     action:strCategory label:strLabel value:nil] build]];
}
@end
