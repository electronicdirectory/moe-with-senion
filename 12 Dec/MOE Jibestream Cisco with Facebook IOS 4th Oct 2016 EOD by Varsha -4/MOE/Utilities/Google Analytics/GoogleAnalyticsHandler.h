//
//  GoogleAnalyticsHandler.h
//  MOE
//
//  Created by Nivrutti on 7/16/15.
//  Copyright (c) 2015 Neosoft. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface GoogleAnalyticsHandler : NSObject
+(void)setupGoggleAnalytics;
+(void)sendScreenName:(NSString *)strName;
+(void)sendEventName:(NSString *)strEventName Category:(NSString *)strCategory Label:(NSString *)strLabel;
@end
