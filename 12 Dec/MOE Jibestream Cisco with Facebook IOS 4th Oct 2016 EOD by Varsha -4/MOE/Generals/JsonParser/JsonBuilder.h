/*!
 @header JsonBuilder.h
 
 @brief This is the header file where my super-code is contained.
 
 This file contains the the json Object parsing and creation method.
 
 @author Nivrutti patil
 @copyright  2015 Nivrutti patil
 @version    1.0.0
 */

#import <Foundation/Foundation.h>
/*!
 @class JsonBuilder
 
 @brief The JsonBuilder class
 
 @discussion
 This file contains the the json Object parsing and creation method.

 @superclass SuperClass: NSObject
 @classdesign    No special design is applied here.
 @coclass
 @helps It helps no other classes.
 @helper    No helper exists for this class.
 */
@interface JsonBuilder : NSObject

/*!
 @Method parseJsonObject:- This method returns the current id as Object type (it will be json object,error or nil value)
 @param  data  The json object will be returned for.
 @return id   it will be json object,error or nil value .
 
 */

+(id)parseJsonObject:(id)data;

/*!
 @Method createJsonObject:- This method returns the current id as Object type (it will be json string,error or nil value)
 @param  id  it will take input as a dictionary array or string.
 @return id  it will be json object,error or nil value .
 */
+(id)createJsonObject:(id)obj;
@end
