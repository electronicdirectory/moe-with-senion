/*!
 @header JsonBuilder.h
 
 @brief This is the header file where my super-code is contained.
 
 This file contains the the json Object parsing and creation method.
 
 @author Nivrutti patil
 @copyright  2015 Nivrutti patil
 @version    1.0.0
 */

#import "JsonBuilder.h"
@implementation JsonBuilder
#pragma mark Parse Json Object

/*!
 @Method parseJsonObject:- This method returns the current id as Object type (it will be json object,error or nil value)
 @param  data  The json object will be returned for.
 @return id   it will be json object,error or nil value .
 
 */
+(id)parseJsonObject:(id)data
{
    if([data isKindOfClass:[NSData class]])
    {
        NSError *parseJsonError;
        id serviceResponse=[NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:&parseJsonError];
        if(!parseJsonError)
        {
            return serviceResponse;
        }
        return parseJsonError;
    }
    return nil;
}
/*!
 @Method createJsonObject:- This method returns the current id as Object type (it will be json string,error or nil value)
 @param  id  it will take input as a dictionary array or string.
 @return id  it will be json object,error or nil value .
 */
#pragma mark create json Object
+(id)createJsonObject:(id)obj
{
    NSError *parseJsonError;
    NSData* data = [NSJSONSerialization dataWithJSONObject:obj options:kNilOptions error:&parseJsonError];
    if(!parseJsonError)
    {
        return data;
    }
    return parseJsonError;
}
@end
