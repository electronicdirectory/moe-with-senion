
#import <Foundation/Foundation.h>

@interface Language : NSObject

+ (void)initialize;
+ (void)setLanguage:(NSString *)l;
+ (NSString *)get:(NSString *)key alter:(NSString *)alternate;


//[Language get:@"lang" alter:nil]
//[Language get:@"Error while loading data" alter:nil]

@end
