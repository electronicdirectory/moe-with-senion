

#import "Language.h"

@implementation Language

static NSBundle *bundle = nil;

+ (void)initialize
{
    NSUserDefaults* defs = [NSUserDefaults standardUserDefaults];
    NSArray* languages = [defs objectForKey:@"AppleLanguages"];
    NSString *current = [languages objectAtIndex:0];
    [self setLanguage:current];
}

/*
 example calls:
 [Language setLanguage:@"en"];
 [Language setLanguage:@"ar"];
 */

+ (void)setLanguage:(NSString *)l
{
    NSString *path = [[ NSBundle mainBundle ] pathForResource:l ofType:@"lproj"];
    bundle = [NSBundle bundleWithPath:path];
}

+ (NSString *)get:(NSString *)key alter:(NSString *)alternate
{
    return [bundle localizedStringForKey:key value:alternate table:nil];
//    return NSLocalizedString(key, alter);
}




@end
