//
//  NSString+Validation.m
//  Sportspad
//
//  Created by Nivrutti on 9/10/13.
//  Copyright (c) 2013 Rahul Mahadik. All rights reserved.
//

#import "NSString+Validation.h"

@implementation NSString(Validation)
-(NSString *)removeBlankSpaces
{
    return [self  stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
}
-(BOOL)CheckNullString
{
    bool result = false;
    if (([(NSString *)self length] != 0) &&
        (NSString *)self !=nil &&
        ((NSString *)self !=(id)[NSNull null]) &&
        ([(NSString *)self caseInsensitiveCompare:@"(null)"] != NSOrderedSame) &&
        ([(NSString *)self caseInsensitiveCompare:@"<null>"] != NSOrderedSame) &&
        ([(NSString *)self caseInsensitiveCompare:@""] != NSOrderedSame)
        )
    {
        result = true;
    }
    return result;
}
@end
