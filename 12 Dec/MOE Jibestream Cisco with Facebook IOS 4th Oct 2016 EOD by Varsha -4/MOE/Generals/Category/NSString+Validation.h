//
//  NSString+Validation.h
//  Sportspad
//
//  Created by Nivrutti on 9/10/13.
//  Copyright (c) 2013 Rahul Mahadik. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString(Validation)

-(NSString *)removeBlankSpaces;
-(BOOL)CheckNullString;

@end
