//
//  DBOpreation.m
//  MOE
//
//  Created by Nivrutti on 6/15/15.
//  Copyright (c) 2015 Neosoft. All rights reserved.
//

#import "DBOpreation.h"
@implementation DBOpreation

#pragma mark GetDBQueue
+(FMDatabaseQueue *)GetDBQueue
{
    if([AppDelegate appDelegateInstance].DBQueue==nil)
    {
        [AppDelegate appDelegateInstance].DBQueue=[FMDatabaseQueue databaseQueueWithPath:[APP_DELEGATE dbPathAddress]];
    }
    return  [AppDelegate appDelegateInstance].DBQueue;
}
#pragma mark createSqlTables
+(void)createSqlTables
{
    FMDatabaseQueue *dbQueue =[self GetDBQueue];
    [dbQueue inTransaction:^(FMDatabase *db, BOOL *rollback)
     {
         NSString *theMallSql=@"CREATE TABLE IF NOT EXISTS TheMall (ID INTEGER PRIMARY KEY,Title TEXT,PageDetail TEXT,Image TEXT)";
         [db executeUpdate:theMallSql];
         
         
         NSString *dineSql=@"CREATE TABLE IF NOT EXISTS DiningCategories (CategoryName TEXT,ID INTEGER PRIMARY KEY)";
         [db executeUpdate:dineSql];
         
         NSString *eventSql=@"CREATE TABLE IF NOT EXISTS Events (ID INTEGER PRIMARY KEY,Title TEXT,Event_End_Date TEXT,Event_Image TEXT,Event_Start_Date TEXT,Short_Description TEXT,Event_Details Text,Type Text)";
         [db executeUpdate:eventSql];
         
         NSString *entSql=@"CREATE TABLE IF NOT EXISTS EntertainmentList (ID INTEGER PRIMARY KEY,Description Text, Destination_ID Text, Entertainment_Telephone Text,FB_URL Text,Floor Text, Nearest_Parking Text, Shop_Code Text, Store_Timings Text, Summary Text, Thumbnail Text, Title Text, Twitter_Page_URL Text,Type Text,Cat_ID INTEGER,CatName TEXT,Featured_Products Blob,ImagesList Blob)";
         [db executeUpdate:entSql];
         
         NSString *hotelSql=@"CREATE TABLE IF NOT EXISTS HotelsListing (ID INTEGER PRIMARY KEY,Description TEXT,Hotel_Name TEXT,Telephone TEXT,URL TEXT,Image TEXT,Type Text)";
         [db executeUpdate:hotelSql];
         
         NSString *offerSql=@"CREATE TABLE IF NOT EXISTS OfferListing (ID INTEGER PRIMARY KEY,Description TEXT,Title TEXT,End_Date TEXT,Start_Date TEXT,Image_URL TEXT,Type Text,Store_ID INTEGER,Category_ID INTEGER,Category_Name TEXT)";
         [db executeUpdate:offerSql];
         
         
         NSString *serviceSql=@"CREATE TABLE IF NOT EXISTS ServiceListing (ID INTEGER PRIMARY KEY,PageDetail TEXT,Title TEXT,Image TEXT,Type Text)";
         [db executeUpdate:serviceSql];
         
         NSString *shoppingSql=@"CREATE TABLE IF NOT EXISTS ShoppingCategories (CategoryName TEXT,ID INTEGER PRIMARY KEY)";
         [db executeUpdate:shoppingSql];
         
         NSString *diningListing=@"CREATE TABLE IF NOT EXISTS DiningListing (ID INTEGER PRIMARY KEY,Title TEXT,Floor TEXT,Dining_Nearest_Parking TEXT,Dining_Telephone TEXT,Destination_ID TEXT,Summary TEXT,Shop_Code TEXT,Floor_ID TEXT,Store_Timings TEXT,Landmark TEXT,Logo TEXT,Thumbnail TEXT,FB_URL TEXT,Twitter_URL TEXT,Description Text,Cat_ID INTEGER,CatName TEXT,Type Text,Featured_Products Blob,ImagesList Blob)";
         [db executeUpdate:diningListing];
         
         NSString *shopSql=@"CREATE TABLE IF NOT EXISTS ShoppingListing (ID INTEGER PRIMARY KEY,Description Text, Destination_ID Text, Shop_Telephone Text,FB_URL Text,Floor Text, Shop_Nearest_Parking Text, Summary Text, Image Text, Shop_Title Text, Twitter_URL Text,Cat_ID INTEGER,CatName TEXT,Type Text,Featured_Products Blob,ImagesList Blob)";
         [db executeUpdate:shopSql];
         
         NSString *atmSql=@"CREATE TABLE IF NOT EXISTS ATMListing (ID INTEGER PRIMARY KEY,PageDetail TEXT,Title TEXT,Image TEXT,Type Text)";
         [db executeUpdate:atmSql];
         
         NSString *searchSql=@"CREATE TABLE IF NOT EXISTS SearchTable (ID INTEGER PRIMARY KEY,Title Text,Type Text)";
         [db executeUpdate:searchSql];
         
         NSString *favSql=@"CREATE TABLE IF NOT EXISTS FavouriteList (ID INTEGER PRIMARY KEY,Type Text,Title Text)";
         [db executeUpdate:favSql];
         
         NSString *parking=@"CREATE TABLE IF NOT EXISTS MyParking (ParkingDescription Text,ParkingImagePath text,ParkingDate Text)";
         [db executeUpdate:parking];
         
     }];
}
+(void)deleteDataFromlocal
{
    FMDatabaseQueue *queueObj=[self GetDBQueue];
    [queueObj inTransaction:^(FMDatabase *db, BOOL *rollback)
     {
         [db executeUpdate:@"Delete from SearchTable"];
         [db executeUpdate:@"Delete from DiningCategories"];
         [db executeUpdate:@"Delete from TheMall"];
         [db executeUpdate:@"Delete from Events"];
         [db executeUpdate:@"Delete from EntertainmentList"];
         [db executeUpdate:@"Delete from HotelsListing"];
         [db executeUpdate:@"Delete from OfferListing"];
         [db executeUpdate:@"Delete from ServiceListing"];
         [db executeUpdate:@"Delete from ShoppingCategories"];
         [db executeUpdate:@"Delete from DiningListing"];
         [db executeUpdate:@"Delete from ATMListing"];
         [db executeUpdate:@"Delete from ShoppingListing"];
     }];
}
+(void)copyAndCreateSearchData
{
    FMDatabaseQueue *queueObj=[self GetDBQueue];
    [queueObj inTransaction:^(FMDatabase *db, BOOL *rollback)
     {
         [db executeUpdate:@"Delete from SearchTable"];

         [db executeUpdate:[NSString stringWithFormat:@"INSERT OR REPLACE  INTO SearchTable (ID, Title,Type) SELECT ID,Title as Title,'%@' FROM Events",APP_EVENTS]];
         
         [db executeUpdate:[NSString stringWithFormat:@"INSERT OR REPLACE  INTO SearchTable (ID, Title,Type) SELECT ID,Shop_Title as Title ,'%@' FROM ShoppingListing",APP_SHOP]];
         
         [db executeUpdate:[NSString stringWithFormat:@"INSERT OR REPLACE  INTO SearchTable (ID, Title,Type) SELECT ID,Title as Title,'%@' FROM DiningListing",APP_DINE]];
         
         [db executeUpdate:[NSString stringWithFormat:@"INSERT OR REPLACE  INTO SearchTable (ID, Title,Type)SELECT ID,Title as Title,'%@' FROM EntertainmentList",APP_ENTERTAINMENT]];
         
         [db executeUpdate:[NSString stringWithFormat:@"INSERT OR REPLACE  INTO SearchTable (ID, Title,Type) SELECT ID,Title as Title,'%@' FROM ServiceListing",APP_SERVICES]];
         
         [db executeUpdate:[NSString stringWithFormat:@"INSERT OR REPLACE  INTO SearchTable (ID, Title,Type) SELECT ID,Title as Title,'%@' FROM OfferListing",APP_OFFER]];
         
         [db executeUpdate:[NSString stringWithFormat:@"INSERT OR REPLACE  INTO SearchTable (ID, Title,Type) SELECT ID,Hotel_Name as Title,'%@' FROM HotelsListing",APP_HOTEL]];
         
         [db executeUpdate:[NSString stringWithFormat:@"INSERT OR REPLACE  INTO SearchTable (ID, Title,Type) SELECT ID,Title as Title,'%@' FROM ATMListing",APP_ATM]];
         
         
         
     }];
}

#pragma mark validateText
+(id)validateText:(id)text
{
    if([text isKindOfClass:[NSString class]])
    {
        return text!=nil?[text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]]:NULL;
    }
    return  NULL;
}
#pragma mark validateNumber
+(NSNumber *)validateNumber:(id)text
{
    if([text isKindOfClass:[NSNumber class]])
    {
        return text;
    }
    return  [NSNumber numberWithInt:0];
}
#pragma mark insertTheMallData
+(void)insertTheMallData:(NSArray *)mutArray
{
    FMDatabaseQueue *queueObj=[self GetDBQueue];
    [queueObj inTransaction:^(FMDatabase *db, BOOL *rollback) {
        
        for (NSDictionary *dict in mutArray)
        {
            @autoreleasepool {
                NSNumber *mallId=[NSNumber numberWithInteger:[[self validateText:[dict objectForKey:Mall_ID]]integerValue]];
                NSString *mallTitle=[self validateText:[dict objectForKey:Mall_Title]];
                NSString *mallPageDetail=[self validateText:[dict objectForKey:Mall_PageDetail]];
                NSString *mallImage=[self validateText:[dict objectForKey:Mall_Image]];
                [db executeUpdate:@"INSERT OR REPLACE INTO TheMall (ID,Title,PageDetail,Image) values (?,?,?,?)",mallId,mallTitle,mallPageDetail,mallImage];
            }
        }
    }];
}

#pragma mark insertDineCategories
+(void)insertDineCategories:(NSArray *)mutArray
{
    FMDatabaseQueue *queueObj=[self GetDBQueue];
    [queueObj inTransaction:^(FMDatabase *db, BOOL *rollback) {
        
        for (NSDictionary *dict in mutArray)
        {
            @autoreleasepool {
                NSNumber *catId=[NSNumber numberWithInteger:[[self validateText:[dict objectForKey:Cate_ID]]integerValue]];
                NSString *cateName=[self validateText:[dict objectForKey:Category_Name]];
                [db executeUpdate:@"INSERT OR REPLACE INTO DiningCategories (CategoryName,ID) values (?,?)",cateName,catId];
            }
            
        }
    }];
}
#pragma mark insertEvents
+(void)insertEvents:(NSArray *)mutArray
{
    FMDatabaseQueue *queueObj=[self GetDBQueue];
    [queueObj inTransaction:^(FMDatabase *db, BOOL *rollback)
     {
         for (NSDictionary *dict in mutArray)
         {
             @autoreleasepool {
                 NSNumber *e_ID=nil;
                 e_ID=[NSNumber numberWithInteger:[[self validateText:[dict objectForKey:event_ID]]integerValue]];
                 NSString *e_Title=@"";
                 e_Title=[self validateText:[dict objectForKey:event_Title]];
                 NSString *e_Image=@"";
                 e_Image=[self validateText:[dict objectForKey:event_Image]];
                 NSString *e_Start_Date=@"";
                 e_Start_Date=[self validateText:[dict objectForKey:event_Start_Date]];
                 NSString *e_End_Date=@"";
                 e_End_Date=[self validateText:[dict objectForKey:event_End_Date]];
                 NSString *e_Short_Description=@"";
                 e_Short_Description=[self validateText:[dict objectForKey:event_Short_Description]];
                 NSString *eventdetails=@"";
                 eventdetails=[self validateText:[dict objectForKey:event_Details]];
                 [db executeUpdate:@"INSERT OR REPLACE INTO Events (ID,Title,Event_End_Date,Event_Image,Event_Start_Date,Short_Description,Type,Event_Details) values (?,?,?,?,?,?,?,?)",e_ID,e_Title,e_End_Date,e_Image,e_Start_Date,e_Short_Description,APP_EVENTS,eventdetails];
             }
             
         }
     }];
}
#pragma mark insertEntertainmentList
+(void)insertEntertainmentList:(NSArray *)mutArray
{
    FMDatabaseQueue *queueObj=[self GetDBQueue];
    [queueObj inTransaction:^(FMDatabase *db, BOOL *rollback)
     {
         for (NSDictionary *dataDict in mutArray)
         {
             NSNumber *catId=nil;
             catId=[NSNumber numberWithInteger:[[self validateText:[dataDict objectForKey:Cate_ID]]integerValue]];
             
             NSString *CategoryName=@"";
             CategoryName=[self validateText:[dataDict objectForKey:Category_Name]];
             NSArray *Shops_list=[dataDict objectForKey:shop_List_key];
             
             for (NSDictionary *dict in Shops_list)
             {
                 @autoreleasepool {
                     
                     NSNumber *e_ID=nil;
                     e_ID=[NSNumber numberWithInteger:[[self validateText:[dict objectForKey:entertainment_ID]]integerValue]];
                     
                     NSString *e_Description=@"";
                     e_Description=[self validateText:[dict objectForKey:entertainment_Description]];
                     
                     NSString *e_Destination_ID=@"";
                     e_Destination_ID=[self validateText:[dict objectForKey:entertainment_Destination_ID]];
                     
                     NSString *e_Telephone=@"";
                     e_Telephone=[self validateText:[dict objectForKey:entertainment_Telephone]];
                     
                     NSString *e_FB_URL=@"";
                     e_FB_URL=[self validateText:[dict objectForKey:entertainment_FB_URL]];
                     
                     NSString *e_Floor=@"";
                     e_Floor=[self validateText:[dict objectForKey:entertainment_Floor]];
                     
                     NSString *e_Nearest_Parking=@"";
                     e_Nearest_Parking=[self validateText:[dict objectForKey:entertainment_Nearest_Parking]];
                     
                     NSString *e_Shop_Code=@"";
                     e_Shop_Code=[self validateText:[dict objectForKey:entertainment_Shop_Code]];
                     
                     NSString *e_Store_Timings=@"";
                     e_Store_Timings=[self validateText:[dict objectForKey:entertainment_Store_Timings]];
                     
                     NSString *e_Summary=@"";
                     e_Summary=[self validateText:[dict objectForKey:entertainment_Summary]];
                     
                     NSString *e_Thumbnail=@"";
                     e_Thumbnail=[self validateText:[dict objectForKey:entertainment_Thumbnail]];
                     
                     NSString *e_Title=@"";
                     e_Title=[self validateText:[dict objectForKey:entertainment_Title]];
                     
                     NSString *e_Twitter_Page_URL=@"";
                     e_Twitter_Page_URL=[self validateText:[dict objectForKey:entertainment_Twitter_Page_URL]];
                     
                     
                     NSData *imageList,*featureProduct;
                     if([[dict objectForKey:shop_ImagesList]isKindOfClass:[NSArray class]])
                     {
                         NSArray *array=[dict objectForKey:shop_ImagesList];
                         imageList = [NSKeyedArchiver archivedDataWithRootObject:array];
                     }
                     if([[dict objectForKey:shop_Featured_Products]isKindOfClass:[NSArray class]])
                     {
                         NSArray *array=[dict objectForKey:shop_Featured_Products];
                         featureProduct = [NSKeyedArchiver archivedDataWithRootObject:array];
                     }
                     
                     [db executeUpdate:@"INSERT OR REPLACE INTO EntertainmentList (ID,Description,Destination_ID, Entertainment_Telephone,FB_URL,Floor,Nearest_Parking,Shop_Code,Store_Timings,Summary,Thumbnail,Title, Twitter_Page_URL,Type,Cat_ID,CatName,ImagesList,Featured_Products) values (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)",e_ID,e_Description,e_Destination_ID,e_Telephone,e_FB_URL,e_Floor,e_Nearest_Parking,e_Shop_Code,e_Store_Timings,e_Summary,e_Thumbnail,e_Title,e_Twitter_Page_URL,APP_ENTERTAINMENT,catId,CategoryName,imageList,featureProduct];
                 }
             }
         }
     }];
}
#pragma mark insertHotelList
+(void)insertHotelList:(NSArray *)mutArray
{
    FMDatabaseQueue *queueObj=[self GetDBQueue];
    [queueObj inTransaction:^(FMDatabase *db, BOOL *rollback)
     {
         
         for (NSDictionary *dict in mutArray)
         {
             @autoreleasepool {
                 NSNumber *hListing_ID=nil;
                 hListing_ID=[NSNumber numberWithInteger:[[self validateText:[dict objectForKey:hotelsListing_ID]]integerValue]];
                 NSString *h_Description=@"";
                 h_Description=[self validateText:[dict objectForKey:hotelsListing_Description]];
                 NSString *hListing_Name=@"";
                 hListing_Name=[self validateText:[dict objectForKey:hotelsListing_Name]];
                 NSString *hListing_Telephone=@"";
                 hListing_Telephone=[self validateText:[dict objectForKey:hotelsListing_Telephone]];
                 NSString *hListing_URL=@"";
                 hListing_URL=[self validateText:[dict objectForKey:hotelsListing_URL]];
                 NSString *hListing_Image=@"";
                 hListing_Image=[self validateText:[dict objectForKey:hotelsListing_Image]];
                 [db executeUpdate:@"INSERT OR REPLACE INTO HotelsListing (ID,Description,Hotel_Name,Telephone,URL,Image,Type) values (?,?,?,?,?,?,?)",hListing_ID,h_Description,hListing_Name,hListing_Telephone,hListing_URL,hListing_Image,APP_HOTEL];
             }
             
         }
     }];
}
#pragma mark insertOfferList
+(void)insertOfferList:(NSArray *)mutArray
{
    FMDatabaseQueue *queueObj=[self GetDBQueue];
    [queueObj inTransaction:^(FMDatabase *db, BOOL *rollback)
     {
         for (NSDictionary *dict in mutArray)
         {
             @autoreleasepool
             {
                 NSNumber *oListing_ID=nil;
                 oListing_ID=[NSNumber numberWithInteger:[[self validateText:[dict objectForKey:offerListing_ID]]integerValue]];
                 NSString *oListing_Description=@"";
                 oListing_Description=[self validateText:[dict objectForKey:offerListing_Description]];
                 NSString *oListing_Title=@"";
                 oListing_Title=[self validateText:[dict objectForKey:offerListing_Title]];
                 NSString *oListing_End_Date=@"";
                 oListing_End_Date=[self validateText:[dict objectForKey:offerListing_End_Date]];
                 NSString *oListing_Start_Date=@"";
                 oListing_Start_Date=[self validateText:[dict objectForKey:offerListing_Start_Date]];
                 NSString *oListing_Image_URL=@"";
                 oListing_Image_URL=[self validateText:[dict objectForKey:offerListing_Image_URL]];
                 static NSString * const offerListing_Store_ID=@"Store_ID";
                 static NSString * const offerListing_Category_Name=@"Category_Name";
                 static NSString * const offerListing_Category_ID=@"Category_ID";
                 
                 NSNumber *oStore_ID=nil;
                 oStore_ID=[NSNumber numberWithInteger:[[self validateText:[dict objectForKey:offerListing_Store_ID]]integerValue]];
                 NSNumber *oCategory_ID=nil;
                 oCategory_ID=[NSNumber numberWithInteger:[[self validateText:[dict objectForKey:offerListing_Category_ID]]integerValue]];
                 NSString *oCategory_Name=@"";
                 oCategory_Name=[self validateText:[dict objectForKey:offerListing_Category_Name]];
                 
                 [db executeUpdate:@"INSERT OR REPLACE INTO OfferListing (ID,Description,Title,End_Date,Start_Date,Image_URL,Type,Store_ID,Category_ID,Category_Name) values (?,?,?,?,?,?,?,?,?,?)",oListing_ID,oListing_Description,oListing_Title,oListing_End_Date,oListing_Start_Date,oListing_Image_URL,APP_OFFER,oStore_ID,oCategory_ID,oCategory_Name];
             }
             
         }
         
         
     }];
}
#pragma mark insertServiceList
+(void)insertGuestServicesPages:(NSArray *)mutArray
{
    FMDatabaseQueue *queueObj=[self GetDBQueue];
    [queueObj inTransaction:^(FMDatabase *db, BOOL *rollback)
     {
         for (NSDictionary *dict in mutArray)
         {
             @autoreleasepool {
                 NSNumber *sL_ID=nil;
                 sL_ID=[NSNumber numberWithInteger:[[self validateText:[dict objectForKey:serviceListing_ID]]integerValue]];
                 NSString *sL_Description=@"";
                 sL_Description=[self validateText:[dict objectForKey:serviceListing_PageDetail]];
                 NSString *sL_Title=@"";
                 sL_Title=[self validateText:[dict objectForKey:serviceListing_Title]];
                 NSString *sL_Image=@"";
                 sL_Image=[self validateText:[dict objectForKey:serviceListing_Image]];
                 [db executeUpdate:@"INSERT OR REPLACE INTO ServiceListing (ID,Title,PageDetail,Image) values (?,?,?,?)",sL_ID,sL_Title,sL_Description,sL_Image];
             }
         }
     }];
}
+(void)insertATMList:(NSArray *)mutArray
{
    FMDatabaseQueue *queueObj=[self GetDBQueue];
    [queueObj inTransaction:^(FMDatabase *db, BOOL *rollback)
     {
         for (NSDictionary *dict in mutArray)
         {
             @autoreleasepool {
                 NSNumber *sL_ID=nil;
                 sL_ID=[NSNumber numberWithInteger:[[self validateText:[dict objectForKey:serviceListing_ID]]integerValue]];
                 NSString *sL_Description=@"";
                 sL_Description=[self validateText:[dict objectForKey:serviceListing_PageDetail]];
                 NSString *sL_Title=@"";
                 sL_Title=[self validateText:[dict objectForKey:serviceListing_Title]];
                 NSString *sL_Image=@"";
                 sL_Image=[self validateText:[dict objectForKey:serviceListing_Image]];
                 [db executeUpdate:@"INSERT OR REPLACE INTO ATMListing (ID,Title,PageDetail,Image) values (?,?,?,?)",sL_ID,sL_Title,sL_Description,sL_Image];
             }
         }
     }];
}

#pragma mark insertShoppingCategories
+(void)insertShoppingCategories:(NSArray *)mutArray
{
    FMDatabaseQueue *queueObj=[self GetDBQueue];
    [queueObj inTransaction:^(FMDatabase *db, BOOL *rollback) {
        
        for (NSDictionary *dict in mutArray)
        {
            @autoreleasepool {
                NSNumber *catId=[NSNumber numberWithInteger:[[self validateText:[dict objectForKey:Cate_ID]]integerValue]];
                NSString *cateName=[self validateText:[dict objectForKey:Category_Name]];
                [db executeUpdate:@"INSERT OR REPLACE INTO ShoppingCategories (CategoryName,ID) values (?,?)",cateName,catId];
            }
        }
    }];
}
+(void)insertDineListing:(NSArray *)mutArray
{
    FMDatabaseQueue *queueObj=[self GetDBQueue];
    [queueObj inTransaction:^(FMDatabase *db, BOOL *rollback)
     {
         
         for (NSDictionary *dataDict in mutArray)
         {
             NSNumber *catId=nil;
             catId=[NSNumber numberWithInteger:[[self validateText:[dataDict objectForKey:Cate_ID]]integerValue]];
             
             NSString *CategoryName=@"";
             CategoryName=[self validateText:[dataDict objectForKey:Category_Name]];
             
             NSArray *Shops_list=[dataDict objectForKey:shop_List_key];
             
             for (NSDictionary *dictData in Shops_list)
             {
                 NSDictionary *dict=[dictData objectForKey:shop_key];
                 if([[dict allKeys]count]==0)
                 {
                     continue;
                 }
                 @autoreleasepool
                 {
                     NSNumber *d_ID=nil;
                     d_ID=[NSNumber numberWithInteger:[[self validateText:[dict objectForKey:dining_ID]]integerValue]];
                     
                     NSString *d_Title=@"";
                     d_Title=[self validateText:[dict objectForKey:dining_Title]];
                     
                     NSString *d_Floor=@"";
                     d_Floor=[self validateText:[dict objectForKey:dining_Floor]];
                     
                     NSString *d_Nearest_Parking=@"";
                     d_Nearest_Parking=[self validateText:[dict objectForKey:dining_Nearest_Parking]];
                     
                     NSString *d_Telephone=@"";
                     d_Telephone=[self validateText:[dict objectForKey:dining_Telephone]];
                     
                     NSString *d_Destination_ID=@"";
                     d_Destination_ID=[self validateText:[dict objectForKey:dining_Destination_ID]];
                     
                     NSString *d_Summary=@"";
                     d_Summary=[self validateText:[dict objectForKey:dining_Summary]];
                     
                     NSString *d_Shop_Code=@"";
                     d_Shop_Code=[self validateText:[dict objectForKey:dining_Shop_Code]];
                     
                     NSString *d_Floor_ID=@"";
                     d_Floor_ID=[self validateText:[dict objectForKey:dining_Floor_ID]];
                     
                     NSString *d_Store_Timings=@"";
                     d_Store_Timings=[self validateText:[dict objectForKey:dining_Store_Timings]];
                     
                     NSString *d_Landmark=@"";
                     d_Landmark=[self validateText:[dict objectForKey:dining_Landmark]];
                     
                     NSString *d_Thumbnail=@"";
                     d_Thumbnail=[self validateText:[dict objectForKey:dining_Thumbnail]];
                     
                     NSString *d_FB_URL=@"";
                     d_FB_URL=[self validateText:[dict objectForKey:dining_FB_URL]];
                     
                     NSString *d_Twitter_Page_URL=@"";
                     d_Twitter_Page_URL=[self validateText:[dict objectForKey:dining_Twitter_URL]];
                     
                     NSString *d__Description=@"";
                     d__Description=[self validateText:[dict objectForKey:dining_Description]];
                     
                     NSString *d_Logo=@"";
                     d_Logo=[self validateText:[dict objectForKey:dining_Logo]];
                     
                     
                     NSData *imageList,*featureProduct;
                     if([[dict objectForKey:shop_ImagesList]isKindOfClass:[NSArray class]])
                     {
                         NSArray *array=[dict objectForKey:shop_ImagesList];
                         imageList = [NSKeyedArchiver archivedDataWithRootObject:array];
                     }
                     if([[dict objectForKey:shop_Featured_Products]isKindOfClass:[NSArray class]])
                     {
                         NSArray *array=[dict objectForKey:shop_Featured_Products];
                         featureProduct = [NSKeyedArchiver archivedDataWithRootObject:array];
                     }
                     
                     [db executeUpdate:@"INSERT OR REPLACE INTO DiningListing (ID,Title,Floor,Dining_Nearest_Parking,Dining_Telephone,Destination_ID,Summary,Shop_Code,Floor_ID ,Store_Timings,Landmark,Logo,Thumbnail,FB_URL,Twitter_URL,Description,Cat_ID,CatName,Type,ImagesList,Featured_Products) values (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)",d_ID,d_Title,d_Floor,d_Nearest_Parking,d_Telephone,d_Destination_ID,d_Summary,d_Shop_Code,d_Floor_ID,d_Store_Timings,d_Landmark,d_Logo,d_Thumbnail,d_FB_URL,d_Twitter_Page_URL,d__Description,catId,CategoryName,APP_DINE,imageList,featureProduct];
                 }
                 
             }
         }
         
     }];
}
+(void)insertShoppingListing:(NSArray *)mutArray
{
    FMDatabaseQueue *queueObj=[self GetDBQueue];
    [queueObj inTransaction:^(FMDatabase *db, BOOL *rollback)
     {
         for (NSDictionary *dataDict in mutArray)
         {
             @autoreleasepool {
                 
                 NSNumber *catId=nil;
                 catId=[NSNumber numberWithInteger:[[self validateText:[dataDict objectForKey:Cate_ID]]integerValue]];
                 
                 NSString *CategoryName=@"";
                 CategoryName=[self validateText:[dataDict objectForKey:Category_Name]];
                 NSArray *Shops_list=[dataDict objectForKey:shop_List_key];
                 
                 for (NSDictionary *dictData in Shops_list)
                 {
                     NSDictionary *dict=[dictData objectForKey:shop_key];
                     if([[dict allKeys]count]==0)
                     {
                         continue;
                     }
                     @autoreleasepool
                     {
                         NSNumber *s__ID=nil;
                         s__ID=[NSNumber numberWithInteger:[[self validateText:[dict objectForKey:shop_ID]]integerValue]];
                         NSString *s__Description=@"";
                         s__Description=[self validateText:[dict objectForKey:shop_Description]];
                         
                         NSString *s__Destination_ID=@"";
                         s__Destination_ID=[self validateText:[dict objectForKey:shop_Destination_ID]];
                         
                         NSString *s__Telephone=@"";
                         s__Telephone=[self validateText:[dict objectForKey:shop_Telephone]];
                         
                         NSString *s__FB_URL=@"";
                         s__FB_URL=[self validateText:[dict objectForKey:shop_FB_URL]];
                         
                         NSString *s__Floor=@"";
                         s__Floor=[self validateText:[dict objectForKey:shop_Floor]];
                         
                         NSString *s__Nearest_Parking=@"";
                         s__Nearest_Parking=[self validateText:[dict objectForKey:shop_Nearest_Parking]];
                         
                         NSString *s__Summary=@"";
                         s__Summary=[self validateText:[dict objectForKey:shop_Summary]];
                         
                         NSString *s__Image=@"";
                         s__Image=[self validateText:[dict objectForKey:shop_Image]];
                         
                         NSString *s__Title=@"";
                         s__Title=[self validateText:[dict objectForKey:shop_Title]];
                         
                         NSString *s__Twitter_URL=@"";
                         s__Twitter_URL=[self validateText:[dict objectForKey:shop_Twitter_URL]];
                         
                         NSData *imageList,*featureProduct;
                         if([[dict objectForKey:shop_ImagesList]isKindOfClass:[NSArray class]])
                         {
                             NSArray *array=[dict objectForKey:shop_ImagesList];
                             imageList = [NSKeyedArchiver archivedDataWithRootObject:array];
                         }
                         if([[dict objectForKey:shop_Featured_Products]isKindOfClass:[NSArray class]])
                         {
                             NSArray *array=[dict objectForKey:shop_Featured_Products];
                             featureProduct = [NSKeyedArchiver archivedDataWithRootObject:array];
                         }
                         
                         [db executeUpdate:@"INSERT OR REPLACE INTO ShoppingListing (ID,Description,Destination_ID,Shop_Telephone,FB_URL,Floor,Shop_Nearest_Parking,Summary,Image,Shop_Title,Twitter_URL,Cat_ID,CatName,ImagesList,Featured_Products) values (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)",s__ID,s__Description,s__Destination_ID,s__Telephone,s__FB_URL,s__Floor,s__Nearest_Parking,s__Summary,s__Image,s__Title,s__Twitter_URL,catId,CategoryName,imageList,featureProduct];
                     }
                 }
             }
         }
     }];
}
+(BOOL)insertFavData:(NSString *)strType ID:(NSNumber*)favId title:(NSString *)strTitle
{
    __block BOOL success=NO;
    FMDatabaseQueue *queueObj=[self GetDBQueue];
    [queueObj inTransaction:^(FMDatabase *db, BOOL *rollback)
     {
         success= [db executeUpdate:@"INSERT OR REPLACE INTO FavouriteList(ID,Type,Title)values(?,?,?)",favId,strType,strTitle];
     }];
    if(success)
    {
        [[AppDelegate appDelegateInstance]PlayProgressHudAsPerType:FAV_ADDED View:nil];
    }
    return  success;
}
+(NSMutableArray *)FavouriteList
{
    NSMutableArray *arrFavArray=[[NSMutableArray alloc]init];
    FMDatabaseQueue *dbQueue =[self GetDBQueue];
    [dbQueue inDatabase:^(FMDatabase *db)
     {
         FMResultSet *fmResultSetObj ;
         fmResultSetObj=[db executeQuery:@"SELECT * FROM FavouriteList ORDER BY Title COLLATE NOCASE ASC"];
         while ([fmResultSetObj next])
         {
             FavouriteListModel *model=[[FavouriteListModel alloc]init];
             model.favID=[NSNumber numberWithInteger:[fmResultSetObj intForColumn:search_ID]];
             model.favType=[fmResultSetObj stringForColumn:search_Type];
             model.strTitle=[fmResultSetObj stringForColumn:search_Title];
             [arrFavArray addObject:model];
         }
         [fmResultSetObj close];
     } ];
    return arrFavArray;
}
+(FavouriteListModel *)getFav:(NSNumber *)strID Type:(NSString *)strType
{
    __block FavouriteListModel *model;
    FMDatabaseQueue *dbQueue =[self GetDBQueue];
    [dbQueue inDatabase:^(FMDatabase *db)
     {
         FMResultSet *fmResultSetObj ;
         fmResultSetObj=[db executeQuery:@"SELECT * FROM FavouriteList where ID=? and Type=? ORDER BY Title COLLATE NOCASE ASC",strID,strType];
         while ([fmResultSetObj next])
         {
             model=[[FavouriteListModel alloc]init];
             model.favID=[NSNumber numberWithInteger:[fmResultSetObj intForColumn:search_ID]];
             model.favType=[fmResultSetObj stringForColumn:search_Type];
             model.strTitle=[fmResultSetObj stringForColumn:search_Title];
         }
         [fmResultSetObj close];
     } ];
    return  model;
}
+(BOOL)deleteFav:(NSString *)strType ID:(NSNumber *)favId
{
    __block BOOL success=NO;
    FMDatabaseQueue *queueObj=[self GetDBQueue];
    [queueObj inTransaction:^(FMDatabase *db, BOOL *rollback)
     {
         success= [db executeUpdate:@"delete from FavouriteList where ID=? and Type=?",favId,strType];
     }];
    if(success)
    {
        [[AppDelegate appDelegateInstance]PlayProgressHudAsPerType:FAV_DELETED View:nil];
    }
    return  success;
    
}
+(NSMutableArray *)getSearchData:(NSString *)strKeyword index:(NSUInteger)count noOfRecord:(int)noOfRecords
{
    NSMutableArray *arrFavArray=[[NSMutableArray alloc]init];
    FMDatabaseQueue *dbQueue =[self GetDBQueue];
    [dbQueue inDatabase:^(FMDatabase *db)
     {
         int totalCount=0;
         FMResultSet *fmResultSetObj1;
         fmResultSetObj1=[db executeQuery:[NSString stringWithFormat:@"SELECT count(*) FROM SearchTable WHERE Title LIKE '%@%%'",strKeyword]];
         if([fmResultSetObj1 next])
         {
             totalCount=[fmResultSetObj1 intForColumnIndex:0];
         }
         [fmResultSetObj1 close];
         
         if(totalCount>=count && totalCount!=0)
         {
             FMResultSet *fmResultSetObj ;
             
             NSString *strQuery=[NSString stringWithFormat:@"SELECT * FROM SearchTable WHERE Title LIKE '%@%%'  LIMIT %d OFFSET %lu",strKeyword,noOfRecords,(unsigned long)count];
             
             fmResultSetObj=[db executeQuery:strQuery];
             while ([fmResultSetObj next])
             {
                 SearchModel *model=[[SearchModel alloc]init];
                 model.search_ID=[fmResultSetObj stringForColumn:search_ID];
                 model.search_Title=[fmResultSetObj stringForColumn:search_Title];
                 model.search_Type=[fmResultSetObj stringForColumn:search_Type];
                 [arrFavArray addObject:model];
             }
             [fmResultSetObj close];
         }
     }];
    return arrFavArray;
}

+(ShopAllDataModel *)getStoreData
{
    __block  ShopAllDataModel *shopModel=[[ShopAllDataModel alloc]init];
    FMDatabaseQueue *dbQueue =[self GetDBQueue];
    [dbQueue inDatabase:^(FMDatabase *db)
     {
         NSMutableDictionary *shopDict=[self getShop:db];
         NSMutableDictionary *dine=[self getDine:db];
         NSMutableDictionary *entDict=[self getEntertainmentList:db];
         
         shopModel.arrShopsAll=[shopDict objectForKey:@"AllData"];
         shopModel.arrShopsName=[shopDict objectForKey:@"NameArray"];
         
         shopModel.arrDineName=[dine objectForKey:@"NameArray"];
         shopModel.arrDineAll=[dine objectForKey:@"AllData"];
         
         shopModel.arrEntertainmentsAll=[entDict objectForKey:@"AllData"];
         shopModel.arrEntertainmentsName=[entDict objectForKey:@"NameArray"];
         
         shopModel.arrshopsCategory=[self getCategory:APP_SHOP DBObj:db];
         shopModel.arrDineCategory=[self getCategory:APP_DINE  DBObj:db];
     }];
    return shopModel;
}

+(NSMutableArray *)getCategory:(NSString *)strType DBObj:(FMDatabase *)db
{
    NSMutableArray *arrData=[[NSMutableArray alloc]init];
    
    FMResultSet *fmResultSetObj ;
    if([strType isEqualToString:APP_DINE])
    {
        fmResultSetObj=[db executeQuery: @"SELECT * FROM DiningCategories ORDER BY CategoryName COLLATE NOCASE ASC"];
    }
    else
    {
        fmResultSetObj=[db executeQuery: @"SELECT * FROM ShoppingCategories ORDER BY CategoryName COLLATE NOCASE ASC"];
    }
    while ([fmResultSetObj next])
    {
        CategoryModel *model=[[CategoryModel alloc]init];
        model.catID=[fmResultSetObj intForColumn:Cate_ID];
        model.categoryName=[fmResultSetObj stringForColumn:Category_Name];
        [arrData addObject:model];
    }
    [fmResultSetObj close];
    return arrData;
}
+(NSMutableDictionary *)getShop:(FMDatabase *)db
{
    NSMutableArray *arrAllData=[[NSMutableArray alloc]init];
    NSMutableArray *arrUniqueName=[[NSMutableArray alloc]init];
    NSMutableSet *dataSet=[[NSMutableSet alloc]init];
    NSMutableDictionary *dictData=[[NSMutableDictionary alloc]init];
    
    
    FMResultSet *fmResultSetObj ;
    fmResultSetObj=[db executeQuery: @"SELECT * FROM ShoppingListing ORDER BY Shop_Title COLLATE NOCASE ASC"];
    while ([fmResultSetObj next])
    {
        ShopBaseModel *model=[self getShopModel:fmResultSetObj];
        [arrAllData addObject:model];
        if(![dataSet containsObject:model.strTitle])
        {
            [dataSet addObject:model.strTitle];
            [arrUniqueName addObject:model];
        }
    }
    [fmResultSetObj close];
    [dictData setObject:arrAllData forKey:@"AllData"];
    [dictData setObject:arrUniqueName forKey:@"NameArray"];
    return dictData;
}
+(ShopBaseModel *)getShopById:(NSNumber *)shopID
{
    __block ShopBaseModel *model;
    FMDatabaseQueue *dbQueue =[self GetDBQueue];
    [dbQueue inDatabase:^(FMDatabase *db)
     {
         FMResultSet *fmResultSetObj ;
         fmResultSetObj=[db executeQuery: @"SELECT * FROM ShoppingListing where ID=?",shopID];
         while ([fmResultSetObj next])
         {
             model=[self getShopModel:fmResultSetObj];
         }
         [fmResultSetObj close];
     } ];
    return  model;
}
+(ShopBaseModel *)getShopModel:(FMResultSet *)fmResultSetObj
{
    ShopBaseModel *model=[[ShopBaseModel alloc]init];
    model.catID=[fmResultSetObj intForColumn:@"Cat_ID"];
    model.categoryName=[fmResultSetObj stringForColumn:@"CatName"];
    model.numID=[NSNumber numberWithInteger:[fmResultSetObj intForColumn:shop_ID]];
    model.strDescription=[fmResultSetObj stringForColumn:shop_Description];
    model.strDestination_ID=[fmResultSetObj stringForColumn:shop_Destination_ID];
    model.strTelephone=[fmResultSetObj stringForColumn:shop_Telephone];
    
    model.strFBURL=[fmResultSetObj stringForColumn:shop_FB_URL];
    model.strFloor=[fmResultSetObj stringForColumn:shop_Floor];
    model.strNearest_Parking=[fmResultSetObj stringForColumn:shop_Nearest_Parking];
    model.strSummary=[fmResultSetObj stringForColumn:shop_Summary];
    model.strImage=[fmResultSetObj stringForColumn:shop_Image];
    model.strTitle=[fmResultSetObj stringForColumn:shop_Title];
    model.strTwitterUrL=[fmResultSetObj stringForColumn:shop_Twitter_URL];
    
    NSData *imageData = [fmResultSetObj dataForColumn:shop_ImagesList];
    NSData *featureData = [fmResultSetObj dataForColumn:shop_Featured_Products];
    
    model.arrImageList= [self decodeString:imageData];
    model.arrayFeatureProduct=[self decodeString:featureData];
    
    model.strType=APP_SHOP;
    return model;
}

+(NSArray *)decodeString:(NSData *)data
{
    if(data!=nil && data.length>0)
    {
        NSArray *array = [NSKeyedUnarchiver unarchiveObjectWithData:data];
        return  array;
    }
    return nil;
    
}
+(NSMutableDictionary *)getDine:(FMDatabase *)db
{
    NSMutableArray *arrAllData=[[NSMutableArray alloc]init];
    NSMutableArray *arrUniqueName=[[NSMutableArray alloc]init];
    NSMutableSet *dataSet=[[NSMutableSet alloc]init];
    NSMutableDictionary *dictData=[[NSMutableDictionary alloc]init];
    
    FMResultSet *fmResultSetObj ;
    fmResultSetObj=[db executeQuery: @"SELECT * FROM DiningListing ORDER BY Title COLLATE NOCASE ASC"];
    while ([fmResultSetObj next])
    {
        ShopBaseModel *model=[self getDineModel:fmResultSetObj];
        [arrAllData addObject:model];
        if(![dataSet containsObject:model.strTitle])
        {
            [dataSet addObject:model.strTitle];
            [arrUniqueName addObject:model];
        }
    }
    [fmResultSetObj close];
    [dictData setObject:arrAllData forKey:@"AllData"];
    [dictData setObject:arrUniqueName forKey:@"NameArray"];
    return dictData;
}
+(ShopBaseModel *)getDineById:(NSNumber *)dineID
{
    __block ShopBaseModel *model;
    FMDatabaseQueue *dbQueue =[self GetDBQueue];
    [dbQueue inDatabase:^(FMDatabase *db)
     {
         FMResultSet *fmResultSetObj ;
         fmResultSetObj=[db executeQuery: @"SELECT * FROM DiningListing where ID=?",dineID];
         while ([fmResultSetObj next])
         {
             model=[self getDineModel:fmResultSetObj];
         }
         [fmResultSetObj close];
     } ];
    return  model;
}
+(ShopBaseModel *)getDineModel:(FMResultSet *)fmResultSetObj
{
    ShopBaseModel *model=[[ShopBaseModel alloc]init];
    model.catID=[fmResultSetObj intForColumn:@"Cat_ID"];
    model.categoryName=[fmResultSetObj stringForColumn:@"CatName"];
    model.numID=[NSNumber numberWithInteger:[fmResultSetObj intForColumn:dining_ID]];
    model.strTitle=[fmResultSetObj stringForColumn:dining_Title];
    model.strFloor=[fmResultSetObj stringForColumn:dining_Floor];
    model.strNearest_Parking=[fmResultSetObj stringForColumn:dining_Nearest_Parking];
    
    model.strTelephone=[fmResultSetObj stringForColumn:dining_Telephone];
    model.strDestination_ID=[fmResultSetObj stringForColumn:dining_Destination_ID];
    model.strSummary=[fmResultSetObj stringForColumn:dining_Summary];
    model.strShop_Code=[fmResultSetObj stringForColumn:dining_Shop_Code];
    model.strFloor_ID=[fmResultSetObj stringForColumn:dining_Floor_ID];
    model.strStore_Timings=[fmResultSetObj stringForColumn:dining_Store_Timings];
    model.strLandmark=[fmResultSetObj stringForColumn:dining_Landmark];
    model.strImage=[fmResultSetObj stringForColumn:dining_Thumbnail];
    model.strFBURL=[fmResultSetObj stringForColumn:dining_FB_URL];
    model.strTwitterUrL=[fmResultSetObj stringForColumn:dining_Twitter_URL];
    model.strDescription=[fmResultSetObj stringForColumn:dining_Description];
    model.strLogo=[fmResultSetObj stringForColumn:dining_Logo];
    
    NSData *imageData = [fmResultSetObj dataForColumn:shop_ImagesList];
    NSData *featureData = [fmResultSetObj dataForColumn:shop_Featured_Products];
    
    model.arrImageList= [self decodeString:imageData];
    model.arrayFeatureProduct=[self decodeString:featureData];
    
    model.strType=APP_DINE;
    return  model;
}

+(NSMutableDictionary *)getEntertainmentList:(FMDatabase *)db
{
    NSMutableArray *arrAllData=[[NSMutableArray alloc]init];
    NSMutableArray *arrUniqueName=[[NSMutableArray alloc]init];
    NSMutableSet *dataSet=[[NSMutableSet alloc]init];
    NSMutableDictionary *dictData=[[NSMutableDictionary alloc]init];
    
    FMResultSet *fmResultSetObj ;
    fmResultSetObj=[db executeQuery: @"SELECT * FROM EntertainmentList ORDER BY Title COLLATE NOCASE ASC"];
    while ([fmResultSetObj next])
    {
        ShopBaseModel *model=[self getEntlistModel:fmResultSetObj];
        [arrAllData addObject:model];
        if(![dataSet containsObject:model.strTitle])
        {
            [dataSet addObject:model.strTitle];
            [arrUniqueName addObject:model];
        }
    }
    [fmResultSetObj close];
    [dictData setObject:arrAllData forKey:@"AllData"];
    [dictData setObject:arrUniqueName forKey:@"NameArray"];
    return dictData;
}
+(ShopBaseModel *)getEntListById:(NSNumber *)entID
{
    __block ShopBaseModel *model;
    FMDatabaseQueue *dbQueue =[self GetDBQueue];
    [dbQueue inDatabase:^(FMDatabase *db)
     {
         FMResultSet *fmResultSetObj ;
         fmResultSetObj=[db executeQuery: @"SELECT * FROM EntertainmentList where ID=?",entID];
         while ([fmResultSetObj next])
         {
             model=[self getEntlistModel:fmResultSetObj];
         }
         [fmResultSetObj close];
     } ];
    return  model;
}
+(ShopBaseModel *)getEntlistModel:(FMResultSet *)fmResultSetObj
{
    ShopBaseModel *model=[[ShopBaseModel alloc]init];
    model.numID=[NSNumber numberWithInteger:[fmResultSetObj intForColumn:entertainment_ID]];
    model.catID=[fmResultSetObj intForColumn:@"Cat_ID"];
    model.categoryName=[fmResultSetObj stringForColumn:@"CatName"];
    model.strDestination_ID=[fmResultSetObj stringForColumn:entertainment_Destination_ID];
    model.strDescription=[fmResultSetObj stringForColumn:entertainment_Description];
    model.strTelephone=[fmResultSetObj stringForColumn:entertainment_Telephone];
    model.strFBURL=[fmResultSetObj stringForColumn:entertainment_FB_URL];
    model.strFloor=[fmResultSetObj stringForColumn:entertainment_Floor];
    model.strNearest_Parking=[fmResultSetObj stringForColumn:entertainment_Nearest_Parking];
    model.strShop_Code=[fmResultSetObj stringForColumn:entertainment_Shop_Code];
    model.strStore_Timings=[fmResultSetObj stringForColumn:entertainment_Store_Timings];
    model.strSummary=[fmResultSetObj stringForColumn:entertainment_Summary];
    model.strImage=[fmResultSetObj stringForColumn:entertainment_Thumbnail];
    model.strTitle=[fmResultSetObj stringForColumn:entertainment_Title];
    model.strTwitterUrL=[fmResultSetObj stringForColumn:entertainment_Twitter_Page_URL];
    model.strType=APP_ENTERTAINMENT;
    
    NSData *imageData = [fmResultSetObj dataForColumn:shop_ImagesList];
    NSData *featureData = [fmResultSetObj dataForColumn:shop_Featured_Products];
    
    model.arrImageList= [self decodeString:imageData];
    model.arrayFeatureProduct=[self decodeString:featureData];
    
    return  model;
}
+(NSMutableArray *)getHotels
{
    NSMutableArray *arrBaseArry=[[NSMutableArray alloc]init];
    FMDatabaseQueue *dbQueue =[self GetDBQueue];
    [dbQueue inDatabase:^(FMDatabase *db)
     {
         FMResultSet *fmResultSetObj ;
         fmResultSetObj=[db executeQuery:@"SELECT * FROM HotelsListing ORDER BY Hotel_Name COLLATE NOCASE ASC"];
         while ([fmResultSetObj next])
         {
             BaseModel *model=[self getHotelsModel:fmResultSetObj];
             [arrBaseArry addObject:model];
         }
         [fmResultSetObj close];
     } ];
    return arrBaseArry;
}
+(BaseModel *)getHotelsModel:(FMResultSet *)fmResultSetObj
{
    BaseModel *model=[[BaseModel alloc]init];
    model.tblId=[NSNumber numberWithInteger:[fmResultSetObj intForColumn:hotelsListing_ID]];
    model.strType=APP_HOTEL;
    model.strTitle=[fmResultSetObj stringForColumn:hotelsListing_Name];
    model.strDesc=[fmResultSetObj stringForColumn:hotelsListing_Description];
    model.strTelephone=[fmResultSetObj stringForColumn:hotelsListing_Telephone];
    model.strImageURL=[fmResultSetObj stringForColumn:hotelsListing_Image];
    model.strURL=[fmResultSetObj stringForColumn:hotelsListing_URL];
    return  model;
}
+(BaseModel *)getHotelsById:(NSNumber *)hotelsId
{
    __block BaseModel *model;
    FMDatabaseQueue *dbQueue =[self GetDBQueue];
    [dbQueue inDatabase:^(FMDatabase *db)
     {
         FMResultSet *fmResultSetObj ;
         fmResultSetObj=[db executeQuery: @"SELECT * FROM HotelsListing where ID=?",hotelsId];
         while ([fmResultSetObj next])
         {
             model=[self getHotelsModel:fmResultSetObj];
         }
         [fmResultSetObj close];
     } ];
    return  model;
}

+(NSMutableArray *)getOffer
{
    NSMutableArray *arrBaseArry=[[NSMutableArray alloc]init];
    FMDatabaseQueue *dbQueue =[self GetDBQueue];
    [dbQueue inDatabase:^(FMDatabase *db)
     {
         FMResultSet *fmResultSetObj ;
//         fmResultSetObj=[db executeQuery:@"SELECT * FROM OfferListing"];
         fmResultSetObj=[db executeQuery:@"SELECT * FROM OfferListing ORDER BY Title COLLATE NOCASE ASC"];

         while ([fmResultSetObj next])
         {
             BaseModel *model=[self getOfferBaseModel:fmResultSetObj];
             [arrBaseArry addObject:model];
         }
         [fmResultSetObj close];
     } ];
    return arrBaseArry;
}

+(NSMutableArray *)getOfferByStoreID:(NSNumber *)storeID
{
    NSMutableArray *arrBaseArry=[[NSMutableArray alloc]init];
    FMDatabaseQueue *dbQueue =[self GetDBQueue];
    [dbQueue inDatabase:^(FMDatabase *db)
     {
         FMResultSet *fmResultSetObj ;
         fmResultSetObj=[db executeQuery:@"SELECT * FROM OfferListing where Store_ID=?",storeID];
         while ([fmResultSetObj next])
         {
             BaseModel *model=[self getOfferBaseModel:fmResultSetObj];
             [arrBaseArry addObject:model];
         }
         [fmResultSetObj close];
     } ];
    return arrBaseArry;
}
+(BaseModel *)getOfferData:(NSString *)strID
{
    __block  BaseModel *model;
    FMDatabaseQueue *dbQueue =[self GetDBQueue];
    [dbQueue inDatabase:^(FMDatabase *db)
     {
         FMResultSet *fmResultSetObj ;
         fmResultSetObj=[db executeQuery:@"SELECT * FROM OfferListing where ID=?",[NSNumber numberWithInteger:[strID integerValue]]];
         while ([fmResultSetObj next])
         {
             model=[self getOfferBaseModel:fmResultSetObj];
         }
         [fmResultSetObj close];
     } ];
    return model;
}
+(BaseModel *)getOfferBaseModel:(FMResultSet *)fmResultSetObj
{
    BaseModel *model=[[BaseModel alloc]init];
    model.tblId=[NSNumber numberWithInteger:[fmResultSetObj intForColumn:event_ID]];
    model.strTitle=[fmResultSetObj stringForColumn:offerListing_Title];
    model.strType=[fmResultSetObj stringForColumn:offerListing_Type];
    model.strDesc=[fmResultSetObj stringForColumn:offerListing_Description];
    model.strStartDate=[fmResultSetObj stringForColumn:offerListing_Start_Date];
    model.strEndDate=[fmResultSetObj stringForColumn:offerListing_End_Date];
    model.strImageURL=[fmResultSetObj stringForColumn:offerListing_Image_URL];
    model.strType=APP_OFFER;
    model.tblStore_ID=[NSNumber numberWithInteger:[fmResultSetObj intForColumn:offerListing_Store_ID]];
    model.tblCategory_ID=[NSNumber numberWithInteger:[fmResultSetObj intForColumn:offerListing_Category_ID]];
    model.strCategory_Name=[fmResultSetObj stringForColumn:offerListing_Category_Name];
    return  model;
}
+(BaseModel *)getEventBaseModel:(FMResultSet *)fmResultSetObj
{
    BaseModel *model=[[BaseModel alloc]init];
    model.tblId=[NSNumber numberWithInteger:[fmResultSetObj intForColumn:event_ID]];
    model.strTitle=[fmResultSetObj stringForColumn:event_Title];
    model.strType=[fmResultSetObj stringForColumn:event_Type];
    model.strDesc=[fmResultSetObj stringForColumn:event_Short_Description];
    model.strStartDate=[fmResultSetObj stringForColumn:event_Start_Date];
    model.strEndDate=[fmResultSetObj stringForColumn:event_End_Date];
    model.strDetailsDesc=[fmResultSetObj stringForColumn:event_Details];
    model.strImageURL=[fmResultSetObj stringForColumn:event_Image];
    model.strType=APP_EVENTS;
    return  model;
}
+(BaseModel *)getEventById:(NSNumber *)eventID
{
    __block  BaseModel *baseObj;
    FMDatabaseQueue *dbQueue =[self GetDBQueue];
    [dbQueue inDatabase:^(FMDatabase *db)
     {
         FMResultSet *fmResultSetObj ;
         fmResultSetObj=[db executeQuery:@"SELECT * FROM Events where ID=?",eventID];
         while ([fmResultSetObj next])
         {
             baseObj=[self getEventBaseModel:fmResultSetObj];
         }
         [fmResultSetObj close];
     } ];
    return baseObj;
}
+(NSMutableArray *)getEvent
{
    NSMutableArray *arrBaseArry=[[NSMutableArray alloc]init];
    FMDatabaseQueue *dbQueue =[self GetDBQueue];
    [dbQueue inDatabase:^(FMDatabase *db)
     {
         FMResultSet *fmResultSetObj ;
         fmResultSetObj=[db executeQuery:@"SELECT * FROM Events ORDER BY Title COLLATE NOCASE ASC"];
         while ([fmResultSetObj next])
         {
             BaseModel *model=[self getEventBaseModel:fmResultSetObj];
             [arrBaseArry addObject:model];
         }
         [fmResultSetObj close];
     } ];
    return arrBaseArry;
}
+(NSMutableArray *)getServiceListing
{
    NSMutableArray *arrBaseArry=[[NSMutableArray alloc]init];
    FMDatabaseQueue *dbQueue =[self GetDBQueue];
    [dbQueue inDatabase:^(FMDatabase *db)
     {
         FMResultSet *fmResultSetObj ;
         fmResultSetObj=[db executeQuery:@"SELECT * FROM ServiceListing"];
         while ([fmResultSetObj next])
         {
             ServicesModel *model=[self getServices:fmResultSetObj Type:APP_SERVICES];
             [arrBaseArry addObject:model];
         }
         [fmResultSetObj close];
     } ];
    return arrBaseArry;
}

+(ServicesModel *)getServicesById:(NSNumber *)serviceid
{
    __block  ServicesModel *ServicesModel;
    FMDatabaseQueue *dbQueue =[self GetDBQueue];
    [dbQueue inDatabase:^(FMDatabase *db)
     {
         FMResultSet *fmResultSetObj ;
         fmResultSetObj=[db executeQuery:@"SELECT * FROM ServiceListing where ID=?",serviceid];
         while ([fmResultSetObj next])
         {
             ServicesModel=[self getServices:fmResultSetObj Type:APP_SERVICES];
         }
         [fmResultSetObj close];
     } ];
    return ServicesModel;
}
+(NSMutableArray *)getATMListing
{
    NSMutableArray *arrBaseArry=[[NSMutableArray alloc]init];
    FMDatabaseQueue *dbQueue =[self GetDBQueue];
    [dbQueue inDatabase:^(FMDatabase *db)
     {
         FMResultSet *fmResultSetObj ;
         fmResultSetObj=[db executeQuery:@"SELECT * FROM ATMListing ORDER BY Title COLLATE NOCASE ASC"];
         while ([fmResultSetObj next])
         {
             ServicesModel *model=[self getServices:fmResultSetObj Type:APP_ATM];
             [arrBaseArry addObject:model];
         }
         [fmResultSetObj close];
     } ];
    return arrBaseArry;
}


+(ServicesModel *)getATMById:(NSNumber *)atmid
{
    __block  ServicesModel *ServicesModel;
    FMDatabaseQueue *dbQueue =[self GetDBQueue];
    [dbQueue inDatabase:^(FMDatabase *db)
     {
         FMResultSet *fmResultSetObj ;
         fmResultSetObj=[db executeQuery:@"SELECT * FROM ATMListing where ID=?",atmid];
         while ([fmResultSetObj next])
         {
             ServicesModel=[self getServices:fmResultSetObj Type:APP_ATM];
         }
         [fmResultSetObj close];
     } ];
    return ServicesModel;
}

+(ServicesModel *)getServices:(FMResultSet *)fmResultSetObj Type:(NSString *)strType
{
    ServicesModel *model=[[ServicesModel alloc]init];
    model.tblId=[NSNumber numberWithInteger:[fmResultSetObj intForColumn:serviceListing_ID]];
    model.strTitle=[fmResultSetObj stringForColumn:serviceListing_Title];
    model.strPageDetail=[fmResultSetObj stringForColumn:serviceListing_PageDetail];
    model.strImage_URL=[fmResultSetObj stringForColumn:serviceListing_Image];
    model.strType=strType;
    return model ;
}
+(NSMutableArray *)getTheMallData :(NSString *)strType
{
    NSMutableArray *arrBaseArry=[[NSMutableArray alloc]init];
    FMDatabaseQueue *dbQueue =[self GetDBQueue];
    [dbQueue inDatabase:^(FMDatabase *db)
     {
         FMResultSet *fmResultSetObj ;
         fmResultSetObj=[db executeQuery:@"SELECT * FROM TheMall WHERE Title =?",strType];
         while ([fmResultSetObj next])
         {
             TheMallModel *model=[[TheMallModel alloc] init];
             model.theMall_ID = [NSNumber numberWithInteger:[fmResultSetObj intForColumn:@"ID"]];
             model.theMall_Titile = [fmResultSetObj stringForColumn:@"Title"];
             model.theMall_PageDetail = [fmResultSetObj stringForColumn:@"PageDetail"];
             model.theMall_Image = [fmResultSetObj stringForColumn:@"Image"];
             [arrBaseArry addObject:model];
         }
         [fmResultSetObj close];
     }];
    return arrBaseArry;
}
+(SearchModel *)getSearchModel:(FMResultSet *)fmResultSetObj
{
    SearchModel *model=[[SearchModel alloc]init];
    model.search_Title=[fmResultSetObj stringForColumn:search_Title];
    model.search_Type=[fmResultSetObj stringForColumn:search_Type];
    model.search_ID=[fmResultSetObj stringForColumn:search_ID];
    return model;
}

+(SearchModel *)getSearchDataById:(NSNumber *)searchID
{
    __block SearchModel *model;
    FMDatabaseQueue *dbQueue =[self GetDBQueue];
    [dbQueue inDatabase:^(FMDatabase *db)
     {
         FMResultSet *fmResultSetObj ;
         fmResultSetObj=[db executeQuery: @"SELECT * FROM SearchTable where ID=?",searchID];
         while ([fmResultSetObj next])
         {
             model=[self getSearchModel:fmResultSetObj];
         }
         [fmResultSetObj close];
     } ];
    return  model;
}

#pragma mark getMyParkingData
+(NSMutableArray *)getMyParkingData
{
    NSMutableArray *arrBaseArry=[[NSMutableArray alloc]init];
    FMDatabaseQueue *dbQueue =[self GetDBQueue];
    [dbQueue inDatabase:^(FMDatabase *db)
     {
         FMResultSet *fmResultSetObj ;
         fmResultSetObj=[db executeQuery:[NSString stringWithFormat:@"SELECT * FROM MyParking"]];
         while ([fmResultSetObj next])
         {
             MyParkingModel *model=[[MyParkingModel alloc] init];
             model.strDesc = [fmResultSetObj stringForColumn:@"ParkingDescription"];
             model.strPath = [fmResultSetObj stringForColumn:@"ParkingImagePath"];
             model.strDate = [fmResultSetObj stringForColumn:@"ParkingDate"];
             [arrBaseArry addObject:model];
         }
         [fmResultSetObj close];
     }];
    return arrBaseArry;
}

#pragma mark insertParkingDetail
+(BOOL)insertParkingDetail:(NSMutableDictionary *) dict {
    
    __block BOOL success=NO;
    FMDatabaseQueue *queueObj=[self GetDBQueue];
    [queueObj inTransaction:^(FMDatabase *db, BOOL *rollback)
     {
         NSString *desc=[self validateText:[dict valueForKey:@"desc"]];
         NSString *path=[self validateText:[dict valueForKey:@"path"]];
         NSString *date=[self validateText:[dict valueForKey:@"ParkingDate"]];
         success = [db executeUpdate:@"INSERT OR REPLACE INTO MyParking (ParkingDescription, ParkingImagePath,ParkingDate)values(?,?,?)",desc,path,date];
     }];
    
    return  success;
}

#pragma mark updateParkingDetail
+(BOOL)updateParkingDetail:(NSMutableDictionary *) dict {
    
    __block BOOL success=NO;
    FMDatabaseQueue *queueObj=[self GetDBQueue];
    [queueObj inTransaction:^(FMDatabase *db, BOOL *rollback)
     {
         NSString *date=[self validateText:[dict valueForKey:@"ParkingDate"]];
         NSString *desc=[self validateText:[dict valueForKey:@"desc"]];
         NSString *path_new=[self validateText:[dict valueForKey:@"path"]];
         NSString *path_old=[self validateText:[dict valueForKey:@"path_old"]];
         success = [db executeUpdate:@"UPDATE MyParking SET ParkingDescription = ?, ParkingImagePath = ?,ParkingDate = ? WHERE ParkingImagePath = ?", desc, path_new, date,path_old];
     }];
    return  success;
}

#pragma mark deleteParkingDetail
+(BOOL)deleteParkingDetail:(NSString *) path {
    __block BOOL success=NO;
    FMDatabaseQueue *queueObj=[self GetDBQueue];
    [queueObj inTransaction:^(FMDatabase *db, BOOL *rollback)
     {
         success = [db executeUpdate:@"DELETE FROM MyParking WHERE ParkingImagePath = ?",path];
     }];
    return  success;
}



@end




