//
//  DBOpreation.h
//  MOE
//
//  Created by Nivrutti on 6/15/15.
//  Copyright (c) 2015 Neosoft. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ShopAllDataModel.h"
@class FMDatabaseQueue,FMDatabase,FMResultSet,BaseModel,FavouriteListModel,ShopBaseModel,ServicesModel,SearchModel;

@interface DBOpreation : NSObject
+(FMDatabaseQueue *)GetDBQueue;

+(void)createSqlTables;
+(void)deleteDataFromlocal;
+(void)insertDineCategories:(NSArray *)mutArray;
+(void)insertTheMallData:(NSArray *)mutArray;
+(void)insertEvents:(NSArray *)mutArray;
+(void)insertEntertainmentList:(NSArray *)mutArray;
+(void)insertHotelList:(NSArray *)mutArray;
+(void)insertOfferList:(NSArray *)mutArray;
+(void)insertGuestServicesPages:(NSArray *)mutArray;
+(void)insertShoppingCategories:(NSArray *)mutArray;
+(void)insertDineListing:(NSArray *)mutArray;
+(void)insertShoppingListing:(NSArray *)mutArray;
+(void)insertATMList:(NSArray *)mutArray;
+(void)copyAndCreateSearchData;
+(id)validateText:(id)text;

+(NSMutableArray *)getSearchData:(NSString *)strKeyword index:(NSUInteger)count noOfRecord:(int)noOfRecords;
+(NSMutableArray *)getEvent;
+(NSMutableArray *)getOffer;
+(NSMutableArray *)getHotels;
 +(NSMutableArray *)FavouriteList;
+(NSMutableArray *)getServiceListing;
+(NSMutableArray *)getTheMallData :(NSString *)strType;
+(NSMutableArray *)getOfferByStoreID:(NSNumber *)storeID;
+(NSMutableArray *)getATMListing;
+(NSMutableArray *)getMyParkingData;


+(ShopAllDataModel *)getStoreData;
+(ShopBaseModel *)getShopById:(NSNumber *)shopID;
+(ShopBaseModel *)getEntListById:(NSNumber *)entID;
+(ShopBaseModel *)getDineById:(NSNumber *)dineID;
+(BaseModel *)getEventById:(NSNumber *)eventID;
+(BaseModel *)getHotelsById:(NSNumber *)hotelsId;
+(BaseModel *)getOfferData:(NSString *)strID;
+(FavouriteListModel *)getFav:(NSNumber *)strID Type:(NSString *)strType;
+(ServicesModel *)getServicesById:(NSNumber *)serviceid;
+(ServicesModel *)getATMById:(NSNumber *)atmid;
+(SearchModel *)getSearchModel:(FMResultSet *)fmResultSetObj;
+(SearchModel *)getSearchDataById:(NSNumber *)searchID;


+(BOOL)insertParkingDetail:(NSMutableDictionary *) dict;
+(BOOL)updateParkingDetail:(NSMutableDictionary *) dict;
+(BOOL)deleteParkingDetail:(NSString *) path;
+(BOOL)deleteFav:(NSString *)strType ID:(NSNumber*)favId ;
+(BOOL)insertFavData:(NSString *)strType ID:(NSNumber*)favId title:(NSString *)strTitle;



@end
