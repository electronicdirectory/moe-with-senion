//
//  webViewController.h
//  mobilizer
//
//  Created by Mohanad Kholy on 4/4/13.
//
//

#import <UIKit/UIKit.h>

@interface WebViewController : UIViewController<UIWebViewDelegate>
{
}
@property ( nonatomic)   BOOL hideBottom;

@property (retain, nonatomic) IBOutlet UIWebView *webView;
@property (retain, nonatomic)   NSString  *strType;
@property (retain,nonatomic)NSURL* url;
@property(nonatomic,strong) IBOutlet UIActivityIndicatorView *activityView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *bottomViewHeightConstraint;
@property (retain,nonatomic)NSString* htmlString;

@property(nonatomic,strong) IBOutlet UIBarButtonItem *next;
@property(nonatomic,strong) IBOutlet UIBarButtonItem *prev;
@property (nonatomic, retain) IBOutlet UIToolbar *toolbar;
@end
