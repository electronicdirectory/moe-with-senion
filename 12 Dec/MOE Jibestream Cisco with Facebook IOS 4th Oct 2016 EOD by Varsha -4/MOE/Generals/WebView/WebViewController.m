

#import "WebViewController.h"
@interface WebViewController ()
{
    BOOL isTranslucent;
}
@end


@implementation WebViewController
@synthesize url;
@synthesize webView;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}
 
- (void)viewDidLoad
{
    [super viewDidLoad];
    
    isTranslucent=self.navigationController.navigationBar.translucent;
    if(isTranslucent)
    {
        self.navigationController.navigationBar.translucent=NO;
    }
    webView.scalesPageToFit=YES;
    self.navigationItem.titleView = [CommanMethods getLabel:[self.strType uppercaseString]];

    if ([self.navigationController.viewControllers count]==1)
    {
        self.navigationItem.leftBarButtonItem =[CommanMethods getLeftButton:self Action:@selector(menuMethod)];
    }
    if([self.strType  isEqualToString:screenHeader_Movie])
    {
        [GoogleAnalyticsHandler sendScreenName:screenName_Movie];
    }
    else if([self.strType  isEqualToString:screenHeader_StoreLocator])
    {
        [GoogleAnalyticsHandler sendScreenName:screenName_StoreLocator];
    }
    else if([self.strType  isEqualToString:screenHeader_SocialWall])
    {
        [GoogleAnalyticsHandler sendScreenName:screenName_SocialWall];
    }
    else if([self.strType  isEqualToString:screenHeader_BigBus])
    {
        [GoogleAnalyticsHandler sendScreenName:screenName_BigBus];
    }
    else if([self.strType  isEqualToString:screenHeader_MetroTiming])
    {
        [GoogleAnalyticsHandler sendScreenName:screenName_MetroTiming];
    }
    else if([self.strType  isEqualToString:screenHeader_Whats_Happening])
    {
        [GoogleAnalyticsHandler sendScreenName:screenName_Whats_Happening];
    }
    if(self.hideBottom)
    {
        _bottomViewHeightConstraint.constant=0;
        _toolbar.hidden=YES;
    }
    if(url)
    {
        dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
        dispatch_async(queue, ^{
            
            NSString *strURL=[url absoluteString];
            strURL=[strURL stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding ];
            NSURL *finalURL=[NSURL URLWithString:strURL];
            [self.webView loadRequest:[NSURLRequest requestWithURL:finalURL cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:60.0]];
        });
    }
    else
    {
        [GoogleAnalyticsHandler sendScreenName:self.strType];
        self.webView.opaque = NO;
        self.webView.backgroundColor = [UIColor clearColor];
        [self.webView loadHTMLString:_htmlString baseURL:nil];
    }
}
#pragma mark - Updating the UI
- (void)updateButtons
{
    self.next.enabled = self.webView.canGoForward;
    self.prev.enabled = self.webView.canGoBack;
}
#pragma mark - UIWebViewDelegate
- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType
{
    return YES;
}
- (void)webViewDidStartLoad:(UIWebView *)webView
{
    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
    [self.activityView startAnimating];
    [self updateButtons];
}
- (void)webViewDidFinishLoad:(UIWebView *)webView
{
    [self.activityView stopAnimating];
    [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
    [self updateButtons];
}
- (void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error
{
    [self.activityView stopAnimating];
    [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
    [self updateButtons];
}
-(void)viewDidDisappear:(BOOL)animated
{
    if(isTranslucent)
    {
        self.navigationController.navigationBar.translucent=YES;
    }
    
    [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
    self.webView.delegate = nil;
    [self.activityView stopAnimating];
    

}
- (void)menuMethod {
    [self.sideMenuViewController presentLeftMenuViewController];
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
@end
