//
//  CommanMethods.h
//  MOE
//
//  Created by Nivrutti on 6/11/15.
//  Copyright (c) 2015 Neosoft. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CommanMethods : NSObject

+(NSMutableDictionary*)getPlistDictionary;
+(NSMutableDictionary*)getPlistDictionaryForSideMenu;

+(NSArray *)getSideMenuFields;

+(NSMutableDictionary *)getShoeSizes;
+(NSMutableDictionary *)getFashionSizes;
+(NSArray *)getNationalities;
+(NSArray *)getResident;

+ (BOOL)validateEmail:(NSString *)email;
+ (void)designTextField:(UITextField *)txt;
+(UIImage *)getNavigationBarImage;
+(void)getShoeSizeConvertorBgImage:(UIView*)view;
+(void)getFashionBgImage:(UIView*)view;
+(UIBarButtonItem *)getLeftButton:(id)target Action:(SEL)sel;
+(UIBarButtonItem *)getRightButton:(id)target Action:(SEL)sel;
+ (void)displayAlert :(NSString *)title :(NSString *)message;
+(UILabel *)getLabel:(NSString *)text;
+(NSURL *)getImageURL:(NSString *)strImageURL;
+(NSString *)rePlaceTags:(NSString *)strText;
+(NSString *)rePlaceTagsWithPlainText:(NSString *)strText;

+(NSString *)getMapURL:(NSString  *)strDestId;
+(NSString *)getSocialURL;
+(NSString *)getBigBusURL;
+(NSString *)getMetroTimingURL;
+(NSString *)getShuttleBusURL;
+(NSString *)getMovieURL;
+(NSString *)getCheckBalanceURL;
+(NSString *)getFeedBackURL;


+(NSMutableAttributedString *)parseHTMLValue:(NSString *)strHtml;
+(UIBarButtonItem *)getRightShareButton:(id)target Action:(SEL)sel;
+(NSArray *)getAlphabets;
+(UIFont *)getFont:(float)size;
+(UIFont *)getHeaderFont;
+(UIFont *)getDetailsFont;
+(UIFont *)getDateFont;
+(UIFont *)getHomeTextFont;
+(UIFont *)getNavigationTitleFont;
+(UIFont *)getServiceTitleFont;

+(NSString *)changeDateFormat :(NSString *)dateString upperCase:(BOOL)upperCase;
+(NSString *)changeDateFormatWithTimeZone :(NSString *)dateString;
+(NSArray *)getSortedArray:(NSArray *)inputArray Key:(NSString *)strKey;

+(void)getBgImage:(UIView*)view;
- (UIImage*)imageWithImage:(UIImage*)image
              scaledToSize:(CGSize)newSize;

+(NSString *)getHtml:(NSString *)strTitle Date:(NSString *)strDate Desc:(NSString *)strDesc;
+(void)changeLinkColorForTextView:(UITextView *)textView;
+(NSString *)getServiceHtml:(NSString *)strDesc;
+(NSString *)getGettingHereHtml:(NSString *)strDesc;

+(UIColor *)getTextPlaceholderColor;
+(UIColor *)getTextColor;
+(UIColor *)getblackColor;

@end
