//
//  CommanMethods.m
//  MOE
//
//  Created by Nivrutti on 6/11/15.
//  Copyright (c) 2015 Neosoft. All rights reserved.
//

#import "CommanMethods.h"
#import "NSString+HTML.h"
#import "GTMNSString+HTML.h"

@implementation CommanMethods
static  NSDateFormatter* dateFormatter;
+(NSMutableDictionary*)getPlistDictionary
{
    NSString* plistPath = [[NSBundle mainBundle] pathForResource:@"MOE_Data" ofType:@"plist"];
    NSMutableArray *myArray = [[NSArray arrayWithContentsOfFile:plistPath] copy];
    NSMutableDictionary *dictionary = [myArray objectAtIndex:0];
    return dictionary;
}
+(NSMutableDictionary*)getPlistDictionaryForSideMenu
{
    NSString* plistPath = [[NSBundle mainBundle] pathForResource:@"MOE_Data" ofType:@"plist"];
    NSMutableArray *myArray = [[NSArray arrayWithContentsOfFile:plistPath] copy];
    NSMutableDictionary *dictionary = [myArray objectAtIndex:1];
    return dictionary;
}

+(NSArray *)getSideMenuFields
{
    NSArray *arr=[[CommanMethods getPlistDictionaryForSideMenu] objectForKey:@"sideMenu"];
    return arr;
}

+(NSMutableDictionary *)getShoeSizes
{
    NSMutableDictionary *dict=[[CommanMethods getPlistDictionary] objectForKey:@"ShoeSizeConverter"];
    return dict;
}
+(NSMutableDictionary *)getFashionSizes
{
    NSMutableDictionary *dict=[[CommanMethods getPlistDictionary] objectForKey:@"FashionSizeConverter"];
    return dict;
}
+(NSArray *)getResident
{
    NSArray *arr=[NSArray arrayWithObjects:@"United Arab of Emirates",@"Bahrain",@"Oman",@"Lebanon",@"Egypt",nil];
    return arr;
}
+(NSArray *)getNationalities
{
    NSArray *arr=[[CommanMethods getPlistDictionary] objectForKey:@"Nationalities"];
    return arr;
}
+ (void)displayAlert :(NSString *)title :(NSString *)message {
    
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:title message:message delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
    [alert show];
}
+(UILabel *)getLabel:(NSString *)text
{
    UILabel *label=[[UILabel alloc] init];
    if ([text.uppercaseString isEqualToString:@"Hotels".uppercaseString]) {
        text = text.uppercaseString;
    }
    label.text = text;

    label.textAlignment = NSTextAlignmentCenter;
    label.font=[self getNavigationTitleFont];
    label.backgroundColor = [UIColor clearColor];
    label.frame = CGRectMake(0, 7, 100, 30);
    label.textColor=[CommanMethods getTextColor];
    return label;
}
+(NSURL *)getImageURL:(NSString *)strImageURL
{
    if([strImageURL isKindOfClass:[NSString class]])
    {
        if(strImageURL!=nil && strImageURL.length>0)
        {
            NSString *strURL=[Web_Host_Url stringByAppendingString:strImageURL];
            strURL=[strURL stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding ];
            return  [NSURL URLWithString:strURL];
        }
    }
    return  nil;
}
+(UIImage *)getNavigationBarImage
{
    return  [UIImage   imageNamed:@"navBar"];
}
+(UIBarButtonItem *)getRightButton:(id)target Action:(SEL)sel
{
    UIBarButtonItem *btnSearch = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemSearch target:target action:sel];
    return btnSearch;
}
+(UIBarButtonItem *)getLeftButton:(id)target Action:(SEL)sel
{
    UIBarButtonItem *leftButton = [[UIBarButtonItem alloc] initWithTitle:nil style:UIBarButtonItemStylePlain target:target action:sel];
    leftButton.image=[UIImage imageNamed:@"sidebar6"];
    return leftButton;
}
+(NSString *)rePlaceTagsWithPlainText:(NSString *)strText
{
    strText=[self rePlaceTags:strText];
    strText=[strText stringByConvertingHTMLToPlainText];
    return  strText;
}
+(NSString *)rePlaceTags:(NSString *)strText
{
    strText =[strText stringByReplacingOccurrencesOfString:@"&lt;" withString:@"<"];
    strText =[strText stringByReplacingOccurrencesOfString:@"&gt;" withString:@">"];
    strText =[strText stringByReplacingOccurrencesOfString:@"&39#;" withString:@"'"];
    strText =[strText stringByReplacingOccurrencesOfString:@"&amp;nbsp;" withString:@" "];
    strText =[strText stringByReplacingOccurrencesOfString:@"&nbsp;" withString:@" "];
    strText =[strText stringByReplacingOccurrencesOfString:@"rsquo;" withString:@"’"];
    strText =[strText stringByReplacingOccurrencesOfString:@"&quot;" withString:@"'"];
    strText =[strText stringByReplacingOccurrencesOfString:@"&amp;" withString:@"&"];
    strText =[strText stringByReplacingOccurrencesOfString:@"&cent;" withString:@"¢"];
    strText =[strText stringByReplacingOccurrencesOfString:@"&pound;" withString:@"£"];
    strText =[strText stringByReplacingOccurrencesOfString:@"&yen;" withString:@"¥"];
    strText =[strText stringByReplacingOccurrencesOfString:@"&euro;" withString:@"€"];
    strText =[strText stringByReplacingOccurrencesOfString:@"&copy;" withString:@"©"];
    strText =[strText stringByReplacingOccurrencesOfString:@"&reg;" withString:@"®"];
    strText =[strText stringByReplacingOccurrencesOfString:@"&#39;" withString:@","];
    strText =[strText stringByReplacingOccurrencesOfString:@"&nbsp;" withString:@" "];
    strText =[strText stringByReplacingOccurrencesOfString:@"&iexcl;" withString:@"¡"];
    strText =[strText stringByReplacingOccurrencesOfString:@"&cent;" withString:@"¢"];
    strText =[strText stringByReplacingOccurrencesOfString:@"&pound;" withString:@"£"];
    strText =[strText stringByReplacingOccurrencesOfString:@"&curren;" withString:@"¤"];
    strText =[strText stringByReplacingOccurrencesOfString:@"&yen;" withString:@"¥"];
    strText =[strText stringByReplacingOccurrencesOfString:@"&brvbar;" withString:@"¦"];
    strText =[strText stringByReplacingOccurrencesOfString:@"&sect;" withString:@"§"];
    strText =[strText stringByReplacingOccurrencesOfString:@"&uml;" withString:@"¨"];
    strText =[strText stringByReplacingOccurrencesOfString:@"&copy;" withString:@"©"];
    strText =[strText stringByReplacingOccurrencesOfString:@"&ordf;" withString:@"ª"];
    strText =[strText stringByReplacingOccurrencesOfString:@"&laquo;" withString:@"«"];
    strText =[strText stringByReplacingOccurrencesOfString:@"&not;" withString:@"¬"];
    strText =[strText stringByReplacingOccurrencesOfString:@"&shy;" withString:@""];
    strText =[strText stringByReplacingOccurrencesOfString:@"&reg;" withString:@"®"];
    strText =[strText stringByReplacingOccurrencesOfString:@"&macr;" withString:@"¯"];
    strText =[strText stringByReplacingOccurrencesOfString:@"&deg;" withString:@"°"];
    strText =[strText stringByReplacingOccurrencesOfString:@"&plusmn;" withString:@"±"];
    strText =[strText stringByReplacingOccurrencesOfString:@"&sup2;" withString:@"²"];
    strText =[strText stringByReplacingOccurrencesOfString:@"&sup3;" withString:@"³"];
    strText =[strText stringByReplacingOccurrencesOfString:@"&acute;" withString:@"´"];
    strText =[strText stringByReplacingOccurrencesOfString:@"&micro;" withString:@"µ"];
    strText =[strText stringByReplacingOccurrencesOfString:@"&para;" withString:@"¶"];
    strText =[strText stringByReplacingOccurrencesOfString:@"&middot;" withString:@"·"];
    strText =[strText stringByReplacingOccurrencesOfString:@"&cedil;" withString:@"¸"];
    strText =[strText stringByReplacingOccurrencesOfString:@"&sup1;" withString:@"¹"];
    strText =[strText stringByReplacingOccurrencesOfString:@"&ordm;" withString:@"º"];
    strText =[strText stringByReplacingOccurrencesOfString:@"&raquo;" withString:@"»"];
    strText =[strText stringByReplacingOccurrencesOfString:@"&frac14;" withString:@"¼"];
    strText =[strText stringByReplacingOccurrencesOfString:@"&frac12;" withString:@"½"];
    strText =[strText stringByReplacingOccurrencesOfString:@"&frac34;" withString:@"¾"];
    strText =[strText stringByReplacingOccurrencesOfString:@"&iquest;" withString:@"¿"];
    strText =[strText stringByReplacingOccurrencesOfString:@"&Agrave;" withString:@"À"];
    strText =[strText stringByReplacingOccurrencesOfString:@"&Aacute;" withString:@"Á"];
    strText =[strText stringByReplacingOccurrencesOfString:@"&Acirc;" withString:@"Â"];
    strText =[strText stringByReplacingOccurrencesOfString:@"&Atilde;" withString:@"Ã"];
    strText =[strText stringByReplacingOccurrencesOfString:@"&Auml;" withString:@"Ä"];
    strText =[strText stringByReplacingOccurrencesOfString:@"&Aring;" withString:@"Å"];
    strText =[strText stringByReplacingOccurrencesOfString:@"&AElig;" withString:@"Æ"];
    strText =[strText stringByReplacingOccurrencesOfString:@"&Ccedil;" withString:@"Ç"];
    strText =[strText stringByReplacingOccurrencesOfString:@"&Egrave;" withString:@"È"];
    strText =[strText stringByReplacingOccurrencesOfString:@"&Eacute;" withString:@"É"];
    strText =[strText stringByReplacingOccurrencesOfString:@"&Ecirc;" withString:@"Ê"];
    strText =[strText stringByReplacingOccurrencesOfString:@"&Euml;" withString:@"Ë"];
    strText =[strText stringByReplacingOccurrencesOfString:@"&Igrave;" withString:@"Ì"];
    strText =[strText stringByReplacingOccurrencesOfString:@"&Iacute;" withString:@"Í"];
    strText =[strText stringByReplacingOccurrencesOfString:@"&Icirc;" withString:@"Î"];
    strText =[strText stringByReplacingOccurrencesOfString:@"&Iuml;" withString:@"Ï"];
    strText =[strText stringByReplacingOccurrencesOfString:@"&ETH;" withString:@"Ð"];
    strText =[strText stringByReplacingOccurrencesOfString:@"&Ntilde;" withString:@"Ñ"];
    strText =[strText stringByReplacingOccurrencesOfString:@"&Ograve;" withString:@"Ò"];
    strText =[strText stringByReplacingOccurrencesOfString:@"&Oacute;" withString:@"Ó"];
    strText =[strText stringByReplacingOccurrencesOfString:@"&Ocirc;" withString:@"Ô"];
    strText =[strText stringByReplacingOccurrencesOfString:@"&Otilde;" withString:@"Õ"];
    strText =[strText stringByReplacingOccurrencesOfString:@"&Ouml;" withString:@"Ö"];
    strText =[strText stringByReplacingOccurrencesOfString:@"&times;" withString:@"×"];
    strText =[strText stringByReplacingOccurrencesOfString:@"&Oslash;" withString:@"Ø"];
    strText =[strText stringByReplacingOccurrencesOfString:@"&Ugrave;" withString:@"Ù"];
    strText =[strText stringByReplacingOccurrencesOfString:@"&Uacute;" withString:@"Ú"];
    strText =[strText stringByReplacingOccurrencesOfString:@"&Ucirc;" withString:@"Û"];
    strText =[strText stringByReplacingOccurrencesOfString:@"&Uuml;" withString:@"Ü"];;
    strText =[strText stringByReplacingOccurrencesOfString:@"&Yacute;" withString:@"Ý"];
    strText =[strText stringByReplacingOccurrencesOfString:@"&THORN;" withString:@"Þ"];
    strText =[strText stringByReplacingOccurrencesOfString:@"&szlig;" withString:@"ß"];
    strText =[strText stringByReplacingOccurrencesOfString:@"&agrave;" withString:@"à"];
    strText =[strText stringByReplacingOccurrencesOfString:@"&aacute;" withString:@"á"];
    strText =[strText stringByReplacingOccurrencesOfString:@"&acirc;" withString:@"â"];
    strText =[strText stringByReplacingOccurrencesOfString:@"&atilde;" withString:@"ã"];
    strText =[strText stringByReplacingOccurrencesOfString:@"&auml;" withString:@"ä"];
    strText =[strText stringByReplacingOccurrencesOfString:@"&aring;" withString:@"å"];
    strText =[strText stringByReplacingOccurrencesOfString:@"&aelig;" withString:@"æ"];
    strText =[strText stringByReplacingOccurrencesOfString:@"&ccedil;" withString:@"ç"];
    strText =[strText stringByReplacingOccurrencesOfString:@"&egrave;" withString:@"è"];
    strText =[strText stringByReplacingOccurrencesOfString:@"&eacute;" withString:@"é"];
    strText =[strText stringByReplacingOccurrencesOfString:@"&ecirc;" withString:@"ê"];
    strText =[strText stringByReplacingOccurrencesOfString:@"&euml;" withString:@"ë"];
    strText =[strText stringByReplacingOccurrencesOfString:@"&igrave;" withString:@"ì"];
    strText =[strText stringByReplacingOccurrencesOfString:@"&iacute;" withString:@"í"];
    strText =[strText stringByReplacingOccurrencesOfString:@"&icirc;" withString:@"î"];
    strText =[strText stringByReplacingOccurrencesOfString:@"&iuml;" withString:@"ï"];
    strText =[strText stringByReplacingOccurrencesOfString:@"&eth;" withString:@"ð"];
    strText =[strText stringByReplacingOccurrencesOfString:@"&ntilde;" withString:@"ñ"];
    strText =[strText stringByReplacingOccurrencesOfString:@"&ograve;" withString:@"ò"];
    strText =[strText stringByReplacingOccurrencesOfString:@"&oacute;" withString:@"ó"];
    strText =[strText stringByReplacingOccurrencesOfString:@"&ocirc;" withString:@"ô"];
    strText =[strText stringByReplacingOccurrencesOfString:@"&otilde;" withString:@"õ"];
    strText =[strText stringByReplacingOccurrencesOfString:@"&ouml;" withString:@"ö"];
    strText =[strText stringByReplacingOccurrencesOfString:@"&divide;" withString:@"÷"];
    strText =[strText stringByReplacingOccurrencesOfString:@"&oslash;" withString:@"ø"];
    strText =[strText stringByReplacingOccurrencesOfString:@"&ugrave;" withString:@"ù"];
    strText =[strText stringByReplacingOccurrencesOfString:@"&uacute;" withString:@"ú"];
    strText =[strText stringByReplacingOccurrencesOfString:@"&ucirc;" withString:@"û"];
    strText =[strText stringByReplacingOccurrencesOfString:@"&uuml;" withString:@"ü"];
    strText =[strText stringByReplacingOccurrencesOfString:@"&yacute;" withString:@"ý"];
    strText =[strText stringByReplacingOccurrencesOfString:@"&thorn;" withString:@"þ"];
    strText =[strText stringByReplacingOccurrencesOfString:@"&yuml;" withString:@"ÿ"];
    strText =[strText stringByReplacingOccurrencesOfString:@"&fnof;" withString:@"ƒ"];
    strText =[strText stringByReplacingOccurrencesOfString:@"&Alpha;" withString:@"Α"];
    strText =[strText stringByReplacingOccurrencesOfString:@"&Beta;" withString:@"Β"];
    strText =[strText stringByReplacingOccurrencesOfString:@"&Gamma;" withString:@"Γ"];
    strText =[strText stringByReplacingOccurrencesOfString:@"&Delta;" withString:@"Δ"];
    strText =[strText stringByReplacingOccurrencesOfString:@"&Epsilon;" withString:@"Ε"];
    strText =[strText stringByReplacingOccurrencesOfString:@"&Zeta;" withString:@"Ζ"];
    strText =[strText stringByReplacingOccurrencesOfString:@"&Eta;" withString:@"Η"];
    strText =[strText stringByReplacingOccurrencesOfString:@"&Theta;" withString:@"Θ"];
    strText =[strText stringByReplacingOccurrencesOfString:@"&Iota;" withString:@"Ι"];
    strText =[strText stringByReplacingOccurrencesOfString:@"&Kappa;" withString:@"Κ"];
    strText =[strText stringByReplacingOccurrencesOfString:@"&Lambda;" withString:@"Λ"];
    strText =[strText stringByReplacingOccurrencesOfString:@"&Mu;" withString:@"Μ"];
    strText =[strText stringByReplacingOccurrencesOfString:@"&Nu;" withString:@"Ν"];
    strText =[strText stringByReplacingOccurrencesOfString:@"&Xi;" withString:@"Ξ"];
    strText =[strText stringByReplacingOccurrencesOfString:@"&Omicron;" withString:@"Ο"];
    strText =[strText stringByReplacingOccurrencesOfString:@"&Pi;" withString:@"Π"];
    strText =[strText stringByReplacingOccurrencesOfString:@"&Rho;" withString:@"Ρ"];
    strText =[strText stringByReplacingOccurrencesOfString:@"&Sigma;" withString:@"Σ"];
    strText =[strText stringByReplacingOccurrencesOfString:@"&Tau;" withString:@"Τ"];
    strText =[strText stringByReplacingOccurrencesOfString:@"&Upsilon;" withString:@"Υ"];
    strText =[strText stringByReplacingOccurrencesOfString:@"&Phi;" withString:@"Φ"];
    strText =[strText stringByReplacingOccurrencesOfString:@"&Chi;" withString:@"Χ"];
    strText =[strText stringByReplacingOccurrencesOfString:@"&Psi;" withString:@"Ψ"];
    strText =[strText stringByReplacingOccurrencesOfString:@"&Omega;" withString:@"Ω"];
    strText =[strText stringByReplacingOccurrencesOfString:@"&alpha;" withString:@"α"];
    strText =[strText stringByReplacingOccurrencesOfString:@"&beta;" withString:@"β"];
    strText =[strText stringByReplacingOccurrencesOfString:@"&gamma;" withString:@"γ"];
    strText =[strText stringByReplacingOccurrencesOfString:@"&delta;" withString:@"δ"];
    strText =[strText stringByReplacingOccurrencesOfString:@"&epsilon;" withString:@"ε"];
    strText =[strText stringByReplacingOccurrencesOfString:@"&zeta;" withString:@"ζ"];
    strText =[strText stringByReplacingOccurrencesOfString:@"&eta;" withString:@"η"];
    strText =[strText stringByReplacingOccurrencesOfString:@"&theta;" withString:@"θ"];
    strText =[strText stringByReplacingOccurrencesOfString:@"&iota;" withString:@"ι"];
    strText =[strText stringByReplacingOccurrencesOfString:@"&kappa;" withString:@"κ"];
    strText =[strText stringByReplacingOccurrencesOfString:@"&lambda;" withString:@"λ"];
    strText =[strText stringByReplacingOccurrencesOfString:@"&mu;" withString:@"μ"];
    strText =[strText stringByReplacingOccurrencesOfString:@"&nu;" withString:@"ν"];
    strText =[strText stringByReplacingOccurrencesOfString:@"&xi;" withString:@"ξ"];
    strText =[strText stringByReplacingOccurrencesOfString:@"&omicron;" withString:@"ο"];
    strText =[strText stringByReplacingOccurrencesOfString:@"&pi;" withString:@"π"];
    strText =[strText stringByReplacingOccurrencesOfString:@"&rho;" withString:@"ρ"];
    strText =[strText stringByReplacingOccurrencesOfString:@"&sigmaf;" withString:@"ς"];
    strText =[strText stringByReplacingOccurrencesOfString:@"&sigma;" withString:@"σ"];
    strText =[strText stringByReplacingOccurrencesOfString:@"&tau;" withString:@"τ"];
    strText =[strText stringByReplacingOccurrencesOfString:@"&upsilon;" withString:@"υ"];
    strText =[strText stringByReplacingOccurrencesOfString:@"&phi;" withString:@"φ"];
    strText =[strText stringByReplacingOccurrencesOfString:@"&chi;" withString:@"χ"];
    strText =[strText stringByReplacingOccurrencesOfString:@"&psi;" withString:@"ψ"];
    strText =[strText stringByReplacingOccurrencesOfString:@"&omega;" withString:@"ω"];
    strText =[strText stringByReplacingOccurrencesOfString:@"&thetasym;" withString:@"ϑ"];
    strText =[strText stringByReplacingOccurrencesOfString:@"&upsih;" withString:@"ϒ"];
    strText =[strText stringByReplacingOccurrencesOfString:@"&piv;" withString:@"ϖ"];
    strText =[strText stringByReplacingOccurrencesOfString:@"&bull;" withString:@"•"];
    strText =[strText stringByReplacingOccurrencesOfString:@"&hellip;" withString:@"…"];
    strText =[strText stringByReplacingOccurrencesOfString:@"&prime;" withString:@"′"];
    strText =[strText stringByReplacingOccurrencesOfString:@"&Prime;" withString:@"″"];
    strText =[strText stringByReplacingOccurrencesOfString:@"&oline;" withString:@"‾"];
    strText =[strText stringByReplacingOccurrencesOfString:@"&frasl;" withString:@"⁄"];
    strText =[strText stringByReplacingOccurrencesOfString:@"&weierp;" withString:@"℘"];
    strText =[strText stringByReplacingOccurrencesOfString:@"&image;" withString:@"ℑ"];
    strText =[strText stringByReplacingOccurrencesOfString:@"&real;" withString:@"ℜ"];
    strText =[strText stringByReplacingOccurrencesOfString:@"&trade;" withString:@"™"];
    strText =[strText stringByReplacingOccurrencesOfString:@"&alefsym;" withString:@"ℵ"];
    strText =[strText stringByReplacingOccurrencesOfString:@"&larr;" withString:@"←"];
    strText =[strText stringByReplacingOccurrencesOfString:@"&uarr;" withString:@"↑"];
    strText =[strText stringByReplacingOccurrencesOfString:@"&rarr;" withString:@"→"];
    strText =[strText stringByReplacingOccurrencesOfString:@"&darr;" withString:@"↓"];
    strText =[strText stringByReplacingOccurrencesOfString:@"&harr;" withString:@"↔"];
    strText =[strText stringByReplacingOccurrencesOfString:@"&crarr;" withString:@"↵"];
    strText =[strText stringByReplacingOccurrencesOfString:@"&lArr;" withString:@"⇐"];
    strText =[strText stringByReplacingOccurrencesOfString:@"&uArr;" withString:@"⇑"];
    strText =[strText stringByReplacingOccurrencesOfString:@"&rArr;" withString:@"⇒"];
    strText =[strText stringByReplacingOccurrencesOfString:@"&dArr;" withString:@"⇓"];
    strText =[strText stringByReplacingOccurrencesOfString:@"&hArr;" withString:@"⇔"];
    strText =[strText stringByReplacingOccurrencesOfString:@"&forall;" withString:@"∀"];
    strText =[strText stringByReplacingOccurrencesOfString:@"&part;" withString:@"∂"];
    strText =[strText stringByReplacingOccurrencesOfString:@"&exist;" withString:@"∃"];
    strText =[strText stringByReplacingOccurrencesOfString:@"&empty;" withString:@"∅"];
    strText =[strText stringByReplacingOccurrencesOfString:@"&nabla;" withString:@"∇"];
    strText =[strText stringByReplacingOccurrencesOfString:@"&isin;" withString:@"∈"];
    strText =[strText stringByReplacingOccurrencesOfString:@"&notin;" withString:@"∉"];
    strText =[strText stringByReplacingOccurrencesOfString:@"&ni;" withString:@"∋"];
    strText =[strText stringByReplacingOccurrencesOfString:@"&prod;" withString:@"∏"];
    strText =[strText stringByReplacingOccurrencesOfString:@"&sum;" withString:@"∑"];
    strText =[strText stringByReplacingOccurrencesOfString:@"&minus;" withString:@"−"];
    strText =[strText stringByReplacingOccurrencesOfString:@"&lowast;" withString:@"∗"];
    strText =[strText stringByReplacingOccurrencesOfString:@"&radic;" withString:@"√"];
    strText =[strText stringByReplacingOccurrencesOfString:@"&prop;" withString:@"∝"];
    strText =[strText stringByReplacingOccurrencesOfString:@"&infin;" withString:@"∞"];
    strText =[strText stringByReplacingOccurrencesOfString:@"&ang;" withString:@"∠"];
    strText =[strText stringByReplacingOccurrencesOfString:@"&and;" withString:@"∧"];
    strText =[strText stringByReplacingOccurrencesOfString:@"&or;" withString:@"∨"];
    strText =[strText stringByReplacingOccurrencesOfString:@"&cap;" withString:@"∩"];
    strText =[strText stringByReplacingOccurrencesOfString:@"&cup;" withString:@"∪"];
    strText =[strText stringByReplacingOccurrencesOfString:@"&int;" withString:@"∫"];
    strText =[strText stringByReplacingOccurrencesOfString:@"&there4;" withString:@"∴"];
    strText =[strText stringByReplacingOccurrencesOfString:@"&sim;" withString:@"∼"];
    strText =[strText stringByReplacingOccurrencesOfString:@"&cong;" withString:@"≅"];
    strText =[strText stringByReplacingOccurrencesOfString:@"&asymp;" withString:@"≈"];
    strText =[strText stringByReplacingOccurrencesOfString:@"&ne;" withString:@"≠"];
    strText =[strText stringByReplacingOccurrencesOfString:@"&equiv;" withString:@"≡"];
    strText =[strText stringByReplacingOccurrencesOfString:@"&le;" withString:@"≤"];
    strText =[strText stringByReplacingOccurrencesOfString:@"&ge;" withString:@"≥"];
    strText =[strText stringByReplacingOccurrencesOfString:@"&sub;" withString:@"⊂"];
    strText =[strText stringByReplacingOccurrencesOfString:@"&sup;" withString:@"⊃"];
    strText =[strText stringByReplacingOccurrencesOfString:@"&nsub;" withString:@"⊄"];
    strText =[strText stringByReplacingOccurrencesOfString:@"&sube;" withString:@"⊆"];
    strText =[strText stringByReplacingOccurrencesOfString:@"&supe;" withString:@"⊇"];
    strText =[strText stringByReplacingOccurrencesOfString:@"&oplus;" withString:@"⊕"];
    strText =[strText stringByReplacingOccurrencesOfString:@"&otimes;" withString:@"⊗"];
    strText =[strText stringByReplacingOccurrencesOfString:@"&perp;" withString:@"⊥"];
    strText =[strText stringByReplacingOccurrencesOfString:@"&sdot;" withString:@"⋅"];
    strText =[strText stringByReplacingOccurrencesOfString:@"&lceil;" withString:@"⌈"];
    strText =[strText stringByReplacingOccurrencesOfString:@"&rceil;" withString:@"⌉"];
    strText =[strText stringByReplacingOccurrencesOfString:@"&lfloor;" withString:@"⌊"];
    strText =[strText stringByReplacingOccurrencesOfString:@"&rfloor;" withString:@"⌋"];
    strText =[strText stringByReplacingOccurrencesOfString:@"&lang;" withString:@"〈"];
    strText =[strText stringByReplacingOccurrencesOfString:@"&rang;" withString:@"〉"];
    strText =[strText stringByReplacingOccurrencesOfString:@"&loz;" withString:@"◊"];
    strText =[strText stringByReplacingOccurrencesOfString:@"&spades;" withString:@"♠"];
    strText =[strText stringByReplacingOccurrencesOfString:@"&hearts;" withString:@"♣"];
    strText =[strText stringByReplacingOccurrencesOfString:@"&hearts;" withString:@"♥"];
    strText =[strText stringByReplacingOccurrencesOfString:@"&diams;" withString:@"♦"];
    return  strText;
}

+(NSMutableAttributedString *)parseHTMLValue:(NSString *)strHtml{
    
    NSMutableAttributedString *desc=[[NSMutableAttributedString alloc]init];
    NSDictionary *attrs1 = @{ NSForegroundColorAttributeName : [CommanMethods getTextColor], NSFontAttributeName : [CommanMethods getDetailsFont] };
    NSMutableAttributedString *attributedString1 = [[NSMutableAttributedString alloc] initWithData:[strHtml dataUsingEncoding:NSUnicodeStringEncoding] options:@{ NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType} documentAttributes:nil error:nil];
    desc = attributedString1;
    [attributedString1 addAttributes:attrs1 range:NSMakeRange(0, [desc length])];
    desc = attributedString1;
    return  desc;
}
+(NSArray *)getSortedArray:(NSArray *)inputArray Key:(NSString *)strKey
{
    NSSortDescriptor *sortDescriptor =
    [NSSortDescriptor sortDescriptorWithKey:strKey
                                  ascending:YES
                                   selector:@selector(caseInsensitiveCompare:)];
    [inputArray sortedArrayUsingDescriptors:@[sortDescriptor]];
    return inputArray;
}
+ (BOOL)validateEmail:(NSString *)email {
    
    NSString *emailRegex = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
    NSPredicate *emailTest =[NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    BOOL myStringMatchesRegEx=[emailTest evaluateWithObject:email];
    
    if(myStringMatchesRegEx)
        return YES;
    else
        return NO;
}
+ (void)designTextField:(UITextField *)txt {
    
    UIColor *color = [CommanMethods getTextPlaceholderColor];
    txt.layer.borderColor = [CommanMethods getTextColor].CGColor;
    txt.layer.borderWidth = 0.5;
    txt.textColor = [CommanMethods getTextColor];
    txt.attributedPlaceholder = [[NSAttributedString alloc] initWithString:txt.placeholder attributes:@{NSForegroundColorAttributeName: color}];
}
+(UIBarButtonItem *)getRightShareButton:(id)target Action:(SEL)sel
{
    UIBarButtonItem *shareButton = [[UIBarButtonItem alloc] initWithTitle:nil style:UIBarButtonItemStylePlain target:target action:sel];
    shareButton.image=[UIImage imageNamed:@"ico-share6"];
    return shareButton;
}
+(NSArray *)getAlphabets
{
    static id instance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        instance = [[NSArray alloc] initWithObjects:@"#",@"A", @"B", @"C", @"D", @"E", @"F", @"G", @"H", @"I", @"J", @"K", @"L", @"M", @"N", @"O", @"P", @"Q", @"R", @"S", @"T", @"U", @"V", @"W", @"X", @"Y", @"Z", nil];
    });
    return instance;
}
+(UIFont *)getFont:(float)size
{
    if(IS_IPHONE_6P)
    {
        size+=2.5;
    }
    else if(IS_IPHONE_6)
    {
        size+=1.75f;
    }
    else if(IS_IPHONE_5)
    {
        size+=1.0;
    }
    UIFont *font = [UIFont fontWithName:@"CenturyGothic" size:size];
    return  font;
}
+(UIFont *)getNavigationTitleFont
{
    return  [self getFont:16.0f];
}
+(UIFont *)getHeaderFont
{
    return  [self getFont:14.0f];
}
+(UIFont *)getDetailsFont
{
    return  [self getFont:12.0f];
}
+(UIFont *)getDateFont
{
    return  [self getFont:10.0f];
}
+(UIFont *)getHomeTextFont
{
    return  [self getFont:8.0f];
}
+(UIFont *)getServiceTitleFont
{
    if(IS_IPHONE_6P || IS_IPHONE_6)
    {
        return  [self getFont:12.50f ];
    }
    return  [self getFont:10.0f];
}
+(NSString *)changeDateFormat :(NSString *)dateString upperCase:(BOOL)upperCase
{
    if(!dateFormatter)
    {
        dateFormatter=[[NSDateFormatter alloc] init];
    }
    dateFormatter.dateFormat = @"dd/MM/yyyy";
    NSDate *date = [dateFormatter dateFromString: dateString];
    dateFormatter.dateFormat = @"dd MMM yyyy";
    if([dateFormatter stringFromDate:date]!=nil)
    {
        return [dateFormatter stringFromDate:date];
    }
    return  @"";
}
+(NSString *)appendDateSuffix:(NSString *)dateString upperCase:(BOOL)upperCase
{
    NSArray *arrayWithTwoStrings = [dateString componentsSeparatedByString:@"-"];
    NSString *suffix;
    int date=(int) [[arrayWithTwoStrings objectAtIndex:0]integerValue]%10;
    if (date ==1) {
        suffix = @"st";
    } else if (date ==2){
        suffix = @"nd";
    } else if (date ==3){
        suffix = @"rd";
    } else {
        suffix = @"th";
    }
    if (upperCase)
        return [NSString stringWithFormat:@"%@ %@ %@",[arrayWithTwoStrings objectAtIndex:0],[[arrayWithTwoStrings objectAtIndex:1]uppercaseString],[arrayWithTwoStrings objectAtIndex:2 ]];
    else
        return  [NSString stringWithFormat:@"%@%@ %@",[arrayWithTwoStrings objectAtIndex:0],suffix,[arrayWithTwoStrings objectAtIndex:1]];
    
}
+(NSString *)changeDateFormatWithTimeZone :(NSString *)dateString
{
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SS"];
    NSDate *newDate = [formatter dateFromString:dateString];
    if (newDate == nil)
        formatter.dateFormat = @"yyyy-MM-dd'T'HH:mm:ss";
    NSDate * myDate = [formatter dateFromString:dateString];
    formatter.dateFormat = @"dd-MMMM-yyyy";
    NSString *newString = [formatter stringFromDate:myDate];
    return [self appendDateSuffixWithTimezone:newString];
}
+(NSString *)appendDateSuffixWithTimezone:(NSString *)dateString
{
    NSArray *arrayWithTwoStrings = [dateString componentsSeparatedByString:@"-"];
    NSString *suffix;
    int date=(int) [[arrayWithTwoStrings objectAtIndex:0]integerValue]%10;
    if (date ==1) {
        suffix = @"st";
    } else if (date ==2){
        suffix = @"nd";
    } else if (date ==3){
        suffix = @"rd";
    } else {
        suffix = @"th";
    }
    return  [NSString stringWithFormat:@"%@%@ %@ %@",[arrayWithTwoStrings objectAtIndex:0],suffix,[arrayWithTwoStrings objectAtIndex:1], [arrayWithTwoStrings objectAtIndex:2]];
}
+(void)getBgImage:(UIView*)view
{
    UIImageView *imageView=[[UIImageView alloc]initWithFrame:view.frame];
    imageView.image=[UIImage imageNamed:@"background6"];
    [view addSubview:imageView];
    [view sendSubviewToBack:imageView];
}
+(void)getShoeSizeConvertorBgImage:(UIView*)view
{
    UIImageView *imageView=[[UIImageView alloc]initWithFrame:view.frame];
    imageView.image=[UIImage imageNamed:@"sho-sizebg"];
    [view addSubview:imageView];
    [view sendSubviewToBack:imageView];
}
+(void)getFashionBgImage:(UIView*)view
{
    UIImageView *imageView=[[UIImageView alloc]initWithFrame:view.frame];
    imageView.image=[UIImage imageNamed:@"fashion-conv-bg"];
    [view addSubview:imageView];
    [view sendSubviewToBack:imageView];
}
+(NSString *)getSocialURL
{
    if([AppDelegate appDelegateInstance].strSocialWall!=nil)
    {
        return [AppDelegate appDelegateInstance].strSocialWall;
    }
    return [NSString stringWithFormat:@"%@%@",DEFAULT_WEBSITE_URL,DEFAULT_SOCIALURL];
}
+(NSString *)getBigBusURL
{
    if([AppDelegate appDelegateInstance].strBigBusURL!=nil)
    {
        return [AppDelegate appDelegateInstance].strBigBusURL;
    }
    return DEFAULT_BUS_URL;
}

+(NSString *)getMetroTimingURL
{
    if([AppDelegate appDelegateInstance].strMetroTiming!=nil)
    {
        return [AppDelegate appDelegateInstance].strMetroTiming;
    }
    return [self getURL:DEFAULT_METRO_TIMING];
}
+(NSString *)getShuttleBusURL
{
    return [self getURL:DEFAULT_SHUTTLE_BUS];
}
+(NSString *)getMovieURL
{
    return [self getURL:DEFAULT_MOVIE_URL];
}
+(NSString *)getCheckBalanceURL
{
    return [self getURL:CHECKBALANCEURL];
}
+(NSString *)getFeedBackURL
{
    return [self getURL:DEFAULT_FEEDBACK];
}
+(NSString *)getURL:(NSString *)strType
{
    return [NSString stringWithFormat:@"%@%@",DEFAULT_WEBSITE_URL,strType];
}
+(NSString *)getMapURL:(NSString  *)strDestId
{
    NSString *strURL=nil;
    if([AppDelegate appDelegateInstance].strStoreLocatorURL!=nil)
    {
        strURL=[AppDelegate appDelegateInstance].strStoreLocatorURL;
    }
    else
    {
        strURL=DEFAULT_STORE_LOCATOR;
    }
    return  [NSString stringWithFormat:@"%@destId=%@",strURL,strDestId];
}
- (UIImage*)imageWithImage:(UIImage*)image
              scaledToSize:(CGSize)newSize;
{
    UIGraphicsBeginImageContext( newSize );
    [image drawInRect:CGRectMake(0,0,newSize.width,newSize.height)];
    UIImage* newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return newImage;
}
+(NSString *)getGettingHereHtml:(NSString *)strDesc
{
    NSString *strHTML= [NSString stringWithFormat:@"<html><head><style type=\"text/css\">"
                        "iframe{width:100%% !important; border:none !important;}"
                        "a {color:#93eeff; text-decoration:underline}"
                        "table{border:2px solid #ffffff; width:100%%; margin:0px 0;}"
                        "table td{border-right:2px solid #ffffff; padding:5px 8px; border-bottom:2px solid #ffffff;}"
                        "table tr td:last-child{border-right:none;}"
                        "table tr:last-child td{border-bottom:none;}"
                        "table{width:100%% !important;}"
                        " body {width:100%%; font-family:CenturyGothic;font-weight:inherit; text-align:left; color: #ffffff;font-size:16px;margin:0}"
                        " h3 {font-family:CenturyGothic-Bold;font-size:18px;}"
                        "</style>"
                        "<meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">"
                        "</head>"
                        "<body>"
                        "%@"
                        "</body>"
                        "</html>",strDesc];
    return strHTML;
}
+(NSString *)getServiceHtml:(NSString *)strDesc
{
    NSString *strHTML= [NSString stringWithFormat:@"<html><head><style type=\"text/css\">"
                        "iframe{width:100%% !important; border:none !important;}"
                        "a {color:#93eeff; text-decoration:underline}"
                        "table{border:2px solid #ffffff; width:100%%; margin:0px 0;}"
                        "table td{border-right:2px solid #ffffff; padding:5px 8px; border-bottom:2px solid #ffffff;}"
                        "table tr td:last-child{border-right:none;}"
                        "table tr:last-child td{border-bottom:none;}"
                        "table{width:100%% !important;}"
                        " body {width:95%%; font-family:CenturyGothic;font-weight:inherit; text-align:left; color: #ffffff;font-size:16px;}"
                        " h3 {font-family:CenturyGothic-Bold;font-size:18px;}"
                        "</style>"
                        "<meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">"
                        "</head>"
                        "<body>"
                        "%@"
                        "</body>"
                        "</html>",strDesc];
    return strHTML;
}

+(NSString *)getHtml:(NSString *)strTitle Date:(NSString *)strDate Desc:(NSString *)strDesc
{
    return  [NSString stringWithFormat:@"<html><head><style type=\"text/css\">"
             "iframe{width:100%% !important; border:none !important;}"
             "a {color:#93eeff; text-decoration:underline}"
             "table{border:2px solid #ffffff; width:100%%; margin:0px 0;}"
             "table td{border-right:2px solid #ffffff; padding:5px 8px; border-bottom:2px solid #ffffff;}"
             "table tr td:last-child{border-right:none;}"
             "table tr:last-child td{border-bottom:none;}"
             "table{width:100%% !important;}"
             " body {width:95%%; font-family:CenturyGothic;font-weight:inherit; text-align:left; color: #ffffff;font-size:16px;}"
             " h3 {font-family:CenturyGothic-Bold;font-size:18px;}"
             " h1.CCIOSh1 {font-weight:normal; font-size:20px;}"
             " h2.CCIOSh2 {font-weight:normal; font-size:18px;}"
             "</style>"
             "<meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">"
             "</head>"
             "<body>"
             "<h1 class=\"ccIOSh1\">%@</h1>"
             "<h2 class=\"ccIOSh2\">%@</h2>"
             "%@"
             "</body>"
             "</html>",strTitle,strDate,strDesc];
}
+(void)changeLinkColorForTextView:(UITextView *)textView
{
    textView.linkTextAttributes = @{NSForegroundColorAttributeName: [UIColor colorWithRed:178.0/255.0 green:238.0/255.0 blue:255.0/255.0 alpha:1.0], NSUnderlineStyleAttributeName: [NSNumber numberWithInt:NSUnderlineStyleSingle]};
}
+(UIColor *)getTextColor
{
    return  [UIColor whiteColor];
}
+(UIColor *)getTextPlaceholderColor
{
    return [UIColor  colorWithWhite:1.0 alpha:0.3f];
}
+(UIColor *)getblackColor
{
    return [UIColor blackColor];
}
@end
