//
//  WebServiceCommunicator.h
//  MOE
//
//  Created by Nivrutti on 6/11/15.
//  Copyright (c) 2015 Neosoft. All rights reserved.
//
#import <Foundation/Foundation.h>



@interface WebServiceCommunicator : NSObject
typedef void (^Request_Completion_Block)(id response, NSError * error);
typedef void (^Request_Progress_Block)(NSUInteger numberOfFinishedOperations, NSUInteger totalNumberOfOperations);
typedef void (^Request_Completion_Block_Fetch)();

+ (instancetype)sharedInstance;
-(void)getCurrencyValue:(NSString *)fromString toString :(NSString *)toString  CompletionBlock :(Request_Completion_Block)completionBlock;
-(void)updateUserPushNotifications:(NSString *)strServiceName ResultRespose:(NSString *)strReponseKey ResultValKey:(NSString *)strResultKey Block:(Request_Completion_Block)completionBlock;

-(void)getSiteID:(Request_Completion_Block)completionBlock;
-(void)getListingDataFromServerUsingQueue:(Request_Progress_Block)progressBlock Completion:(Request_Completion_Block_Fetch)completionBlock;
-(void)RegisterDeviceFromServerUsingQueue:(Request_Progress_Block)progressBlock Completion:(Request_Completion_Block_Fetch)completionBlock;
-(void)GetNotificationImage:(NSString *)strServiceName ResultRespose:(NSString *)strReponseKey ResultValKey:(NSString *)strResultKey Block:(Request_Completion_Block)completionBlock;
-(void)updateOfferCategoryHits:(int)intStoreId CategoryID:(int)intCategoryId;


//SPIN & WIN
-(void)spinNwin_getUseronLogin:(NSString *)strServiceName ResultRespose:(NSString *)strReponseKey ResultValKey:(NSString *)strResultKey dataDict:(NSMutableDictionary *)dict Block:(Request_Completion_Block)completionBlock;
-(void)spinNwin_RegisterUser:(NSString *)strServiceName ResultRespose:(NSString *)strReponseKey ResultValKey:(NSString *)strResultKey DataDict:(NSMutableDictionary *) dict Block:(Request_Completion_Block)completionBlock;
-(void)spinNwin_ForgotPassword:(NSString *)strServiceName ResultRespose:(NSString *)strReponseKey ResultValKey:(NSString *)strResultKey Email:(NSString *)EMail Block:(Request_Completion_Block)completionBlock;
-(void)spinNwin_ResetPassword:(NSString *)strServiceName ResultRespose:(NSString *)strReponseKey ResultValKey:(NSString *)strResultKey dataDict:(NSMutableDictionary *)dict Block:(Request_Completion_Block)completionBlock;
-(void)spinNwin_getDrawDates:(NSString *)strServiceName ResultRespose:(NSString *)strReponseKey ResultValKey:(NSString *)strResultKey CampaignId:(int)campaignId Block:(Request_Completion_Block)completionBlock;
-(void)spinNwin_getPastWinners:(NSString *)strServiceName ResultRespose:(NSString *)strReponseKey ResultValKey:(NSString *)strResultKey CampaignId:(int)campaignId Block:(Request_Completion_Block)completionBlock;
-(void)spinNwin_getQuestionsByCampaignId:(NSString *)strServiceName ResultRespose:(NSString *)strReponseKey ResultValKey:(NSString *)strResultKey CampaignId:(int)campaignId Block:(Request_Completion_Block)completionBlock;
-(void)spinNwin_RegisterUserRightAnswer:(NSString *)strServiceName ResultRespose:(NSString *)strReponseKey ResultValKey:(NSString *)strResultKey userId:(int)userId CampaignId:(int)campaignId questionId:(int)questionId AnswerId:(int)AnswerId AttemptId:(int)AttemptId Block:(Request_Completion_Block)completionBlock;

 @end
