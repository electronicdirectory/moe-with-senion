
//
//  WebServiceCommunicator.m
//  MOE
//
//  Created by Nivrutti on 6/11/15.
//  Copyright (c) 2015 Neosoft. All rights reserved.
//

#import "WebServiceCommunicator.h"
#import "AFNetworking.h"
#import "AppDelegate.h"

@implementation WebServiceCommunicator
#pragma mark sharedInstance
+ (instancetype)sharedInstance
{
    //create single ton instance of webService class
    static id instance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        instance = [[self alloc] init];
    });
    return instance;
}
#pragma mark getServiceURLString
-(id)getServiceURLString:(NSString *)strServiceParam
{
    return [NSString stringWithFormat:@"%@/%@%@",Web_Host_Url,Web_Host_Url_Directory,strServiceParam];
}
#pragma mark getServiceSoapAction
-(id)getServiceSoapAction:(NSString *)strServiceParam
{
    return [NSString stringWithFormat:@"%@%@",Web_Host_SOAP_Action,strServiceParam];
}
#pragma mark getSiteID
-(void)getSiteID:(Request_Completion_Block)completionBlock
{
    //create siteID request perform it
    NSString  *strServerUrl=[self getServiceURLString:Service_GetSiteID];
    NSString  *strAction=[self getServiceSoapAction:Service_GetSiteID];
    NSString *strSoapBody=[NSString stringWithFormat:@"<GetSiteID xmlns=\"%@\"></GetSiteID>",Web_Host_SOAP_Action];
    //set up body and other param
    AFHTTPRequestOperation *operation=[self prepareRequest:strServerUrl Action:strAction SoapBody:strSoapBody];
    [self performOperation:operation ResultRespose:Service_GetSiteID_Response_key ResultValKey:Service_GetSiteID_Result_key CompletionBlock:completionBlock Type:Service_GetSiteID];
    //cal the service
    [operation start];
}
#pragma mark getListingDataFromServerUsingQueue
-(void)getListingDataFromServerUsingQueue:(Request_Progress_Block)progressBlock Completion:(Request_Completion_Block_Fetch)completionBlock
{
    NSMutableArray *arrayOpration=[[NSMutableArray alloc]init];
    
    //get Spot light data from server
    [arrayOpration addObject:[self getSiteIdXMLByName:Service_GetSpotlightBannerListing  ResultRespose:Service_GetSpotlightBannerListing_Response_Key ResultValKey:Service_GetSpotlightBannerListing_Result_Key]];
    
    //GetHomeEntertainmentListing
    [arrayOpration addObject:[self getSiteIdXMLByName:Service_GetHomeEntertainmentListing ResultRespose:Service_GetHomeEntListing_Response_Key ResultValKey:Service_GetHomeEntListing_Result_Key]];
    
    //Service_GetUsefulLinks
    [arrayOpration addObject:[self getMallIDXMLByName:Service_GetUsefulLinks ResultRespose:Service_GetUsefulLinks_Response_Key ResultValKey:Service_GetUsefulLinks_Result_Key]];
    
    //GetRandomMovie
    [arrayOpration addObject:[self getSiteIdXMLByName:Service_GetRandomMovie ResultRespose:Service_GetRandomMovie_Response_Key ResultValKey:Service_GetRandomMovie_Result_Key]];
    
    //getShoppingCategories
    [arrayOpration addObject:[self getSiteIdXMLByName:Service_GetShoppingCategories ResultRespose:Service_GetShoppingCategories_Response_Key ResultValKey:Service_GetShoppingCategories_Result_Key]];
    
    //Service_GetATMLocations
    [arrayOpration addObject:[self getSiteIdXMLByName:Service_GetATMLocations ResultRespose:Service_GetATMLocations_Response_Key ResultValKey:Service_GetATMLocations_Resultkey]];
   
    //cmapaign
    [arrayOpration addObject:[self spinNwin_getCampaigns:Service_SpinAndWin_getCampaigns ResultRespose:Service_SpinAndWin_getCampaigns_Response_key ResultValKey:Service_SpinAndWin_getCampaigns_Result_key Block:nil]];
    
    //getDining
    [arrayOpration addObject:[self getSiteIdXMLByName:Service_GetDiningCategories ResultRespose:Service_GetDiningCategories_Response_Key ResultValKey:Service_GetDiningCategories_Result_Key]];
    
    // GetEventsListingByNoOfEvents
    [arrayOpration addObject:[self GetEventsListingByNoOfEventsXMLByName:Service_GetEventsListingByNoOfEvents ResultRespose:Service_GetEventsListingByNoOfEvents_Response_Key ResultValKey:Service_GetEventsListingByNoOfEvents_Results_Key]];
    
    //getDineingListing
   [arrayOpration addObject:[self getCategoriesXMLByName:Service_GetDiningListing  ResultRespose:Service_GetGetDiningListing_Response_Key ResultValKey:Service_GetDiningListing_Resultkey]];
    
    //getContentPages
    [arrayOpration addObject:[self getSiteIdXMLByName:Service_GetContentPages ResultRespose:Service_GetContentPages_Response_Key ResultValKey:Service_GetContentPages_Result_Key]];
   
    //getEvents
    [arrayOpration addObject:[self getSiteIdXMLByName:Service_GetEventsListing ResultRespose:Service_GetEventsListing_Response_key ResultValKey:Service_GetEventsListing_Result_key]];
   
    //getEntertainment
    [arrayOpration addObject:[self getSiteIdXMLByName:Service_GetEntertainmentListing ResultRespose:Service_GetEntertainmentListing_Response_Key ResultValKey:Service_GetEntertainmentListing_Result_Key]];
    
    //GetHotels
    [arrayOpration addObject:[self getSiteIdXMLByName:Service_GetHotelsListing ResultRespose:Service_GetHotelsListing_Response_Key ResultValKey:Service_GetHotelsListing_Result_Key]];
    
    //getOffers
    [arrayOpration addObject:[self getSiteIdXMLByName:Service_GetOffersListing ResultRespose:Service_GetOffersListing_Response_Key ResultValKey:Service_GetOffersListing_Result_Key]];
   
    //getServiceListing
    [arrayOpration addObject:[self getSiteIdXMLByName:Service_GetGuestServicesPages ResultRespose:Service_GetGuestServicesPages_Response_Key ResultValKey:Service_GetGuestServicesPages_Result_Key]];
    
    // getShoppingListing
    [arrayOpration addObject:[self getCategoriesXMLByName:Service_GetShoppingListing ResultRespose:Service_GetShoppingListing_Response_Key ResultValKey:Service_GetShoppingListing_Result_Key]];

    //call Queue
    [self performQueueOpration:arrayOpration ProgressBlock:progressBlock Completion:completionBlock];
}
-(void)RegisterDeviceFromServerUsingQueue:(Request_Progress_Block)progressBlock Completion:(Request_Completion_Block_Fetch)completionBlock
{
    NSMutableArray *arrayOpration=[[NSMutableArray alloc]init];
    NSUserDefaults *pref = [NSUserDefaults standardUserDefaults];
    [pref synchronize];
    NSString *strDeviceToken=[pref objectForKey:device_token];
    //register Device & call get OfferList
    if(strDeviceToken!=nil)
    {
        //set up device token web service
        [arrayOpration addObject:[self setDeviceToken:Service_SetDeviceToken ResultRespose:Service_SetDeviceToken_Response_Key ResultValKey:Service_SetDeviceToken_Resultkey]];
        // get Offer list behaviours
        [arrayOpration addObject:[self GetOffersListingByUserBehaviorXMLByName:Service_GetOffersListingByUserBehavior ResultRespose:Service_GetOffersListingByUserBehavior_Response_Key ResultValKey:Service_GetOffersListingByUserBehavior_Results_Key DeviceToken:strDeviceToken]];
        //call Queue
        [self performQueueOpration:arrayOpration ProgressBlock:progressBlock Completion:completionBlock];
    }
    else
    {
        completionBlock(nil,nil);
    }
}
-(void)performQueueOpration:(NSMutableArray *)arrayOpration ProgressBlock:(Request_Progress_Block)progressBlock Completion:(Request_Completion_Block_Fetch)completionBlock
{
    NSArray *operations=[AFURLConnectionOperation batchOfRequestOperations:arrayOpration progressBlock:^(NSUInteger numberOfFinishedOperations, NSUInteger totalNumberOfOperations)
                         {
                             //update progrss block
                             progressBlock(numberOfFinishedOperations,totalNumberOfOperations);
                         } completionBlock:^(NSArray *operations)
                         {
                             //all web servcies are completed ...I am sending block base response
                             completionBlock(nil,nil);
                         }];
    //call Queue
    NSOperationQueue *operationQueue = [[NSOperationQueue alloc] init];
    [operationQueue setSuspended:YES];
    [operationQueue setMaxConcurrentOperationCount:2];
    [operationQueue addOperations:operations waitUntilFinished:NO];
    [operationQueue setSuspended:NO];
}
#pragma mark setupRequest
-(AFHTTPRequestOperation *)setupRequest:(NSString *)strServiceName ResultResponse:(NSString *)strReponseKey ResultValKey:(NSString *)strResultKey Body:(NSString *)strBody CompletionBlock:(Request_Completion_Block_Fetch)completionBlock
{
    NSString  *strServerUrl=[self getServiceURLString:strServiceName];
    NSString  *strAction=[self getServiceSoapAction:strServiceName];
    //preparing the request and return the request Object
    AFHTTPRequestOperation *operation=[self prepareRequest:strServerUrl Action:strAction SoapBody:strBody];
    [self performOperation:operation ResultRespose:strReponseKey ResultValKey:strResultKey CompletionBlock:completionBlock Type:strServiceName];
    return operation;
}
#pragma mark getSiteIdXMLByName
-(AFHTTPRequestOperation  *)getSiteIdXMLByName:(NSString *)strServiceName ResultRespose:(NSString *)strReponseKey ResultValKey:(NSString *)strResultKey
{
    AppDelegate *appDel=(AppDelegate   *)[[UIApplication sharedApplication]delegate];
    NSString *strSoapBody=[NSString stringWithFormat:@"<%@ xmlns=\"%@\"><SiteID>%ld</SiteID></%@>",strServiceName,Web_Host_SOAP_Action,(long)[appDel.strSiteID integerValue],strServiceName];
    //preparing the request and return the request Object
    return  [self setupRequest:strServiceName ResultResponse:strReponseKey ResultValKey:strResultKey Body:strSoapBody CompletionBlock:nil];
}
-(AFHTTPRequestOperation  *)getMallIDXMLByName:(NSString *)strServiceName ResultRespose:(NSString *)strReponseKey ResultValKey:(NSString *)strResultKey
{
    AppDelegate *appDel=(AppDelegate   *)[[UIApplication sharedApplication]delegate];
    NSString *strSoapBody=[NSString stringWithFormat:@"<%@ xmlns=\"%@\"><mallID>%ld</mallID></%@>",strServiceName,Web_Host_SOAP_Action,(long)[appDel.strSiteID integerValue],strServiceName];
    //preparing the request and return the request Object
    return  [self setupRequest:strServiceName ResultResponse:strReponseKey ResultValKey:strResultKey Body:strSoapBody CompletionBlock:nil];
}
#pragma mark getCategoriesXMLByName
-(AFHTTPRequestOperation  *)getCategoriesXMLByName:(NSString *)strServiceName ResultRespose:(NSString *)strReponseKey ResultValKey:(NSString *)strResultKey
{
    AppDelegate *appDel=(AppDelegate   *)[[UIApplication sharedApplication]delegate];
    NSString *strSoapBody=[NSString stringWithFormat:@"<%@ xmlns=\"%@\"><SiteID>%ld</SiteID><CatID>0</CatID></%@>",strServiceName,Web_Host_SOAP_Action,(long)[appDel.strSiteID integerValue],strServiceName];
    //preparing the request and return the request Object
    return [self setupRequest:strServiceName ResultResponse:strReponseKey ResultValKey:strResultKey Body:strSoapBody CompletionBlock:nil];
}
#pragma mark GetEventsListingByNoOfEventsXMLByName
-(AFHTTPRequestOperation  *)GetEventsListingByNoOfEventsXMLByName:(NSString *)strServiceName ResultRespose:(NSString *)strReponseKey ResultValKey:(NSString *)strResultKey
{
    AppDelegate *appDel=(AppDelegate   *)[[UIApplication sharedApplication]delegate];
    NSString *strSoapBody=[NSString stringWithFormat:@"<%@ xmlns=\"%@\"><SiteID>%ld</SiteID><noOfEvents>%d</noOfEvents></%@>",strServiceName,Web_Host_SOAP_Action,(long)[appDel.strSiteID integerValue],EVENT_COUNT,strServiceName];
    //preparing the request and return the request Object
    return [self setupRequest:strServiceName ResultResponse:strReponseKey ResultValKey:strResultKey Body:strSoapBody CompletionBlock:nil];
}
#pragma mark GetOffersListingByUserBehaviorXMLByName
-(AFHTTPRequestOperation  *)GetOffersListingByUserBehaviorXMLByName:(NSString *)strServiceName ResultRespose:(NSString *)strReponseKey ResultValKey:(NSString *)strResultKey DeviceToken:(NSString *)strDeviceToken
{
    NSUInteger offerCount=12-[HomeDataModel   sharedInstance].eventCount;
    AppDelegate *appDel=(AppDelegate   *)[[UIApplication sharedApplication]delegate];
    NSString *strSoapBody=[NSString stringWithFormat:@"<%@ xmlns=\"%@\"><SiteID>%ld</SiteID><NoOfOffers>%lu</NoOfOffers><DeviceToken>%@</DeviceToken></%@>",strServiceName,Web_Host_SOAP_Action,(long)[appDel.strSiteID integerValue],(unsigned long)offerCount,strDeviceToken,strServiceName];
    //preparing the request and return the request Object
    return [self setupRequest:strServiceName ResultResponse:strReponseKey ResultValKey:strResultKey Body:strSoapBody CompletionBlock:nil];
}
-(void)updateOfferCategoryHits:(int)intStoreId CategoryID:(int)intCategoryId
{
    NSUserDefaults *pref = [NSUserDefaults standardUserDefaults];
    [pref synchronize];
    NSString *strDeviceToken=[pref objectForKey:device_token];
    NSString *strSoapBody=[NSString stringWithFormat:@"<UpdateUserCategoryHits xmlns=\"%@\"><mallID>%d</mallID><deviceToken>%@</deviceToken><categoryID>%d</categoryID></UpdateUserCategoryHits>",Web_Host_SOAP_Action,intStoreId,strDeviceToken,intCategoryId];
    //preparing the request
    AFHTTPRequestOperation *operation=[self setupRequest:Service_UpdateUserCategoryHits ResultResponse:Service_UpdateUserCategoryHits_Response_Key ResultValKey:Service_UpdateUserCategoryHits_Result_Key Body:strSoapBody CompletionBlock:nil];
    [operation start];
 }
#pragma mark getCategoriesXMLByName
-(AFHTTPRequestOperation *)setDeviceToken:(NSString *)strServiceName ResultRespose:(NSString *)strReponseKey ResultValKey:(NSString *)strResultKey
{
    NSUserDefaults *pref = [NSUserDefaults standardUserDefaults];
    [pref synchronize];
    NSString *strDeviceToken=[pref objectForKey:device_token];
    double latitudeVal=0;
    latitudeVal=[pref doubleForKey:latitude];
    double longitudeVal=0;
    longitudeVal=[pref doubleForKey:longitude];
    
    AppDelegate *appDel=(AppDelegate   *)[[UIApplication sharedApplication]delegate];
    NSString *strSoapBody=[NSString stringWithFormat:@"<SetDeviceToken xmlns=\"%@\"><mallID>%ld</mallID><deviceToken>%@</deviceToken><AppType>%@</AppType><latitude>%f</latitude><longitude>%f</longitude></SetDeviceToken>",Web_Host_SOAP_Action,(long)[appDel.strSiteID integerValue],strDeviceToken,APP_TYPE,latitudeVal,longitudeVal];
    //preparing the request
    AFHTTPRequestOperation *operation=[self setupRequest:strServiceName ResultResponse:strReponseKey ResultValKey:strResultKey Body:strSoapBody CompletionBlock:nil];
    return operation;
}
#pragma mark GetNotificationImage
-(void)GetNotificationImage:(NSString *)strServiceName ResultRespose:(NSString *)strReponseKey ResultValKey:(NSString *)strResultKey Block:(Request_Completion_Block)completionBlock
{
    
    AppDelegate *appDel=(AppDelegate   *)[[UIApplication sharedApplication]delegate];
    NSString *strSoapBody=[NSString stringWithFormat:@"<GetNotificationImage xmlns=\"%@\"><mallID>%ld</mallID></GetNotificationImage>",Web_Host_SOAP_Action,(long)[appDel.strSiteID integerValue]];
    //preparing the request
    AFHTTPRequestOperation *operation=[self setupRequest:strServiceName ResultResponse:strReponseKey ResultValKey:strResultKey Body:strSoapBody CompletionBlock:completionBlock];
    //call the service
    [operation  start];
}

#pragma mark updateUserPushNotifications
-(void)updateUserPushNotifications:(NSString *)strServiceName ResultRespose:(NSString *)strReponseKey ResultValKey:(NSString *)strResultKey Block:(Request_Completion_Block)completionBlock
{
    [defaults synchronize];
    NSString *strDeviceToken=[defaults objectForKey:device_token];
    int Fashion_Ladies = [defaults boolForKey:NOTIFICATION_FASHION_LADIES];
    int Fashion_men = [defaults boolForKey:NOTIFICATION_FASHION_MEN];
    int Dinning = [defaults boolForKey:NOTIFICATION_DINNING];
    int Offer = [defaults boolForKey:NOTIFICATION_OFFERS];
    int Events = [defaults boolForKey:NOTIFICATION_EVENTS];
    int Weekly_1_6 = [defaults boolForKey:NOTIFICATION_WEEKLY_1_6ALERTS];
    int Weekly_6_12 = [defaults boolForKey:NOTIFICATION_WEEKLY_6_12ALERTS];
    NSString *Name = [defaults objectForKey:NOTIFICATION_USER_NAME];
    NSString *Email = [defaults objectForKey:NOTIFICATION_USER_EMAIL];
    NSString *MobileNumber = [defaults objectForKey:NOTIFICATION_USER_MOBILE];
    
    NSNumber *mrVal=[defaults objectForKey:NOTIFICATION_IS_MR];
    int maleVal;
    if(!mrVal)
    {
        maleVal=-1;
    }
    else
    {
        maleVal=[mrVal intValue];
    }
    AppDelegate *appDel=(AppDelegate   *)[[UIApplication sharedApplication]delegate];
    NSString *strSoapBody=[NSString stringWithFormat:@"<UpdateUserPushNotifications xmlns=\"%@\"><MallID>%ld</MallID><DeviceToken>%@</DeviceToken><Fashion_Ladies>%d</Fashion_Ladies><Fashion_men>%d</Fashion_men><Dinning>%d</Dinning><Offer>%d</Offer><Events>%d</Events><Weekly_1_6>%d</Weekly_1_6><Weekly_6_12>%d</Weekly_6_12><Name>%@</Name><Email>%@</Email><MobileNumber>%@</MobileNumber><isMr>%d</isMr></UpdateUserPushNotifications>",Web_Host_SOAP_Action,(long)[appDel.strSiteID integerValue], strDeviceToken, Fashion_Ladies, Fashion_men, Dinning, Offer, Events, Weekly_1_6, Weekly_6_12, Name, Email, MobileNumber, maleVal];
    //preparing the request
    AFHTTPRequestOperation *operation=[self setupRequest:strServiceName ResultResponse:strReponseKey ResultValKey:strResultKey Body:strSoapBody CompletionBlock:completionBlock];
    //call the service
    [operation  start];
}
//===============================================================================================================
#pragma mark prepareRequest
-(AFHTTPRequestOperation *)prepareRequest:(NSString *)strUrl Action:(NSString *)strAction SoapBody:(NSString *)strSoap
{
    NSString *soapBody =[NSString stringWithFormat:@"<?xml version=\"1.0\" encoding=\"utf-8\"?><soap:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:soap=\"http://schemas.xmlsoap.org/soap/envelope/\"><soap:Body>%@</soap:Body></soap:Envelope>",strSoap];
    
    NSMutableURLRequest *request =[[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:strUrl]
                                                               cachePolicy:NSURLRequestReloadIgnoringCacheData  timeoutInterval:180.0];
    
    [request setHTTPMethod:@"POST"];
    [request setHTTPBody:[soapBody dataUsingEncoding:NSUTF8StringEncoding]];
    [request addValue:strAction forHTTPHeaderField:@"SOAPAction"];
    [request addValue:@"text/xml; charset=utf-8" forHTTPHeaderField:@"Content-Type"];
    NSString *msgLength = [NSString stringWithFormat:@"%lu", (unsigned long)[soapBody length]];
    [request addValue: msgLength forHTTPHeaderField:@"Content-Length"];
    
    AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
    return operation;
}
//===============================================================================================================
#pragma mark performOperation
-(void)performOperation:(AFHTTPRequestOperation *)operation ResultRespose:(NSString *)strReponseKey ResultValKey:(NSString *)strResultKey  CompletionBlock:(Request_Completion_Block)completionBlock Type:(NSString *)strType
{
    [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject)
     {

         if([strType isEqualToString:Service_SetDeviceToken] || [strType isEqualToString:Service_UpdateUserCategoryHits] ||[strType isEqualToString:Service_UpdateUserPushNotifications])
         {
             return ;
         }
        if([responseObject isKindOfClass:[NSData class]])
         {
             //parse the XML  data and stored in dictionary
             NSDictionary *dictWholeResponse = [[NSDictionary alloc] init];
             dictWholeResponse =[XMLReader dictionaryForXMLData:responseObject error:nil];
             if([dictWholeResponse isKindOfClass:[NSDictionary class]])
             {
                 NSDictionary *dictResponse = (NSDictionary *)dictWholeResponse;
                 if([[[[[[dictResponse objectForKey:@"soap:Envelope"]objectForKey:@"soap:Body"]objectForKey:strReponseKey] objectForKey:strResultKey] objectForKey:@"text"]isKindOfClass:[NSString class]])
                 {
                     NSString *strResponse = [[NSString alloc] init];
                     strResponse = [[[[[dictResponse objectForKey:@"soap:Envelope"]objectForKey:@"soap:Body"]objectForKey:strReponseKey] objectForKey:strResultKey] objectForKey:@"text"];
                     
                     //remove unwanted data and parse the json data from string
                     NSData *data=[[self stringByRemovingControlCharacters:strResponse] dataUsingEncoding:NSUTF8StringEncoding];
                     id response=[JsonBuilder parseJsonObject:data];
                     if([response  isKindOfClass:[NSArray class]])
                     {
                         NSArray *array=(NSArray *)response;
                         if([array count]>0)
                         {
                             if([strType isEqualToString:Service_GetSiteID])
                             {
                                 NSDictionary *dict1=[array objectAtIndex:0];
                                 if(completionBlock)
                                 {
                                     completionBlock(dict1,nil);
                                     return;
                                 }
                             }
                             else if([strType isEqualToString:Service_GetUsefulLinks])
                             {
                                 NSDictionary *dict1=[array objectAtIndex:0];
                                 if([[dict1 objectForKey:@"SocialWall"]isKindOfClass:[NSString class]])
                                 {
                                     NSString *strURL=[DBOpreation validateText:[NSString stringWithFormat:@"%@",[dict1 objectForKey:@"SocialWall"]]];
                                     strURL=[strURL stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding ];
                                     [AppDelegate appDelegateInstance].strSocialWall=strURL;
                                 }
                                 if([[dict1 objectForKey:@"MetroTiming"]isKindOfClass:[NSString class]])
                                 {
                                     NSString *strURL=[DBOpreation validateText:[NSString stringWithFormat:@"%@",[dict1 objectForKey:@"MetroTiming"]]];
                                     strURL=[strURL stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding ];
                                     [AppDelegate appDelegateInstance].strMetroTiming=strURL;
                                 }
                                 if([[dict1 objectForKey:@"byBigBus"]isKindOfClass:[NSString class]])
                                 {
                                     NSString *strURL=[DBOpreation validateText:[NSString stringWithFormat:@"%@",[dict1 objectForKey:@"byBigBus"]]];
                                     strURL=[strURL stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding ];
                                     [AppDelegate appDelegateInstance].strBigBusURL=strURL;
                                 }
                                 if([[dict1 objectForKey:@"StoreLocator"]isKindOfClass:[NSString class]])
                                 {
                                     NSString *strURL=[DBOpreation validateText:[NSString stringWithFormat:@"%@",[dict1 objectForKey:@"StoreLocator"]]];
                                     strURL=[strURL stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding ];
                                     [AppDelegate appDelegateInstance].strStoreLocatorURL=strURL;
                                 }
                             }
                             else if([strType isEqualToString:Service_GetNotificationImage])
                             {
                                 if(completionBlock)
                                 {
                                     completionBlock(array,nil);
                                     return;
                                 }
                             }
                             else  if([strType isEqualToString:Service_GetEntertainmentListing])
                             {
                                 [DBOpreation insertEntertainmentList:array];
                             }
                             else if([strType isEqualToString:Service_GetATMLocations])
                             {
                                 [DBOpreation insertATMList:array];
                             }
                             else  if ([strType isEqualToString:Service_GetContentPages])
                             {
                                 //insert Content Categories in local db
                                 [DBOpreation insertTheMallData:response];
                             }
                             else  if ([strType isEqualToString:Service_GetDiningCategories])
                             {
                                 //insert Dine Categories in local db
                                 [DBOpreation insertDineCategories:response];
                             }
                             else if([strType isEqualToString:Service_GetEventsListing])
                             {
                                 //insert event in local db
                                 [DBOpreation insertEvents:array];
                             }
                             else if([strType isEqualToString:Service_GetHotelsListing])
                             {
                                 //insert hotels in local db
                                 [DBOpreation insertHotelList:array];
                             }
                             else if([strType isEqualToString:Service_GetOffersListing])
                             {
                                 //insert offers in local db
                                 [DBOpreation insertOfferList:array];
                             }
                             else if([strType isEqualToString:Service_GetGuestServicesPages])
                             {
                                 //insert services in local db
                                 [DBOpreation insertGuestServicesPages:array];
                             }
                             else if([strType isEqualToString:Service_GetShoppingCategories])
                             {
                                 //insert shopping categories in local db
                                 [DBOpreation insertShoppingCategories:array];
                             }
                             else if([strType isEqualToString:Service_GetDiningListing])
                             {
                                 //insert DineList in local db
                                 [DBOpreation insertDineListing:array];
                             }
                             else if([strType isEqualToString:Service_GetShoppingListing])
                             {
                                 //insert shopping listing in local db
                                 [DBOpreation insertShoppingListing:array];
                             }
                             else if([strType isEqualToString:Service_GetSpotlightBannerListing])
                             {
                                 //get spotlight banner
                                 if(![HomeDataModel   sharedInstance].arrSpotLightBanner)
                                 {
                                     [HomeDataModel   sharedInstance].arrSpotLightBanner=[[NSMutableArray alloc]init];
                                 }
                                 [[HomeDataModel   sharedInstance].arrSpotLightBanner addObjectsFromArray:array];
                             }
                             else if([strType isEqualToString:Service_GetHomeEntertainmentListing])
                             {
                                 //get spotlight banner
                                 if(![HomeDataModel   sharedInstance].arrEntList)
                                 {
                                     [HomeDataModel   sharedInstance].arrEntList=[[NSMutableArray alloc]init];
                                 }
                                 [[HomeDataModel   sharedInstance].arrEntList addObjectsFromArray:array];
                             }
                             else if([strType isEqualToString:Service_GetRandomMovie])
                             {
                                 //get spotlight banner
                                 if(![HomeDataModel   sharedInstance].arrMovie)
                                 {
                                     [HomeDataModel   sharedInstance].arrMovie=[[NSMutableArray alloc]init];
                                 }
                                 [[HomeDataModel   sharedInstance].arrMovie addObjectsFromArray:array];
                             }
                             else if([strType isEqualToString:Service_GetEventsListingByNoOfEvents])
                             {
                                 //get spotlight banner
                                 if(![HomeDataModel   sharedInstance].arrEvent)
                                 {
                                     [HomeDataModel   sharedInstance].arrEvent=[[NSMutableArray alloc]init];
                                 }
                                 [[HomeDataModel   sharedInstance].arrEvent addObjectsFromArray:array];
                                 [HomeDataModel   sharedInstance].eventCount=[array count];
                             }
                             else if([strType isEqualToString:Service_GetOffersListingByUserBehavior])
                             {
                                 //get spotlight banner
                                 if(![HomeDataModel   sharedInstance].arrOffer)
                                 {
                                     [HomeDataModel   sharedInstance].arrOffer=[[NSMutableArray alloc]init];
                                 }
                                 [[HomeDataModel   sharedInstance].arrOffer addObjectsFromArray:array];
                             }
                         }
                     }
                     else if ([strType isEqualToString:Service_UpdateUserPushNotifications])
                     {
                         if(completionBlock)
                         {
                             completionBlock(strResponse,nil);
                             return;
                         }
                     }
                 }
             }
         }
         if(completionBlock)
         {
             NSDictionary *dict=[NSDictionary dictionaryWithObject:@"Error occured while processing request" forKey:@"localizedDescription"];
             NSError *error=[NSError errorWithDomain:@"" code:0 userInfo:dict];
             completionBlock(nil, error);
         }
     } failure:^(AFHTTPRequestOperation *operation, NSError *error)
     {
         if(completionBlock)
         {
             completionBlock(nil, error);
         }
     }];
}
-(BOOL)ValidateValue:(id)value
{
    if([value isKindOfClass:[NSNumber class]])
    {
        return [value boolValue];
    }
    else if([value isKindOfClass:[NSString class]])
    {
        return [value boolValue];
    }
    return NO;
}
#pragma mark validateText
-(id)validateText:(id)text
{
    if([text isKindOfClass:[NSString class]])
    {
        return text!=nil?text:@"null";
    }
    return  @"null";
}
//===============================================================================================================
#pragma mark get Currency Value
-(void)getCurrencyValue:(NSString *)fromString toString :(NSString *)toString  CompletionBlock :(Request_Completion_Block)completionBlock
{
    NSString *strStart = @"https://query.yahooapis.com/v1/public/yql?q=select%20*%20from%20csv%20where%20url%3D%22http%3A%2F%2Ffinance.yahoo.com%2Fd%2Fquotes.csv%3Fe%3D.csv%26f%3Dc4l1%26s%3D";
    NSString *strMiddle = [NSString stringWithFormat:@"%@%@",fromString,toString];
    NSString *strEnd = @"%3DX%22%3B&format=json";
    NSString *encodedText = [[NSString stringWithFormat:@"%@%@%@",strStart,strMiddle,strEnd] stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    NSString *decodedText = [encodedText stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    [self callGetBaseService:decodedText Block:completionBlock];
}
-(void)callGetBaseService:(NSString *)strURL Block:(Request_Completion_Block)completionBlock
{
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.requestSerializer=[AFHTTPRequestSerializer serializer];
    manager.responseSerializer=[AFHTTPResponseSerializer serializer];
    [manager GET:strURL parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        id response=[JsonBuilder parseJsonObject:responseObject];
        if([response isKindOfClass:[NSError class]] || !response)
        {
            completionBlock(nil,response);
        }
        else
        {
            completionBlock(response,nil);
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        completionBlock(nil,error);
    }];
}
- (NSString *)stringByRemovingControlCharacters: (NSString *)inputString
{
    NSString *jsonString = [[NSString alloc]initWithString:inputString];
    
    jsonString = [jsonString stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    jsonString = [jsonString stringByReplacingOccurrencesOfString:@"\\\"" withString:@""];
    jsonString = [jsonString stringByReplacingOccurrencesOfString:@"\\" withString:@""];
    jsonString = [jsonString stringByReplacingOccurrencesOfString:@"\b" withString:@"\\b"];
    jsonString = [jsonString stringByReplacingOccurrencesOfString:@"\f" withString:@"\\f"];
    jsonString = [jsonString stringByReplacingOccurrencesOfString:@"\r" withString:@"\\r"];
    jsonString = [jsonString stringByReplacingOccurrencesOfString:@"\t" withString:@"\\t"];
    jsonString = [jsonString stringByReplacingOccurrencesOfString:@"’" withString: @""];
    jsonString = [jsonString stringByReplacingOccurrencesOfString:@"\\'" withString: @"'"];
    jsonString = [jsonString stringByReplacingOccurrencesOfString:@"\\u" withString: @"\\\\u"];
    jsonString = [jsonString stringByReplacingOccurrencesOfString:@"\\/" withString: @"\\/"];
    jsonString=[CommanMethods rePlaceTags:jsonString];
    
    NSMutableString *mutable = [NSMutableString stringWithString:jsonString];
    NSCharacterSet *controlChars = [NSCharacterSet controlCharacterSet];
    NSRange range = [jsonString rangeOfCharacterFromSet:controlChars];
    if (range.location != NSNotFound)
    {
        while (range.location != NSNotFound) {
            [mutable deleteCharactersInRange:range];
            range = [mutable rangeOfCharacterFromSet:controlChars];
        }
    }
    return mutable;
}
#pragma mark - SPIN & WIN
#pragma mark  SPIN & WIN
#pragma mark performOperation SPIN & Win
-(void)performOperation_SpinAndWin:(AFHTTPRequestOperation *)operation ResultRespose:(NSString *)strReponseKey ResultValKey:strResultKey CompletionBlock:(Request_Completion_Block)completionBlock Type:(NSString *)strType
{
    [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject)
     {
         if([responseObject isKindOfClass:[NSData class]])
         {
             //parse the XML  data and stored in dictionary
             NSDictionary *dictWholeResponse = [[NSDictionary alloc] init];
             dictWholeResponse =[XMLReader dictionaryForXMLData:responseObject error:nil];
             if([dictWholeResponse isKindOfClass:[NSDictionary class]])
             {
                 NSDictionary *dictResponse = (NSDictionary *)dictWholeResponse;
                 if([[[[[[dictResponse objectForKey:@"soap:Envelope"]objectForKey:@"soap:Body"]objectForKey:strReponseKey] objectForKey:strResultKey] objectForKey:@"text"]isKindOfClass:[NSString class]])
                 {
                     NSString *strResponse = [[NSString alloc] init];
                     strResponse = [[[[[dictResponse objectForKey:@"soap:Envelope"]objectForKey:@"soap:Body"]objectForKey:strReponseKey] objectForKey:strResultKey] objectForKey:@"text"];
                     NSData *data=[[self stringByRemovingControlCharacters:strResponse] dataUsingEncoding:NSUTF8StringEncoding];

                     id response=[JsonBuilder parseJsonObject:data];
                     if(response)
                     {
                         if([strType isEqualToString:Service_SpinAndWin_getCampaigns])
                         {
                             [HomeDataModel   sharedInstance].dictCampaign=nil;
                             if([response isKindOfClass:[NSDictionary class]])
                             {
                                 if([[response objectForKey:@"Data"] isKindOfClass:[NSArray class]])
                                 {
                                     NSArray *array=[response valueForKey:@"Data"];
                                     if([array count]>0)
                                     {
                                         [HomeDataModel   sharedInstance].dictCampaign=[[NSDictionary alloc]initWithDictionary:response];
                                     }
                                 }
                             }
                             return ;
                         }
                         if(completionBlock)
                         {
                             completionBlock(response,nil);
                             return;
                         }
                     }
                     
                 }
             }
         }
         if(completionBlock)
         {
             NSDictionary *dict=[NSDictionary dictionaryWithObject:@"Error occured while processing request" forKey:@"localizedDescription"];
             NSError *error=[NSError errorWithDomain:@"" code:0 userInfo:dict];
             completionBlock(nil, error);
         }
     } failure:^(AFHTTPRequestOperation *operation, NSError *error)
     {
         if(completionBlock)
         {
             completionBlock(nil, error);
         }
     }];
}
#pragma mark setupRequest_SPINnWIN
-(AFHTTPRequestOperation *)setupRequest_SpinAndWin:(NSString *)strServiceName ResultResponse:(NSString *)strReponseKey ResultValKey:(NSString *)strResultKey Body:(NSString *)strBody CompletionBlock:(Request_Completion_Block_Fetch)completionBlock
{
    NSString  *strServerUrl=[self getServiceURLString_SpinAndWin:strServiceName];
    NSString  *strAction=[self getServiceSoapAction_SpinAndWin:strServiceName];
    //preparing the request and return the request Object
    AFHTTPRequestOperation *operation=[self prepareRequest:strServerUrl Action:strAction SoapBody:strBody];
    [self performOperation_SpinAndWin:operation ResultRespose:strReponseKey ResultValKey:strResultKey CompletionBlock:completionBlock Type:strServiceName];
    return operation;
}
#pragma mark getServiceURLString
-(id)getServiceURLString_SpinAndWin:(NSString *)strServiceParam
{
    return [NSString stringWithFormat:@"%@%@%@",Web_Host_Url_spinNwin,Web_Host_Url_Directory_spinNwin,strServiceParam];
}
#pragma mark getServiceSoapAction
-(id)getServiceSoapAction_SpinAndWin:(NSString *)strServiceParam
{
    return [NSString stringWithFormat:@"%@/%@",Web_Host_SOAP_spinNwin,strServiceParam];
}

#pragma mark - spinNwin_getCampaigns

-(AFHTTPRequestOperation *)spinNwin_getCampaigns:(NSString *)strServiceName ResultRespose:(NSString *)strReponseKey ResultValKey:(NSString *)strResultKey Block:(Request_Completion_Block)completionBlock
{
    NSString *strSoapBody=[NSString stringWithFormat:@"<getCampaigns xmlns=\"%@\"><mallcode>%@</mallcode></getCampaigns>",Web_Host_SOAP_spinNwin, mallcode];
    //preparing the request
    AFHTTPRequestOperation *operation = [self setupRequest_SpinAndWin:strServiceName ResultResponse:strReponseKey ResultValKey:strResultKey Body:strSoapBody CompletionBlock:completionBlock];
    
    //call the service
    return operation;
}

#pragma mark spinNwin_getUseronLogin

-(void)spinNwin_getUseronLogin:(NSString *)strServiceName ResultRespose:(NSString *)strReponseKey ResultValKey:(NSString *)strResultKey dataDict:(NSMutableDictionary *)dict Block:(Request_Completion_Block)completionBlock
{
    NSString *Email = [dict objectForKey:spinNwin_login_Email];
    NSString *Password = [dict objectForKey:spinNwin_login_Password];
    
    NSString *strSoapBody=[NSString stringWithFormat:@"<getUseronLogin xmlns=\"%@\"><Email>%@</Email><Password>%@</Password></getUseronLogin>",Web_Host_SOAP_spinNwin, Email, Password];
    //preparing the request
    
    AFHTTPRequestOperation *operation = [self setupRequest_SpinAndWin:strServiceName ResultResponse:strReponseKey ResultValKey:strResultKey Body:strSoapBody CompletionBlock:completionBlock];
    
    //call the service
    [operation  start];
}

#pragma mark spinNwin_RegisterUser

-(void)spinNwin_RegisterUser:(NSString *)strServiceName ResultRespose:(NSString *)strReponseKey ResultValKey:(NSString *)strResultKey DataDict:(NSMutableDictionary *) dict Block:(Request_Completion_Block)completionBlock
{
    
    NSString *title = [dict objectForKey:@"title"];
    NSString *name = [dict objectForKey:@"name"];
    NSString *Email = [dict objectForKey:@"Email"];
    NSString *Mobile = [dict objectForKey:@"Mobile"];
    NSString *Country_of_Residence = [dict objectForKey:@"Country_of_Residence"];
    NSString *Password = [dict objectForKey:@"Password"];
    NSString *OptionId = @"1";
    NSString *emirates_passportId = @"123456789";
    NSString *nationality = [dict objectForKey:@"nationality"];
    
    
    
    NSString *strSoapBody=[NSString stringWithFormat:@"<RegisterUser xmlns=\"%@\"><title>%@</title><name>%@</name><Email>%@</Email><Mobile>%@</Mobile><Country_of_Residence>%@</Country_of_Residence><Password>%@</Password><OptionId>%@</OptionId><emirates_passportId>%@</emirates_passportId><nationality>%@</nationality></RegisterUser>",Web_Host_SOAP_spinNwin, title, name, Email, Mobile, Country_of_Residence, Password, OptionId, emirates_passportId, nationality];
    //preparing the request
    AFHTTPRequestOperation *operation=[self setupRequest_SpinAndWin:strServiceName ResultResponse:strReponseKey ResultValKey:strResultKey Body:strSoapBody CompletionBlock:completionBlock];
    //call the service
    [operation  start];
}

#pragma mark spinNwin_ForgotPassword

-(void)spinNwin_ForgotPassword:(NSString *)strServiceName ResultRespose:(NSString *)strReponseKey ResultValKey:(NSString *)strResultKey Email:(NSString *)EMail Block:(Request_Completion_Block)completionBlock
{
    NSString *Email = EMail;
    
    NSString *strSoapBody=[NSString stringWithFormat:@"<ForgotPassword xmlns=\"%@\"><Email>%@</Email></ForgotPassword>",Web_Host_SOAP_spinNwin,Email];
    //preparing the request
    
    AFHTTPRequestOperation *operation = [self setupRequest_SpinAndWin:strServiceName ResultResponse:strReponseKey ResultValKey:strResultKey Body:strSoapBody CompletionBlock:completionBlock];
    
    //call the service
    [operation  start];
}

#pragma mark spinNwin_ResetPassword

-(void)spinNwin_ResetPassword:(NSString *)strServiceName ResultRespose:(NSString *)strReponseKey ResultValKey:(NSString *)strResultKey dataDict:(NSMutableDictionary *)dict Block:(Request_Completion_Block)completionBlock
{
    NSString *Email = [dict objectForKey:spinNwin_reset_Email];
    NSString *SecurityCode = [dict objectForKey:spinNwin_reset_SecurityCode];
    NSString *newpassword = [dict objectForKey:spinNwin_reset_newpassword];
    
    NSString *strSoapBody=[NSString stringWithFormat:@"<ResetPassword xmlns=\"%@\"><Email>%@</Email><SecurityCode>%@</SecurityCode><newpassword>%@</newpassword></ResetPassword>",Web_Host_SOAP_spinNwin, Email, SecurityCode, newpassword];
    //preparing the request
    
    AFHTTPRequestOperation *operation = [self setupRequest_SpinAndWin:strServiceName ResultResponse:strReponseKey ResultValKey:strResultKey Body:strSoapBody CompletionBlock:completionBlock];
    
    //call the service
    [operation  start];
}


#pragma mark spinNwin_getDrawDates

-(void)spinNwin_getDrawDates:(NSString *)strServiceName ResultRespose:(NSString *)strReponseKey ResultValKey:(NSString *)strResultKey CampaignId:(int)campaignId Block:(Request_Completion_Block)completionBlock
{
    NSString *strSoapBody=[NSString stringWithFormat:@"<getDrawDates xmlns=\"%@\"><campaignId>%d</campaignId></getDrawDates>",Web_Host_SOAP_spinNwin, campaignId];
    //preparing the request
    AFHTTPRequestOperation *operation = [self setupRequest_SpinAndWin:strServiceName ResultResponse:strReponseKey ResultValKey:strResultKey Body:strSoapBody CompletionBlock:completionBlock];
    //call the service
    [operation  start];
}

#pragma mark spinNwin_getPastWinners

-(void)spinNwin_getPastWinners:(NSString *)strServiceName ResultRespose:(NSString *)strReponseKey ResultValKey:(NSString *)strResultKey CampaignId:(int)campaignId Block:(Request_Completion_Block)completionBlock
{
    NSString *strSoapBody=[NSString stringWithFormat:@"<getPastWinners xmlns=\"%@\"><campaignId>%d</campaignId></getPastWinners>",Web_Host_SOAP_spinNwin, campaignId];
    //preparing the request
    AFHTTPRequestOperation *operation = [self setupRequest_SpinAndWin:strServiceName ResultResponse:strReponseKey ResultValKey:strResultKey Body:strSoapBody CompletionBlock:completionBlock];
    //call the service
    [operation  start];
}

#pragma mark spinNwin_getQuestionsByCampaignId

-(void)spinNwin_getQuestionsByCampaignId:(NSString *)strServiceName ResultRespose:(NSString *)strReponseKey ResultValKey:(NSString *)strResultKey CampaignId:(int)campaignId Block:(Request_Completion_Block)completionBlock
{
    [defaults synchronize];
    int userId = (int)[defaults integerForKey:@"UserId"];
    //    int userId = 279;
    
    NSString *strSoapBody=[NSString stringWithFormat:@"<getQuestionsByCampaignId xmlns=\"%@\"><campaignId>%d</campaignId><userId>%d</userId></getQuestionsByCampaignId>",Web_Host_SOAP_spinNwin, campaignId, userId];
    
    AFHTTPRequestOperation *operation = [self setupRequest_SpinAndWin:strServiceName ResultResponse:strReponseKey ResultValKey:strResultKey Body:strSoapBody CompletionBlock:completionBlock];
    
    //call the service
    [operation  start];
}

#pragma mark spinNwin_RegisterUserRightAnswer

-(void)spinNwin_RegisterUserRightAnswer:(NSString *)strServiceName ResultRespose:(NSString *)strReponseKey ResultValKey:(NSString *)strResultKey userId:(int)userId CampaignId:(int)campaignId questionId:(int)questionId AnswerId:(int)AnswerId AttemptId:(int)AttemptId Block:(Request_Completion_Block)completionBlock
{
    
    NSString *strSoapBody=[NSString stringWithFormat:@"<RegisterUserRightAnswer xmlns=\"%@\"><userId>%d</userId>                           <campaignId>%d</campaignId><questionId>%d</questionId><AnswerId>%d</AnswerId><AttemptId>%d</AttemptId></RegisterUserRightAnswer>",Web_Host_SOAP_spinNwin, userId, campaignId, questionId, AnswerId, AttemptId];
    //preparing the request
    
    AFHTTPRequestOperation *operation = [self setupRequest_SpinAndWin:strServiceName ResultResponse:strReponseKey ResultValKey:strResultKey Body:strSoapBody CompletionBlock:completionBlock];
    
    //call the service
    [operation  start];
}

@end

