

#import <Foundation/Foundation.h>
#import <CoreLocation/CoreLocation.h>

@protocol CurrentCLControllerDelegate
 @optional

- (void)locationUpdate:(CLLocation *)location;
- (void)locationError:(NSError *)error;
- (void)locationServiceDisabled;
@end

@interface CurrentCLController : NSObject <CLLocationManagerDelegate>
{
	CLLocationManager *locationManager;
    CLGeocoder *geocoder;
}
@property (nonatomic, retain) CLLocationManager *locationManager;  
@property (nonatomic, weak) id delegate;
+ (CurrentCLController *)sharedInstance;
 - (void)refreshCurrentLocationWithView:(id)viewController;
@end
