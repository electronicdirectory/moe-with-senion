#import "CurrentCLController.h"
#import <UIKit/UIKit.h>
@implementation CurrentCLController
@synthesize locationManager;
@synthesize delegate;
+ (CurrentCLController *)sharedInstance
{
    static CurrentCLController *sharedInstance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedInstance = [[CurrentCLController alloc] init];
    });
    return sharedInstance;
}
- (id) init
{
    self = [super init];
    if (self != nil)
    {
    }
    return self;
}
-(void)refreshCurrentLocationWithView:(id)viewController
{
    self.delegate = viewController;
    
    //check the location services
    BOOL canAppGetLocation =    [CLLocationManager locationServicesEnabled] &&
    [CLLocationManager authorizationStatus] != kCLAuthorizationStatusRestricted &&
    [CLLocationManager authorizationStatus] != kCLAuthorizationStatusDenied;
    
    if(canAppGetLocation)
    {
        if(!locationManager)
        {
            locationManager=[[CLLocationManager alloc]init];
            locationManager.delegate=self;
            locationManager.desiredAccuracy = kCLLocationAccuracyBestForNavigation;
        }
        float deviceVersion=[[[UIDevice currentDevice]systemVersion]floatValue];
        if(deviceVersion>=8)
        {
            if ([locationManager respondsToSelector:@selector(requestWhenInUseAuthorization)])
            {
                [locationManager requestWhenInUseAuthorization];
            }
            else
            {
                [locationManager startUpdatingLocation];
            }
        }
        else
        {
            [locationManager startUpdatingLocation];
        }
    }
    else
    {
        if ([self.delegate respondsToSelector:@selector(locationServiceDisabled)])
        {
            [self.delegate locationServiceDisabled];
        }
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Location Service Disabled"
                                                        message:@"To re-enable, please go to Settings and turn on Location Service for this app."
                                                       delegate:nil  cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alert show];
    }
}
- (void)locationManager:(CLLocationManager *)manager didChangeAuthorizationStatus:  (CLAuthorizationStatus)status
{
    switch (status)
    {
        case kCLAuthorizationStatusNotDetermined:
            [locationManager requestWhenInUseAuthorization];
            break;
        case kCLAuthorizationStatusAuthorizedWhenInUse:
            [locationManager startUpdatingLocation];
            break;
        case kCLAuthorizationStatusAuthorizedAlways:
            [locationManager startUpdatingLocation];
            break;
        case kCLAuthorizationStatusRestricted:
            // restricted by e.g. parental controls. User can't enable Location Services
            break;
        case kCLAuthorizationStatusDenied:
            // user denied your app access to Location Services, but can grant access from Settings.app
            break;
        default:
            break;
    }
}
//Delegate method from the CLLocationManagerDelegate protocol.
- (void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations
{
    if([locations count]>0)
    {
        CLLocation *newLocation=[locations lastObject];
        //check horizontal accuracy greater than or not
        if (newLocation.horizontalAccuracy >=0 && newLocation.horizontalAccuracy<=500)
        {
            [locationManager stopUpdatingLocation];
            if([delegate respondsToSelector:@selector(locationUpdate:)])
                [self.delegate locationUpdate:newLocation];
        }
    }
}
- (void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error
{
    switch([error code])
    {
        case kCLErrorDenied:
            [self.locationManager stopUpdatingLocation];
            break;
       	default:
            break;
    }
    if ([self.delegate respondsToSelector:@selector(locationError:)])
    {
        [self.delegate locationError:nil];
    }
}
@end
