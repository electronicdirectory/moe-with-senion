//
//  Notification_key.h
//  MOE
//
//  Created by Neosoft on 6/18/15.
//  Copyright (c) 2015 Neosoft. All rights reserved.
//

#ifndef MOE_Notification_key_h
#define MOE_Notification_key_h


#define NOTIFICATION_FASHION_LADIES @"Notification_FashionLadies"
#define NOTIFICATION_FASHION_MEN @"Notification_FashionMen"
#define NOTIFICATION_DINNING @"Notification_Dinning"
#define NOTIFICATION_OFFERS @"Notification_Offers"
#define NOTIFICATION_EVENTS @"Notification_Events"
#define NOTIFICATION_WEEKLY_1_6ALERTS @"Notification_Weekly1-6Alerts"
#define NOTIFICATION_WEEKLY_6_12ALERTS @"Notification_Weekly6-12Alerts"
#define NOTIFICATION_USER_NAME @"Notification_UserName"
#define NOTIFICATION_USER_EMAIL @"Notification_UserEmail"
#define NOTIFICATION_USER_MOBILE @"Notification_UserMobile"
#define NOTIFICATION_IS_MR @"Notification_isMr_Val"

#endif
