
//
//  Constant.h
//  MOE
//
//  Created by Nivrutti on 6/11/15.
//  Copyright (c) 2015 Neosoft. All rights reserved.
//

#import <Foundation/Foundation.h>
#define APP_DELEGATE ((AppDelegate*)[[UIApplication sharedApplication] delegate])

#ifndef MOE_Constant_h
#define MOE_Constant_h

#define defaults [NSUserDefaults standardUserDefaults]
#define DEVICE_SIZE [[[[UIApplication sharedApplication] keyWindow] rootViewController].view convertRect:[[UIScreen mainScreen] bounds] fromView:nil].size

#define IS_IPAD (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)

#define IS_IPHONE (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)

#define IS_RETINA ([[UIScreen mainScreen] scale] >= 2.0)

#define SCREEN_WIDTH DEVICE_SIZE.width

#define SCREEN_HEIGHT DEVICE_SIZE.height

#define SCREEN_MAX_LENGTH (MAX(SCREEN_WIDTH, SCREEN_HEIGHT))

#define SCREEN_MIN_LENGTH (MIN(SCREEN_WIDTH, SCREEN_HEIGHT))

#define IS_IPHONE_4_OR_LESS (IS_IPHONE && SCREEN_MAX_LENGTH < 568.0)

#define IS_IPHONE_5 (IS_IPHONE && SCREEN_MAX_LENGTH == 568.0)

#define IS_IPHONE_GREATER_THAN_5 (IS_IPHONE && SCREEN_MAX_LENGTH > 568.0)

#define IS_IPHONE_6 (IS_IPHONE && SCREEN_MAX_LENGTH == 667.0)

#define IS_IPHONE_6P (IS_IPHONE && SCREEN_MAX_LENGTH == 736.0)

#define IS_GREATER_THAN_IOS8 [[[UIDevice currentDevice] systemVersion] compare:@"8.0" options:NSNumericSearch] != NSOrderedAscending

#define DEGREES_TO_RADIANS(angle) ((angle) / 180.0 * M_PI)

#define MALL_LATITUDE 25.118107

#define MALL_LONGITUDE 55.200608

#define DISTANCE_METER 1000

#define IPHONE4_DETAILS_IMAGE_HEIGHT  210

#define IPHONE5_DETAILS_IMAGE_HEIGHT  220

#define IPHONE6_DETAILS_IMAGE_HEIGHT  240

#define IPHONE6PLUS_DETAILS_IMAGE_HEIGHT 260

#define EVENT_COUNT 6







#define  GOOGLE_ANALYTICS_ID @"UA-65265354-1"


#define  APP_TYPE @"IOS"

#define DB_NAME @"Moe"

#define APP_NODATA @"No Data Found"

#define USER_NOTIFICATION_TOKEN @"1213131313"

#define FAV_ADDED @"Added to Favorite"

#define FAV_DELETED @"Deleted From Favorite"

#define INSIDE_MALL @"Inside"

#define OUTSIDE_MALL @"Outside"

#define SITE_ID @"siteID"

#define APP_DINE @"Dining"

#define APP_OFFER @"Offers"

#define APP_EVENTS @"Events"

#define APP_ENTERTAINMENT @"Entertainment"

#define APP_ALLSTORE @"All Stores"


#define APP_SHOP @"Shopping"

#define APP_HOTEL @"Hotels"

#define APP_SERVICES @"Services"

#define APP_ATM @"ATM"

#define APP_MOVIE @"Movies"

#define APP_SPOTLIGHT @"SpotLight"

#define APP_OPENING_HOURS @"OPENING HOURS"

#define APP_FACILITIES @"FACILITIES"

#define APP_CONTACT_US @"CONTACT US"

#define APP_GETTING_HERE @"GETTING HERE"

#define APP_THE_MALL @"THE MALL"

#define ADD_PARKING_INFO @"ADD PARKING INFO"

#define MALLGIFTCARDTITLE @"MALL GIFT CARD"

#define APP_HTPP_KEYWORD @"http"

#define DUBAITANSPORT @"+97142080808"

#define CITYTAXISHARJAH @"600-525252"

#define ALGHAZALTAXI @"02-4447787"

#define DEFAULT_SITE_ID @"2807"


#define DEFAULT_WEBSITE_URL  @"http://www.malloftheemirates.com/"

#define DEFAULT_SOCIALURL @"social-wall.aspx"

#define DEFAULT_METRO_TIMING @"metro-timing.aspx"

#define DEFAULT_MOVIE_URL  @"vox-cinemas.aspx"

#define DEFAULT_FEEDBACK @"appfeedback.aspx"

#define CHECKBALANCEURL @"balance-enquiry"

#define DEFAULT_SHUTTLE_BUS @"mobile/shuttle-bus-service"

#define IMAGEACTIONURL @"http://mallgiftcard.ae/home"

#define DEFAULT_STORE_LOCATOR @"http://map.maf.ae/rest/web/x/8039/en?"

#define DEFAULT_BUS_URL @"http://eng.bigbustours.com/dubai/home.html"

#define GIFTCARD_KNOW_MORE @"http://giftcard.mallgiftcard.ae"

#define GIFTCARDDESCRIPTION @"<html><body>One convenient card allows recipients to buy what they want when they want. They will enjoy shopping at over 2100 stores at all of their favorite shopping malls in a number of different countries.</body></html>"


#define PLACE_HODLER_IMAGE [UIImage imageNamed:@"place-holder-spotlight6p"]


static NSString * const HasLaunchedOnce = @"HasLaunchedOnce";

static NSString * const device_token = @"deviceToken";

static NSString * const latitude = @"latitude";

static NSString * const longitude = @"Longitude";

static NSString * const mall_Checker = @"CheckMall";

static NSString * const HasLandingLaunchedOnce = @"HasLandingLaunchedOnce";

static NSString * const HasShoppingLaunchedOnce = @"HasShoppingLaunchedOnce";



#endif
