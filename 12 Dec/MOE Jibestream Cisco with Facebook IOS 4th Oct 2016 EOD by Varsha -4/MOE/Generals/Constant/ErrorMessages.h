//
//  ErrorMessages.h
//  MOE
//
//  Created by Nivrutti on 09/03/15.
//  Copyright (c) 2015 Neosoft. All rights reserved.
//

#ifndef MOE_ErrorMessages_h
#define MOE_ErrorMessages_h

static NSString * const error = @"Error";
static NSString * const error_message = @"Please check Internet connection.";
static NSString * const error_title_MOE = @"Mall Of Emirates";

static NSString * const alert_spin_and_win_title = @"SPIN AND WIN ALERT";

static NSString *const alert_spin_per_day_3_chances = @"You have a maximum of 3 chances per day to take part in the draw";

static NSString * const error_invalid_email = @"Please enter valid Email Address to proceed";
static NSString * const error_invalid_password = @"Please enter Password to proceed";
static NSString * const error_invalid_security_code = @"Please enter Security code you received to proceed";
static NSString * const error_invalid_title_MRorMS = @"Please choose title Mr./Ms. to proceed";
static NSString * const error_invalid_gender = @"Please select gender to proceed";
static NSString * const error_updatedProfile = @"Profile updated successfully.";
static NSString * const error_registerProfile = @"Thank you for registation.";
static NSString * const error_invalid_valid_mobile = @"Please enter valid Mobile number to proceed";
static NSString * const error_invalid_valid_mobile_digit = @"Please enter valid Mobile number to proceed";

static NSString * const error_invalid_CountryCode = @"Please enter valid CountryCode to proceed";


static NSString * const error_invalid_name = @"Please enter Name to proceed";

static NSString * const error_invalid_first_name = @"Please enter First name to proceed";
static NSString * const error_invalid_last_name = @"Please enter Last name to proceed";
static NSString * const error_invalid_nationality = @"Please select nationality to proceed";
static NSString * const error_invalid_resident_type = @"Please select resident type to proceed";
static NSString * const error_invalid_mobile = @"Please enter Mobile number to proceed";
static NSString * const error_invalid_confirm_password = @"Please enter Confirm password to proceed";
static NSString * const error_invalid_confirm_password_mismatach = @"The Confirm password does not match the Password";

static NSString * const error_spin_no_campaigns = @"No Campaigns available for selected Mall.";
static NSString * const error_spin_no_contest_selected = @"Please select a contest first.";
static NSString * const error_spin_termsAndConditions_not_accepted = @"Please accept the Terms and Conditions to Play.";
static NSString * const error_spin_no_answer_option_selected = @"Please select an option to proceed.";

static NSString * const error_spin_no_draw_date = @"No previous draw dates found.";
static NSString * const error_spin_no_past_winners = @"No past winners found.";

static NSString * const error_in_connection = @"Error in connection. \n Please try again later";
static NSString * const call_facility_not_Available = @"Call Facility Not Available";

static NSString * const camera_permission_error = @"Permission to access Camera is Denied. Please grant permission from Settings app.";
static NSString * const save_error = @"Could not save.\nPlease Try again.";
static NSString * const save_picture_error = @"Please click a picture to save";
static NSString * const delete_picture_error = @"Could not deleted Parking info.\nPlease try again.";

static NSString * const delete_picture = @"Parking Info Deleted";




#endif
