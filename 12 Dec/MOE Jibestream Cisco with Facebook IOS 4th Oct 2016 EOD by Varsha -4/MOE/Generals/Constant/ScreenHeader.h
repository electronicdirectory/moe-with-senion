
//
//  ScreenHeader.h
//  MOE
//
//  Created by Nivrutti on 7/17/15.
//  Copyright (c) 2015 Neosoft. All rights reserved.
//

#ifndef MOE_ScreenHeader_h
#define MOE_ScreenHeader_h

static NSString * const screenHeader_MY_PARKING = @"MY PARKING";
static NSString * const screenHeader_ATMS = @"ATM'S";
static NSString * const screenHeader_BOOK_A_TAXI = @"CALL A TAXI";
static NSString * const screenHeader_Movie = @"MOVIES@VOX";
static NSString * const screenHeader_Featured_Products=@"FEATURED PRODUCTS";
static NSString * const screenHeader_StoreLocator = @"STORE LOCATOR";
static NSString * const screenHeader_SocialWall = @"SOCIAL WALL";
static NSString * const screenHeader_MetroTiming = @"METRO TIMING";
static NSString * const screenHeader_BigBus = @"BY BIG BUS";
static NSString * const screenHeader_Whats_Happening = @"WHAT'S HAPPENING";
static NSString * const screenHeader_MetroMAp = @"METRO MAP";
static NSString * const screenHeader_LandingPage = @"MALL OF THE EMIRATES";
static NSString * const screenHeader_Notification = @"MANAGE NOTIFICATIONS";
static NSString * const screenHeader_NotificationManagement = @"MANAGE NOTIFICATIONS";
static NSString * const screenHeader_Favorites = @"MY FAVORITES";
static NSString * const screenHeader_Settings = @"SETTINGS";
static NSString * const screenHeader_Events = @"EVENTS";
static NSString * const screenHeader_Offers = @"OFFERS";
static NSString * const screenHeader_Hotels = @"HOTELS";
static NSString * const screenHeader_Utilities = @"UTILITIES";
static NSString * const screenHeader_Utilities_ShoeSize = @"SHOE SIZE CONVERTER";
static NSString * const screenHeader_Utilities_Fashion = @"FASHION CONVERTER";
static NSString * const screenHeader_Utilities_Currency = @"CURRENCY CONVERTER";
static NSString * const screenHeader_Utilities_QRCODE = @"QR SCANNER";


static NSString * const screenHeader_Contact_Info = @"CONTACT INFO";
static NSString * const screenHeader_Feedback = @"FEEDBACK";
static NSString * const screenHeader_WhatsApp_Us = @"WhatsApp Us";
static NSString * const screenHeader_Contact_Us = @"Contact Us";

#endif
