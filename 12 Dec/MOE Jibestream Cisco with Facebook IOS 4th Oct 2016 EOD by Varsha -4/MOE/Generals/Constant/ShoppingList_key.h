//
//  ShoppingList_key.h
//  MOE
//
//  Created by Nivrutti on 6/16/15.
//  Copyright (c) 2015 Neosoft. All rights reserved.
//

#ifndef MOE_ShoppingList_key_h
#define MOE_ShoppingList_key_h


static NSString * const shop_ID = @"ID";
static NSString * const shop_Description= @"Description";
static NSString * const shop_Destination_ID = @"Destination_ID";
static NSString * const shop_Telephone = @"Shop_Telephone";
static NSString * const shop_FB_URL = @"FB_URL";
static NSString * const shop_Floor = @"Floor";
static NSString * const shop_Nearest_Parking= @"Shop_Nearest_Parking";
static NSString * const shop_Summary= @"Summary";
static NSString * const shop_Image= @"Image";
static NSString * const shop_Title= @"Shop_Title";
static NSString * const shop_Twitter_URL= @"Twitter_URL";
static NSString * const shop_Featured_Products= @"Featured_Products";
static NSString * const shop_ImagesList= @"ImagesList";

static NSString * const shop_List_key = @"Shops_list";
static NSString * const shop_key = @"Shop";
 #endif



