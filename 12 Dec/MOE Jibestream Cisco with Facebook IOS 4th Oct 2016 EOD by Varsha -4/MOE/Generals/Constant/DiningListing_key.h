//
//  DiningListing_key.h
//  MOE
//
//  Created by Nivrutti on 6/16/15.
//  Copyright (c) 2015 Neosoft. All rights reserved.
//

#ifndef MOE_DiningListing_key_h
#define MOE_DiningListing_key_h
static NSString * const dining_ID = @"ID";
static NSString * const dining_Title= @"Title";
static NSString * const dining_Floor = @"Floor";
static NSString * const dining_Nearest_Parking= @"Dining_Nearest_Parking";
static NSString * const dining_Telephone = @"Dining_Telephone";
static NSString * const dining_Destination_ID = @"Destination_ID";
static NSString * const dining_Summary= @"Summary";
static NSString * const dining_Shop_Code= @"Shop_Code";
static NSString * const dining_Floor_ID = @"Floor_ID";
static NSString * const dining_Store_Timings= @"Store_Timings";
static NSString * const dining_Landmark = @"Landmark";
static NSString * const dining_Thumbnail= @"Thumbnail";
static NSString * const dining_FB_URL = @"FB_URL";
static NSString * const dining_Twitter_URL= @"Twitter_URL";
static NSString * const dining_Logo= @"Logo";
static NSString * const dining_Description= @"Description";


#endif
