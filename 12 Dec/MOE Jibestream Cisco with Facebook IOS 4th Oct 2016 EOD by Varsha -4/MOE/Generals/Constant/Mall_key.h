//
//  Mall_key.h
//  MOE
//
//  Created by Neosoft on 6/26/15.
//  Copyright (c) 2015 Neosoft. All rights reserved.
//

#ifndef MOE_Mall_key_h
#define MOE_Mall_key_h

static NSString * const Mall_Title = @"Title";
static NSString * const Mall_ID = @"ID";
static NSString * const Mall_PageDetail = @"PageDetail";
static NSString * const Mall_Image = @"Image";

static NSString * const TheMall_TheMall = @"The Mall";
static NSString * const TheMall_Facilities = @"Facilities";
static NSString * const TheMall_Opening_Hours = @"Opening Hours";
static NSString * const TheMall_Getting_Here = @"Getting Here";
static NSString * const TheMall_Contact_Details = @"Contact Details";

static NSString * const TheMall_GettingHere_webView_URL = @"<iframe src='https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3612.519935661272!2d55.20060799999998!3d25.11810700000001!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3e5f6bbec39f339d%3A0x47afe5626875154c!2sMall+of+the+Emirates!5e0!3m2!1sen!2sae!4v1435645872060' width='600' height='450' frameborder='0' style='border: 0;position:absolute;left:0;top:0;right:0;' allowfullscreen=''></iframe>";

#endif
