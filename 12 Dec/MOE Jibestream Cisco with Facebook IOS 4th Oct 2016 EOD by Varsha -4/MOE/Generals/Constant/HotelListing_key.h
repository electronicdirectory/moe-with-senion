//
//  HotelListing_key.h
//  MOE
//
//  Created by Nivrutti on 6/15/15.
//  Copyright (c) 2015 Neosoft. All rights reserved.
//

#ifndef MOE_HotelListing_key_h
#define MOE_HotelListing_key_h


static NSString * const hotelsListing_Description = @"Description";
static NSString * const hotelsListing_ID=@"ID";
static NSString * const hotelsListing_Name=@"Hotel_Name";
static NSString * const hotelsListing_Telephone=@"Telephone";
static NSString * const hotelsListing_URL=@"URL";
static NSString * const hotelsListing_Image=@"Image";
static NSString * const hotelsListing_Type=@"Type";

#endif
