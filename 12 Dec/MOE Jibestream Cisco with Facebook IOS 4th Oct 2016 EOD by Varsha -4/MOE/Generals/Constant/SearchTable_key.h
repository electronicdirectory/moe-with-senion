//
//  SearchTable_key.h
//  MOE
//
//  Created by Nivrutti on 6/17/15.
//  Copyright (c) 2015 Neosoft. All rights reserved.
//

#ifndef MOE_SearchTable_key_h
#define MOE_SearchTable_key_h
static NSString * const search_Title= @"Title";
static NSString * const search_ID = @"ID";
static NSString * const search_Type = @"Type";
#endif
