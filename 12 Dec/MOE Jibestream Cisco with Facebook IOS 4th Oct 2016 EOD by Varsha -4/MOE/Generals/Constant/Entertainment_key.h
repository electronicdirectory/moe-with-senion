//
//  Entertainment_key.h
//  MOE
//
//  Created by Nivrutti on 6/15/15.
//  Copyright (c) 2015 Neosoft. All rights reserved.
//

#ifndef MOE_Entertainment_key_h
#define MOE_Entertainment_key_h

static NSString * const entertainment_Description = @"Description";
static NSString * const entertainment_Destination_ID = @"Destination_ID";
static NSString * const entertainment_Telephone = @"Entertainment_Telephone";
static NSString * const entertainment_FB_URL = @"FB_URL";
static NSString * const entertainment_Floor = @"Floor";
static NSString * const entertainment_ID = @"ID";
static NSString * const entertainment_Nearest_Parking= @"Nearest_Parking";
static NSString * const entertainment_Shop_Code= @"Shop_Code";
static NSString * const entertainment_Store_Timings= @"Store_Timings";
static NSString * const entertainment_Summary= @"Summary";
static NSString * const entertainment_Thumbnail= @"Thumbnail";
static NSString * const entertainment_Title= @"Title";
static NSString * const entertainment_Twitter_Page_URL= @"Twitter_Page_URL";
#endif



