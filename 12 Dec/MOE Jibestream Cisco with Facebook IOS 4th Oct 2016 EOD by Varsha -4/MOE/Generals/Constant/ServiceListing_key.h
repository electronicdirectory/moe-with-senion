


//
//  ServiceListing_key.h
//  MOE
//
//  Created by Nivrutti on 6/15/15.
//  Copyright (c) 2015 Neosoft. All rights reserved.
//

#ifndef MOE_ServiceListing_key_h
#define MOE_ServiceListing_key_h

static NSString * const serviceListing_PageDetail = @"PageDetail";
static NSString * const serviceListing_ID=@"ID";
static NSString * const serviceListing_Image=@"Image";
static NSString * const serviceListing_Title=@"Title";
#endif