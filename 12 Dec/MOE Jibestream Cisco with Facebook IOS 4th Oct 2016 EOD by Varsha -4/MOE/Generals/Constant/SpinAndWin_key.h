//
//  SpinAndWin_key.h
//  MOE
//
//  Created by Neosoft on 6/18/15.
//  Copyright (c) 2015 Neosoft. All rights reserved.
//

#ifndef MOE_SpinAndWin_key_h
#define MOE_SpinAndWin_key_h

#define SPINANDWIN_OPTION_ID @"1"
#define SPINANDWIN_EMIRATES_PASSPORT_ID @"123456789"

static NSString * const spinNwin_login_Email = @"Email";
static NSString * const spinNwin_login_Password = @"Password";

static NSString * const spinNwin_register_title = @"title";
static NSString * const spinNwin_register_name = @"name";
static NSString * const spinNwin_register_Email = @"Email";
static NSString * const spinNwin_register_Mobile = @"Mobile";
static NSString * const spinNwin_register_Country_of_Residence = @"Country_of_Residence";
static NSString * const spinNwin_register_Password = @"Password";
static NSString * const spinNwin_register_OptionId = @"OptionId";
static NSString * const spinNwin_register_emirates_passportId = @"emirates_passportId";
static NSString * const spinNwin_register_nationality = @"nationality";

static NSString * const spinNwin_reset_Email = @"Email";
static NSString * const spinNwin_reset_SecurityCode = @"SecurityCode";
static NSString * const spinNwin_reset_newpassword = @"newpassword";


#endif
