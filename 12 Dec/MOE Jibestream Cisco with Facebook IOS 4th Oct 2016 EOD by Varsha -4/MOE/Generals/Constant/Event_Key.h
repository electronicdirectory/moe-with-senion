//
//  Event_Keys.h
//  MOE
//
//  Created by Nivrutti on 6/15/15.
//  Copyright (c) 2015 Neosoft. All rights reserved.
//

#ifndef MOE_Event_Keys_h
#define MOE_Event_Keys_h

static NSString * const event_Title = @"Title";
static NSString * const event_ID = @"ID";
static NSString * const event_End_Date = @"Event_End_Date";
static NSString * const event_Image = @"Event_Image";
static NSString * const event_Start_Date = @"Event_Start_Date";
static NSString * const event_Short_Description = @"Short_Description";
static NSString * const event_Type = @"Type";
static NSString * const event_Details = @"Event_Details";
 #endif
