//
//  CisoTags.h
 //
//  Created by webwerks on 4/11/16.
//  Copyright © 2016 Nivrutti. All rights reserved.
//

#ifndef CisoTags_h
#define CisoTags_h


static NSString * const cisco_mall_code = @"MOECC";

static NSString * const tag_Overlay = @"OVERLAY";
static NSString * const tag_PushDown = @"PUSHDOWN_CENTER";

static NSString * const tag_OverlayLocation = @"overlayLocation";
static NSString * const tag_PushDownLocation = @"pushdown_centerLocation";


static NSString * const tag_EventScreen = @"EVENT";
static NSString * const tag_OfferScreen = @"OFFER";
static NSString * const tag_HomeScreen = @"HOME";
static NSString * const tag_ParkingScreen = @"PARKING";
static NSString * const tag_ShopScreen = @"SHOP";
static NSString * const tag_ServiceScreen = @"SERVICES";

#endif /* CisoTags_h */
