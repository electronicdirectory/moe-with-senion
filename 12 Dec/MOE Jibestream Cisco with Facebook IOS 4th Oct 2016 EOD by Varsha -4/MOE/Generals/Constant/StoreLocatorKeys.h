//
//  StoreLocatorKeys.h
//  MOE
//
//  Created by webwerks on 5/20/16.
//  Copyright © 2016 Neosoft. All rights reserved.
//

#ifndef StoreLocatorKeys_h
#define StoreLocatorKeys_h

static NSString * const st_Title = @"Title";
static NSString * const st_CatName = @"CatName";
static NSString * const st_Id = @"Id";

#endif /* StoreLocatorKeys_h */
