//
//  SpinAndWin_title.h
//  MOE
//
//  Created by Neosoft on 6/22/15.
//  Copyright (c) 2015 Neosoft. All rights reserved.
//

#ifndef MOE_SpinAndWin_title_h
#define MOE_SpinAndWin_title_h

static NSString * const SPIN_AND_WIN_TITLE = @"SPIN & WIN";
static NSString * const SPIN_AND_WIN_REGISTER_TITLE = @"REGISTER";
static NSString * const SPIN_AND_WIN_FORGOT_PASSWORD_TITLE = @"FORGOT PASSWORD";
static NSString * const SPIN_AND_WIN_RESET_PASSWORD_TITLE = @"RESET PASSWORD";

#endif
