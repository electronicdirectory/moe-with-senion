//
//  GoogleAnalytics_key.h
//  MOE
//
//  Created by Nivrutti on 7/16/15.
//  Copyright (c) 2015 Neosoft. All rights reserved.
//

#ifndef MOE_GoogleAnalytics_key_h
#define MOE_GoogleAnalytics_key_h

/* following are list of Screen Names */

/* SplashView Screen */
static NSString * const screenName_SplashView = @"Splash View";

/* Home Screen */
static NSString * const screenName_Inside_Mall = @"Home Inside Mall";
static NSString * const screenName_Outside_Mall = @"Home Outside Mall";
static NSString * const screenName_Whats_Happening = @"What's Happening";
static NSString * const screenName_Movie = @"Movie";

/* Shop/Dine/entertainment Screen */
static NSString * const screenName_Shopping = @"Shopping";
static NSString * const screenName_Dining = @"Dining";
static NSString * const screenName_Entertainment = @"Entertainment";
static NSString * const screenName_Featured_Products=@"Featured Products";

/* Offer/Hotel/Events Screen */
static NSString * const screenName_Offer = @"Offers";
static NSString * const screenName_Hotel = @"Hotels";
static NSString * const screenName_Events = @"Events";

/* SocialWall Screen */
static NSString * const screenName_SocialWall = @"Social Wall";

/* GiftCards Screen */
static NSString * const screenName_GiftCards = @"Gift Cards";

/* Services Screen */
static NSString * const screenName_Services = @"Services";
static NSString * const screenName_ATMList = @"ATM'S List";
static NSString * const screenName_ATM = @"ATM";

/* Settings Screen */
static NSString * const screenName_SettingsList = @"Settings List";
static NSString * const screenName_Notification_FirstLaunch = @"First Notification management";
static NSString * const screenName_SettingsNotification = @"Inside Application Notification management";
static NSString * const screenName_Favorites = @"Favorites";

/* Search Screen */
static NSString * const screenName_Search = @"Search";

/* MyParking Screen */
static NSString * const screenName_MyParking = @"My Parking";

/* Store Locator Screen */
static NSString * const screenName_StoreLocator = @"Store Locator";
static NSString * const screenName_KnowMore = @"Know More";


/* TheMall Screen */
static NSString * const screenName_TheMall = @"TheMall";
static NSString * const screenName_OpeningHours = @"The Mall - Opening Hours";
static NSString * const screenName_GettingHere = @"The Mall - Getting Here";
static NSString * const screenName_Facilities = @"The Mall - Facilities";
static NSString * const screenName_ContactUS = @"The Mall - Contact US";

/* Book A Taxi Screen */
static NSString * const screenName_BookATaxi = @"Call A Taxi";
static NSString * const screenName_MetroMap = @"Call A Taxi - Metro Map";
static NSString * const screenName_MetroTiming = @"Call A Taxi - Metro Timing";
static NSString * const screenName_BigBus = @"Call A Taxi - By Big Bus";

/* UtilitiesListing Screen */
static NSString * const screenName_UtilitiesListing = @"Utilities Listing";
static NSString * const screenName_ShoeSizeContverter = @"Utilities - Shoe Size Converter";
static NSString * const screenName_MenShoeSizeConverter = @"Utilities - Men Shoe Size Converter";
static NSString * const screenName_KidsShoeSizeConverter = @"Utilities - Kids Shoe Size Converter";
static NSString * const screenName_WomenShoeSizeConverter = @"Utilities - Women Shoe Size Converter";
static NSString * const screenName_FashionConverter = @"Utilities - Fashion Converter";
static NSString * const screenName_MenFashionConverter = @"Utilities - Men  Fashion Converter";
static NSString * const screenName_KidsFashionConverter = @"Utilities - Kids Fashion Converter";
static NSString * const screenName_WomenFashionConverter = @"Utilities - Women Fashion Converter";
static NSString * const screenName_CurrencyConverter = @"Utilities - Currency Converter";

/* Spin & WIN Screen */
static NSString * const screenName_SpinWin = @"Spin & WIN";
static NSString * const screenName_SpinWinLogin = @"Spin & WIN Login";
static NSString * const screenName_SpinWinRegistration = @"Spin & WIN Registration ";
static NSString * const screenName_SpinWinForgotPassword = @"Spin & WIN Forgot Password";
static NSString * const screenName_SpinWinResetPassword = @"Spin & WIN Reset Password";
static NSString * const screenName_SpinWinPlay = @"Spin & WIN Play ";


static NSString * const screenName_Contact_Info = @"Contact Info";
static NSString * const screenName_Feedback = @"Contact Info-Feedback";


#endif
