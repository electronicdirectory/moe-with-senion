//
//  OfferListing_key.h
//  MOE
//
//  Created by Nivrutti on 6/15/15.
//  Copyright (c) 2015 Neosoft. All rights reserved.
//

#ifndef MOE_OfferListing_key_h
#define MOE_OfferListing_key_h


static NSString * const offerListing_Description = @"Description";
static NSString * const offerListing_ID=@"ID";
static NSString * const offerListing_Title=@"Title";
static NSString * const offerListing_End_Date=@"End_Date";
static NSString * const offerListing_Start_Date=@"Start_Date";
static NSString * const offerListing_Image_URL=@"Image_URL";
static NSString * const offerListing_Type=@"Type";

static NSString * const offerListing_Store_ID=@"Store_ID";
static NSString * const offerListing_Category_Name=@"Category_Name";
static NSString * const offerListing_Category_ID=@"Category_ID";
#endif
