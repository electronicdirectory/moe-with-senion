//
//  WebServiceConstant.h
//  MOE
//
//  Created by Nivrutti on 6/12/15.
//  Copyright (c) 2015 Neosoft. All rights reserved.
//

#ifndef MOE_WebServiceConstant_h
#define MOE_WebServiceConstant_h

//Live URLS
static NSString * const Web_Host_Url = @"http://www.malloftheemirates.com";
static NSString * const Web_Host_Url_Directory = @"ws/ws_mafMobileApplication.asmx?op=";
static NSString * const Web_Host_SOAP_Action =@"http://www.maf.ae/";

//Demo server

/*static NSString * const Web_Host_Url = @"http://moe.mafpmalltest.com";
static NSString * const Web_Host_SOAP_Action =@"http://www.maf.ae/";
static NSString *  const Web_Host_Url_Directory=@"ws/ws_mafMobileApplication.asmx?op=";*/

 //spin and Win URLS
static NSString * const Web_Host_Url_spinNwin = @"http://spin.belongi.com/";
static NSString * const Web_Host_SOAP_spinNwin = @"http://spin.o2-cafe.net";
static NSString * const Web_Host_Url_Directory_spinNwin = @"spin_and_win_new.asmx?op=";



static NSString * const  mallcode = @"MOE";

static NSString * const Service_GetSiteID = @"GetSiteID";

static NSString * const Service_GetContentPages = @"GetContentPages";

static NSString * const Service_GetDiningCategories = @"GetDiningCategories";
static NSString * const Service_GetDiningListing = @"GetDiningListing";
static NSString * const Service_GetEntertainmentListing = @"GetEntertainmentListing";

static NSString * const Service_GetEventsListing = @"GetEventsListing";
static NSString * const Service_GetHotelsListing = @"GetHotelsListing";
static NSString * const Service_GetGuestServicesPages = @"GetGuestServicesPages";

static NSString * const Service_GetShoppingCategories = @"GetShoppingCategories";
static NSString * const Service_GetShoppingListing = @"GetShoppingListing";
static NSString * const Service_GetOffersListing = @"GetOffersListing";

static NSString * const Service_GetSpotlightBannerListing = @"GetSpotlightBannerListing";
static NSString * const Service_GetHomeEntertainmentListing= @"GetHomeEntertainmentListing";
static NSString * const Service_GetRandomMovie= @"GetRandomMovie";
static NSString * const Service_GetEventsListingByNoOfEvents= @"GetEventsListingByNoOfEvents";
static NSString * const Service_GetOffersListingByUserBehavior= @"GetOffersListingByUserBehavior";

static NSString * const Service_SetDeviceToken = @"SetDeviceToken";
static NSString * const Service_UpdateUserPushNotifications = @"UpdateUserPushNotifications";
static NSString * const Service_GetNotificationImage = @"GetNotificationImage";

static NSString * const Service_UpdateUserCategoryHits = @"UpdateUserCategoryHits";
static NSString * const Service_GetATMLocations = @"GetATMLocations";
static NSString * const Service_GetUsefulLinks = @"GetUsefulLinks";



//===========web service Response Key=========================================================================

static NSString * const Service_GetSiteID_Result_key = @"GetSiteIDResult";
static NSString * const Service_GetSiteID_Response_key = @"GetSiteIDResponse";

static NSString * const Service_GetEventsListing_Result_key = @"GetEventsListingResult";
static NSString * const Service_GetEventsListing_Response_key = @"GetEventsListingResponse";

static NSString * const Service_GetContentPages_Response_Key = @"GetContentPagesResponse";
static NSString * const Service_GetContentPages_Result_Key = @"GetContentPagesResult";

static NSString * const Service_GetDiningCategories_Response_Key = @"GetDiningCategoriesResponse";
static NSString * const Service_GetDiningCategories_Result_Key = @"GetDiningCategoriesResult";

static NSString * const Service_GetEntertainmentListing_Response_Key = @"GetEntertainmentListingResponse";
static NSString * const Service_GetEntertainmentListing_Result_Key = @"GetEntertainmentListingResult";

static NSString * const Service_GetHotelsListing_Response_Key = @"GetHotelsListingResponse";
static NSString * const Service_GetHotelsListing_Result_Key = @"GetHotelsListingResult";

static NSString * const Service_GetOffersListing_Response_Key = @"GetOffersListingResponse";
static NSString * const Service_GetOffersListing_Result_Key = @"GetOffersListingResult";

static NSString * const Service_GetGuestServicesPages_Response_Key = @"GetGuestServicesPagesResponse";
static NSString * const Service_GetGuestServicesPages_Result_Key = @"GetGuestServicesPagesResult";

static NSString * const Service_GetShoppingCategories_Response_Key = @"GetShoppingCategoriesResponse";
static NSString * const Service_GetShoppingCategories_Result_Key = @"GetShoppingCategoriesResult";

static NSString * const Service_GetShoppingListing_Response_Key = @"GetShoppingListingResponse";
static NSString * const Service_GetShoppingListing_Result_Key = @"GetShoppingListingResult";

static NSString * const Service_GetGetDiningListing_Response_Key = @"GetDiningListingResponse";
static NSString * const Service_GetDiningListing_Resultkey = @"GetDiningListingResult";

static NSString * const Service_GetSpotlightBannerListing_Response_Key = @"GetSpotlightBannerListingResponse";
static NSString * const Service_GetSpotlightBannerListing_Result_Key = @"GetSpotlightBannerListingResult";


static NSString * const Service_GetHomeEntListing_Response_Key = @"GetHomeEntertainmentListingResponse";
static NSString * const Service_GetHomeEntListing_Result_Key = @"GetHomeEntertainmentListingResult";

static NSString * const Service_GetRandomMovie_Response_Key = @"GetRandomMovieResponse";
static NSString * const Service_GetRandomMovie_Result_Key = @"GetRandomMovieResult";

static NSString * const Service_GetUsefulLinks_Response_Key = @"GetUsefulLinksResponse";
static NSString * const Service_GetUsefulLinks_Result_Key = @"GetUsefulLinksResult";

static NSString * const Service_GetEventsListingByNoOfEvents_Response_Key=@"GetEventsListingByNoOfEventsResponse";
static NSString * const Service_GetEventsListingByNoOfEvents_Results_Key=@"GetEventsListingByNoOfEventsResult";

static NSString * const Service_GetOffersListingByUserBehavior_Response_Key=@"GetOffersListingByUserBehaviorResponse";
static NSString * const Service_GetOffersListingByUserBehavior_Results_Key=@"GetOffersListingByUserBehaviorResult";

static NSString * const Service_SetDeviceToken_Response_Key = @"SetDeviceTokenResponse";
static NSString * const Service_SetDeviceToken_Resultkey = @"SetDeviceTokenResult";

static NSString * const Service_UpdateUserCategoryHits_Response_Key = @"UpdateUserCategoryHitsResponse";
static NSString * const Service_UpdateUserCategoryHits_Result_Key = @"UpdateUserCategoryHitsResult";

static NSString * const Service_UpdateUserPushNotifications_Response_Key = @"UpdateUserPushNotificationsResponse";
static NSString * const Service_UpdateUserPushNotifications_Resultkey = @"UpdateUserPushNotificationsResult";

static NSString * const Service_GetATMLocations_Response_Key = @"GetATMLocationsResponse";
static NSString * const Service_GetATMLocations_Resultkey = @"GetATMLocationsResult";

static NSString * const Service_GetNotificationImage_Response_Key = @"GetNotificationImageResponse";
static NSString * const Service_GetNotificationImage_Resultkey = @"GetNotificationImageResult";

//===========web service SPIN & WIN KEY============

static NSString * const Service_SpinAndWin_getUseronLogin = @"getUseronLogin";
static NSString * const Service_SpinAndWin_getUseronLogin_Response_key = @"getUseronLoginResponse";
static NSString * const Service_SpinAndWin_getUseronLogin_Result_key = @"getUseronLoginResult";

static NSString * const Service_SpinAndWin_RegisterUser = @"RegisterUser";
static NSString * const Service_SpinAndWin_RegisterUser_Response_key = @"RegisterUserResponse";
static NSString * const Service_SpinAndWin_RegisterUser_Result_key = @"RegisterUserResult";

static NSString * const Service_SpinAndWin_ForgotPassword = @"ForgotPassword";
static NSString * const Service_SpinAndWin_ForgotPassword_Response_key = @"ForgotPasswordResponse";
static NSString * const Service_SpinAndWin_ForgotPassword_Result_key = @"ForgotPasswordResult";

static NSString * const Service_SpinAndWin_ResetPassword = @"ResetPassword";
static NSString * const Service_SpinAndWin_ResetPassword_Response_key = @"ResetPasswordResponse";
static NSString * const Service_SpinAndWin_ResetPassword_Result_key = @"ResetPasswordResult";

static NSString * const Service_SpinAndWin_getCampaigns = @"getCampaigns";
static NSString * const Service_SpinAndWin_getCampaigns_Response_key = @"getCampaignsResponse";
static NSString * const Service_SpinAndWin_getCampaigns_Result_key = @"getCampaignsResult";

static NSString * const Service_SpinAndWin_getDrawDates = @"getDrawDates";
static NSString * const Service_SpinAndWin_getDrawDates_Response_key = @"getDrawDatesResponse";
static NSString * const Service_SpinAndWin_getDrawDates_Result_key = @"getDrawDatesResult";

static NSString * const Service_SpinAndWin_getPastWinners = @"getPastWinners";
static NSString * const Service_SpinAndWin_getPastWinners_Response_key = @"getPastWinnersResponse";
static NSString * const Service_SpinAndWin_getPastWinners_Result_key = @"getPastWinnersResult";

static NSString * const Service_SpinAndWin_getQuestionsByCampaignId = @"getQuestionsByCampaignId";
static NSString * const Service_SpinAndWin_getQuestionsByCampaignId_Response_key = @"getQuestionsByCampaignIdResponse";
static NSString * const Service_SpinAndWin_getQuestionsByCampaignId_Result_key = @"getQuestionsByCampaignIdResult";

static NSString * const Service_SpinAndWin_RegisterUserRightAnswer = @"RegisterUserRightAnswer";
static NSString * const Service_SpinAndWin_RegisterUserRightAnswer_Response_key = @"RegisterUserRightAnswerResponse";
static NSString * const Service_SpinAndWin_RegisterUserRightAnswer_Result_key = @"RegisterUserRightAnswerResult";


 #endif
