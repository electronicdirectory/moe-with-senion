//
//  Utilities_Key.h
//  MOE
//
//  Created by Neosoft on 7/2/15.
//  Copyright (c) 2015 Neosoft. All rights reserved.
//

#ifndef MOE_Utilities_Key_h
#define MOE_Utilities_Key_h

#define SHOE_BG_IMAGE [UIImage imageNamed:@"sho-sizebg"]
#define FASHION_BG_IMAGE [UIImage imageNamed:@"fashion-conv-bg"]

//shoe
#define SHOE_BTN_IMAGE_MEN [UIImage imageNamed:@"btn-men-shoe"]
#define SHOE_BTN_IMAGE_WOMEN [UIImage imageNamed:@"btn-women-shoe"]
#define SHOE_BTN_IMAGE_KIDS [UIImage imageNamed:@"btn-kids-shoe"]
//fashion
#define FASHION_BTN_IMAGE_MEN [UIImage imageNamed:@"btn-men-fashion"]
#define FASHION_BTN_IMAGE_WOMEN [UIImage imageNamed:@"btn-women-fashion"]
#define FASHION_BTN_IMAGE_KIDS [UIImage imageNamed:@"btn-kids-fashion"]

#endif
