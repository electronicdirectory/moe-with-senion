//
//  EMSPDeviceSubscriber.h
//  EMSP.framework
//
//  Copyright (c) 2015 July System. All rights reserved.
//

#import "EMSPUserSubscriber.h"

@interface EMSPDeviceSubscriber : NSObject

@property(readonly, nonatomic, strong) EMSPUserSubscriber *associatedUserSubscriber;

- (instancetype)init __unavailable;
- (NSString *)getPreferenceValueForName:(NSString *)name;
- (NSString *)getSharedPreferenceValueForName:(NSString *)name;
- (void)setPreferenceValue:(NSString *)value forName:(NSString *)name;
- (void)setSharedPreferenceValue:(NSString *)value forName:(NSString *)name;
- (void)removePreferenceForName:(NSString *)name;
- (void)removeSharedPreferenceForName:(NSString *)name;
- (void)saveNow;
- (void)associateUserWithIdentity:(NSString *)identity ofType:(NSString *)type onCompletion:(void (^)(EMSPUserSubscriber *userSubscriber, NSError *error))completionBlock;
- (void)associateWithUserSubscriber:(EMSPUserSubscriber *)user;

@end