//
//  EMSP.h
//  EMSP.framework
//
//  Copyright (c) 2015 July System. All rights reserved.
//

#import "EMSP.h"
#import "EMSPLocation.h"
#import "EMSPLocationInfo.h"
#import "EMSPWifiLocationInfo.h"
#import "EMSPMapInfo.h"
#import "EMSPDeviceLocation.h"
#import "EMSPDeviceSubscriber.h"
#import "EMSPUserSubscriber.h"
#import "AppXView.h"
#import "AppXPushDown.h"
#import "AppXOverlay.h"
#import "AppXViewController.h"
#import "AppXHotButton.h"

typedef NS_ENUM(int, EMSP_LOG_LEVEL){
    EMSP_LOG_LEVEL_TRACE=1,
    EMSP_LOG_LEVEL_DEBUG,
    EMSP_LOG_LEVEL_INFO,
    EMSP_LOG_LEVEL_WARN,
    EMSP_LOG_LEVEL_SEVERE
};

@class EMSP;

@protocol EMSPRegistrationDelegate <NSObject>

@required
- (void)emspDidFinishRegistration:(EMSP *)emsp;
- (void)emspDidFailToRegisterWithError:(NSError *)error;

@end


@interface EMSP : NSObject

@property(nonatomic, strong)           NSString                         *appId;
@property(readonly, nonatomic, strong) EMSPLocation                     *location;
@property(readonly, nonatomic, strong) EMSPDeviceSubscriber             *deviceSubscriber;
@property(nonatomic, weak)             id<EMSPRegistrationDelegate>     delegate;
@property(nonatomic, strong)           UIImage                          *headerLogoImage;
@property(nonatomic)                   UIBarStyle                       navbarStyle;

+ (EMSP *)sharedInstance;
- (void)setLogLevel:(EMSP_LOG_LEVEL)level;
- (void)registerWithAuthToken:(NSString *)theAuthToken;
- (void)getUserSubscriberWithIdentity:(NSString *)identity ofType:(NSString *)type onCompletion:(void (^)(EMSPUserSubscriber *user, NSError *error))completionBlock;
- (void)registerDeviceTokenForPushNotification:(NSData *)theDeviceToken;
- (BOOL)handleReceivedRemoteNotification:(NSDictionary *)userInfo withAppViewController:(UIViewController *)appViewController;
- (BOOL)canOpenUrl:(NSURL *)url;

@end
