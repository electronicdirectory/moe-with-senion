//
//  EMSPLocation.h
//  EMSP.framework
//
//  Copyright (c) 2015 July System. All rights reserved.
//

#import "EMSPDeviceLocation.h"
#import "EMSPStreamingDeviceLocation.h"

@protocol EMSPLocationDelegate;


@interface EMSPLocation : NSObject

@property (nonatomic, weak)   id<EMSPLocationDelegate>  delegate;
@property (nonatomic, assign) BOOL                      useLocation;

- (instancetype)init __unavailable;
- (void)registerDeviceAtCurrentLocation;
- (void)registerWhenConnectedWithSSID:(NSString *)ssid;
- (void)registerWithLocationId:(NSString *)locationId;
- (void)findWifiLocationsWithLatitude:(double)aLatitude longitude:(double)aLongitude;
- (void)findWifiLocationsWithLatitude:(double)aLatitude longitude:(double)aLongitude SSID:(NSString *)ssid;
- (void)getDeviceLocation;
- (void)getStreamingDeviceLocation;

@end


@protocol EMSPLocationDelegate <NSObject>

@optional
- (void)emspLocationDidRegisterDevice:(EMSPLocation *)empLocation;
- (void)emspLocation:(EMSPLocation *)emspLocation didFailToRegisterDevice:(NSError *)error;

- (void)emspLocation:(EMSPLocation *)emspLocation didReceiveWifiLocations:(NSArray *)locations;
- (void)emspLocation:(EMSPLocation *)emspLocation didFailToReceiveWifiLocations:(NSError *)error;

- (void)emspLocation:(EMSPLocation *)empLocation didReceiveDeviceLocation:(EMSPDeviceLocation *)locationDetails;
- (void)emspLocation:(EMSPLocation *)empLocation didFailToReceiveDeviceLocation:(NSError *)error;

- (void)emspLocation:(EMSPLocation *)empLocation didReceiveStreamingDeviceLocation:(EMSPStreamingDeviceLocation *)locationDetails;
- (void)emspLocation:(EMSPLocation *)empLocation didFailToReceiveStreamingDeviceLocation:(NSError *)error;

@end

