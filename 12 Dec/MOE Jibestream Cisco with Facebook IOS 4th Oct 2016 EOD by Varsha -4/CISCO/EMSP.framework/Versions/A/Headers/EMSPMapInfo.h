//
//  MapInfo.h
//  EMSP.framework
//
//  Copyright (c) 2015 July System. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface EMSPMapInfo : NSObject
@property (nonatomic, strong) NSString * path;
@property (nonatomic, strong) NSString *image;
@property (nonatomic) double widthMeters;
@property (nonatomic) double lengthMeters;
@end
