//
//  AppXViewController.h
//  EMSP.framework
//
//  Copyright (c) 2015 July System. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppXView.h"

@interface AppXViewController : UIViewController <AppXViewDelegate, UIToolbarDelegate>{
    id __weak delegate;
}

@property (nonatomic, weak) id delegate;

- (instancetype)initWithTag:(NSString *)aTag;
- (instancetype)initWithTag:(NSString *)aTag contextParams:(NSDictionary *)params;

@end


@protocol AppXViewControllerDelegate <NSObject>
@optional
- (void)appXVCViewDidFailToLoad:(AppXView *)appXView withError:(NSError *)error;
- (void)appXVCViewDidFinishLoad:(AppXView *)appXView;
- (void)appXVCViewDidChangeLayout:(AppXView *)appXView;
- (void)appXVCDidReceiveCustomView:(NSString *)viewName withJsonData:(NSDictionary *)customViewDict;
- (void)appXVCDidDismissed;
@end
