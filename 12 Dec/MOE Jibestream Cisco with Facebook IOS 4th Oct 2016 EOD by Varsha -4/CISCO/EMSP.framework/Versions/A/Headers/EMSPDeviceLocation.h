//
//  DeviceLocationInfo.h
//  EMSP.framework
//
//  Copyright (c) 2015 July System. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "EMSPMapInfo.h"
#import "EMSPLocationInfo.h"

/**
 *    DataModel for device location returned by getDeviceLocation method
 */

@interface EMSPDeviceLocation : NSObject
@property (nonatomic, strong) EMSPMapInfo *mapInfo;
@property (nonatomic, strong) NSString *SSID;
@property (nonatomic, strong) NSString *deviceStatus;
@property (nonatomic, strong) NSString *firstSeenEpoc;
@property (nonatomic, strong) EMSPLocationInfo *deviceLocation;
@end
