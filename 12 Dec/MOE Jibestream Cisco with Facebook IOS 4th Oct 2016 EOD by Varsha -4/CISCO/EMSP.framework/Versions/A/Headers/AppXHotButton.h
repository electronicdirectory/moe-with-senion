//
//  AppXHotButton.h
//  EMSP.framework
//
//  Copyright (c) 2015 July System. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppXHotButton : UIView

- (instancetype)initWithTag:(NSString *)aTag controller:(UIViewController *)theAppViewController delegate:(id)aDelegate;
- (instancetype)initWithTag:(NSString *)aTag contextParams:(NSDictionary *)params controller:(UIViewController *)theAppViewController delegate:(id)aDelegate;

@end
