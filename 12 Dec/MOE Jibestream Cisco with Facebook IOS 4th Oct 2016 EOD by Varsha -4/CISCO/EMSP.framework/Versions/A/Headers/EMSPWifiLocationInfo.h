//
//  EMSPWifiLocationInfo.h
//  EMSP.framework
//
//  Copyright (c) 2015 July System. All rights reserved.
//

#import <Foundation/Foundation.h>

/**
 *   DataModel for wifi locations returned by "findWifiLocationsWithLatitude:longitude:" and "findWifiLocationsWithLatitude:longitude:SSID:" methods
 */

@interface EMSPWifiLocationInfo : NSObject
@property (nonatomic, strong) NSString *locid;
@property (nonatomic, strong) NSString *name;
@property (nonatomic) float lng;
@property (nonatomic) float lat;
@property (nonatomic, strong) NSArray *ssids;
@end


