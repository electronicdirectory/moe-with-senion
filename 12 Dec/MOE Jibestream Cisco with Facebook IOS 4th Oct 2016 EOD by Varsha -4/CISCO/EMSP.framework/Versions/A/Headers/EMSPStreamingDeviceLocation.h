//
//  EMSPStreamingDeviceLocation.h
//
//  EMSP.framework
//
//  Copyright (c) 2015 July System. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreGraphics/CoreGraphics.h>

/**
 *    DataModel for device location returned by getStreamingDeviceLocation method
 */

@interface EMSPStreamingDeviceLocation : NSObject
@property (nonatomic, strong) NSString *timeStamp;
@property (nonatomic, assign) NSInteger floorRefId;
@property (nonatomic, strong) NSString *eventType;
@property (nonatomic, strong) NSString *locationMapHierarchy;
@property (nonatomic, strong) NSString *locationCoordinateUnit;
@property (nonatomic, assign) CGFloat locationCoordinateX;
@property (nonatomic, assign) CGFloat locationCoordinateY;
@end
