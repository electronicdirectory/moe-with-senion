//
//  EMSPUserSubscriber.h
//  EMSP.framework
//
//  Copyright (c) 2015 July System. All rights reserved.
//


@interface EMSPUserSubscriber : NSObject

@property(nonatomic, strong) NSString *firstName;
@property(nonatomic, strong) NSString *lastName;
@property(nonatomic, strong) NSString *email;

- (instancetype)init __unavailable;
- (NSString *)getPreferenceValueForName:(NSString *)name;
- (NSString *)getSharedPreferenceValueForName:(NSString *)name;
- (void)setPreferenceValue:(NSString *)value forName:(NSString *)name;
- (void)setSharedPreferenceValue:(NSString *)value forName:(NSString *)name;
- (void)removePreferenceForName:(NSString *)name;
- (void)removeSharedPreferenceForName:(NSString *)name;
- (void)saveNow;

@end

