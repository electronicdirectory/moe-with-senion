//
//  AppXPushDown.h
//  EMSP.framework
//
//  Copyright (c) 2015 July System. All rights reserved.
//

#import "AppXView.h"

@interface AppXPushDown : AppXView
- (instancetype)initWithFrame:(CGRect)frame tag:(NSString *)aTag controller:(UIViewController *)theAppViewController delegate:(id)aDelegate;
- (instancetype)initWithFrame:(CGRect)frame tag:(NSString *)aTag contextParams:(NSDictionary *)params controller:(UIViewController *)theAppViewController delegate:(id)aDelegate;
- (void)refresh:(BOOL)ignoreCache;
@end


@protocol AppXPushDownDelegate <NSObject>
@required
- (void)appXPushDownDidChangeLayout:(AppXPushDown *)appXPushDown;
@optional
- (void)appXPushDownDidFailToLoad:(AppXPushDown *)appXPushDown withError:(NSError *)error;
- (void)appXPushDownDidFinishLoad:(AppXPushDown *)appXPushDown;
@end