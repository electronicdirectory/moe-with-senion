//
//  EMSPLocationInfo.h
//  EMSP.framework
//
//  Copyright (c) 2015 July System. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface EMSPLocationInfo : NSObject
@property (nonatomic, strong) NSString * mapX;
@property (nonatomic, strong) NSString * mapY;
@property (nonatomic)double lat;
@property (nonatomic)double lng;
@property (nonatomic, strong) NSString *lastUpdatedEpoc;
@end
